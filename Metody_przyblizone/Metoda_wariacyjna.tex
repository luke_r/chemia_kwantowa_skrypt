\section{Metoda wariacyjna}

\subsection{Zasada wariacyjna}

Jeśli nie znamy rozwiązań równania Schrödingera
	\beq
	\hat{H} \ket{\psi_n} = E_n \ket{\psi_n},
	\label{eq:zw_Hpsin}
	\eeq
wówczas możemy w ogólności posłużyć się \emph{metodą wariacyjną}. W metodzie tej wybieramy porządną \emph{funkcję próbną} $\phi$ zależną od tego samego zestawu zmiennych co funkcja $\psi_n$ i obliczamy wartość średnią hamiltonianu z tą funkcją:
	\beq
	\epsilon = \frac{\ev**{\hat{H}}{\phi}}{\braket{\vphantom{\hat{H}}\phi}}.
	\label{eq:zw_funk}
	\eeq
\emph{Zasada wariacyjna} stwierdza, że
	\beq
	\epsilon \ge E_0 \wedge \epsilon = E_0 \Leftrightarrow \phi = \psi_0.
	\eeq
Dowód zasady wariacyjnej jest prosty. Od razu widać, że jeśli wziąć $\phi = \psi_0$, wówczas z równania~(\ref{eq:zw_funk}) otrzymujemy $\epsilon = E_0$. Rozwińmy funkcję próbną $\phi$ w bazie funkcji własnych hamiltonianu:
	\beq
	\ket{\phi} = \sum_{k = 0}^\infty c_k \ket{\psi_k},
	\label{eq:zw_rozw}
	\eeq
przy czym oczywiście zachodzi
	\beq
	\bra{\psi_k}\ket{\psi_l} = \delta_{kl}.
	\eeq
Załóżmy, że funkcja $\ket{\phi}$ jest znormalizowana:
	\beq
	\braket{\phi} = 1 \Rightarrow \sum_{k = 0}^\infty |c_k|^2 = 1.
	\label{eq:zw_norm}
	\eeq
Z wyrażeń~(\ref{eq:zw_Hpsin}), (\ref{eq:zw_funk}), (\ref{eq:zw_rozw}) i~(\ref{eq:zw_norm}) otrzymujemy
	\beq
	\epsilon = \sum_{k = 0}^\infty \sum_{l = 0}^\infty c_k^* c_l \mel**{\psi_k}{\hat{H}}{\psi_l} =
	\sum_{k = 0}^\infty \sum_{l = 0}^\infty c_k^* c_l E_l \delta_{kl} =
	\sum_{k = 0}^\infty |c_k|^2 E_k,
	\eeq
tak więc
	\beq
	\epsilon - E_0 = \sum_{k = 0}^\infty |c_k|^2 E_k - E_0 \underbrace{\sum_{k = 0}^\infty |c_k|^2}_1 =
	\sum_{k = 0}^\infty |c_k|^2 (E_k - E_0) \ge 0.
	\eeq
W praktyce wprowadza się do funkcji próbnej $\phi$ pewien zestaw \emph{parametrów wariacyjnych} $\{c_1; c_2; \ldots; c_p\}$, zatem jeśli funkcja $\ket{\psi_n}$ zależy od $m$ współrzędnych:	$\psi_n(q_1; q_2; \ldots; q_m)$, to funkcja $\phi$ zależy już od $m + p$ współrzędnych: $\phi(q_1; q_2; \ldots; q_m; c_1; c_2; \ldots; c_p)$. Obliczając $\epsilon$ zgodnie z wzorem~(\ref{eq:zw_funk}), wykonujemy całkowanie po współrzędnych $\{q_1; q_2; \ldots; q_n\}$, zatem $\epsilon$ jest funkcją $p$ współrzędnych: $\epsilon(c_1; c_2; \ldots; c_p)$. Naszym celem jest znalezienie minimum tej funkcji. Optymalne wartości parametrów wariacyjnych można wyznaczyć z warunku znikania pochodnych cząstkowych w ekstremum:
	\beq
	\frac{\partial \epsilon(c_1; c_2; \ldots; c_p)}{\partial c_i} = 0 \mbox{, } i \in \langle 1; p \rangle \cap \mathbb{N}.
	\label{eq:zw_pochcz}
	\eeq
Wyrażenie~(\ref{eq:zw_pochcz}) jest układem $p$ równań algebraicznych, najczęściej nieliniowych. Co więcej, jest tylko warunkiem koniecznym występowania ekstremum funkcji $\epsilon(c_1; c_2; \ldots; c_p)$, zaś znalezienie minimum globalnego tej funkcji jest już zagadnieniem zupełnie nietrywialnym. Jeśli oznaczymy zestaw parametrów minimalizujących wartość funkcji $\epsilon(c_1; c_2; \ldots; c_p)$ przez $\{\mathscr{C}_1; \mathscr{C}_2; \ldots; \mathscr{C}_p\}$, wówczas najlepszym przybliżeniem funkcji stanu podstawowego jest funkcja $\phi(q_1; q_2; \ldots; q_m; \mathscr{C}_1; \mathscr{C}_2; \ldots; \mathscr{C}_p)$, zaś najlepszym przybliżeniem energii tego stanu jest liczba $\epsilon(\mathscr{C}_1; \mathscr{C}_2; \ldots; \mathscr{C}_p)$.
	\begin{task}
	\label{ts:zw_Slater}
	Przy użyciu metody wariacyjnej oszacować energię stanu podstawowego atomu wodoru używając funkcji próbnej $(c > 0)$
	\beq
	\phi(\mathbf{r}; c) = N(c) e^{-c r}.
	\eeq
	Obliczenia przeprowadzić w jednostkach atomowych.
	\end{task}

	\begin{sol}
	Zaczynamy od znalezienia stałej normalizacyjnej funkcji próbnej:
	\beq
	\begin{split}
	1 & = \braket{\phi} = |N(c)|^2 \int_0^\infty \int_0^\pi \int_0^{2 \pi} r^2 e^{-2 c r} \sin{\theta} \, dr d\theta d\phi =
	4 \pi |N(c)|^2 \int_0^\infty r^2 e^{-2 c r} \, dr = \\
	 & = \frac{\pi}{c^3} |N(c)|^2 \Rightarrow N(c) = \sqrt{\frac{c^3}{\pi}}.
	\end{split}
	\eeq
	Znajdujemy teraz wynik działania hamiltonianu~\eqref{eq:ham_jat_jn_wod} na funkcję próbną. Zauważmy przy tym, że funkcja próbna nie zależy jawnie od współrzędnych kątowych, zatem wystarczy rozważyć działanie części radialnej hamiltonianu:
	\beq
		\begin{split}
			\hat{H} \phi(\mathbf{r}; c) & = -\frac{1}{2 r^2} \frac{d}{dr} \left[ r^2 \frac{d \phi(\mathbf{r}; c)}{dr} \right] - \frac{1}{r} \phi(\mathbf{r}; c) \\
			 & = c \left( \frac{1}{r} - \frac{c}{2} \right) \phi(\mathbf{r}; c) - \frac{1}{r} \phi(\mathbf{r}; c) \\
			 & = \left( \frac{c - 1}{r} - \frac{c^2}{2} \right) \phi(\mathbf{r}; c).
		\end{split}
	\eeq
	Teraz wyznaczamy wartość średnią hamiltonianu:
	\beq
	\epsilon(c) = \ev**{\hat{H}}{\phi} = 4 c^3 \int_0^\infty \left[ (c - 1) r - \frac{c^2 r^2}{2} \right] e^{-2 c r} \, dr =
	c \left( \frac{c}{2} - 1 \right).
	\eeq
	Wyznaczamy ekstremum energii:
	\beq
	\epsilon'(c) = 0 \Leftrightarrow c = \mathscr{C} = 1,
	\eeq
	a zatem
	\beq
	\mathscr{E} = \epsilon(\mathscr{C}) = -\frac{1}{2}.
	\eeq
	Otrzymaliśmy więc dokładną energię stanu podstawowego atomu wodoru. Wynika to z wyboru funkcji próbnej --- w tym przypadku użyliśmy, z dokładnością do parametru wariacyjnego $c$, dokładnej funkcji falowej stanu podstawowego.
	\end{sol}

	\begin{task}
	Przy użyciu metody wariacyjnej oszacować energię stanu podstawowego atomu wodoru używając funkcji próbnej $(c > 0)$
	\beq
	\phi(\mathbf{r}; c) = N(c) e^{-c r^2}.
	\eeq
	Obliczenia przeprowadzić w jednostkach atomowych.
	\end{task}

	\begin{sol}
	Zaczynamy od znalezienia stałej normalizacyjnej funkcji próbnej:
	\beq
	\begin{split}
	1 & = \braket{\phi} = |N(c)|^2 \int_0^\infty \int_0^\pi \int_0^{2 \pi} r^2 e^{-2 c r^2} \sin{\theta} \, dr d\theta d\phi =
	4 \pi |N(c)|^2 \int_0^\infty r^2 e^{-2 c r^2} \, dr = \\
	 & = \left( \frac{\pi}{2 c} \right)^\frac{3}{2} |N(c)|^2 \Rightarrow N(c) = \left( \frac{2 c}{\pi} \right)^\frac{3}{4}.
	\end{split}
	\eeq
	Podobnie jak w zadaniu~\ref{ts:zw_Slater} działamy hamiltonianem na funkcję próbną, która także nie zależy od współrzędnych kątowych:
	\beq
	\hat{H} \psi(\mathbf{r}; c) = -\frac{1}{r^2} \frac{d}{dr} \left[ r^2 \frac{d \phi(\mathbf{r}; c)}{dr} \right] - \frac{1}{r} \phi(\mathbf{r}; c) =
	c (3 - 2 c r^2) \phi(\mathbf{r}; c) - \frac{1}{r} \phi(\mathbf{r}; c) =
	\left( 3 c - 2 c^2 r^2 - \frac{1}{r} \right) \phi(\mathbf{r}; c).
	\eeq
	Teraz wyznaczamy wartość średnią hamiltonianu:
	\beq
	\epsilon(c) = \ev**{\hat{H}}{\phi} = 8 \sqrt{\frac{2 c^3}{\pi}} \int_0^\infty \left( 3 c r^2 - 2 c^2 r^4 - r \right) e^{-2 c r^2} \, dr =
	\frac{3}{2} c - 2 \sqrt{\frac{2 c}{\pi}}.
	\eeq
	Wyznaczamy ekstremum energii:Wyznaczamy ekstremum energii:
	\beq
	\epsilon'(c) = 0 \Leftrightarrow c = \mathscr{C} = \frac{8}{9 \pi},
	\eeq
	a zatem
	\beq
	\mathscr{E} = \epsilon(\mathscr{C}) = -\frac{4}{3 \pi} \approx -0,424.
	\eeq
	Tym razem uzyskaliśmy energię nieco większą od dokładnej energii stanu podstawowego.
	\end{sol}

\subsubsection{Stan podstawowy atomu helu}
\label{ssc:zw_he_podst}

Obecnie przy użyciu metody wariacyjnej spróbujemy oszacować energię stanu podstawowego atomu helu. Obliczenia dla uproszczenia będziemy prowadzić w jednostkach atomowych. W tym celu musimy zastanowić się nad wyborem funkcji próbnej. Zacznijmy od zapisania hamiltonianu dla atomu helu:
	\beq
	\hat{H} = \hat{T}_1 + \hat{T}_2 + \hat{V}_1 + \hat{V}_2 + \hat{V}_{12} = -\frac{1}{2} \Delta_{\mathbf{r}_1} - \frac{1}{2} \Delta_{\mathbf{r}_2} - \frac{2}{r_1} - \frac{2}{r_2} + \frac{1}{r_{12}}.
	\label{eq:zw_he_slater_ham}
	\eeq
Zauważmy, że jeśli w hamiltonianie~\eqref{eq:zw_he_slater_ham} pominąć człon opisujący oddziaływanie dwóch elektronów, $r_{12}^{-1}$, wówczas funkcją własną operatora~\eqref{eq:zw_he_slater_ham} byłby iloczyn funkcji falowych dla jonu wodoropodobnego $(Z = 2)$:
	\beq
	\psi_0(\mathbf{r}_1; \mathbf{r}_2) = \psi_{100}(\mathbf{r}_1) \psi_{100}(\mathbf{r}_2) = N e^{-2 (r_1 + r_2)}.
	\eeq
Ta obserwacja uzasadnia, iż jako funkcję próbną możemy przyjąć
	\beq
	\phi(\mathbf{r}_1; \mathbf{r}_2; Z) = \varphi(\mathbf{r}_1; Z) \varphi(\mathbf{r}_2; Z) = N(Z) e^{-Z (r_1 + r_2)},
	\label{eq:zw_he_slater_fpr}
	\eeq
gdzie
	\beq
	\varphi(\mathbf{r}; Z) = n(Z) e^{-Z r}
	\label{eq:zw_he_slater_fprcz}
	\eeq
oraz $N(Z) = n^2(Z)$. $Z$ traktujemy jako parametr wariacyjny $(Z > 0)$ --- zobaczymy, jaki wpływ na wartość $Z$ ma oddziaływanie $\hat{V}_{12}$. Zaczynamy od znalezienia stałej normalizacyjnej funkcji próbnej. W tym celu wystarczy znormalizować funkcję $\varphi$:
	\beq
	\begin{split}
	1 & = \braket{\varphi} = |n(Z)|^2 \int_{\mathbb{R}^3} |\varphi(\mathbf{r}; Z)|^2 \, d^3\mathbf{r} =
	|n(Z)|^2 \int_0^\infty \int_0^\pi \int_0^{2 \pi} r^2 \sin{\theta} e^{-2 Z r} \, dr d\theta d\phi = \\
	 & = 4 \pi |n(Z)|^2 \int_0^\infty r^2 e^{-2 Z r} dr =
	\frac{\pi}{Z^3} |n(Z)|^2 \Rightarrow n^2(Z) = N(Z) = \frac{Z^3}{\pi}.
	\end{split}
	\eeq
Ponieważ funkcja próbna nie zależny jawnie od współrzędnych kątowych żadnego z elektronów, wystarczy rozważyć działanie tylko części radialnej laplasjanów na tę funkcję. Skorzystamy teraz z zależności~\eqref{eq:zw_he_slater_fpr} i~\eqref{eq:zw_he_slater_fprcz}, z których wynika, że wartość oczekiwana jednoelektronowego operatora $\hat{G}_i \equiv \hat{G}(\mathbf{r}_i)$ z całkowitą funkcją falową jest równa jego wartości oczekiwanej z funkcją $\varphi$. Ponadto nazwa zmiennej, po której całkujemy, nie ma znaczenia przy obliczaniu całek oznaczonych, więc możemy zapisać:
	\beq
	\ev**{\hat{G_1}}{\phi} = \ev**{\hat{G_2}}{\phi} = \ev**{\hat{G}}{\varphi}.
	\label{eq:zw_he_slater_1elop}
	\eeq
Korzystając z zależności~\eqref{eq:zw_he_slater_1elop} obliczamy kolejne wartości oczekiwane operatorów jednoelektronowych wchodzących w skład hamiltonianu~\eqref{eq:zw_he_slater_ham}:
	\beq
	\begin{split}
	\ev**{\hat{T}_1}{\phi} = \ev**{\hat{T}_2}{\phi} & = -\frac{1}{2} \ev**{\Delta_\mathbf{r}}{\varphi} =
	-2 \pi \int_0^\infty \phi(\mathbf{r}; Z) \frac{d}{dr} \left[ r^2 \frac{d \phi(\mathbf{r}; Z)}{dr} \right] \, dr = \\
	 & = -2 Z^3 \int_0^\infty e^{-Z r} \frac{d}{dr} \left( r^2 \frac{d e^{-Z r}}{dr} \right) \, dr = \\
	 & = 4 Z^4 \int_0^\infty \left(r - \frac{Z r^2}{2} \right) e^{-2 Z r} \, dr = \frac{Z^2}{2},
	\end{split}
	\eeq

	\beq
	\ev**{\hat{V}_1}{\phi} = \ev**{\hat{V}_2}{\phi} = -2 \ev**{\frac{1}{r}}{\varphi} =
	-8 Z^3 \int_0^\infty r e^{-2 Z r} \, dr = -2 Z.
	\eeq
Pozostaje jeszcze obliczenie wartości średniej operatora dwuelektronowego $\hat{V}_{12}$. Najpierw wyznaczamy całkę dwueletronową
	\beq
	J_{12} = \int_{\mathbb{R}^3} \int_{\mathbb{R}^3} \frac{e^{-\alpha (r_1 + r_2)}}{r_{12}} \, d^3\mathbf{r}_1 d^3\mathbf{r}_2 = \frac{20 \pi^2}{\alpha^5},
	\eeq
a więc
	\beq
	\ev**{\hat{V}_{12}}{\phi} =
	\left( \frac{Z^3}{\pi} \right)^2 \int_{\mathbb{R}^3} \int_{\mathbb{R}^3} \frac{e^{-2 Z (r_1 + r_2)}}{r_{12}} \, d^3\mathbf{r}_1 d^3\mathbf{r}_2 = \frac{5 Z}{8}.
	\eeq
Ostatecznie otrzymujemy
	\beq
	\epsilon(Z) = Z \left( Z - \frac{27}{8} \right).
	\label{eq:zw_he_slater_eps}
	\eeq
Znajdujemy minimum funkcji~\eqref{eq:zw_he_slater_eps}:
	\beq
	\epsilon'(Z) = 2 \left( Z - \frac{27}{16} \right) = 0 \Leftrightarrow Z = \mathscr{Z} = \frac{27}{16}.
	\eeq
Zatem energię stanu podstawowego przybliżamy liczbą
	\beq
	\mathscr{E} = \epsilon(\mathscr{Z}) = -\frac{729}{256} \approx -2.848.
	\eeq
Zauważmy, że w wyniku wariacji parametru $Z$ otrzymaliśmy wartość $\mathscr{Z} = \frac{27}{16} = 1.6875$. Tymczasem opisywaliśmy atom helu, dla którego oczywiście $Z = 2$. Fizycznie można to uzasadnić tym, że każdy elektron zasłania częściowo jądro przed drugim, a więc drugi elektron "widzi" tylko część ładunku jądra. Zjawisko to nazywa się \emph{ekranowaniem jądra}. Możemy więc powiedzieć, że każdy elektron w atomie helu widzi tylko \emph{ładunek efektywny}
	\beq
	Z_\mathrm{eff} = \mathscr{Z} = 1.6875
	\eeq
zamiast całkowitego ładunku jądra, $Z = 2$.

\subsection{Metoda Ritza}

\emph{Metoda Ritza} jest metodą wariacyjną, w której występują liniowe parametry wariacyjne. W metodzie tej funkcję próbną zakładamy w postaci kombinacji liniowej $p$ znanych funkcji porządnych, niekoniecznie unormowanych:
	\beq
	\phi = \sum_{k = 1}^p c_k \chi_k.
	\label{eq:zw_mr_rozw}
	\eeq
Wstawiając~(\ref{eq:zw_mr_rozw}) do~(\ref{eq:zw_funk}) otrzymujemy
	\beq
	\epsilon =
		\frac{
			\displaystyle \sum_{k = 1}^p \sum_{l = 1}^p c_k^* c_l \mel**{\chi_k}{\hat{H}}{\chi_l}
		}{
			\displaystyle \sum_{k = 1}^p \sum_{l = 1}^p c_k^* c_l \ip{\vphantom{\hat{H}} \chi_k}{\chi_l}
		}.
	\label{eq:zw_mr_eps}
	\eeq
Wprowadzając oznaczenia
	\beq
		H_{kl} = \mel**{\chi_k}{\hat{H}}{\chi_l}
	\eeq	
i
	\beq
		S_{kl} = \ip{\vphantom{\hat{H}}\chi_k}{\chi_l}
	\eeq
	dla \emph{całek nakrywania}, możemy zapisać równanie~(\ref{eq:zw_mr_eps}) w postaci
	\beq
	\epsilon =
		\frac{
			\displaystyle \sum_{k = 1}^p \sum_{l = 1}^p c_k^* c_l H_{kl}
		}{
			\displaystyle \sum_{k = 1}^p \sum_{l = 1}^p c_k^* c_l S_{kl}
		} = \frac{F}{G}.
	\label{eq:zw_mr_HS}
	\eeq
Zatem $\epsilon$ jest funkcją $p$ zmiennych i naszym celem jest znów znalezienie minimum tej funkcji. Ponieważ w równaniu~(\ref{eq:zw_mr_HS}) występują dwa zestawy zmiennych niezależnych: $\{c_1; c_2; \ldots; c_p\}$ oraz $\{c_1^*; c_2^*; \ldots; c_p^*\}$, możemy rozpatrzyć warunek znikania pochodnych cząstkowych oddzielnie dla obu zestawów współrzędnych uzyskując ten sam wynik. Dla zestawu zmiennych sprzężonych warunek ten ma postać
	\beq
	\frac{\partial \epsilon}{\partial c_i^*} = 0.
	\label{eq:zw_mr_pochcz}
	\eeq
Z warunków~(\ref{eq:zw_mr_HS}) i~(\ref{eq:zw_mr_pochcz}) otrzymujemy
	\beq
	\frac{G \displaystyle \sum_{l = 1}^p c_l H_{il} - F \sum_{l = 1}^p c_l S_{il}}{G^2} =
	\frac{\displaystyle \sum_{l = 1}^p c_l H_{il} - \epsilon \sum_{l = 1}^p c_l S_{il}}{G} =
	\frac{\displaystyle \sum_{l = 1}^p c_l (H_{il} - \epsilon S_{il})}{G} = 0,
	\eeq
skąd otrzymujemy
	\beq
	\sum_{l = 1}^p c_l (H_{il} - \epsilon S_{il}) = 0 \mbox{, } i \in \langle 1; p \rangle \wedge \mathbb{N}.
	\label{eq:zw_mr_sekj}
	\eeq
Wyrażenie~(\ref{eq:zw_mr_sekj}) jest układem $p$ liniowych równań jednorodnych, który można przedstawić w formie macierzowej:
	\beq
	(\mathbb{H} - \epsilon \mathbb{S}) \mathbf{c} = \mathbf{0}.
	\eeq
Układ~(\ref{eq:zw_mr_sekj}) posiada więc rozwiązania nietrywialne (niezerowe) tylko wtedy, gdy wyznacznik tego równania (tzw. \emph{wyznacznik wiekowy} lub \emph{sekularny}) jest zerem:
	\beq
	|\mathbb{H} - \epsilon \mathbb{S}| = 0,
	\eeq
lub zapisując jawnie macierze
	\beq
	\left| \begin{array}{cccc}
	H_{11} - \epsilon S_{11} & H_{12} - \epsilon S_{12} & \ldots & H_{1p} - \epsilon S_{1p} \\
	H_{21} - \epsilon S_{21} & H_{22} - \epsilon S_{22} & \ldots & H_{2p} - \epsilon S_{2p} \\
	\vdots & \vdots & \ddots & \vdots \\
	H_{p1} - \epsilon S_{p1} & H_{p2} - \epsilon S_{p2} & \ldots & H_{pp} - \epsilon S_{pp} \\
	\end{array} \right| = 0.
	\label{eq:zw_mr_wyznwk}
	\eeq
Równanie~(\ref{eq:zw_mr_wyznwk}) prowadzi do wielomianu $p$-tego stopnia ze względu na $\epsilon$. Wielomian ten, z uwagi na hermitowskość operatora $\hat{H}$, posiada $p$ pierwiastków rzeczywistych $\epsilon_i$, $i \in \{1; p\} \cap \mathbb{N}$. Jeśli uporządkować je w kolejności rosnącej:
	\beq
	\epsilon_1 \le \epsilon_2 \le \ldots \le \epsilon_p,
	\eeq
wówczas $\epsilon_i$ jest górnym przybliżeniem energii $i$-tego stanu, $E_i$. W ogólności zachodzi
	\beq
	\epsilon_i \ge E_i.
	\eeq
Jest to tzw. \emph{twierdzenie MacDonalda-Undheima-Hylleraasa-Löwdina}.
