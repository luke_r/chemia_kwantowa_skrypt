Równanie Schrödingera możemy rozwiązać analitycznie w bardzo niewielu przypadkach, omawianych w poprzednich rozdziałach. Stąd niezbędne stało się opracowanie metod służących przynajmniej do podania przybliżonych rozwiązań tegoż równania. W poniższych podrozdziałach omówimy dwie takie metody.

\newcommand{\pert}{\mathscr{V}}

\newcommand{\pertop}{\hat{\pert}}

\section{Rachunek zaburzeń Rayleigha-Schrödingera}

Interesuje nas rozwiązanie zagadnienia własnego
	\beq
	\hat{H} \ket{\psi_n} = E_n \ket{\psi_n}
	\label{eq:rz_prs}
	\eeq
w przypadku, gdy hamiltonian układu daje się rozseparować na sumę
	\beq
	\hat{H} = \hat{H}_0 + \pertop,
	\label{eq:rz_op}
	\eeq
gdzie $\hat{H}_0$ jest \emph{niezaburzonym} hamiltonianem, którego widmo jest znane:
	\beq
	\hat{H}_0 \fp{n}{0} = \ep{n}{0} \fp{n}{0},
	\eeq
zaś \emph{zaburzenie} $\pertop$ jest małe w porównaniu z $\hat{H}_0$ i jest operatorem hermitowskim.

Skoro zaburzenie jest małe, spodziewamy się, że dokładna funkcja falowa i energia jest zbliżona do znanej funkcji i energii niezaburzonych. \emph{Rachunek zaburzeń} jest procedurą, która pozwala na wyznaczanie kolejnych poprawek do funkcji i energii niezaburzonych tak, by po ich dodawaniu stawały się coraz bliższe rzeczywistej funkcji i energii. W tym celu wprowadzamy parametr porządkowy $\lambda$, który będzie regulował wielkość zaburzenia, a więc uzmienniamy hamiltonian:
	\beq
	\hat{H}(\lambda) = \hat{H}_0 + \lambda \pertop,
	\label{eq:rz_op_lambda}
	\eeq
przy czym na końcu przyjmiemy $\lambda = 1$, aby równanie~\eqref{eq:rz_op_lambda} było identyczne z~\eqref{eq:rz_op}. Skoro hamiltonian układu stał się zależny od parametru $\lambda$, a więc odeń zależą też funkcja falowa i energia. Równanie Schrödingera~\eqref{eq:rz_prs} możemy więc zapisać w postaci
	\beq
	\hat{H}(\lambda) \ket{\psi_n(\lambda)} = E_n(\lambda) \ket{\psi_n(\lambda)}.
	\eeq
Nie znamy oczywiście zależności funkcji falowej ani energii od $\lambda$, ale możemy je rozwinąć w szereg Taylora wokół punktu $\lambda = 0$:
	\beq
	\ket{\psi_n(\lambda)} = \sum_{i = 0}^\infty \lambda^i \fp{n}{i} =
	\fp{0}{0} + \lambda \fp{n}{1} + \lambda^2 \fp{n}{2} + \ldots
	\label{eq:rz_spot_psin}
	\eeq
i
	\beq
	E_n(\lambda) = \sum_{i = 0}^\infty \lambda^i \ep{n}{i} =
	\ep{n}{0} + \lambda \ep{n}{1} + \lambda^2 \ep{n}{2} + \ldots,
	\label{eq:rz_spot_En}
	\eeq
gdzie
	\beq
	\begin{cases}
	\fp{n}{k} = \frac{1}{k!} \frac{\partial^k \ket{\psi_n(\lambda)}}{\partial \lambda^k} \\
	\ep{n}{k} = \frac{1}{k!} \frac{\partial^k E_n(\lambda)}{\partial \lambda^k}
	\end{cases}.
	\eeq
Zakładamy teraz, że niezaburzona funkcja falowa jest unormowana:
	\beq
	\braket{\psi_n^{(0)}} = 1
	\eeq
oraz, że dokładna funkcja falowa jest sumą niezaburzonej funkcji i funkcji ortogonalnych do niezaburzonej funkcji, czyli
	\beq
	\bra{\psi_n^{(0)}}\ket{\psi_n} = 1.
	\label{eq:rz_nposr}
	\eeq
Warunek~\eqref{eq:rz_nposr} oznacza~\emph{normalizację pośrednią} dokładnej funkcji falowej. Nie powoduje on starty ogólności naszych rozważań. Poprawek szukamy co prawda w podprzestrzeni prostopadłej do funkcji niezaburzonej, ale i tak jesteśmy w stanie utworzyć każdy wektor tej przestrzeni, bowiem mamy do dyspozycji całą podprzestrzeń prostopadłą (przy użyciu której możemy tworzyć tylko wektory prostopadłe do niezaburzonej funkcji) i funkcję niezaburzoną (a więc teraz możemy utworzyć dowolny wektor z przestrzeni). Normalizacja pośrednia jest wygodna, powoduje bowiem zerowanie się wielu całek w dalszej części wyprowadzenia. Łącząc tenże warunek i rozwinięcie~(\ref{eq:rz_spot_psin}) otrzymujemy ponadto
	\beq
	\bra{\psi_n^{(k)}}\ket{\psi_n^{(0)}} = \delta_{0k}.
	\label{eq:rz_delta0k}
	\eeq
Wstawiając rozwinięcia~\eqref{eq:rz_spot_psin} i~\eqref{eq:rz_spot_En} z hamiltonianem~\eqref{eq:rz_op_lambda} do równania~\eqref{eq:rz_prs} otrzymujemy
	\beq
	\sum_{i = 0}^\infty \lambda^i (\hat{H}_0 + \lambda \pertop) \fp{n}{i} =
	\sum_{i = 0}^\infty \sum_{j = 0}^\infty \lambda^{i + j} \ep{n}{i} \fp{n}{j}.
	\label{eq:rz_potkl}
	\eeq
Ponieważ równanie~(\ref{eq:rz_potkl}) musi być spełnione dla każdej możliwej wartości $\lambda$, wyrażenia stojące przy odpowiednich potęgach $\lambda$ po obu stronach tego równania muszą być sobie równe. Na przykład, z warunku równości wyrazów stojących przy $\lambda^0$, $\lambda^1$ i $\lambda^2$ otrzymujemy kolejno równania:
	\beq
	\hat{H}_0 \fp{n}{0} = \ep{n}{0} \fp{n}{0},
	\eeq
a więc po prostu równanie Schrödingera dla funkcji niezaburzonej,
	\beq
	\hat{H}_0 \fp{n}{1} + \pertop \fp{n}{0} = \ep{n}{0} \fp{n}{1} + \ep{n}{1} \fp{n}{0}
	\label{eq:rz_lam1}
	\eeq
i
	\beq
	\hat{H}_0 \fp{n}{2} + \pertop \fp{n}{1} = \ep{n}{0} \fp{n}{2} + \ep{n}{1} \fp{n}{1} + \ep{n}{2} \fp{n}{0}.
	\label{eq:rz_lam2}
	\eeq
W ogólności z porównania wyrazów przy $\lambda^k$~$(k > 0)$ otrzymujemy
	\beq
	\hat{H}_0 \fp{n}{k} + \pertop \fp{n}{k - 1} = \sum_{i = 0}^k \ep{n}{i} \fp{n}{k - i}.
	\label{eq:rz_psik}
	\eeq
Chcemy znaleźć wynik mnożenia skalarnego równania~\eqref{eq:rz_psik} przez $\Bra{\fpn{n}{0}}$. Skorzystamy przy tym z hermitowskości hamiltonianu i zależności~\eqref{eq:rz_delta0k}, dzięki czemu wyznaczamy iloczyny
	\beq
	\mel**{\fpn{n}{0}}{\hat{H}_0}{\fpn{n}{k}} = \bra{\hat{H}_0 \fpn{n}{0}}\ket{\fpn{n}{k}} = \ep{n}{0} \bra{\fpn{n}{0}}\ket{\fpn{n}{k}} = 0
	\eeq
oraz
	\beq
	\bra{\fpn{n}{0}}\ket{\sum_{i = 0}^k \ep{n}{i} \fpn{n}{k - i}} =
	\sum_{i = 0}^{k - 1} \ep{n}{i} \underbrace{\bra{\fpn{n}{0}}\ket{\fpn{n}{k - i}}}_0 + \ep{n}{k} \underbrace{\braket{\fpn{n}{0}}}_1 = \ep{n}{k}.
	\eeq
W końcu uzyskujemy wyrażenie na $k$-tą poprawkę do energii,
	\beq
	\boxed{\ep{n}{k} = \mel**{\fpn{n}{0}}{\pertop}{\fpn{n}{k - 1}}},
	\label{eq:ez_enk_pop}
	\eeq
a w szczególności
	\beq
	\ep{n}{1} = \ev**{\pertop}{\fpn{n}{0}}
	\label{eq:ez_e1og}
	\eeq
i
	\beq
	\ep{n}{2} = \mel**{\fpn{n}{0}}{\pertop}{\fpn{n}{1}}.
	\label{eq:ez_e2og}
	\eeq
Jeśli więc chcemy obliczyć drugą poprawkę do energii, musimy znać pierwszą poprawkę do funkcji falowej. Skorzystamy tu ze znajomości widma operatora $\hat{H}_0$: funkcje własne tego operatora tworzą bazę przestrzeni wektorowej, do której należy także dokładna funkcja falowa układu oraz poprawki. Zatem pierwszą poprawkę możemy rozwinąć w bazie funkcji własnych niezaburzonego hamiltonianu:
	\beq
	\fp{n}{1} = \sum_{k = 0}^\infty c_{kn} \fp{k}{0}.
	\label{eq:rz_sumckn}
	\eeq
Pojawia się jednak problem: założyliśmy uprzednio, że funkcje $\fp{n}{1}$ i $\fp{n}{0}$ są ortogonalne. Wstawiając rozwinięcie~\eqref{eq:rz_sumckn} do warunku~\eqref{eq:rz_delta0k} uzyskujemy
	\beq
	\bra{\fpn{n}{1}}\ket{\fpn{n}{0}} = \sum_{k = 0}^\infty c_{kn} \underbrace{\bra{\fpn{k}{0}}\ket{\fpn{n}{0}}}_{\delta_{kn}} = c_{nn} = 0.
	\eeq
Oznacza to, że w sumie~(\ref{eq:rz_sumckn}) znika człon dla $k = n$, zatem wzór~(\ref{eq:rz_sumckn}) możemy zapisać w postaci
	\beq
	\fp{n}{1} = \sum_{k \not= n}^\infty c_{kn} \fp{k}{0}.
	\label{eq:rz_sumcknn_fp2}
	\eeq
Wstawiając wyrażenie~(\ref{eq:rz_sumcknn_fp2}) do równania~(\ref{eq:rz_lam1}) otrzymujemy
	\beq
	\sum_{k \not= n}^\infty c_{kn} \pqty{\hat{H}_0 - \ep{n}{0}} \fp{k}{0} =
	\sum_{k \not= n}^\infty c_{kn} \pqty{\ep{k}{0} - \ep{n}{0}} \fp{k}{0} =
	\pqty{\ep{n}{1} - \pertop} \fp{n}{0}.
	\label{eq:rz_kldolam2a}
	\eeq
Mnożąc skalarnie równanie~(\ref{eq:rz_kldolam2a}) przez $\Bra{\fpn{m}{0}}$, $m \not= n$, otrzymujemy wyrażenie pozwalające wyznaczyć nieznane współczynniki:
	\beq
	c_{mn} = \frac{\mel**{\fpn{m}{0}}{\pertop}{\fpn{n}{0}}}{\ep{n}{0} - \ep{m}{0}}.
	\eeq
Zatem pierwszą poprawkę do funkcji falowej możemy obliczyć ze wzoru
	\beq
	\fp{n}{1} = \sum_{k \not= n}^\infty \frac{\mel**{\fpn{k}{0}}{\pertop}{\fpn{n}{0}}}{\ep{n}{0} - \ep{k}{0}} \fp{k}{0}.
	\label{eq:rz_ff_1pop}
	\eeq
Łącząc~(\ref{eq:ez_e2og}) i~(\ref{eq:rz_ff_1pop}) możemy już obliczyć drugą poprawkę do energii:
	\beq
	\ep{n}{2} = \sum_{k \not= n}^\infty \frac{\mel**{\fpn{k}{0}}{\pertop}{\fpn{n}{0}} \mel**{\fpn{n}{0}}{\pertop}{\fpn{k}{0}}}{\ep{n}{0} - \ep{k}{0}} =
	\sum_{k \not= n}^\infty \frac{\norm{\mel**{\fpn{k}{0}}{\pertop}{\fpn{n}{0}}}^2}{\ep{n}{0} - \ep{k}{0}}.
	\eeq
Zauważmy, że do obliczenia pierwszej i drugiej poprawki całkowicie wystarcza nam znajomość widma niezaburzonego hamiltonianu i zaburzenia. W ogólności naszym celem jest wyrażenie kolejnych poprawek do energii poprzez energie własne $\hat{H}_0$ i elementy macierzowe
	\beq
	\pert_{kl} = \mel**{\fpn{k}{0}}{\pertop}{\fpn{l}{0}}
	\eeq
zaburzenia z funkcjami własnymi $\hat{H}_0$. Zrobiliśmy to już dla pierwszej,
	\beq
	\ep{n}{1} = \pert_{nn},
	\eeq
i drugiej,
	\beq
	\ep{n}{2} = \sum_{k \not= n}^\infty \frac{|\pert_{kn}|^2}{\ep{n}{0} - \ep{k}{0}},
	\eeq
poprawki do energii. Znajdziemy jeszcze wyrażenie na trzecią poprawkę do energii. Z zależności~\eqref{eq:ez_enk_pop} wynika, że
	\beq
	\ep{n}{3} = \mel**{\fpn{n}{0}}{\pertop}{\fpn{n}{2}}.
	\label{eq:ez_e3og}
	\eeq
Tok postępowania jest analogiczny jak przy wyprowadzeniu wzoru na drugą poprawkę. Najpierw rozwijamy nieznaną funkcję $\fp{n}{2}$ w bazie funkcji własnych niezaburzonego hamiltonianu, podobnie jak w równaniu~\eqref{eq:rz_sumcknn_fp2}:
	\beq
	\fp{n}{2} = \sum_{k \not= n}^\infty d_{kn} \fp{k}{0},
	\label{eq:rz_sumcknn_fp3}
	\eeq
przy czym znów suma nie obejmuje członu $k = n$. Wstawiając rozwinięcia~\eqref{eq:rz_sumcknn_fp3} i~\eqref{eq:rz_ff_1pop} do równania~\eqref{eq:rz_lam2} uzyskujemy
	\beq
	\begin{split}
	& \sum_{k \ne n}^\infty d_{kn} \pqty{\hat{H}_0 - \ep{n}{0}} \fp{k}{0} + \pertop \fp{n}{1} =
	\sum_{k \ne n}^\infty d_{kn} \pqty{\ep{k}{0} - \ep{n}{0}} \fp{k}{0} = \\
	& = \pqty{\ep{n}{1} - \pertop} \sum_{k \ne n}^\infty \frac{\pert_{kn}}{\ep{n}{0} - \ep{k}{0}} \fp{k}{0} + \ep{n}{2} \fp{n}{0}.
	\end{split}
	\label{eq:rz_kldolam2b}
	\eeq
Aby wyznaczyć nieznane współczynniki $d_{kn}$, mnożymy równanie~\eqref{eq:rz_kldolam2b} skalarnie przez $\Bra{\fpn{m}{0}}$,~$m \ne n$. Po prostych przekształceniach otrzymujemy
	\beq
	d_{mn} = \frac{1}{\ep{n}{0} - \ep{m}{0}} \sum_{k \ne n}^\infty \frac{\pert_{kn} \pert_{mk}}{\ep{n}{0} - \ep{k}{0}} -
	\frac{\overbrace{\ep{n}{1}}^{\pert_{nn}} \pert_{mn}}{\left( \ep{n}{0} - \ep{m}{0} \right)^2}.
	\eeq
Tak więc druga poprawka do funkcji falowej dana jest wzorem
	\beq
	\fp{	n}{2} = \left\{ \sum_{k \ne n}^\infty \sum_{l \ne n}^\infty \frac{\pert_{kl} \pert_{ln}}{\left( \ep{n}{0} - \ep{k}{0} \right) \left( \ep{n}{0} - \ep{l}{0} \right)} -
	\pert_{nn} \sum_{k \ne n}^\infty \frac{\pert_{kn}}{\left( \ep{n}{0} - \ep{k}{0} \right)^2} \right\} \fp{k}{0}.
	\label{eq:rz_ff_2pop}
	\eeq
Wstawiając poprawkę~\eqref{eq:rz_ff_2pop} do równania~\eqref{eq:ez_e3og} uzyskujemy ogólny wzór na trzecią poprawkę do energii:
	\beq
	\boxed{\ep{n}{3} = \sum_{k \ne n}^\infty \sum_{l \ne n}^\infty \frac{\pert_{nk} \pert_{kl} \pert_{ln}}{\left( \ep{n}{0} - \ep{k}{0} \right) \left( \ep{n}{0} - \ep{l}{0} \right)} -
	\pert_{nn} \sum_{k \ne n}^\infty \frac{|\pert_{nk}|^2}{\left( \ep{n}{0} - \ep{k}{0} \right)^2}}.
	\eeq

	\begin{task}
	Obliczyć pierwszą i drugą poprawkę do energii dla jednowymiarowego oscylatora harmonicznego umieszczonego w jednorodnym polu magnetycznym o natężeniu $F$. Wynik porównać ze ścisłą wartością energii.
	\end{task}
	\begin{sol}
	Hamiltonian oscylatora w polu elektrycznym ma postać
	\beq
	\hat{H} = \frac{\hat{p}_x^2}{2 m} + \frac{k x^2}{2} - e F x.
	\label{eq:rz_oh_pe}
	\eeq
	\end{sol}
	Hamiltonian~\eqref{eq:rz_oh_pe} możemy zapisać w nieco innej postaci:
	\beq
	\begin{split}
	\hat{H} & = \frac{\hat{p}_x^2}{2 m} + \frac{k}{2} \left( x^2 - \frac{2 e F}{k} x \right) =
	\frac{\hat{p}_x^2}{2 m} + \frac{k}{2} \left[ \left( x - \frac{e F}{k} \right)^2 - \left( \frac{e F}{k} \right)^2 \right] = \\
	 & = \frac{\hat{p}_x^2}{2 m} + \frac{k}{2} \left( x - \frac{e F}{k} \right)^2 - \frac{e^2 F^2}{2 k}.
	\end{split}
	\label{eq:hamx}
	\eeq
	Wprowadźmy teraz nową zmienną
	\beq
	q = x - \frac{e F}{k},
	\eeq
	co odpowiada przesunięciu zmiennej $x$. Oczywiście
	\beq
	\frac{d}{dq} = \frac{d}{dx}.
	\eeq
	Zapiszmy hamiltonian~\eqref{eq:rz_oh_pe} w nowej zmiennej:
	\beq
	\hat{H} = \frac{\hat{p}_q^2}{2 m} + \frac{k q^2}{2} - \frac{e^2 F^2}{2 k}.
	\label{eq:hamq}
	\eeq
	Skorzystajmy teraz z oczywistego faktu: jeśli zachodzi $\hat{A} \ket{\psi} = a \ket{\psi}$, to $(\hat{A} + b) \ket{\psi} = (a + b) \ket{\psi}$. Hamiltonian~\eqref{eq:rz_oh_pe} ma postać taką jak hamiltonian~\eqref{eq:hamnzab_oh} niezaburzonego oscylatora, różni się tylko nazwą zmiennej i jest przesunięty o
	\beq
	\Delta E = - \frac{e^2 F^2}{2 k}.
	\eeq
	Tak więc jego funkcje własne $(\widetilde{\psi}_n)$ uzyskujemy z funkcji własnych oscylatora bez pola przez translację zmiennej:
	\beq
	\widetilde{\psi}_n(x) = \psi_n(q) = \psi_n \left( x - \frac{e F}{k} \right),
	\eeq
	zaś energie otrzymujemy przesuwając energie oscylatora bez pola $(E_n)$ o $\Delta E$:
	\beq
	\widetilde{E}_n = E_n - \frac{e^2 F^2}{2 k} = \hbar \omega \left( n + \frac{1}{2} \right) - \frac{e^2 F^2}{2 k}.
	\label{eq:rz_oh_pe_en}
	\eeq
	Jest to ścisła wartość energii oscylatora w polu elektrycznym. Obecnie sprawdzimy, czy formalizm rachunku zaburzeń pozwoli uzyskać podobny wynik. Jako zaburzenie przyjmujemy
	\beq
	\pertop = - e F x.
	\eeq
	Zapiszmy operator zaburzenia w drugiej kwantyzacji: korzystając z zależności~\eqref{eq:x_drkw_oh} otrzymujemy
	\beq
	\pertop = -e F \sqrt{\frac{\hbar}{2 m \omega}} \pqty{\ana + \cra}.
	\eeq
	Znajdziemy teraz ogólne wyrażenie na elementy macierzowe zaburzenia z funkcjami własnymi oscylatora $(\ket{n})$:
	\beq
	\begin{split}
	\pert_{kn} & = \mel**{k}{\pertop}{n} = -e F \sqrt{\frac{\hbar}{2 m \omega}} \mel**{k}{\ana + \cra}{n} = \\
	 & = -e F \sqrt{\frac{\hbar}{2 m \omega}} \pqty{\sqrt{n} \bra{k}\ket{n - 1} + \sqrt{n + 1} \bra{k}\ket{n + 1}} = \\
	 & = -e F \sqrt{\frac{\hbar}{2 m \omega}} \pqty{\sqrt{n} \delta_{k, n - 1} + \sqrt{n + 1} \delta_{k, n + 1}}.
	\end{split}
	\eeq
	Tak więc jedyne niezerowe elementy macierzowe to
	\beq
	\pert_{n - 1, n} = -e F \sqrt{\frac{n \hbar}{2 m \omega}}
	\eeq
	i
	\beq
	\pert_{n + 1, n} = -e F \sqrt{\frac{ (n + 1) \hbar}{2 m \omega}}.
	\eeq
	Od razu dostajemy wzory na pierwszą i drugą poprawkę do energii:
	\beq
	\ep{n}{1} = \pert_{nn} = 0,
	\eeq

	\beq
	\ep{n}{2} = \frac{\pert_{n - 1, n}^2}{E_n - E_{n - 1}} + \frac{\pert_{n + 1, n}^2}{E_n - E_{n + 1}} =
	\frac{n e^2 F^2}{2 m \omega^2} - \frac{(n + 1) e^2 F^2}{2 m \omega^2} =
	-\frac{e^2 F^2}{2 m \omega^2} = -\frac{e^2 F^2}{2 k}.
	\eeq
	A więc już druga poprawka daje ścisłą wartość energii:
	\beq
	\widetilde{E}_n = E_n + \ep{n}{1} + \ep{n}{2}.
	\eeq
	\begin{task}
	Obliczyć pierwszą poprawkę do energii elektronu w stanie podstawowym jonu wodoropodobnego wynikającą z relatywistycznej zmiany masy elektronu poruszającego się z prędkością $v$:
	\beq
	m = \frac{m_0}{\sqrt{1 - \frac{v^2}{c^2}}},
	\eeq
	gdzie $m_0$ jest masą spoczynkową elektronu.
	\end{task}
	\begin{sol}
	Energia kinetyczna elektronu wynosi
	\beq
	T = m c^2.
	\eeq
	Obliczmy wyrażenie
	\beq
	T^2 - (m_0 c^2)^2 = (m^2 - m_0^2) c^4 =
	m_0^2 c^4 \left( \frac{1}{1 - \frac{v^2}{c^2}} - 1 \right) =
	m_0^2 c^4 \frac{v^2}{c^2 - v^2} = \frac{m_0^2 v^2 c^2}{1 - \frac{v^2}{c^2}} =
	m^2 v^2 c^2 = p^2 c^2,
	\eeq
	zatem
	\beq
	T = \sqrt{m_0^2 c^4 + p^2 c^2} = m_0 c^2 \sqrt{1 + \frac{p^2}{m_0^2 c^2}}.
	\eeq
	Energię kinetyczną można więc potraktować jako funkcję pędu i rozwinąć ją w szereg Taylora~(\ref{eq:sz_tay}) względem $p = 0$. Ograniczając rozwinięcie do członów kwadratowych w $p$ (czyli do czwartej pochodnej $T$ względem $p$) uzyskujemy
	\beq
	T = \underbrace{m_0 c^2}_{T_\mathrm{sp}} +
	\underbrace{\frac{p^2}{2 m_0}}_{T_0} +
	\underbrace{\left( -\frac{p^4}{8 m_0^3 c^2} \right)}_{T_\mathrm{mv}}.
	\label{eq:Tsuma}
	\eeq
	Jak wynika z wyrażenia~(\ref{eq:Tsuma}), $T_\mathrm{sp}$ jest energią spoczynkową elektronu (jest to nieistotna dla nas wielkość, bowiem wybór zera na skali energii jest dowolny i możemy wybrać zero właśnie w $m_0 c^2$), a $T_0$ jest nierelatywistyczną energią kinetyczną elektronu [operator tej energii występuje w hamiltonianie~(\ref{eq:ham_jn_wod_l2})]. A więc $T_\mathrm{mv}$ jest relatywistyczną poprawką do energii kinetycznej elektronu. Możemy więc zapisać operator zaburzenia jako
	\beq
	\pertop = \hat{H}_\mathrm{mv} = -\frac{\hat{p}^4}{8 m_0^3 c^2}.
	\eeq
	Przy obliczeniu poprawki do energii możemy skorzystać z hermitowskości operatora $\hat{p}^2$:
	\beq
	\begin{split}
	\ep{1}{1} & = \ev**{\hat{H}_\mathrm{mv}}{\psi_{100}} =
	-\frac{1}{8 m_0^3 c^2} \ev**{\hat{p}^4}{\psi_{100}} =
	-\frac{1}{8 m_0^3 c^2} \braket{\hat{p}^2 \psi_{100}} = \\
	 & = -\frac{\hbar^4}{8 m_0^3 c^2} \braket{\Delta_\mathbf{r} \psi_{100}}.
	\end{split}
	\eeq
	Wyrażenie $\Delta_\mathbf{r} \psi_{100}(\mathbf{r})$ obliczyliśmy już w równaniu~(\ref{eq:lapl_psi100}). Tak więc
	\beq
	\begin{split}
	\ep{1}{1} & = -\frac{\hbar^4}{8 \pi m_0^3 c^2} \left( \frac{Z}{a_0} \right)^5 \int_0^\infty \int_0^\pi \int_0^{2 \pi} \left( \frac{2}{r} -\frac{Z}{a_0} \right)^2 e^{-\frac{2 Z r}{a_0}} r^2 \sin{\theta} \, d r d \theta d \phi = \\
	 & = -\frac{\hbar^4}{2 m_0^3 c^2} \left( \frac{Z}{a_0} \right)^5 \int_0^\infty \left[ 4 - \frac{4 Z}{a_0} r + \left( \frac{Z}{a_0} \right)^2 r^2 \right] e^{-\frac{2 Z r}{a_0}} \, d r =
	-\frac{5 Z^4 \hbar^4}{8 m_0^3 c^2 a_0^4}.
	\end{split}
	\label{eq:pop_rel_pln}
	\eeq
	Korzystając z wyrażeń~(\ref{eq:a0}),~(\ref{eq:en_jn_wod}) i~(\ref{eq:pop_rel_pln}) możemy zapisać pierwszą poprawkę do energii jako
	\beq
	\ep{1}{1} = -\frac{5}{2 m_0 c^2} \left[ \ep{1}{0} \right]^2.
	\eeq
	\end{sol}

	\begin{task}
	Dla stanu podstawowego jonu wodoropodobnego znaleźć poprawkę pierwszego rzędu spowodowaną skończonymi rozmiarami jądra atomowego. Potraktować jądro atomowe jako jednorodnie naładowaną kulę o promieniu $R$.
	\end{task}
	\begin{sol}
	Niepunktowość jądra powoduje zmianę oddziaływania z elektronem. Aby wyznaczyć postać funkcyjną tego oddziaływania, korzystamy z prawa Gaussa:
	\beq
	\oint_S \mathbf{E} (\mathbf{r}) \cdot d \mathbf{S} =
	\frac{1}{\epsilon_0} \int_V \rho(\mathbf{r}) d V,
	\eeq
	gdzie $\mathbf{E} (\mathbf{r})$ jest natężeniem pola elektrostatycznego w danym punkcie, zaś $\rho(\mathbf{r})$ --- gęstością ładunku elektrycznego. Ponieważ mamy do czynienia z jednorodnie naładowaną kulą, więc zależności wektorowe sprowadzają się do zależności radialnych i prawo Gaussa przyjmuje postać
	\beq
	4 \pi r^2 E(r) = \frac{1}{\epsilon_0} \int_0^r \int_0^\pi \int_0^{2 \pi} \rho(r) r^2 \sin{\theta} \, d r d \theta d \phi =
	\frac{4 \pi}{\epsilon_0} \int_0^r \rho(r) r^2 \, d r \Rightarrow
	E(r) = \frac{1}{\epsilon_0 r^2} \int_0^r \rho(r) r^2 \, d r.
	\eeq
	Gęstość ładunku dla kuli jednorodnie naładowanej o ładunku $Z e$ i promieniu $R$, czyli objętości $\frac{4}{3} \pi R^3$, wynosi oczywiście
	\beq
	\rho(r) = \frac{3 Z e}{4 \pi R^3}.
	\eeq
	Aby uzyskać zależność $E(r)$, musimy rozważyć dwa przypadki: $r \in \langle 0; R \rangle$ i $r > R$. Dla $r \in \langle 0; R \rangle$ otrzymujemy
	\beq
	E(r) = \frac{3 Z e}{4 \pi \epsilon_0 r^2 R^3} \int_0^r r^2 \, d r =
	\frac{Z e r}{4 \pi \epsilon_0 R^3},
	\eeq
	zaś dla $r > R$ mamy\footnote{Tym razem całkujemy w granicach od 0 do $R$, bowiem cały ładunek jądra jest zawarty w kuli o promieniu $R$.}
	\beq
	E(r) = \frac{3 Z e}{4 \pi \epsilon_0 r^2 R^3} \int_0^R r^2 \, d r =
	\frac{Z e}{4 \pi \epsilon_0 r^2}.
	\eeq
	Potencjał elektrostatyczny w danym punkcie wynosi
	\beq
	\varphi(r) = -\int E(r) \, d r =
	\left\{ \begin{array}{l}
	-\frac{Z e r^2}{8 \pi \epsilon_0 R^3} + C_1 \mbox{, } r \in \langle 0; R \rangle, \\
	\frac{Z e}{4 \pi \epsilon_0 r} + C_2 \mbox{, } r > R.
	\end{array} \right.
	\eeq
	Zakładając, że potencjał elektrostatyczny znika w nieskończoności, wyznaczamy stałą $C_2$:
	\beq
	\lim_{r \rightarrow \infty} \varphi(r) = 0 \Leftrightarrow C_2 = 0,
	\eeq
	zaś stałą $C_1$ --- z warunku zszycia potencjału elektrostatycznego na powierzchni kuli o promieniu $R$:
	\beq
	-\frac{Z e}{8 \pi \epsilon_0 R} + C_1 = \frac{Z e}{4 \pi \epsilon_0 R} \Rightarrow
	C_1 = \frac{3 Z e}{8 \pi \epsilon_0 R}.
	\eeq
	Szukany potencjał oddziaływania obliczamy z zależności
	\beq
	V(r) = -e \varphi(r) =
	\left\{ \begin{array}{l}
	-\frac{Z e^2}{8 \pi \epsilon_0 R} \left( 3 - \frac{r^2}{R^2} \right) \mbox{, } r \in \langle 0; R \rangle, \\
	-\frac{Z e^2}{4 \pi \epsilon_0 r} \mbox{, } r > R.
	\end{array} \right.
	\eeq
	Zaburzenie obliczamy jako różnicę wyznaczonego potencjału oddziaływania i zwykłego potencjału kulombowskiego:
	\beq
	V'(r) = V(r) - \pert_c(r),
	\eeq
	przy czym oczywiście
	\beq
	\pert_c(r) = -\frac{Z e^2}{4 \pi \epsilon_0 r}.
	\eeq
	Zaburzenie wynosi zatem
	\beq
	V'(r) =
	\left\{ \begin{array}{l}
	-\frac{Z e^2}{4 \pi \epsilon_0} \left[ \frac{1}{2 R} \left( 3 - \frac{r^2}{R^2} \right) - \frac{1}{r} \right] \mbox{, } r \in \langle 0; R \rangle, \\
	0 \mbox{, } r > R.
	\end{array} \right.
	\eeq
	Poprawka do energii stanu podstawowego w pierwszym rzędzie rachunku zaburzeń wynosi
	\beq
	\begin{split}
	\ep{1}{1} & = \ev**{V'(r)}{\psi_{100}} =
	-\frac{Z^4 e^2}{4 \pi^2 \epsilon_0 a_0^3} \int_0^R \int_0^\pi \int_0^{2 \pi} \left[ \frac{r^2}{2 R} \left( 3 - \frac{r^2}{R^2} \right) - r \right] e^{-\frac{2 Z r}{a_0}} \sin{\theta} \, d r d \theta d \phi = \\
	 & = -\frac{Z^4 e^2}{\pi \epsilon_0 a_0^3} \int_0^R \left( \frac{3 r^2}{2 R} - \frac{r^4}{2 R^3} - r \right) e^{-\frac{2 Z r}{a_0}} \, d r.
	\end{split}
	\eeq
	Ponieważ $\frac{R}{a_0} \approx 10^{-5}$, możemy założyć, że dla $r \in \langle 0; R \rangle$ zachodzi $e^{-\frac{2 Z r}{a_0}} = 1$ i wówczas
	\beq
	\ep{1}{1} = -\frac{Z^4 e^2}{\pi \epsilon_0 a_0^3} \int_0^R \left( \frac{3 r^2}{2 R} - \frac{r^4}{2 R^3} - r \right) \, d r =
	\frac{Z^4 e^2 R^2}{10 \pi \epsilon_0 a_0^3}.
	\label{eq:rz_npjd}
	\eeq
	Korzystając z zależności~(\ref{eq:a0}), (\ref{eq:en_jn_wod}) i~(\ref{eq:rz_npjd}) otrzymujemy
	\beq
	\ep{1}{1} = -\frac{4}{5} \left( \frac{Z R}{a_0} \right)^2 \ep{1}{0}.
	\eeq
	\end{sol}
