\newcommand{\ehf}{\ensuremath{E_\mathrm{HF}}}
% Hartree-Fock energy

\newcommand{\ecor}{\ensuremath{E_\mathrm{cor}}}
% Correlation energy

\newcommand{\wfci}{\ensuremath{\ket{\Psi_\mathrm{FCI}}}}
% FCI wavefunction

\newcommand{\wcisd}{\ensuremath{\ket{\Psi_\mathrm{CISD}}}}
% CISD wavefunction

\newcommand{\wcc}{\ensuremath{\ket{\Psi_\mathrm{CC}}}}
% CC wavefunction

\section{Korelacja elektronowa}

W metodzie Hartree-Focka, opartej na przybliżeniu jednoelektronowym, każdemu elektronowi przyporządkowaliśmy jednoelektronową funkcję falową, czyli spinorbital. Była to funkcja pseudowłasna operatora Focka, obejmującego energię kinetyczną elektronu, jego oddziaływanie z jądrami oraz, poprzez operatory kulombowski i wymienny, oddziaływanie ze \textbf{średnim} polem od pozostałych elektronów. Założenie funkcji falowej w postaci jednego wyznacznika Slatera gwarantowało, że tylko jeden elektron mógł być opisany danym spinorbitalem. Dany elektron odczuwał uśrednione pole od pozostałych elektronów, więc takie podejście nie różnicowało oddziaływania elektronów w zależności od ich wzajemnej odległości. Skoro jednak elektrony oddziałują ze sobą kulombowsko, oczywiste jest, że bardziej korzystna energetycznie jest sytuacja, gdy elektrony znajdują się dalej od siebie. Oczywiście optymalne położenie jest wypadkową oddziaływań elektronu z jądrami i pozostałymi elektronami, dlatego elektrony nie uciekają do nieskończoności, gdy energia oddziaływania między nimi byłaby najmniejsza --- zerowa. Z drugiej strony, mówiąc o położeniach elektronów, musimy pamiętać o zasadzie nieoznaczności Heisenberga, zgodnie z którą nie znamy położeń elektronów, tym samym ich trajektorii. Mówienie o bardziej lub mniej korzystnych położeniach elektronów jest więc tylko zabiegiem czysto myślowym, służącym do lepszego zrozumienia \emph{korelacji elektronowej}.

Korelacja elektronowa jest wzajemnym powiązaniem ruchów elektronów w układzie wieloelektronowym. Jak wiemy ze statystyki, liczbową miarą korelacji jest \emph{współczynnik korelacji}, który dla zmiennych dynamicznych $A$ i $B$ ma postać
	\beq
	\rho(A; B) = \frac{\cov(A; B)}{\sigma(A) \sigma(B)} = \frac{\braket{A B} - \braket{A} \braket{B}}{\sigma(A) \sigma(B)} \in \langle -1; 1 \rangle,
	\eeq
gdzie $\cov(A; B)$ oznacza kowariancję zmiennych $A$ i $B$. W przypadku gdy $\rho(A; B) = \pm 1$, wówczas zmienne $A$ i $B$ są całkowicie skorelowane, gdy zaś $\rho(A; B) = 0$, pomiędzy tymi zmiennymi nie występuje żadna korelacja, więc zmienne $A$ i $B$ są \emph{niezależne}. W przypadku korelacji elektronowej interesuje nas zależność między położeniami elektronów, a więc w przypadku dwóch elektronów liczbową miarą korelacji jest współczynnik $\rho(\mathbf{r}_1; \mathbf{r}_2)$. W meotdzie Hartree-Focka $\rho(\mathbf{r}_1; \mathbf{r}_2) = 0$, więc ruch elektronów w tej metodzie jest nieskorelowany.

\subsection{Energia korelacji}

Obliczając energię układu w metodzie Hartree-Focka uzyskujemy wartość~\ehf, która jest tylko przybliżeniem dokładnej energii układu, $E$, którą otrzymalibyśmy rozwiązaując równanie Schrödingera z hamiltonianem~\eqref{eq:mo_ham_bj}. Ponieważ tego równania nie potrafimy rozwiązać analitycznie dla układów wieloelektronowych, nie znamy dokładnej wartości $E$, możemy ją tylko obliczać w sposób przybliżony, stosując metody dokładniejsze od metody Hartree-Focka. Należy dodać, że wartość $E$ nie jest fizyczną (mierzalną) energią układu, gdyż hamiltonian~\eqref{eq:mo_ham_bj} nie uwzględnia efektów relatywistycznych.

\emph{Energia korelacji} jest różnicą energii układu obliczoną w metodzie Hartree-Focka i dokładną wartością własną nierelatywistycznego hamiltonianu~\eqref{eq:mo_ham_bj}:
	\beq
	\boxed{\ecor = \ehf - E}.
	\eeq
Metoda Hartree-Focka jest oparta na zasadzie wariacyjnej, a więc $\ehf > E$, czyli energia korelacji jest ujemna.

\subsection{Funkcje jawnie skorelowane}

Najbardziej intuicyjnym sposobem fizycznego opisu korelacji elektronowej jest jawne wbudowanie zależności między elektronami w postać funkcji falowej i zastosowanie metody wariacyjnej do wyznaczenia energii układu. Na przykład dla atomu helu moglibyśmy nieznacznie zmodyfikować postać funkcji, której użyliśmy w podrozdziale~\ref{ssc:zw_he_podst}, jawnie wprowadzając odległość między dwoma elektronami, $r_{12}$:
	\beq
	\phi(\mathbf{r_1}; \mathbf{r}_2; c; \lambda) = N(c; \lambda) e^{-c (r_1 + r_2)} e^{-\lambda r_{12}},
	\label{eq:kor_hel}
	\eeq
gdzie $c$~i~$\lambda$ są parametrami wariacyjnymi (oczywiście całkowita funkcja falowa będzie iloczynem funkcji próbnej i funkcji spinowej, która jednak nie jest wariowana i możemy ją pominąć w obliczeniach wariacyjnych). W praktyce obliczenia z funkcjami jawnie skorelowanymi są bardzo czasochłonne i ich zastosowanie ogranicza się do małych układów (złożonych z kilku elektronów).

\subsection{Metoda oddziaływania konfiguracji}

Stosując bazę złożoną z $M$ spinorbitali w metodzie Hartree-Focka do opisu układu $N$-eletronowego uzyskujemy $M$ spinorbitali, z których $N$ jest zajętych, a $M - N$ wirtualnych. A więc ze wszystkich spinorbitali możemy zbudować $\binom{M}{N}$ wyznaczników Slatera. Wyznaczniki te tworzą bazę przestrzeni, w której szukamy dokładnego rozwiązania w ramach przybliżenia jednoelektronowego. Jako funkcję falową możemy więc użyć kombinację liniową wszystkich $\binom{M}{N}$ funkcji i wariacyjnie wyznaczyć współczynniki tej kombinacji. Tak właśnie postępujemy w metodzie \emph{pełnego oddziaływania konfiguracji}, FCI (ang.~\emph{full configuration interaction}). Funkcję falową układu $N$-elektronowego w metodzie FCI możemy więc zapisać w postaci
	\beq
	\wfci = c_0 \fvac + \hat{C}_1 \fvac + \hat{C}_2 \fvac + \ldots + \hat{C}_N \fvac,
	\label{eq:cor_ffci}
	\eeq
gdzie $\hat{C}_n$ jest operatorem $n$-krotnych wzbudzeń. Operator taki najdogodniej jest zapisać przy użyciu kreatorów i anihilatorów, korzystając z zależności~\eqref{eq:hf_ff_wzbudz}:
	\beq
	\begin{split}
	\hat{C}_n & = \sum_{i < j < \ldots < k} \sum_{a < b < \ldots < c} c_{i j \ldots k}^{a b \ldots c} \, \crt{a} \crt{b} \ldots \crt{c} \ann{k} \ldots \ann{j} \ann{i} = \\
	 & = \frac{1}{n!^2} \sum_{i j \ldots k} \sum_{a b \ldots c} c_{i j \ldots k}^{a b \ldots c} \, \crt{a} \crt{b} \ldots \crt{c} \ann{k} \ldots \ann{j} \ann{i},
	\end{split}
	\label{eq:cor_Cn}
	\eeq
gdzie $c_{i j \ldots k}^{a b \ldots c}$ to współczynniki kombinacji liniowej i jednocześnie parametry wariacyjne. Łącząc równanie~\eqref{eq:cor_Cn} z konwencją użytą w równaniu~\eqref{eq:hf_ff_wzbudz} możemy zapisać
	\beq
	\hat{C}_n \fvac = \frac{1}{n!^2} \sum_{i j \ldots k} \sum_{a b \ldots c} c_{i j \ldots k}^{a b \ldots c} \, \crt{a} \crt{b} \ldots \crt{c} \ann{k} \ldots \ann{j} \ann{i} \fvac =
	\frac{1}{n!^2} \sum_{i j \ldots k} \sum_{a b \ldots c} c_{i j \ldots k}^{a b \ldots c} \, \ewk{i j \ldots k}{a b \ldots c}.
	\eeq
W tym miejscu należy uściślić powszechnie stosowaną konwencję sumacyjną: $i$,~$j$,~$k$~$\ldots$ odnoszą się do spinorbitali zajętych, zaś $a$,~$b$,~$c$,~$\ldots$ --- wirtualnych. Aby więc uzyskać wszystkie możliwe wzbudzenia $n$-krotne, musimy podziałać na stan próżni Fermiego $n$ kreatorami i $n$ anihilatorami. Jeśli mamy $N$ zajętych i $M - N$ wirtualnych spinorbitali, wówczas $n$ spinorbitali zajętych możemy wybrać na $\binom{N}{n}$ sposobów, zaś $n$ spinorbitali wirtualnych --- na $\binom{M - N}{n}$ sposobów. A więc liczba wzbudzeń $n$-krotnych wynosi
	\beq
	l_n = \binom{N}{n} \binom{M - N}{n}
	\eeq
i tyle też jest współczynników wariacyjnych zawartych w operatorze~$\hat{C}_n$. Czynnik $\frac{1}{n!^2}$ wprowadzony w wyrażeniu~\eqref{eq:cor_Cn} bierze się stąd, że przy sumowaniu po całym zakresie indeksów spinorbitali w członie $\hat{C}_n \fvac$ uzyskujemy $N^n (M - N)^n$ członów, a więc za dużo, sumujemy bowiem wielokrotnie zestawy indeksów, które nie pojawiają się przy sumowaniu rozłącznym ($i < j < \ldots < k$,~$a < b < \ldots < c$). Ponieważ w takim członie występuje $n$ indeksów spinorbitali zajętych i $n$ indeksów spinorbitali wirtualnych, zaś przy założeniu, że współczynniki $c_{i j \ldots k}^{a b \ldots c}$ są, tak jak i funkcja falowa, antysymetryczne ze względu na zamianę indeksów spinorbitali zajętych lub wirtualnych miejscami, dowolna permutacja jednego lub drugiego zestawu indeksów daje ten sam człon, więc na przykład
	\beq
	c_{i j \ldots k}^{a b \ldots c} = -c_{j i \ldots k}^{a b \ldots c} = -c_{i j \ldots k}^{b a \ldots c} = c_{j i \ldots k}^{b a \ldots c}.
	\eeq
Wobec tego każdy człon pojawiający się przy sumowaniu rozłącznym pojawia się $n! \cdot n! = n!^2$ razy przy sumowaniu po całym zestawie indeksów --- stąd właśnie sumę obliczoną przy takim sumowaniu dzielimy przez $n!^2$.

Metoda FCI jest najdokładniejszą metodą służącą do obliczania energii korelacji i tym samym nierelatywistycznej energii układu. Z uwagi jednak na błyskawiczny wzrost liczby parametrów wariacyjnych ze wzrostem liczby elektronów układu i rozmiaru bazy, metoda FCI w praktyce jest stosowana do obliczeń dla bardzo małych układów i służy jako metoda wzorcowa do testowania innych metod. Stąd rozwinięcie~\eqref{eq:cor_ffci} ucina się na pojedynczych lub podwójnych wzbudzeniach, jak na przykład w metodzie CISD (ang. \emph{configuration interaction singles and doubles}):
	\beq
	\wcisd = c_0 \fvac + \sum_i \sum_a c_i^a \, \crt{a} \ann{i} \fvac + \frac{1}{2} \sum_{ij} \sum_{ab} c_{ij}^{ab} \, \crt{a} \crt{b} \ann{j} \ann{i} \fvac.
	\eeq
Metoda CISD może w praktyce być stosowana do obliczeń dla bardzo dużych układów.

\subsection{Metoda sprzężonych klastrów}

W \emph{metodzie sprzężonych klastrów} (ang. \emph{coupled cluster}) funkcję falową zakłada się w postaci
	\beq
	\wcc = e^{\hat{T}} \fvac,
	\eeq
gdzie
	\beq
	\hat{T} = \hat{T}_1 + \hat{T}_2 + \ldots + \hat{T}_N
	\label{eq:cor_Tn}
	\eeq
jest sumą operatorów wzbudzeń definiowanych tak jak w metodzie CI:
	\beq
	\hat{T}_n = \frac{1}{n!^2} \sum_{i j \ldots k} \sum_{a b \ldots c} t_{i j \ldots k}^{a b \ldots c} \, \crt{a} \crt{b} \ldots \crt{c} \ann{k} \ldots \ann{j} \ann{i},
	\eeq
gdzie współczynniki $t_{i j \ldots k}^{a b \ldots c}$ to tzw. amplitudy klasterowe. Wyznaczenie tychże właśnie amplitud jest celem metody sprzężonych klastrów. Można to oczywiście zrobić wariacyjnie, jest to jednak bardzo skomplikowane zadanie i w praktyce amplitudy wyznacza się rzutując wzbudzone funkcje na stan podstawowy. Oznacza to, że CC nie jest metodą wariacyjną, więc energia uzyskana w tej metodze może być mniejsza od dokładnej, nierelatywistycznej energii układu, w przeciwieństwie do metody CI (energia uzyskana w metodzie CI jest zawsze większa od ścisłej energii nierelatywistycznej układu). Pomimo, że podstawowe równania metody CC są zwodniczo proste, zadanie to jest nietrywialne, co wynika z eksponenjcalnej postaci operatora klasterowego, $e^{\hat{T}}$. Podobnie jak w metodzie CI, w metodzie CC w praktycznych obliczeniach obcina się rozwinięcie operatora wzbudzeń~\eqref{eq:cor_Tn} na przykład na wzbudzeniach pojedynczych i podwójnych (metoda CCSD).