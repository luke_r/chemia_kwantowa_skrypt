#!/usr/bin/gnuplot

set terminal postscript eps enhanced colour solid "NewCenturySchlbk-Roman" 18

set style line 1 lc rgb "blue" lw 5 pt 6 ps 2
set style line 2 lc rgb "red" lw 5 pt 6 ps 2
set style line 3 lc rgb "green" lw 5 pt 6 ps 2
set style line 4 lc rgb "black" lw 5 pt 6 ps 2

c = 299792458 # m / s
h = 6.6260693E-34 # J * s
k = 1.3806505E-23 # J / K

vmax = 5e14
umax = 6.1e-17
scale_v(v) = vmax * v

u(v) = (8 * pi * h * scale_v(v)**3 / (c**3 * exp(h * scale_v(v) / (k * T))-1)) / umax

set output "Planck.eps"
set key spacing 1.5
xl = sprintf("{/Symbol-Oblique n} / (%g Hz)", vmax)
yl = sprintf("{/Italic u}({/Symbol-Oblique n}, {/Italic T}) / (%g J {/Symbol \327} s / m^3)", umax)
set xlabel xl
set ylabel yl

plot [v = 0 : 1] [0 : 1] \
T = 500, u(v) t "{/Italic T} = " . T. " K" w l ls 1, \
T = 1000, u(v) t "{/Italic T} = " . T. " K" w l ls 2, \
T = 1500, u(v) t "{/Italic T} = " . T. " K" w l ls 3, \
T = 2000, u(v) t "{/Italic T} = " . T. " K" w l ls 4
