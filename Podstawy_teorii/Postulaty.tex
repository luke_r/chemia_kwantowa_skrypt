\section{Postulaty mechaniki kwantowej}

\subsection{Postulat o stanie układu kwantowego $N$ cząstek}

Stan kwantowomechaniczny układu złożonego z $N$ cząstek, z których każda znajduje się w punkcie $\mathbf{r}_i = \begin{bmatrix} x_i \\ y_i \\ z_i \end{bmatrix}$ jest określony funkcją falową $\Psi(\mathbf{r}_1, \mathbf{r}_2, \ldots, \mathbf{r}_N, t)$ (zależną od $3N+1$ współrzędnych) taką, że kwadrat jej modułu jest proporcjonalny do gęstości prawdopodobieństwa znalezienia się układu w tym stanie w czasie $t$. Aby kwadrat modułu\footnote{Kwadrat modułu funkcji jest równy iloczynowi tej funkcji przez jej sprzężenie: $|\Psi|^2 = \Psi^* \Psi$.} tej funkcji był gęstością prawdopodobieństwa, funkcja falowa $\Psi$ wycałkowana po całym zakresie zmienności współrzędnych przestrzennych musi być jednością dla dowolnego czasu $t$:
	\beq
	\int_{\mathbf{r}_1} \int_{\mathbf{r}_2} \ldots \int_{\mathbf{r}_N}
	|\Psi(\mathbf{r}_1; \mathbf{r}_2; \ldots; \mathbf{r}_N; t)|^2 \,
	d^3 \mathbf{r}_1 d^3 \mathbf{r}_2 \ldots d^3 \mathbf{r}_N = 1,
	\label{eq:pos_norm_ff}
	\eeq
wówczas gęstość prawdopodobieństwa dana jest wzorem
	\beq
		\pi (\mathbf{r}_1, \mathbf{r}_2, \ldots, \mathbf{r}_N, t) =
	|\Psi(\mathbf{r}_1, \mathbf{r}_2, \ldots, \mathbf{r}_N, t)|^2.
	\eeq
Jest to \emph{probalistyczna interpretacja funkcji falowej Borna}. Funkcja falowa musi być jednoznaczna, skończona i ciągła w całej przestrzeni. Funkcje spełniające te warunki są \emph{funkcjami klasy Q} (ang. \emph{quantum}).

\subsection{Postulat o operatorach zmiennych dynamicznych}

Każdej zmiennej dynamicznej $A$ (np. współrzędnej położenia, pędu, momentu pędu, energii kinetycznej i potencjalnej) odpowiada \emph{operator liniowy} $\hat{A}$, a więc operator spełniający warunek
	\beq
	\forall_{c_1, c_2 \in \mathbb{C}}: \hat{A}(c_1 \Psi_1 + c_2 \Psi_2) =
	c_1 \hat{A} \Psi_1 + c_2 \hat{A} \Psi_2.
	\eeq
Najistotniejsze w mechanice kwantowej są operatory $(\alpha \in \{x; y; z\})$:
	\begin{itemize}
	\item
	położenia cząstki~$(\alpha)$,
	\beq
	\hat{\alpha} = \alpha,
	\eeq
	jest to więc operator mnożenia przez daną współrzędną,
	\item
	$q$-tej składowej pędu $(p_\alpha)$,
	\beq
	\hat{p}_\alpha = -i \hbar \frac{\partial}{\partial \alpha},
	\eeq
	a ponieważ pęd jest wielkością wektorową i zachodzi
	\beq
	\mathbf{p} = p_x \hat{\mathbf{x}} + p_y \hat{\mathbf{y}} + p_z \hat{\mathbf{z}},
	\eeq
	więc operator całkowitego pędu ma postać
	\beq
		\hat{\mathbf{p}} = -i \hbar \left(
			\frac{\partial}{\partial x} \hat{\mathbf{x}} +
			\frac{\partial}{\partial y} \hat{\mathbf{y}} +
			\frac{\partial}{\partial z} \hat{\mathbf{z}}
		\right) = -i \hbar \nabla_\mathbf{r},
	\eeq
	\item
	energii potencjalnej oddziaływania elektronu z jądrem o ładunku $Ze$,
	\beq
	\hat{V} = -\frac{Z e^2}{4 \pi \epsilon_0 r}.
	\eeq
	\end{itemize}
Operatory pozostałych wielkości dynamicznych tworzy się wyrażając je poprzez $\alpha$, $p_\alpha$ i $V$ i zastępując je odpowiednimi operatorami.
	\begin{task}
	Znaleźć operator składowej zetowej momentu pędu.
	\end{task}
	\begin{sol}
	Moment pędu jest iloczynem wektorowym
	\begin{align}
		\mathbf{l} = \mathbf{r} \times \mathbf{p} & =
			\begin{vmatrix}
				\hat{\mathbf{x}} & \hat{\mathbf{y}} & \hat{\mathbf{z}} \\
				x & y & z \\
				p_x & p_y & p_z
			\end{vmatrix} \\
		 & = (y p_z - z p_y) \hat{\mathbf{x}} +
			(z p_x - x p_z) \hat{\mathbf{y}} +
			(x p_y - y p_z) \hat{\mathbf{z}} \\
		 & \Rightarrow l_z = x p_y - y p_z,
	\end{align}
	zatem
	\beq
	\hat{l}_z = i \hbar
	\left(y \frac{\partial}{\partial x} - x \frac{\partial}{\partial y} \right).
	\eeq
	\label{ts:mom_pd_kart}
	\end{sol}
	\begin{task}
	Znaleźć operator energii kinetycznej cząstki o masie $m$ i pędzie $p$.
	\end{task}
	\begin{sol}
	Ponieważ zachodzi
	\beq
	T = \frac{p^2}{2 m},
	\eeq
	stąd
	\beq
	\hat{T} = -\frac{\hbar^2}{2m}
	\left( \frac{\partial^2}{\partial x^2} +
	\frac{\partial^2}{\partial y^2} +
	\frac{\partial^2}{\partial z^2} \right) =
	-\frac{\hbar^2}{2m} \Delta_\mathbf{r}.
	\eeq
	\end{sol}
\emph{Operator Hamiltona} (\emph{hamiltonian}) to operator energii całkowitej układu. Hamiltonian elektronu w atomie wodoropodobnym o liczbie porządkowej $Z$ ma postać
	\beq
	\hat{H} = \hat{T} + \hat{V} =
	-\frac{\hbar^2}{2m} \Delta_\mathbf{r} - \frac{Z e^2}{4 \pi \epsilon_0 r}.
	\eeq

\subsubsection{Przemienność operatorów}

Jeśli dla dwóch operatorów, $\hat{A}$ i $\hat{B}$, zachodzi
	\beq
	\hat{A} \hat{B} = \hat{B} \hat{A},
	\eeq
wówczas operatory $\hat{A}$ i $\hat{B}$ są \emph{przemienne}, czyli \emph{komutują}. \emph{Komutator} tych operatorów ma postać
	\beq
	\comm{\hat{A}}{\hat{B}} = \hat{A} \hat{B} - \hat{B} \hat{A}
	\label{eq:kom}
	\eeq
i jest zerem dla przemiennych operatorów. Z równania~(\ref{eq:kom}) od razu wynika
	\beq
	\comm{\hat{A}}{\hat{A}} = 0
	\eeq
oraz
	\beq
	\comm{\hat{A}}{\hat{B}} = -\comm{\hat{B}}{\hat{A}}.
	\eeq
Ponadto dla dowolnych stałych $a,b \in \mathbb{C}$ zachodzi
	\beq
	\comm{\hat{A}}{a} = \comm{a}{\hat{A}} = 0
	\eeq
oraz
	\beq
	\comm{\hat{A} + a}{\hat{B} + b} = \comm{\hat{A}}{\hat{B}}.
	\label{eq:kom_st}
	\eeq
	\begin{task}
	Obliczyć komutator $\comm{\hat{x}}{\hat{p}_x}$.
	\end{task}
	\begin{sol}
	Aby obliczyć jakikolwiek komutator, należy rozważyć jego działanie na daną funkcję. W tym przypadku może to być funkcja jednej zmiennej, $\psi(x)$:
	\beq
	\comm{\hat{x}}{\hat{p}_x} \psi(x) = -i \hbar \qty\Big(x f'(x) - \qty\big(x f(x))') =
		-i \hbar \qty\Big(x f'(x) - f(x) - x f'(x)) = i \hbar f(x),
	\eeq
	więc
	\beq
	\comm{\hat{x}}{\hat{p}_x} = i \hbar.
	\label{eq:kom_xpx}
	\eeq
	\end{sol}
	\begin{task}
		Udowodnić równość $\comm{\hat{A} \hat{B}}{\hat{C}} = \hat{A} \comm{\hat{B}}{\hat{C}} + \comm{\hat{A}}{\hat{C}} \hat{B}$.
	\end{task}
	\begin{sol}
	Rozpisujemy prawą stronę powyższego równania:
	\beq
	\hat{A} \comm{\hat{B}}{\hat{C}} + \comm{\hat{A}}{\hat{C}} \hat{B} =
	\hat{A} \hat{B} \hat{C} - \hat{A} \hat{C} \hat{B} + \hat{A} \hat{C} \hat{B} - \hat{C} \hat{A} \hat{B} =
	\hat{A} \hat{B} \hat{C} - \hat{C} \hat{A} \hat{B} =
	\comm{\hat{A} \hat{B}}{\hat{C}}.
	\eeq
	\end{sol}

\subsubsection{Operatory samosprzężone}

Rozważmy operator $\hat{A}$ działający w przestrzeni funkcji klasy $Q$ jednej zmiennej. Operator $\hat{A}$ jest \emph{hermitowski}~(samosprzężony), jeśli dla dowolnych funkcji $\psi$ i $\phi$ zachodzi
	\beq
	\int_{-\infty}^{\infty} \psi^*(x) \hat{A} \phi(x) \, dx =
	\int_{-\infty}^{\infty} \left[\hat{A} \psi(x)\right]^* \phi(x) \, dx.
	\label{eq:herm}
	\eeq
W tym miejscu dogodnie jest wprowadzić \emph{notację braketową Diraca}. W notacji tej zwykłą funkcję umieszcza się w \emph{kecie}: $\psi \equiv \ket{\psi}$, zaś jej funkcję do niej sprzężoną --- w \emph{bra}: $\psi^* \equiv \bra{\psi}$. Zapis $\bra{\psi}\ket{\phi}$ oznacza iloczyn skalarny funkcji $\psi$ i $\phi$. Iloczyn ten dla funkcji jednej zmiennej można zdefiniować na całym zbiorze $\mathbb{R}$:
	\beq
	\bra{\psi}\ket{\phi} = \int_{-\infty}^\infty \psi^*(x) \phi(x) \, dx.
	\label{eq:il_sk}
	\eeq
Ze wzoru~(\ref{eq:il_sk}) od razu wynika
	\beq
	\bra{\psi}\ket{\phi} = \bra{\phi}\ket{\psi}^*.
	\eeq
Warunek~(\ref{eq:herm}) można zapisać zwięźle
	\beq
	\bra{\psi}\ket{\hat{A} \phi} = \bra{\hat{A} \psi}\ket{\phi},
	\eeq
przy czym
	\begin{align}
		\bra{\psi}\ket{\hat{A} \phi} \equiv \mel**{\psi}{\hat{A}}{\phi} & =
			\int_{-\infty}^\infty \psi^*(x) \hat{A} \phi(x) \dd{x}, \\
		\bra{\hat{A} \psi}\ket{\phi} & = \int_{-\infty}^\infty \pqty{\hat{A} \psi(x)}^* \phi(x) \dd{x}.
	\end{align}
Sprzężenie hermitowskie operatora oznaczamy zazwyczaj krzyżem,~$\hat{A}^\dag$, tak jak dla macierzy. Definicję operatora sprzężonego możemy zapisać korzystając z notacji braketowej,
	\beq
	\mel**{\psi}{\hat{A}}{\phi} = \bra{\hat{A}^\dag \psi}\ket{\phi}.
	\eeq
	\begin{task}
	Sprawdzić, czy operator $\hat{A} = x$ jest hermitowski.
	\end{task}
	\begin{sol}
	Warunek~(\ref{eq:herm}) w tym przypadku ma postać
	\beq
	\int_{-\infty}^\infty \psi^*(x) x \phi(x) \, dx =
	\int_{-\infty}^\infty [x \psi(x)]^* \phi(x) \, dx.
	\label{eq:herm_x}
	\eeq
	Ponieważ
	\beq
	\forall_{x \in \mathbb{R}}: [x \psi(x)]^* = x \psi^*(x),
	\eeq
	stąd obie strony w równaniu~(\ref{eq:herm_x}) są równe, czyli operator $x$ jest hermitowski.
	\end{sol}
	\begin{task}
	Sprawdzić, czy operator $\hat{A} = \frac{\partial}{\partial x}$ jest hermitowski.
	\end{task}
	\begin{sol}
	Warunek~(\ref{eq:herm}) w tym przypadku ma postać
	\beq
	\int_{-\infty}^\infty \psi^*(x) \frac{d}{d x} \phi(x) \, dx =
	\int_{-\infty}^\infty \left[ \frac{d}{d x} \psi(x) \right]^* \phi(x) \, dx.
	\eeq
	Rozpisujemy lewą stronę powyższego równania wykorzystując całkowanie przez części:
	\begin{eqnarray}
	\nonumber \int_{-\infty}^\infty \psi^*(x) \frac{d}{d x} \phi(x) \, dx & = &
	\left\{ \begin{array}{l}
	f(x) = \psi^*(x) \mbox{, } f'(x) = \left[ \frac{d}{d x} \psi(x) \right]^* \\
	g'(x) = \frac{d}{d x} \phi(x) \mbox{, } g(x) = \phi(x)
	\end{array} \right\}_{-\infty}^\infty = \\
	\nonumber & = & \underbrace{\left[ \psi^*(x) \phi(x) \right]_{-\infty}^\infty}_0 -
	\int_{-\infty}^\infty \left[ \frac{d}{d x} \psi(x) \right]^* \phi(x) \, dx = \\
	 & = & -\int_{-\infty}^\infty \left[ \frac{d}{d x} \psi(x) \right]^* \phi(x) \, dx,
	\label{eq:herm_ddx}
	\end{eqnarray}
	tak więc operator $\hat{A} = \frac{\partial}{\partial x}$ nie jest hermitowski, bowiem~$\hat{A}^\dag = -\hat{A}$. Nietrudno zauważyć na podstawie równania~(\ref{eq:herm_ddx}), że operator $\hat{B} = i \frac{\partial}{\partial x}$ jest hermitowski.
	\label{ts:herm_ddx}
	\end{sol}
	\begin{task}
	Wykazać, że jeśli operator $\hat{A}$ jest hermitowski, to operator $\hat{A}^2$ --- również.
	\end{task}
	\begin{sol}
	Korzystamy z hermitowskości operatora $\hat{A}$:
	\beq
	\ev**{\hat{A}^2}{\psi} = \braket{\hat{A} \psi} = \bra{\hat{A}^2 \psi}\ket{\psi}.
	\eeq
	\end{sol}

\subsection{Postulat o ewolucji funkcji falowej w czasie}

Zmianę funkcji falowej układu $N$ cząstek w czasie opisuje \emph{równanie Schrödingera zależne od czasu}:
	\beq
	i \hbar \frac{\partial \Psi(\mathbf{r}_1, \mathbf{r}_2, \ldots, \mathbf{r}_N, t)}{\partial t} =
	\hat{H} \Psi(\mathbf{r}_1, \mathbf{r}_2, \ldots, \mathbf{r}_N, t),
	\label{eq:rs_zcz}
	\eeq
gdzie $\hat{H}$ jest hamiltonianem układu. $\hat{H}$~może zależeć od czasu.

\emph{Stany stacjonarne} to stany układu, dla których gęstość prawdopodobieństwa nie zależy od czasu. Stwierdzenie to nie oznacza niezależności od czasu samej funkcji falowej. Dla uproszczenia rozpatrujmy w dalszym ciągu układ złożony z jednej cząstki. Ponieważ gęstość prawdopodobieństwa
	\beq
		\rho(\mathbf{r}; t) = \Psi^*(\mathbf{r}; t) \Psi(\mathbf{r}; t),
	\eeq
$\rho(\mathbf{r}; t)$ nie zmienia się w czasie, jeśli
	\beq
	\Psi(\mathbf{r}; t) = \psi(\mathbf{r}) e^{-i f(t)} \mbox{, } f(t) \in \mathbb{R}.
	\label{eq:ff_exp}
	\eeq
Dla funkcji~(\ref{eq:ff_exp}) zachodzi
	\beq
	\rho(\mathbf{r}; t) = \psi^*(\mathbf{r}) e^{i f(t)} \psi(\mathbf{r}) e^{-i f(t)} = |\psi(\mathbf{r})|^2 \equiv
	\rho(\mathbf{r}).
	\eeq
Wstawiając funkcję~(\ref{eq:ff_exp}) do równania~(\ref{eq:rs_zcz}) otrzymujemy
	\beq
	\hbar \psi(\mathbf{r}) \frac{d f(t)}{d t} = \hat{H} \psi(\mathbf{r}).
	\label{eq:hpsi}
	\eeq
Jeśli hamiltionian nie zależy od czasu, wówczas od czasu nie zależy prawa strona równania~(\ref{eq:hpsi}), czyli lewa także jest niezależna od czasu. Jest to spełnione wtedy, gdy
	\beq
	f(t) = a t + b \mbox{, } a, b \in \mathbb{R}.
	\eeq
Stałą $b$ można wyznaczyć narzucając warunek pokrywania się funkcji $\Psi$ i $\psi$ w chwili $t=0$:
	\beq
	\Psi(\mathbf{r}, 0) = \psi(\mathbf{r}) \Rightarrow b = 0,
	\eeq
tak więc równanie~(\ref{eq:hpsi}) przyjmuje postać
	\beq
	\hat{H} \psi(\mathbf{r}) = a \hbar \psi(\mathbf{r}).
	\eeq
Wprowadzając oznaczenie $E = a \hbar$ i nadając temu wyrażeniu sens energii, można podać ogólną postać funkcji falowej układu $N$ cząstek w stanie stacjonarnym:
	\beq
	\Psi(\mathbf{r}_1; \mathbf{r}_2; \ldots; \mathbf{r}_N; t) =
	\psi(\mathbf{r}_1; \mathbf{r}_2; \ldots; \mathbf{r}_N) e^{-\frac{iEt}{\hbar}}
	\eeq
oraz \emph{równanie Schrödingera niezależne od czasu}:
	\beq
	\hat{H} \psi(\mathbf{r}_1; \mathbf{r}_2; \ldots; \mathbf{r}_N) =
	E \psi(\mathbf{r}_1; \mathbf{r}_2; \ldots; \mathbf{r}_N).
	\eeq
	\begin{task}
	Funkcja falowa elektronu w stanie podstawowym w atomie wodoropodobnym ma postać
	\beq
	\psi(\mathbf{r}) = N e^{-\frac{Z r}{a_0}}.
	\eeq
	Wyznaczyć stałą normalizacyjną $N$.
	\end{task}
	\begin{sol}
	Warunek normalizacji w układzie współrzędnych sferycznych dla funkcji zależnej od współrzędnych jednej cząstki ma postać
	\beq
	\int_0^\infty \int_0^\pi \int_0^{2 \pi} |\psi(\mathbf{r})|^2 r^2 \sin \theta \, dr \, d\theta \, d\phi = 1,
	\eeq
	gdzie $\mathbf{r} = (r; \theta; \phi)$. W tym przypadku otrzymujemy
	\beq
	\int_0^\infty \int_0^\pi \int_0^{2 \pi} N^2 r^2 e^{-\frac{2 Z r}{a_0}} \sin \theta \, dr \, d\theta \, d\phi =
	N^2 \int_0^\infty r^2 e^{-\frac{2 Z r}{a_0}} \, dr
	\int_0^\pi \sin \theta \, d\theta
	\int_0^{2 \pi} \, d\phi = 1.
	\label{eq:norm_1s}
	\eeq
	Równanie~(\ref{eq:norm_1s}) prowadzi do zależności
	\beq
	\frac{N^2 \pi a_0^3}{Z^3} = 1 \Rightarrow N^2 = \frac{Z^3}{\pi a_0^3},
	\eeq
	tak więc stałą $N$ możemy wybrać na przykład jako
	\beq
	N = \frac{1}{\sqrt{\pi}} \left( \frac{Z}{a_0} \right)^{\frac{3}{2}}.
	\eeq
	\end{sol}

\subsection{Postulat o wartościach własnych}

Jeśli dany operator $\hat{A}$ odpowiadający zmiennej dynamicznej $A$ spełnia wraz z funkcją klasy $Q$ równanie
	\beq
	\hat{A} \ket{\psi_n} = a_n \ket{\psi_n},
	\label{eq:zag_wl}
	\eeq
to funkcja $\ket{\psi_n}$ jest \emph{funkcją własną} operatora $\hat{A}$, a liczba $A$ --- \emph{wartością własną} tego operatora. Równanie~(\ref{eq:zag_wl}) jest tzw. \emph{zagadnieniem własnym} operatora $\hat{A}$. Jeśli danej wartości własnej odpowiada $K$ liniowo niezależnych funkcji własnych:
	\beq
	\hat{A} \ket{\psi_{ni}} = a_n \ket{\psi_{ni}} \mbox{, } i \in \langle 1; K \rangle \cap \mathbb{N},
	\eeq
to wartość własna $a_n$ jest $K$-krotnie \emph{zdegenerowana} (\emph{zwyrodniała}).
	\begin{task}
	Udowodnić, że jeśli funkcja $\ket{\psi_n}$ jest funkcją własną operatora $\hat{A}$, to funkcja $c \ket{\psi_n}$, $c \in \mathbb{R}$ jest nią także i odpowiada tej samej wartości własnej.
	\end{task}
	\begin{sol}
	Dowód opiera się na liniowości operatora $\hat{A}$:
	\beq
	\hat{A} \ket{\psi_n} = a_n \ket{\psi_n} \Rightarrow \hat{A} (c \ket{\psi_n}) = c \hat{A} \ket{\psi_n} =
	c a_n \ket{\psi_n} = a_n (c \ket{\psi_n}).
	\eeq
	\label{ts:ww_st}
	\end{sol}

Jeśli układ znajduje się w stanie opisanym funkcją $\ket{\psi_n}$, to wynikiem pomiaru wielkości $A$ może być tylko wartość własna tego operatora, $a_n$. Operatory występujące w mechanice kwantowej posiadają następujące własności:
	\begin{enumerate}
	\item
	wartości własne operatorów hermitowskich są rzeczywiste. Niech $\hat{A}$ będzie operatorem hermitowskim, czyli spełniającym zależność
	\beq
	\ev**{\hat{A}}{\phi_n} = \bra{\hat{A} \psi_n}\ket{\phi_n}.
	\eeq
	Rozpatrzmy zagadnienie własne operatora $\hat{A}$
	\beq
	\hat{A} \ket{\psi_n} = a_n \ket{\psi_n}
	\label{eq:herm_rl}
	\eeq
	i równanie do niego sprzężone:
	\beq
	\hat{A}^* \bra{\psi_n} = a_n^* \bra{\psi_n}.
	\label{eq:herm_rp}
	\eeq
	Mnożąc skalarnie równanie~(\ref{eq:herm_rl}) przez $\bra{\psi_n}$, zaś równanie~(\ref{eq:herm_rp}) przez $\ket{\psi_n}$ i wykorzystując hermitowskość operatora $\hat{A}$ otrzymujemy
	\beq
	\ev**{a_n}{\psi_n} = \bra{a_n \psi_n}\ket{\psi_n}
	\Rightarrow a_n = a_n^*
	\Rightarrow a_n \in \mathbb{R},
	\eeq
	\item
	funkcje własne operatorów hermitowskich należące do różnych wartości własnych są ortogonalne. Funkcje ortogonalne to takie, których iloczyn skalarny jest zerem, tzn.
	\beq
		\bra{\psi}\ket{\phi} = 0.
	\eeq
	Rozważmy dwie funkcje własne operatora hermitowskiego $\hat{A}$:
	\beq
	\hat{A} \ket{\psi_1} = a_1 \ket{\psi_1}
	\label{eq:herm_ol}
	\eeq
	i
	\beq
	\hat{A} \ket{\psi_2} = a_2 \ket{\psi_2}
	\label{eq:herm_op}
	\eeq
	takie, że $a_1 \not= a_2$. Mnożąc skalarnie równanie~(\ref{eq:herm_ol}) przez $\bra{\psi_2}$ i równanie sprzężone do równania~(\ref{eq:herm_op})\footnote{Operator $\hat{A}$ jest hermitowski, więc $a_2^* = a_2$.},
	\beq
	\hat{A}^* \bra{\psi_2} = a_2 \bra{\psi_2},
	\eeq
	przez $\ket{\psi_1}$ i wykorzystują hermitowskość operatora $\hat{A}$ otrzymujemy
	\beq
	(a_1 - a_2) \bra{\psi_2}\ket{\psi_1} = 0,
	\label{eq:herm_dd}
	\eeq
	a ponieważ założyliśmy, że obie wartości własne są różne, równość~(\ref{eq:herm_dd}) jest spełniona wtedy i tylko wtedy, gdy
	\beq
	\bra{\psi_2}\ket{\psi_1} = 0,
	\eeq
	więc funkcje $\ket{\psi_1}$ i $\ket{\psi_2}$ są ortogonalne,
	\item
	funkcje własne operatora hermitowskiego tworzą układ zupełny, czyli każdą funkcję należącą do przestrzeni funkcji własnych można przedstawić jako ich kombinację liniową:
	\beq
	\ket{\phi} = \sum_{l = 1}^M c_l \ket{\psi_l},
	\label{eq:herm_kl}
	\eeq
	przy czym zachodzi
	\beq
	\hat{A} \ket{\psi_l} = a_l \ket{\psi_l}.
	\eeq
Współczynniki kombinacji liniowej~(\ref{eq:herm_kl}) można wyznaczyć mnożąc skalarnie to rozwinięcie przez $\bra{\psi_k}$. Otrzymujemy wówczas\footnote{Funkcje własne operatora $\hat{A}$ należące do różnych wartości własnych są ortogonalne: $\bra{\psi_k}\ket{\psi_l} = \delta_{kl}$.}
	\beq
	c_k = \bra{\psi_k}\ket{\phi},
	\eeq
	\item
	jeśli dla operatorów hermitowskich zachodzi $[\hat{A}; \hat{B}] = 0$ (tzn. operatory te są przemienne), wówczas operatory te mają wspólny układ funkcji własnych. Załóżmy, że wszystkie wartości własne obu operatorów są niezdegenerowane. Jeśli zachodzi
	\beq
	\hat{A} \ket{\psi_n} = a_n \ket{\psi_n},
	\eeq
	wówczas korzystając z liniowości operatora $\ket{B}$
	\beq
	\hat{B} \hat{A} \ket{\psi_n} = \hat{B} a_n {\psi_n} = a_n \hat{B} \ket{\psi_n},
	\eeq
	tak więc funkcja $\hat{B} \ket{\psi_n}$ jest także funkcją własną operatora $\hat{A}$ i odpowiada tej samej wartości własnej, $a_n$. Jak wykazano w ćwiczeniu~\ref{ts:ww_st}, jest to spełnione wówczas, gdy funkcja
	\beq
	\hat{B} \ket{\psi_n} = b_n \ket{\psi_n},
	\eeq
	czyli spełnia równanie własne operatora $\hat{B}$,
	\item
	jeśli operatory hermitowskie $\hat{A}$ i $\hat{B}$ posiadają wspólny zbiór funkcji własnych, to operatory te komutują. Operatory $\hat{A}$ i $\hat{B}$ spełniają równania własne
	\beq
	\left\{ \begin{array}{l}
	\hat{A} \ket{\psi_n} = a_n \ket{\psi_n} \\
	\hat{B} \ket{\psi_n} = b_n \ket{\psi_n}
	\end{array} \right.,
	\label{eq:op_AB}
	\eeq
	zaś z ich przemienności wynika, że dla dowolnej funkcji $\ket{\chi}$ zachodzi
	\beq
	\comm{\hat{A}}{\hat{B}} \ket{\chi} = \qty\Big(\hat{A} \hat{B} - \hat{B} \hat{A}) \ket{\chi}.
	\label{eq:op_prz}
	\eeq
	Funkcję $\ket{\chi}$ można przedstawić jako kombinację liniową funkcji układu zupełnego:
	\beq
	\ket{\chi} = \sum_{l = 1}^M c_l \ket{\psi_l}.
	\label{eq:op_kl}
	\eeq
	Łącząc~(\ref{eq:op_prz}) i~(\ref{eq:op_kl}) otrzymujemy
	\beq
	\sum_{l = 1}^M c_l \qty\Big(\hat{A} \hat{B} - \hat{B} \hat{A}) \ket{\psi_l} = 0 \Leftrightarrow
	\forall_{l \in \langle 1; M \rangle \cap \mathbb{N}}: \qty\Big(\hat{A} \hat{B} - \hat{B} \hat{A}) \ket{\psi_l} = 0.
	\eeq
	Z układu równań~(\ref{eq:op_AB}) wynika, że
	\beq
	\hat{A} \hat{B} \ket{\psi_l}= \hat{B} \hat{A} \ket{\psi_l} = a_l b_l \ket{\psi_l},
	\eeq
	zatem w ogólności zachodzi $\comm{\hat{A}}{\hat{B}} = 0$.
	\end{enumerate}

Jeżeli operatory $\hat{A}$ i $\hat{B}$ wielkości fizycznych $A$ i $B$ komutują, to wielkości te można jednocześnie zmierzyć.

Jeśli dla danego operatora $\hat{A}$ zachodzi równość~(\ref{eq:zag_wl}), zaś układ znajduje się w stanie kwantowym opisanym funkcją $\ket{\phi}$ nie będącą funkcją własną tego operatora, wówczas pomiar wielkości $A$ da w wyniku którąś z wartości własnych operatora $\hat{A}$. Można jedynie określić średnią wartość wyniku pomiarów:
	\beq
	\ev{A} = \frac{\ev**{\hat{A}}{\phi}}{\braket{\phi}},
	\eeq
jeśli zaś funkcja $\phi$ jest znormalizowana, to
	\beq
	\mv{A} = \ev**{\hat{A}}{\phi}.
	\label{eq:wsr}
	\eeq
Funkcję $\phi$ można przedstawić jako kombinację liniową funkcji układu zupełnego:
	\beq
	\ket{\phi} = \sum_{l=1}^M c_l \ket{\psi_l},
	\eeq
wtedy równanie~(\ref{eq:wsr}) można zapisać jako:
	\beq
	\ev**{A} = \sum_{k=1}^M \sum_{l=1}^M c_k^* c_l \mel**{\psi_k}{\hat{A}}{\psi_l} =
	\sum_{k=1}^M \sum_{l=1}^M c_k^* c_l a_l \bra{\psi_k}\ket{\psi_l} =
	\sum_{k=1}^M \sum_{l=1}^M c_k^* c_l a_l \delta_{kl} =
	\sum_{k=1}^M |c_k|^2 a_k.
	\label{eq:ws_wag}
	\eeq
Dla znormalizowanej funkcji $\ket{\phi}$ zachodzi
	\beq
	\braket{\phi} = \sum_{k=1}^M \sum_{l=1}^M c_k^* c_l \bra{\psi_k}\ket{\psi_l} =
	\sum_{k=1}^M |c_k|^2 = 1.
	\label{eq:sum_wg}
	\eeq
Z równań~(\ref{eq:ws_wag}) i~(\ref{eq:sum_wg}) wynika, że liczbę $|c_k|^2$ można interpretować jako prawdopodobieństwo, że w wyniku pomiaru wielkości $A$ otrzymamy wartość $a_k$.

	\begin{task}
		Niech~$\ket{A}$ będzie stanem własnym operatora~$\hat{A}$ z wartością własną~$a$:~$\hat{A} \ket{A} = a \ket{A}$. Znajdź wartość własną operatora~$e^{\alpha \hat{A}}$ ze stanem~$\ket{A}$.
	\end{task}
	\begin{sol}
		Korzystając z rozwinięcia~(\ref{eq:rT_ex}), możemy zapisać
			\beq
				e^{\alpha \hat{A}} = \sum_{k = 0}^\infty {\frac{\alpha^k}{k!} \hat{A}^k}.
			\eeq
		Ponieważ~$\hat{A}^k \ket{A} = a^k \ket{A}$, uzyskujemy
			\beq
				e^{\alpha \hat{A}} \ket{A} =
					\sum_{k = 0}^\infty {\frac{(\alpha a)^k }{k!} \ket{A}} =
					e^{\alpha a} \ket{A}.
			\eeq
	\end{sol}

\subsection{Postulat o spinie}
\label{rz:post_sp}

Cząstki elementarne posiadają dwa rodzaje momentu pędu: orbitalny, $\mathbf{L} = \mathbf{r} \times \mathbf{p}$, oraz wewnętrzny (spinowy), $\mathbf{S}$. Mierzalne są kwadrat całkowitego spinu:
	\beq
	S^2 = S_x^2 + S_y^2 + S_z^2 = s (s + 1) \hbar^2
	\eeq
oraz zetowa składowa spinu:
	\beq
	S_z = m_s \hbar,
	\eeq
gdzie $s$ jest \emph{spinową liczbą kwantową}, a $m_s$ --- \emph{magnetyczną kwantową liczbą spinową}. Cząstki o spinie połówkowym:
	\beq
	s = \frac{2 n + 1}{2} \mbox{, } n \in \mathbb{N} \cup \{0\}
	\eeq
to fermiony, a te o spinie całkowitym
	\beq
	s \in \mathbb{N} \cup \{0\}
	\eeq
to bozony. Tak więc danej cząstce elementarnej przypisuje się dodatkową, oprócz przestrzennych, współrzędną --- współrzędną spinową, która może przyjmować $2 s + 1$ wartości dyskretnych:
	\beq
		\sigma \in \{-s, -s + 1, \ldots, s - 1, s\} = \mathcal{S}_s.
	\eeq
Dla elektronu $s = \frac{1}{2}$.

Każdy elektron jest zatem opisany trzema współrzędnymi $\mathbf{r}_i = \begin{bmatrix} x_i \\ y_i \\ z_i \end{bmatrix} \in \mathbb{R}^3$~(z przestrzeni \emph{ konfiguracyjnej}) i jedną współrzędną $\sigma_i \in \mathcal{S}_\frac{1}{2}$~(z przestrzeni \emph{spinowej}), a zatem jest opisany czwórką współrzędnych $\spsp_i = \begin{bmatrix} \mathbf{r}_i \\ \sigma_i \end{bmatrix} \in \mathbb{R}^3 \times \mathcal{S}_\frac{1}{2}$~(z przestrzeni \emph{konfiguracyjno-spinowej}). Po uwzględnieniu spinu funkcja falowa układu $N$ cząstek zależy już od $4N$ współrzędnych:
	\beq
		\psi(\spsp_1, \spsp_2, \ldots, \spsp_N).
	\eeq

\subsection{Postulat o symetrii funkcji falowej}
\label{rz:post_sym}

Kwadrat modułu funkcji falowej układu~$N$ nierozróżnialnych cząstek, zgodnie z interpretacją nadaną przez Borna, jest gęstością prawdopodobieństwa znalezienia układu w danym stanie kwantowym,
	\begin{align}
		\pi(\spsp_1, \ldots, \spsp_N) & =
			\psi^*(\spsp_1, \ldots, \spsp_N)
			\psi(\spsp_1, \ldots, \spsp_N) \\
		 & = |\psi(\spsp_1, \ldots, \spsp_N)|^2.
	\end{align}
Ponieważ cząstki są identyczne, zamiana dowolnych dwóch miejscami (czyli permutacja dwóch cząstek, opisana operatorem~$\hat{\mathscr{P}}_{12}$):
	\beq
		\hat{\mathscr{P}}_{12} \psi(\spsp_1, \spsp_2, \ldots, \spsp_N) =
			\psi(\spsp_2, \spsp_1, \ldots, \spsp_N),
	\eeq
nie zmienia gęstości prawdopodobieństwa:
	\begin{align}
		\pi(\spsp_1, \ldots, \spsp_N) & =
			|\psi(\spsp_1, \spsp_2, \ldots, \spsp_N)|^2 \\
		 & = |\hat{\mathscr{P}}_{12} \psi(\spsp_1, \spsp_2, \ldots, \spsp_N)|^2,
	\end{align}
skąd uzyskujemy
	\beq
		\hat{\mathscr{P}}_{12} \psi(\spsp_1, \spsp_2, \ldots, \spsp_N) =
			e^{i \varphi} \psi(\spsp_1, \spsp_2, \ldots, \spsp_N).
	\eeq
Wynikiem podwójnego działania operatorem permutacji na funkcję falową jest wyjściowa funkcja, więc
	\begin{align}
		\hat{\mathscr{P}}_{12}^2 \psi(\spsp_1, \spsp_2, \ldots, \spsp_N) & =
			e^{2 i \varphi } \psi(\spsp_1, \spsp_2, \ldots, \spsp_N) = \\
		 & = \psi(\spsp_1, \spsp_2, \ldots, \spsp_N).
	\end{align}
A więc
	\beq
		e^{2 \varphi i} = 1
			\Leftrightarrow \varphi = k \pi, \, k \in \mathbb{Z}
			\Rightarrow e^{\varphi i} = \pm 1.
	\eeq
Ostatecznie mamy więc dwie możliwości,
	\beq
		\psi(\spsp_1, \spsp_2, \ldots, \spsp_N) =
			\pm \psi(\spsp_2, \spsp_1, \ldots, \spsp_N),
	\eeq
które są treścią poniższego postulatu o symetrii funkcji falowej.

Funkcja falowa układu $N$ bozonów tego samego rodzaju jest \emph{symetryczna} względem zamiany współrzędnych dwóch cząstek:
	\beq
		\psi(\spsp_1, \spsp_2, \ldots, \spsp_N) =
			\psi(\spsp_2, \spsp_1, \ldots, \spsp_N),
	\eeq
zaś dla układu $N$ fermionów tego samego rodzaju --- jest \emph{antysymetryczna}:
	\beq
		\psi(\spsp_1, \spsp_2, \ldots, \spsp_N) =
			-\psi(\spsp_2, \spsp_1, \ldots, \spsp_N),
		\label{eq:ws_antysym_ferm}
	\eeq

Należy podkreślić, iż symetria funkcji falowej dotyczy cząstek tego samego rodzaju. A więc dla cząsteczki wodoru~\ce{H2}, złożonej z dwóch elektronów opisanych współrzędnymi~$\spsp_1$ i~$\spsp_2$ i dwóch protonów o współrzędnych~$\mathbf{Q}_1$ i~$\mathbf{Q}_2$ \emph{dokładna} funkcja falowa spełnia warunki
	\begin{align}
		\psi(\spsp_1, \spsp_2, \mathbf{Q}_1, \mathbf{Q}_2) & =
			-\psi(\spsp_2, \spsp_1, \mathbf{Q}_1, \mathbf{Q}_2) \\
		 & = -\psi(\spsp_1, \spsp_2, \mathbf{Q}_2, \mathbf{Q}_1) \\
		 & = +\psi(\spsp_2, \spsp_1, \mathbf{Q}_2, \mathbf{Q}_1).
	\end{align}
