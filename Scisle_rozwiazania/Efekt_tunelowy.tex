\section{Efekt tunelowy}

Cząstka o masie $m$ porusza się z energią kinetyczną $E$ w jednym wymiarze wzdłuż osi $x$ od strony ujemnych wartosci $x$ i po drodze napotyka na próg potencjału o wysokosci $V_0$, przedstawiony na rysunku~\ref{fig:pot_eftun}.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Schodek}
	\caption{Bariera potencjału}
	\label{fig:pot_eftun}
	\end{center}
	\end{figure}
Potencjał ten opisany jest równaniem
	\beq
	V(x) =
	\left\{ \begin{array}{l}
	V_0 > 0 \mbox{, } x \in A = \langle 0; L \rangle \\
	0 \mbox{, } x \in A'
	\end{array} \right.,
	\eeq
więc jego wysokość i szerokość wynoszą odpowiednio $V_0$ i $L$.

\subsection{$E < V_0$}

Rozważmy najpierw przypadek, gdy cząstka ma energię mniejszą od wysokości bariery potencjału, $E < V_0$. Klasycznie cząstka powinna odbić się od bariery. W mechanice kwantowej jednak możliwe jest wniknięcie cząstki w obszar bariery, a nawet przejscie przez barierę, czyli tzw. \emph{tunelowanie}! Podzielmy os $x$ na trzy obszary: $X_1 = (-\infty; 0)$, $X_2 = \langle 0; L \rangle$ i $X_3 = (L; \infty)$. Równanie Schrödingera w obszarach $X_1$ i $X_2$ ma postać taką, jak dla cząstki swobodnej, więc ma takie same rozwiązania. Ponieważ jednak w obszarze $X_3$ cząstka może znależć się jedynie na skutek tunelowania przez schodek potencjału, może tam poruszać się wyłącznie w prawo, zatem częsć funkcji~(\ref{eq:rs_czsw_rozw}) opisująca cząstkę poruszającą się w prawo nie występuje w funkcji falowej dla obszaru $X_3$. Z kolei w obszarze $X_2$ równanie Schrödingera ma postać
	\beq
	\left( -\frac{\hbar^2}{2 m} \frac{d^2}{d x^2} + V_0 \right) \psi_2(x) = E \psi_2(x),
	\label{eq:ur_rSX2_a}
	\eeq
co można zapisać jako
	\beq
	\frac{d^2 \psi_2(x)}{d x^2} = \kappa^2 \psi_2(x).
	\eeq
Zatem funkcje falowe dla obszarów $X_1$, $X_2$ i $X_3$ mają odpowiednio postacie
	\beq
	\left\{ \begin{array}{l}
	\psi_1(x) = A_1 e^{i k x} + B_1 e^{-i k x} \\
	\psi_2(x) = A_2 e^{\kappa x} + B_2 e^{-\kappa x} \\
	\psi_3(x) = A_3 e^{i k x}
	\end{array} \right.,
	\label{eq:ur_ff}
	\eeq
gdzie
	\beq
	\left\{ \begin{array}{l}
	k = \frac{\sqrt{2 m E}}{\hbar} \\
	\kappa = \frac{\sqrt{2 m (V_0 - E)}}{\hbar}
	\end{array} \right..
	\eeq
Zauważmy, że
	\beq
	\psi_2(x) \xrightarrow{L \to \infty} \infty
	\eeq
z uwagi na pierwszy człon funkcji~$\psi_2(x)$, jednakże w przypadku bariery o skończonej długości człon ten jest fizyczny.

Funkcja falowa cząstki oraz jej pierwsza pochodna muszą być ciągłe w całej dziedzinie (czyli w $\mathbb{R}$), w szczególnosci zas --- na granicach obszarów. Warunki te, czyli warunki zszycia, mają postać:
	\beq
	\left\{ \begin{array}{l}
	\psi_1(0) = \psi_2(0) \\
	\psi'_1(0) = \psi'_2(0)
	\end{array} \right.,
	\label{eq:ur_warcp0}
	\eeq
oraz
	\beq
	\left\{ \begin{array}{l}
	\psi_2(L) = \psi_3(L) \\
	\psi'_2(L) = \psi'_3(L)
	\end{array} \right..
	\label{eq:ur_warcpL}
	\eeq
Z~\eqref{eq:ur_warcp0} otrzymujemy
	\beq
	\left\{ \begin{array}{l}
	A_1 + B_1 = A_2 + B_2 \\
	A_1 - B_1 = -\frac{i \kappa}{k} (A_2 - B_2)
	\end{array} \right.,
	\eeq
więc
	\beq
	\left\{ \begin{array}{l}
	A_1 = \frac{1}{2} \left( A_2 + B_2 - \frac{i \kappa}{k} (A_2 - B_2) \right) \\
	B_1 = \frac{1}{2} \left( A_2 + B_2 + \frac{i \kappa}{k} (A_2 - B_2) \right)
	\end{array} \right.,
	\label{eq:ur_A1B1}
	\eeq
zaś z~\eqref{eq:ur_warcpL} mamy
	\beq
	\left\{ \begin{array}{l}
	A_2 e^{\kappa L} + B_2 e^{-\kappa L} = A_3 e^{i k L} \\
	A_2 e^{\kappa L} - B_2 e^{-\kappa L} = \frac{i k }{\kappa} A_3 e^{i k L}
	\end{array} \right.,
	\eeq
skąd
	\beq
	\left\{ \begin{array}{l}
	A_2 = \frac{1}{2} \left( 1 + \frac{i k}{\kappa} \right) A_3 e^{(i k - \kappa) L} \\
	B_2 = \frac{1}{2} \left( 1 - \frac{i k}{\kappa} \right) A_3 e^{(i k + \kappa) L} \\
	\end{array} \right..
	\label{eq:ur_A2B2}
	\eeq
Łącznie mamy więc cztery równania:~\eqref{eq:ur_A1B1} i~\eqref{eq:ur_A2B2} i pięć niewiadomych. Prawdopodonieństwo, że w obszarze $X_1$ cząstka porusza się w kierunku bariery jest proporcjonalne do $|A_1|^2$, a prawdopodonieństwo, że w obszarze $X_3$ cząstka oddala się od bariery --- do $|A_3|^2$ (wynika to z postaci funkcji falowej; w obszarze $X_1$ jej składowa opisująca ruch cząstki w kierunku bariery to $A_1 e^{i k x}$, stą prawdopodonieństwo wystąpienia cząstki w tym stanie jest proporcjonalne do $|A_1|^2$; podobnie jest w obszarze $X_3$. Tak więc wielkosć
	\beq
	T = \left| \frac{A_3}{A_1} \right|^2
	\eeq
jest prawdopodobieństwem przejscia (\emph{transmisji}) cząstki przez barierę potencjału. Z postaci równań~\eqref{eq:ur_A1B1} i~\eqref{eq:ur_A2B2} wynika, że łatwiej nam będzie wyznaczyć odwrotny stosunek, tj.~$\frac{A_1}{A_3}$. Oznaczmy teraz odpowiednimi małymi literami stosunki współczynników do $A_3$, więc np.
	\beq
		b_1 = \frac{B_1}{A_3},
	\eeq
itp., przy czym oczywiście $a_3 = 1$ i
	\beq
	T = \frac{1}{|a_1|^2}.
	\eeq
Stosując powyższą konwencję, z warunków~\eqref{eq:ur_A1B1} i~\eqref{eq:ur_A2B2} uzyskujemy
	\beq
	a_1 =
		\Bigg(
			\cosh{\kappa L} + \frac{i}{2} \bigg( \frac{\kappa}{k} - \frac{k}{\kappa} \bigg) \sinh{\kappa L}
		\Bigg)
	e^{i k L}.
	\label{eq:ur_a1}
	\eeq
Zatem
	\beq
	|a_1|^2 = a_1 a_1^* = \cosh^2{\kappa L} + \frac{1}{4} \left( \frac{\kappa}{k} - \frac{k}{\kappa} \right)^2 \sinh^2{\kappa L}.
	\eeq
Wprowadzając wielkość
	\beq
	\alpha = \frac{E}{V_0}
	\eeq
oraz korzystając z jedynki hiperbolicznej,
	\beq
	\cosh^2{x} - \sinh^2{x} = 1,
	\eeq
uzyskujemy
	\beq
	|a_1|^2 = 1 + \frac{\sinh^2{\kappa L}}{4 \alpha (1 - \alpha)}.
	\eeq
Wprowadzając dodatkowo bezwymiarową wielkość
	\beq
	\beta = \frac{\sqrt{2 m V_0} L}{\hbar},
	\eeq
otrzymujemy ostatecznie,
	\beq
	\boxed{T = \left( 1 + \frac{\sinh^2{\sqrt{1 - \alpha} \beta}}{4 \alpha (1 - \alpha)} \right)^{-1}}.
	\label{eq:ur_T_ElV0}
	\eeq
Obliczymy teraz współczynnik prawdopodobieństwa odbicia się cząstki od bariery potencjału. Ponieważ prawdopodobieństwo oddalania się cząstki od bariery w obszarze~$X_1$ jest proporcjonalne do~$|B_1|^2$, stąd współczynnik odbicia dany jest wzorem
	\beq
	R = \left| \frac{B_1}{A_1} \right|^2.
	\label{eq:ur_refl}
	\eeq
Z warunków~\eqref{eq:ur_A1B1} i~\eqref{eq:ur_A2B2} uzyskujemy
	\beq
	\frac{B_1}{A_1} = \frac{(k^2 + \kappa^2) \sinh{\kappa L}}{(k^2 - \kappa^2) \sinh{\kappa L} + 2 i k \kappa \cosh{\kappa L}},
	\eeq
i, jak poprzednio, wygodniej będzie nam wyznaczyć odwrotną wielkość:
	\begin{eqnarray}
	\frac{A_1}{B_1} & = & \frac{k^2 - \kappa^2}{k^2 + \kappa^2} + \frac{2 i k \kappa}{k^2 + \kappa^2} \coth{\kappa L} = \\
	 & = & 2 \alpha - 1 + 2 i \sqrt{\alpha (1 - \alpha)} \coth{\kappa L}. \label{eq:ur_A1B1_inv}
	\end{eqnarray}
Ostatecznie,
	\beq
	\boxed{R = \left( 1 + \frac{4 \alpha (1 - \alpha)}{\sinh^2{\sqrt{1 - \alpha} \beta}} \right)^{-1}}.
	\eeq

Na zakończenie sprawdzimy jeszcze, czy cząstka nie zatrzymuje się wewnątrz bariery. Prawdopodobieństwo ugrzęźnięcia w barierze wynosi oczywiście
	\beq
	S = 1 - (T + R).
	\eeq
Nietrudno zauważyć, że
	\beq
	\big( 1 + u \big)^{-1} + \left( 1 + u^{-1} \right)^{-1} \equiv 1,
	\eeq
skąd natychmiast dostajemy
	\beq
	S = 0.
	\eeq
Tak więc cząstka nie zatrzymuje się w barierze.

Wykreślimy jeszcze zależność funkcji falowej od położenia cząstki dla rozpatrywanego przypadku. Ponieważ nie mamy do czynienia ze stanem związanym, funkcja falowa nie znika w nieskończoności i nie możemy jej unormować. Przyjmijmy arbitralnie
	\beq
	A_1 \equiv 1.
	\eeq
Do wykreślenia potrzebne nam będą wielkości
	\beq
	\begin{cases}
	\frac{k}{\kappa} = \sqrt{\frac{\alpha}{1 - \alpha}} \\
	k L = \sqrt{\alpha} \beta \\
	\kappa L = \sqrt{1 - \alpha} \beta \\
	k x = \sqrt{\alpha} \tilde{x} \\
	\kappa x = \sqrt{1 - \alpha} \tilde{x} \\
	\tilde{x} = \frac{x}{L}
	\end{cases}.
	\eeq
Z zależności~\eqref{eq:ur_A1B1_inv} dostajemy
	\beq
	B_1 = \left( 2 \alpha - 1 + 2 i \sqrt{\alpha (1 - \alpha)} \coth{\sqrt{1 - \alpha} \beta} \right)^{-1}.
	\eeq
Z równania~\eqref{eq:ur_a1} otrzymujemy
	\beq
	A_3 =
		\Bigg(
		\cosh{\sqrt{(1 - \alpha)} \beta} +
		\frac{i}{2} \left( \sqrt{\frac{1 - \alpha}{\alpha}} - \sqrt{\frac{\alpha}{1 - \alpha}} \right)
		\sinh{\sqrt{(1 - \alpha)} \beta}
		\Bigg)^{-1} e^{-i \sqrt{\alpha} \beta}
	\eeq
Współczynniki funkcji falowej w obszarze~$X_2$ mają postać~\eqref{eq:ur_A2B2}:
	\beq
	\left\{ \begin{array}{l}
	A_2 = \frac{1}{2} \left( 1 + i \sqrt{\frac{\alpha}{1 - \alpha}} \right) A_3 e^{i \sqrt{\alpha} \beta} e^{-\sqrt{1 - \alpha} \beta}  \\
	B_2 = \frac{1}{2} \left( 1 - i \sqrt{\frac{\alpha}{1 - \alpha}} \right) A_3 e^{i \sqrt{\alpha} \beta} e^{\sqrt{1 - \alpha} \beta}
	\end{array} \right.,
	\eeq
Wykres części rzeczywistej i zespolonej funkcji falowej przedstawiono na rys.~\ref{fig:ur_psi_ReIm}, zaś wykres kwadratu modułu funkcji falowej --- na rys.~\ref{fig:ur_psi_abs2}
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/wavefunction_re_im}
	\caption{Część rzeczywista i zespolona funkcji falowej elektronu padającego na barierę o wymiarach~$V_0 = 1$~eV i~$L = 10$~\AA.}
	\label{fig:ur_psi_ReIm}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/wavefunction}
	\caption{Kwadrat modułu funkcji falowej elektronu padającego na barierę o wymiarach~$V_0 = 1$~eV i~$L = 10$~\AA. Dla $x > L$,~$|\psi(x)|^2 = |A_3|^2.$}
	\label{fig:ur_psi_abs2}
	\end{center}
	\end{figure}

\subsection{$E > V_0$}

Rozważmy teraz przypadek~$E > V_0$. Rozwiązania równania Schrödingera dla tego przypadku mają postać~\eqref{eq:ur_ff} z tym, że
	\beq
	\mathbb{C} \ni \kappa = \frac{\sqrt{2 m (E - V_0)}}{\hbar} = \frac{\sqrt{-2 m (V_0 - E)}}{\hbar} = \sqrt{-1} \frac{\sqrt{2 m (V_0 - E)}}{\hbar} = i \tilde{\kappa} \text{, } \tilde{\kappa} \in \mathbb{R}.
	\eeq
Wyrażenie~\eqref{eq:ur_T_ElV0} jest więc słuszne także dla rozpatrywanego przypadku, ale wygodniej zapisać je korzystając z wielkości rzeczywistych. Korzystając z zalezności
	\beq
	\sinh{i x} = i \sin{x},
	\eeq
uzyskujemy
	\beq
	\boxed{T = \left( 1 + \frac{\sin^2{\sqrt{\alpha - 1} \beta}}{4 \alpha (\alpha - 1)} \right)^{-1}}.
	\label{eq:ur_T_ElV0_fin}
	\eeq

\subsection{$E = V_0$}

Do rozpatrzenia pozostaje jeszcze szczególny przypadek~$E = V_0$. Dla tej sytuacji funkcja falowa w obszarze~$X_2$ spełnia równanie
	\beq
	\psi_2''(x) = 0,
	\eeq
a więc
	\beq
	\psi_2(x) = A_2 x + B_2.
	\eeq
Warunki zszycia~\eqref{eq:ur_warcp0} prowadzą do zależności
	\beq
	\left\{ \begin{array}{l}
	A_1 + B_1 = B_2 \\
	A_1 - B_1 = -\frac{i}{k} A_2
	\end{array} \right.,
	\eeq
skąd
	\beq
	\left\{ \begin{array}{l}
	A_1 = \frac{1}{2} \left( B_2 - \frac{i}{k} A_2 \right) \\
	B_1 = \frac{1}{2} \left( B_2 + \frac{i}{k} A_2 \right)
	\end{array} \right.,
	\label{eq:ur_warcp0_rez}
	\eeq
z kolei z warunków~\eqref{eq:ur_warcpL} uzyskujemy
	\beq
	\left\{ \begin{array}{l}
	A_2 L + B_2 = A_3 e^{i k L} \\
	A_2 = i k A_3 e^{i k L}
	\end{array} \right.,
	\label{eq:ur_warcpL_rez1}
	\eeq
skąd
	\beq
	B_2 = (1 - i k L) A_3 e^{i k L}.
	\label{eq:ur_warcpL_rez2}
	\eeq
Stosując konwencję taką, jak w poprzednich przypadkach, z zależności~\eqref{eq:ur_warcp0_rez}, \eqref{eq:ur_warcpL_rez1} \eqref{eq:ur_warcpL_rez2} dostajemy
	\beq
	a_1 = \left( 1 - \frac{i k L}{2} \right) e^{i k L},
	\eeq
i ostatecznie
	\beq
	\boxed{T = \frac{4}{\beta^2 + 4}}.
	\eeq
Korzystając z~\eqref{eq:ur_refl}, \eqref{eq:ur_warcp0_rez} oraz~\eqref{eq:ur_warcpL_rez1} uzyskujemy wyrażenie na współczynnik odbicia dla tego przypadku,
	\beq
	\boxed{R = \frac{\beta^2}{\beta^2 + 4}},
	\eeq
zatem ponownie współczynnik grzęźnięcia w barierze jest zerowy,~$S = 0$.

Podsumowując, zależność współczynnika przejścia od stosunku energii do wyskości bariery jest dana funkcją
	\beq
	\boxed{T(\alpha) =
	\begin{cases}
		\left( 1 + \frac{\sinh^2{\sqrt{1 - \alpha} \beta}}{4 \alpha (1 - \alpha)} \right)^{-1}, &
			\, \alpha \in (0; 1) \\
		\frac{4}{\beta^2 + 4}, &
			\, \alpha = 1 \\
		\left( 1 + \frac{\sin^2{\sqrt{\alpha - 1} \beta}}{4 \alpha (\alpha - 1)} \right)^{-1}, &
			\, \alpha > 1
	\end{cases}}.
	\eeq

Na rysunku~\ref{fig:ur_TR} przedstawiono przykładową zależność współczynników przejścia i odbicia w zależności od energii padającej cząstki.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/coefficients}
	\caption{Współczynniki przejścia i odbicia dla elektronu padającego na barierę o wymiarach~$V_0 = 1$~eV i~$L = 10$~\AA.}
	\label{fig:ur_TR}
	\end{center}
	\end{figure}
