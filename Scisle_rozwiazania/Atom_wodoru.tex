\section{Atom wodoru i jon wodoropodobny}

\subsection{Zagadnienie własne i liczby kwantowe}
\label{ch:jn_wod_zag}

Hamiltonian dla atomu wodoru bądź jonu wodoropodobnego, czyli układu złożonego z elektronu o masie $m_1$, opisanego wektorem wodzącym $\mathbf{r}_1$, i punktowego jądra o ładunku $Z e$ o masie $m_2$, opisanego wektorem wodzącym $\mathbf{r}_2$, składa się z operatora energii kinetycznej układu [równanie~(\ref{eq:op_en_kin_dwmas})] oraz operatora energii potencjalnej:
	\beq
	\hat{\mathcal{H}} = -\frac{\hbar^2}{2 m_1} \Delta_{\mathbf{r}_1}
	-\frac{\hbar^2}{2 m_2} \Delta_{\mathbf{r}_2} + \hat{V},
	\eeq
gdzie
	\beq
	\hat{V} = -\frac{Z e^2}{4 \pi \epsilon_0 |\mathbf{r}_1 - \mathbf{r}_1|}.
	\label{eq:pot_jn_wod}
	\eeq
Dokonując separacji translacji tak, jak zrobiliśmy to w rozdziale~\ref{rz:sep_trans} i korzystając z zależności
	\beq
	r = |\mathbf{r}_1 - \mathbf{r}_1|,
	\eeq
możemy zapisać hamiltonian w układzie środka mas:
	\beq
	\hat{H} = -\frac{\hbar^2}{2 \mu} \Delta_\mathbf{r} - \frac{Z e^2}{4 \pi \epsilon_0 r}.
	\label{eq:ham_jn_wod}
	\eeq
Ponieważ potencjał~(\ref{eq:pot_jn_wod}) jest sferycznie symetryczny, dogodnie jest przedstawić hamiltonian~(\ref{eq:ham_jn_wod}) we współrzędnych sferycznych. Łącząc~(\ref{eq:lapl_sfer}) i~(\ref{eq:ham_jn_wod}) otrzymujemy
	\beq
	\hat{H} = -\frac{\hbar^2}{2 \mu r^2} \left[
	\frac{\partial}{\partial r} \left( r^2 \frac{\partial}{\partial r} \right) +
	\frac{1}{\sin{\theta}} \frac{\partial}{\partial \theta} \left( \sin{\theta} \frac{\partial}{\partial \theta} \right) +
	\frac{1}{\sin^2{\theta}} \frac{\partial^2}{\partial \phi^2} \right] -
	\frac{Z e^2}{4 \pi \epsilon_0 r}.
	\label{eq:ham_clk_jn_wod}
	\eeq
Porównując uzyskane wyrażenie z wyrażeniem~(\ref{eq:wsp_sf_l2}) łatwo zauważyć, że hamiltonian daje się przedstawić w postaci
	\beq
	\hat{H} = -\frac{\hbar^2}{2 \mu r^2} \frac{\partial}{\partial r}
	\left( r^2 \frac{\partial}{\partial r} \right) +
	\frac{\hat{l}^2}{2 \mu r^2} - \frac{Z e^2}{4 \pi \epsilon_0 r},
	\label{eq:ham_jn_wod_l2}
	\eeq
gdzie $\hat{l}^2$ jest operatorem kwadratu momentu pędu.
	\begin{task}
	Obliczyć komutatory~$\comm{\hat{H}}{\hat{l}^2}$ i $\comm{\hat{H}}{\hat{l}_z}$.
	\end{task}
	\begin{sol}
	Aby obliczyć powyższe komutatory, wystarczy przyjrzeć się wyrażeniom opisującym $\hat{H}$~[(\ref{eq:ham_jn_wod_l2})], $\hat{l}^2$~[(\ref{eq:wsp_sf_l2})] i $\hat{l}_z$~[(\ref{eq:wsp_sf_lxyz})]. Zauważamy, że w wyrażeniu opisującym $\hat{H}$ współrzędne sferyczne $(\theta; \phi)$ występują tylko w części zawierającej operator $\hat{l}^2$, zaś w wyrażeniach opisujących $\hat{l}^2$ i $\hat{l}_z$ nie występuje zależność od $r$. Wiedząc ponadto, że operatory $\hat{l}^2$ i $\hat{l}_z$ komutują, od razu stwierdzamy, że pary operatorów $\hat{H}$, $\hat{l}^2$ oraz $\hat{H}$, $\hat{l}_z$ są przemienne, zatem
	\beq
	\comm{\hat{H}}{\hat{l}^2} = 0
	\label{eq:kom_H_l2}
	\eeq
	i
	\beq
	\comm{\hat{H}}{\hat{l}_z} = 0.
	\label{eq:kom_H_lz}
	\eeq
	\end{sol}
Jak wynika z zależności~(\ref{eq:kom_H_l2}) i~(\ref{eq:kom_H_lz}) możemy stwierdzić, że rozwiązania równania Schrödingera
	\beq
	\hat{H} \psi(r; \theta; \phi) = E \psi(r; \theta; \phi)
	\label{eq:rs_jn_wod}
	\eeq
są także funkcjami własnymi operatorów $\hat{l}^2$ i $\hat{l}_z$. Układ funkcji własnych tych operatorów już znamy z rozdziału~\ref{ff_mompd} --- to harmoniki sferyczne. Całkowita funkcja falowa spełniająca równanie~(\ref{eq:rs_jn_wod}) musi więc być iloczynem harmonik sferycznych i jakiejś funkcji zależnej od $r$. Jeśli ponadto poziomy energetyczne ponumerujemy indeksem $n$, szukaną funkcję falową można zapisać w postaci iloczynu części radialnej i części kątowej:
	\beq
	\psi_{nlm}(r; \theta; \phi) = R_{nlm}(r) \sh{l}{m}.
	\label{eq:sep_ff_jn_wod}
	\eeq
Oczywiście zachodzi
	\beq
	\left\{ \begin{array}{l}
	\hat{l}^2 \psi_{nlm}(r; \theta; \phi) = l (l + 1) \hbar^2 \psi_{nlm}(r; \theta; \phi) \\
	\hat{l}_z \psi_{nlm}(r; \theta; \phi) = m \hbar \psi_{nlm}(r; \theta; \phi)
	\end{array} \right.,
	\eeq
zatem hamiltonian układu można zapisać w postaci
	\beq
	\hat{H} = -\frac{\hbar^2}{2 \mu r^2} \left[
	\frac{\partial}{\partial r} \left( r^2 \frac{\partial}{\partial r} \right) -
	l (l + 1) \right] - \frac{Z e^2}{4 \pi \epsilon_0 r}.
	\label{eq:ham_zl}
	\eeq
Podstawiając do równania~(\ref{eq:rs_jn_wod}) hamiltonian~(\ref{eq:ham_zl}) i funkcję~(\ref{eq:sep_ff_jn_wod}) i podzieleniu całości obustronnie przez $\sh{l}{m}$ (możemy to zrobi{\.c}, bo hamiltonian nie zależy od kątów sferycznych) otrzymujemy równanie różniczkowe pozwalające wyznaczyć funkcję radialną:
	\beq
	\left\{ -\frac{\hbar^2}{2 \mu r^2} \left[
	\frac{\partial}{\partial r} \left( r^2 \frac{\partial}{\partial r} \right) -
	l (l + 1) \right] - \frac{Z e^2}{4 \pi \epsilon_0 r} \right\} R_{nl}(r) =
	E_n R_{nl}(r),
	\label{eq:rr_czrad}
	\eeq
przy czym w opisie funkcji radialnej opuściliśmy indeks $m$, bowiem w części operatorowej równania~(\ref{eq:rr_czrad}) liczba ta nie występuje. Rozwiązania równania~(\ref{eq:rr_czrad}) dla stanów związanych mają postać
	\beq
	R_{nl}(r) = N_{nl} r^l L_{n + l}^{2 l + 1} \left( \frac{2 Z r}{n a_0} \right) e^{-\frac{Z r}{n a_0}},
	\eeq
gdzie
	\beq
	a_0 = \frac{4 \pi \epsilon_0 \hbar^2}{\mu e^2}
	\label{eq:a0}
	\eeq
jest promieniem pierwszej orbity Bohra,
	\beq
	L_n^m(x) = \frac{d^m L_n(x)}{d x^m}
	\label{eq:st_wiel_lag}
	\eeq
jest \emph{stowarzyszonym wielomianem Laguerre'a}, zaś
	\beq
	L_n(x) = e^x \frac{d^n (x^n e^{-x})}{d x^n}
	\label{eq:zw_wiel_lag}
	\eeq
jest \emph{zwykłym wielomianem Laguerre'a}. Poziomy energetyczne dla stanów związanych są skwantowane:
	\beq
	\boxed{E_n = -\frac{Z^2 \mu e^4}{8 n^2 \epsilon_0^2 h^2}}.
	\label{eq:en_jn_wod}
	\eeq
Funkcje falowe atomu wodoru oznacza się czasem w uproszczony sposób jako $\ket{n l m}$. W tabeli~\ref{tab:ff_jn_wod} przedstawiono jawne postacie kilku funkcji falowych $\psi_{nlm}(r; \theta; \phi)$, zaś na rysunku~\ref{fig:H_en} --- schemat poziomów energetycznych atomu wodoru.
	\begin{table}[htbp]
	\caption{Postać funkcji falowych jonu wodoropodobnego dla kilku wartości $n$, $l$ i $m$}
	\begin{center}
	\begin{tabu} spread 0pt {*{3}{D{.}{.}{0}}X[$$]}
	\toprule
	n & l & m & \psi_{nlm}(r, \theta, \phi) \\
	\toprule
	1 & 0 & 0 & \frac{1}{\sqrt{\pi}} \left( \frac{Z}{a_0} \right)^\frac{3}{2} e^{-\frac{Z r}{a_0}} \\
	\midrule
	\multirow{3}{*}{2} & 1 & \pm 1 & \frac{1}{8 \sqrt{\pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} e^{-\frac{Z r}{2 a_0}} r \sin{\theta} e^{\pm i \phi} \\
	 & 1 & 0 & \frac{1}{4 \sqrt{2 \pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} r e^{-\frac{Z r}{2 a_0}} \cos{\theta} \\
	 & 0 & 0 & \frac{1}{4 \sqrt{2 \pi}} \left( \frac{Z}{a_0} \right)^\frac{3}{2} \left( 2 - \frac{Z r}{a_0} \right)e^{-\frac{Z r}{a_0}} \\
	\midrule
	\multirow{6}{*}{3} & 2 & \pm 2 & \frac{1}{162 \sqrt{\pi}} \left( \frac{Z}{a_0} \right)^\frac{7}{2} e^{-\frac{Z r}{3 a_0}} r^2 \sin^2{\theta} e^{\pm 2 i \phi} \\
	 & 2 & \pm 1 & \frac{1}{81 \sqrt{\pi}} \left( \frac{Z}{a_0} \right)^\frac{7}{2} e^{-\frac{Z r}{3 a_0}} r^2 \sin{\theta} \cos{\theta} e^{\pm i \phi} \\
	 & 2 & 0 & \frac{1}{81 \sqrt{6 \pi}} \left( \frac{Z}{a_0} \right)^\frac{7}{2} e^{-\frac{Z r}{3 a_0}} r^2 (3 \cos^2{\theta} - 1) \\
	 & 1 & \pm 1 & \frac{1}{162 \sqrt{\pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} e^{-\frac{Z r}{3 a_0}} \left( 6 - \frac{Z r}{a_0} \right) r \sin{\theta} e^{\pm i \phi} \\
	 & 1 & 0 & \frac{1}{81 \sqrt{2 \pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} e^{-\frac{Z r}{3 a_0}} \left( 6 - \frac{Z r}{a_0} \right) r \cos{\theta} \\
	 & 0 & 0 & \frac{1}{81 \sqrt{3 \pi}} \left( \frac{Z}{a_0} \right)^\frac{3}{2} e^{-\frac{Z r}{3 a_0}} \left( 27 - \frac{18 Z r}{a_0} + 2 \left( \frac{Z r}{a_0} \right)^2 \right) \\
	\bottomrule
	\end{tabu}
	\end{center}
	\label{tab:ff_jn_wod}
	\end{table}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/H_energ}
	\caption{Schemat poziomów energetycznych atomu wodoru}
	\label{fig:H_en}
	\end{center}
	\end{figure}
Indeksy $n$, $l$ i $m$ pojawiające się w wyrażeniach na funkcje falowe to liczby kwantowe, odpowiednio: \emph{główna}, \emph{poboczna} i \emph{magnetyczna}. Nie mogą one przyjmować dowolnych wartości, a tylko ściśle określone:
	\beq
	\left\{ \begin{array}{l}
	n \in \mathbb{N} \\
	l \in \langle 0; n - 1 \rangle \cap \mathbb{Z} \\
	m \in \langle -l; l \rangle \cap \mathbb{Z}
	\end{array} \right..
	\label{eq:jn_wod_lkw_war}
	\eeq
Ograniczenie możliwych wartości $l$ wynika bezpośrednio z postaci wielomianów Laguerre'a. Z równań~(\ref{eq:st_wiel_lag}) i~(\ref{eq:zw_wiel_lag}) wynika, że $L_n(x)$ i $L_n^m(x)$ są wielomianami stopni odpowiednio $n$ i $n - m$, zatem wielomian $L_n^m(x)$ nie znika dla $m \le n$. W przypadku funkcji radialnej warunek ten przyjmuje postać $2 l + 1 \le n + l$, czyli $l \le n - 1$. Warunek ten można otrzymać też w nieco odmienny sposób. Oczywiste jest, że radialna energia kinetyczna\footnote{Jest to energia związana ze zmianą w czasie odległości elektronu od jądra.} nie może być ujemna, więc to samo stwierdzenie jest prawdziwe także dla jej wartości średniej. W ogólności energia elektronu znajdującego się w odległości $r$ od jądra jest sumą radialnej energii kinetycznej $T_\mathrm{rad}$, energii kinetycznej rotacji
	\beq
	T_\mathrm{rot} = \frac{|\mathbf{l}|}{2 I} = \frac{l (l + 1) \hbar^2}{2 m r^2},
	\eeq
gdzie $|\mathbf{l}|$ oznacza długość wektora momentu pędu elektronu i energii potencjalnej
	\beq
	V = -\frac{Z e^2}{4 \pi \epsilon_0 r}.
	\eeq
Wkład dwóch ostatnich udziałów do energii elektronu wynosi
	\beq
	U(r) = \frac{l (l + 1) \hbar^2}{2 m r^2} - \frac{Z e^2}{4 \pi \epsilon_0 r}.
	\label{eq:potef}
	\eeq
Warto zauważyć, że $U(r)$ jest też \emph{potencjałem efektywnym} występującym w hamiltonianie~(\ref{eq:ham_zl}) z tą różnicą, że w wyrażeniu na $U(r)$ występuje masa elektronu $m$ zamiast masy zredukowanej $\mu$ układu elektron-proton, ale liczby te są sobie niemalże równe z uwagi na to, że masa protonu jest znacznie większa od masy elektronu.\footnote{W dalszej części będziemy zakładać $m = \mu$.}
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/ekinrad_epot.eps}
	\caption{Postać funkcji $U(r)$ dla kilku wartości liczby kwantowej $l$}
	\label{fig:H_potef}
	\end{center}
	\end{figure}
	\begin{task}
	Wyznaczyć minimum funkcji $U(r)$.
	\end{task}
	\begin{sol}
	Obliczamy pierwszą i drugą pochodną funkcji $U(r)$:
	\beq
	U'(r) = -\frac{l (l + 1) \hbar^2}{m r^3} + \frac{Z e^2}{4 \pi \epsilon_0 r^2}
	\eeq
	i
	\beq
	U''(r) = \frac{3 l (l + 1) \hbar^2}{m r^4} - \frac{Z e^2}{2 \pi \epsilon_0 r^3}.
	\eeq
	Odległość, dla której funkcja $U(r)$ ma minimum, wyznaczamy z warunków $U'(r_\mathrm{min}) = 0$ i $U''(r_\mathrm{min}) < 0$. Otrzymujemy
	\beq
	r_{min} = \frac{4 l (l + 1) \pi \epsilon_0 \hbar^2}{Z m e^2}.
	\eeq
	Zakładając $m = \mu$ i korzystając ze wzorów~(\ref{eq:potef}) i~(\ref{eq:en_jn_wod}) otrzymujemy
	\beq
	U_\mathrm{min} = U(r_\mathrm{min}) = -\frac{Z^2 m e^4}{8 l (l + 1) \epsilon_0^2 h^2} = \frac{E_1}{l (l + 1)}.
	\eeq
	\end{sol}
Co oczywiste, wartość średnia potencjału efektywnego musi być większa od jego najmniejszej wartości. Stąd dla energii całkowitej elektronu zachodzi
	\beq
	E_n = \ev**{T_\mathrm{rad}} + \ev**{U} \ge \ev**{T_\mathrm{rad}} + U_\mathrm{min},
	\eeq
zatem
	\beq
	\ev**{T_\mathrm{rad}} \in \langle 0; E_n - U_\mathrm{min} \rangle \Rightarrow
	E_n - U_\mathrm{min} = \frac{E_1}{n^2} - \frac{E_1}{l (l + 1)} \ge 0.
	\eeq
Ponieważ $E_1 < 0$, otrzymujemy
	\beq
	\frac{1}{n^2} - \frac{1}{l (l + 1)} \le 0,
	\eeq
co prowadzi do warunku $l (l + 1) \le n^2$, który w zbiorze $\mathbb{N} \cup \{0\}$ jest spełniony dla $l \le n - 1$.

Ponieważ energia zależna jest tylko od liczby kwantowej $n$, jest ona zdegenerowana.
	\begin{task}
	Wyznaczyć stopień degeneracji energii dla danego $n$.
	\end{task}
	\begin{sol}
	Jak wynika z warunków~(\ref{eq:jn_wod_lkw_war}), dla danego $n$ liczba kwantowa $l$ przyjmuje $n$ wartości: $l \in \{0; 1; \ldots; n - 1\}$, a dla danego $l$ liczba kwantowa $m$ przyjmuje $2 l + 1$ wartości: $m \in \{-l; -l + 1; \ldots; l - 1; l\}$. Stopień degeneracji energii wynosi zatem
	\beq
	d_n = \sum_{l = 0}^{n - 1} (2 l + 1).
	\label{eq:jn_wod_dn_sum}
	\eeq
	Zauważmy, że wyrażenie~(\ref{eq:jn_wod_dn_sum}) jest sumą $n$ pierwszych wyrazów ciągu arytmetycznego $(1; 3; 5; 7; \ldots)$, a więc opisanego przez liczby
	\beq
	\left\{ \begin{array}{l}
	a_1 = 1 \\
	d = a_{k + 1} - a_k = 2
	\end{array} \right..
	\eeq
	$n$-ty wyraz tego ciągu wynosi
	\beq
	a_n = a_1 + d (n - 1) = 2 n - 1,
	\eeq
	a suma $n$ jego początkowych wyrazów
	\beq
	S_n = \frac{n (a_1 + a_n)}{2} = n^2,
	\eeq
	więc
	\beq
	d_n = n^2.
	\eeq
	\end{sol}

\subsection{Orbitale}
\label{ch:jn_wod_orb}

Funkcje falowe $\psi_{nlm}$ to \emph{orbitale}, oznaczane zazwyczaj symbolem
\begin{equation}
	n\gamma(l)_m,
	\label{eq:fkxu}
\end{equation}
gdzie $\gamma(l)$ jest tradycyjnym oznaczeniem pobocznej liczby kwantowej~$l$~(tab.~\ref{tab:jwjt}).
\begin{table}[h]
	\centering
	\caption{Oznaczenie liczby~$l$ w symbolu orbitalu}
	\begin{tabular}{r|*{6}{l}}
		\toprule
		$l$ & $0$ & $1$ & $2$ & $3$ & $4$ & \ldots \\
		\midrule
		$\gamma(l)$ & s & p & d & f & g & \ldots \\
		\bottomrule
	\end{tabular}
	\label{tab:jwjt}
\end{table}
W przypadku $l = 0$ pomija się liczbę $m$. Dla przejrzystości notacji w dalszej części ogólne symboliczne oznaczenia orbitali~\eqref{eq:fkxu} będziemy podawać w kecie:
\begin{equation}
	n\gamma(l)_m \equiv \ket{n\gamma(l)_m},
\end{equation}
oznaczenia szczegółowe, jak~$2\mathrm{p}_x$, pozostawiając na wolności. W~tab.~\ref{tab:sugj} podano przykłady symbolicznych oznaczeń orbitali.
\begin{table}[h!]
	\centering
	\caption{Przykłady symbolicznych oznaczeń orbitali}
	\begin{tabular}{*{2}{>{$}l<{$}}}
		\toprule
		\psi_{nlm} & n\gamma(l)_m \\
		\midrule
		\psi_{100} & 1\mathrm{s} \\
		\psi_{211} & 2\mathrm{p}_1 \\
		\psi_{32-1} & 3\mathrm{d}_{-1} \\
		\psi_{432} & 4\mathrm{f}_2 \\
		\bottomrule
	\end{tabular}
	\label{tab:sugj}
\end{table}

Dla $m \not= 0$ orbitale są funkcjami zespolonymi. Można jednakże stworzyć z nich orbitale rzeczywiste --- jako kombinacje liniowe orbitali zespolonych. Przykładami takich kombinacji są orbitale:
	\beq
	2\mathrm{p}_x = \frac{1}{\sqrt{2}} \Big( 2\mathrm{p}_1 + 2\mathrm{p}_{-1} \Big) =
	\frac{1}{4 \sqrt{2 \pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} e^{-\frac{Z r}{2 a_0}} r \sin{\theta} \cos{\phi},
	\eeq
	\beq
	2\mathrm{p}_y = -\frac{i}{\sqrt{2}} \Big( 2\mathrm{p}_1 - 2\mathrm{p}_{-1} \Big) =
	\frac{1}{4 \sqrt{2 \pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} e^{-\frac{Z r}{2 a_0}} r \sin{\theta} \sin{\phi},
	\eeq
	z kolei dla rzeczywistego orbitalu $2p_0$ stosuje się zamiennie oznaczenie $2p_z$. W ogólności dla $m > 0$ z pary zespolonych orbitali
\begin{equation}
	\Big\{ \ket{n\gamma(l)_m}, \ket{n\gamma(l)_{-m}} \Big\}
	\label{eq:iqdx}
\end{equation}
tworzy się parę orbitali rzeczywistych
\begin{equation}
	\Big\{ \ket{n\gamma(l)_a}, \ket{n\gamma(l)_b} \Big\}
	\label{eq:kaox}
\end{equation}
zgodnie z konwencją
\begin{align}
	\ket{n\gamma(l)_a} & =
		\begin{cases}
			\frac{1}{\sqrt{2}} \Big(
				\ket{n\gamma(l)_m} + \ket{n\gamma(l)_{-m}}
			\Big), & m \text{ nieparzyste} \\
			\frac{1}{\sqrt{2}} \Big(
				\ket{n\gamma(l)_{-m}} - \ket{n\gamma(l)_m}
			\Big), & m \text{ parzyste},
		\end{cases} \label{eq:jn_wod_phi} \\
	\ket{n\gamma(l)_b} & =
		\begin{cases}
			-\frac{i}{\sqrt{2}} \Big(
				\ket{n\gamma(l)_m} - \ket{n\gamma(l)_{-m}}
			\Big), & m \text{ nieparzyste} \\
			\frac{i}{\sqrt{2}} \Big(
				\ket{n\gamma(l)_{m}} + \ket{n\gamma(l)_{-m}}
			\Big), & m \text{ parzyste}.
		\end{cases} \label{eq:jn_wod_chi}
\end{align}
Indeksy~$a$ i~$b$ rozróżniające uzyskane orbitale są zwykle przyjmowane w postaci kombinacji współrzędnych kartezjańskich związanych z orientacją orbitali w przestrzeni.

Zauważmy, że z układu zagadnień własnych dla orbitali~\eqref{eq:iqdx}:
\begin{align}
	\hat{H} \ket{n\gamma(l)_{\pm m}} & = E_n \ket{n\gamma(l)_{\pm m}} \\
	\hat{l}^2 \ket{n\gamma(l)_{\pm m}} & = l (l + 1) \hbar^2 \ket{n\gamma(l)_{\pm m}} \\
	\hat{l}_z \ket{n\gamma(l)_{\pm m}} & = \pm l \hbar \ket{n\gamma(l)_{\pm m}}
\end{align}
i kombinacji~\eqref{eq:jn_wod_phi} oraz~\eqref{eq:jn_wod_chi} wynika, że orbitale rzeczywiste~\eqref{eq:kaox} są funkcjami własnymi operatorów $\hat{H}$ i $\hat{l}^2$, ale już nie operatora $\hat{l}_z$.

Wizualizacja orbitali polega na przedstawieniu konturów ich powierzchni granicznych. Kontury takie są zbiorem wszystkich punktów przestrzeni, którym odpowiada arbitralnie zadana wartość modułu orbitalu~$\varphi$:
	\beq
	|\varphi(r, \theta, \phi)| = \epsilon,
	\eeq
co można zapisać równoważnie we współrzędnych kartezjańskich
	\beq
	|\varphi(x, y, z)| = \epsilon,
	\eeq
są to więc wykresy funkcji uwikłanej
	\beq
	F(x, y, z) = |\varphi(x, y, z)| - \epsilon = 0.
	\eeq
Na rysunkach przedstawiono kontury graniczne kilku orbitali atomu wodoru dla $\epsilon = 0,01 \mbox{ } a_0^{-\frac{3}{2}}$. Warto zwrócić uwagę na wzrost rozmiarów orbitali (w sensie rozrastania się konturów dla tej samej wartości $\epsilon$) w miarę wzrostu liczby kwantowej $n$.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/1s}
	\caption{Kontur orbitalu $1\mathrm{s}$ atomu wodoru}
	\label{fig:orb_1s}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/2px}
	\caption{Kontur orbitalu $2\mathrm{p}_x$ atomu wodoru}
	\label{fig:orb_2px}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/2py}
	\caption{Kontur orbitalu $2\mathrm{p}_y$ atomu wodoru}
	\label{fig:orb_2py}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/2pz}
	\caption{Kontur orbitalu $2\mathrm{p}_z$ atomu wodoru}
	\label{fig:orb_2pz}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/3dz2}
	\caption{Kontur orbitalu $3\mathrm{d}_{z^2}$ atomu wodoru}
	\label{fig:orb_3dz2}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/3dx2-y2}
	\caption{Kontur orbitalu $3\mathrm{d}_{x^2 - y^2}$ atomu wodoru}
	\label{fig:orb_3dzx2-y2}
	\end{center}
	\end{figure}

	\begin{task}
		Znaleźć powierzchnię węzłową dla orbitalu $3\mathrm{d}_{z^2}$ atomu wodoru.
	\end{task}
	\begin{sol}
	Wystarczy rozważyć tę część orbitalu, która może zmieniać znak:
	\beq
	f(r; \theta; \phi) = r^2 (3 \cos{\theta}^2 - 1).
	\eeq
	Zapiszmy funkcję $f$ we współrzędnych kartezjańskich:
	\beq
	f(x; y; z) = 3 z^2 - r^2 = 2 z^2 - x^2 - y^2.
	\eeq
	Powierzchnia węzłowa jest zbiorem tych punktów, dla których wartość orbitalu jest zerowa i w otoczeniu których wartość ta zmienia znak. Punkty tej powierzchni wyznaczamy z równania
	\beq
	f(x; y; z) = 0 \Leftrightarrow = z^2 = \frac{x^2 + y^2}{2} \Rightarrow
	z = \pm \sqrt{\frac{x^2 + y^2}{2}}.
	\eeq
	Tak więc powierzchnią węzłową orbitalu $3\mathrm{d}_{z^2}$ jest stożek eliptyczny opisany równaniem $x^2 + y^2 = 2 z^2$. Jego wykres wraz z orbitalem $3d_{z^2}$ przedstawiono na rysunku~\ref{fig:pow_wzl_3dz2}.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/pow_wzl_3dz2}
	\caption{Powierzchnia węzłowa dla orbitalu $3\mathrm{d}_{z^2}$ wraz z konturem orbitalu}
	\label{fig:pow_wzl_3dz2}
	\end{center}
	\end{figure}

	\end{sol}

\subsection{Radialna gęstość prawdopodobieństwa}

\emph{Radialna gęstość prawdopodobieństwa} określa prawdopodobieństwo znalezienia elektronu w odległości $r$ od jądra niezależnie od kątów sferycznych i jest dana wzorem
	\beq
	\rho_{nl}(r) = \int_0^\pi \int_0^{2 \pi} |\psi_{nlm} (r; \theta; \phi)|^2 r^2 \sin{\theta} \, d \theta d \phi,
	\label{eq:gestrad1}
	\eeq
a ponieważ składowa radialna funkcji falowej jest rzeczywista, wzór~(\ref{eq:gestrad1}) przyjmuje postać
	\beq
	\rho_{nl}(r) = r^2 R_{nl}^2 \int_0^\pi \int_0^{2 \pi} |\sh{l}{m}|^2 \sin{\theta} \, d \theta d \phi = r^2 R_{nl}^2.
	\label{eq:gestrad2}
	\eeq
Na rysunkach~\ref{fig:grad_10}--\ref{fig:grad_32} przedstawiono wykresy radialnej gęstości prawdopodobieństwa dla kilku wartości $n$ i $l$.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/grad_10}
	\caption{Radialna gęstość prawdopodobieństwa dla orbitalu $1s$ atomu wodoru}
	\label{fig:grad_10}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/grad_20}
	\caption{Radialna gęstość prawdopodobieństwa dla orbitalu $2s$ atomu wodoru}
	\label{fig:grad_20}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/grad_21}
	\caption{Radialna gęstość prawdopodobieństwa dla orbitalu $2p$ atomu wodoru}
	\label{fig:grad_21}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/grad_30}
	\caption{Radialna gęstość prawdopodobieństwa dla orbitalu $3s$ atomu wodoru}
	\label{fig:grad_30}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/grad_31}
	\caption{Radialna gęstość prawdopodobieństwa dla orbitalu $3p$ atomu wodoru}
	\label{fig:grad_31}
	\end{center}
	\end{figure}

	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/grad_32}
	\caption{Radialna gęstość prawdopodobieństwa dla orbitalu $3d$ atomu wodoru}
	\label{fig:grad_32}
	\end{center}
	\end{figure}

	\begin{task}
	Wyznaczyć najbardziej prawdopodobną i oczekiwaną odległość elektronu na orbitalu $2p$ atomu wodoru.
	\end{task}
	\begin{sol}
	Odległość najbardziej prawdopodobną znajdujemy wyznaczając maksimum radialnej gęstości prawdopodobieństwa dla orbitalu $2p$:
	\beq
	\rho_{21}(r) = \frac{1}{24 a_0^5} r^4 e^{-\frac{r}{a_0}} = N r^4 e^{-\frac{r}{a_0}}.
	\eeq
	Obliczamy
	\beq
	\rho_{21}'(r) = N r^3 \left( 4 - \frac{r}{a_0} \right) e^{-\frac{r}{a_0}}
	\eeq
	i
	\beq
	\rho_{21}''(r) = N r^2 \left[ 12 - \frac{8 r}{a_0} + \left( \frac{r}{a_0} \right)^2 \right] e^{-\frac{r}{a_0}}.
	\eeq
	Odległość $r_{max}$, dla której funkcja $\rho_{21}(r)$ ma maksimum, wyznaczamy z warunków $\rho_{21}'(r_{max}) = 0$ i $\rho_{21}''(r_{max}) < 0$. Otrzymujemy
	\beq
	r_{max} = 4 a_0.
	\eeq
	Obliczamy teraz wartość oczekiwaną odległości:
	\beq
	\begin{split}
	\ev**{r} & = \ev**{\hat{r}}{\psi_{210}} =
	\int_0^\infty \int_0^\pi \int_0^{2 \pi} |\psi_{210}(r; \theta; \phi)| r^3 \sin{\theta} \, d r d \theta d \phi = \\
	 & = \frac{1}{32 \pi a_0^5} \int_0^\infty \int_0^\pi \int_0^{2 \pi} r^5 e^{-\frac{r}{a_0}} \sin{\theta} \cos^2{\theta} \, d r d \theta d \phi = \\
	 & = \frac{1}{32 \pi a_0^5} \int_0^\infty r^5 e^{-\frac{r}{a_0}} \, d r \int_0^\pi \sin{\theta} \cos^2{\theta} \, d \theta \int_0^{2 \pi} \, d \phi.
	\end{split}
	\eeq
	Obliczamy całkę
	\beq
	\int_0^\pi \sin{\theta} \cos^2{\theta} \, d \theta =
	\left\{ \begin{array}{l}
	t = -\cos{\theta} \mbox{, } d t = \sin{\theta} d \theta \\
	\theta = 0 \Rightarrow t = -1 \mbox{, } \theta = \pi \Rightarrow t = 1
	\end{array} \right\} =
	\int_{-1}^1 t^2 \, d t = \frac{2}{3}.
	\eeq
	Z kolei korzystając ze wzoru~(\ref{eq:rzparam}) obliczamy metodą różniczkowania po parametrach całkę
	\beq
	\int_0^\infty x^5 e^{-\alpha x} \, dx = \frac{120}{\alpha^6}.
	\eeq
	Więc ostatecznie
	\beq
	\ev**{r} = 5 a_0.
	\eeq
	Tak więc $\ev**{r} > r_\mathrm{max}$, co jest intuicyjnie zrozumiałe, jeśli spojrzeć na rozkład radialnej gęstości prawdopodobieństwa dla orbitalu $2p$ (rysunek~\ref{fig:grad_21}).
	\end{sol}

	\begin{task}
	Obliczyć średnią wartość energii kinetycznej elektronu znajdującego się w stanie podstawowym dla jonu wodoropodobnego.
	\end{task}
	\begin{sol}
	Ponieważ
	\beq
	T = \frac{p^2}{2 m},
	\eeq
	stąd
	\beq
	\ev**{T} = \frac{\ev**{p^2}}{2 m}.
	\label{eq:mvT_mvp2}
	\eeq
	Obliczamy zatem
	\beq
\ev**{p^2} = \ev**{\hat{p}^2}{\psi_{100}} = -\hbar^2 \ev**{\Delta_\mathbf{r}}{\psi_{100}}
	\label{eq:mvp2_psi100}
	\eeq
	Ponieważ funkcja falowa stanu podstawowego nie zależy od kątów sferycznych, stąd w laplasjanie~(\ref{eq:op_en_kin_sm_wt}) możemy pominąć człony zawierające różniczkowanie po kątach. Tak więc
	\beq
	\Delta_\mathbf{r} \psi_{100}(r) = \frac{1}{r^2} \frac{d}{d r} \left[ r^2 \frac{d \psi_{100}(r)}{d r} \right] =
	-\frac{1}{\sqrt{\pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} \frac{1}{r^2} \frac{d}{d r} \left( r^2 e^{-\frac{Z r}{a_0}} \right) =
	-\frac{1}{\sqrt{\pi}} \left( \frac{Z}{a_0} \right)^\frac{5}{2} \left( \frac{2}{r} - \frac{Z}{a_0} \right) e^{-\frac{Z r}{a_0}}.
	\label{eq:lapl_psi100}
	\eeq
	Wstawiając~(\ref{eq:lapl_psi100}) do~(\ref{eq:lapl_psi100}) otrzymujemy
	\beq
	\ev**{p^2} = -\hbar^2 \int_0^\infty \int_0^\pi \int_0^{2 \pi} \psi_{100}^*(r) \Delta_\mathbf{r} \psi_{100}(r) r^2 \sin{\theta} \, d r d \theta d \phi =
	4 \hbar^2 \left( \frac{Z}{a_0} \right)^4 \int_0^\infty \left( 2 r - \frac{Z r^2}{a_0} \right) e^{-\frac{2 Z r}{a_0}} \, d r,
	\eeq
	a po wykonaniu prostego całkowania korzystając z zależności~(\ref{eq:rzparam}) otrzymujemy
	\beq
	\ev**{p^2} = \left( \frac{Z \hbar}{a_0} \right)^2.
	\label{eq:mvp2}
	\eeq
	Korzystając ze wzorów~(\ref{eq:a0}), (\ref{eq:mvT_mvp2}) i~(\ref{eq:mvp2}) otrzymujemy ostatecznie
	\beq
	\ev**{T} = \frac{Z^2 m e^4}{8 \epsilon_0^2 h^2} = -E_n.
	\eeq
	\end{sol}

	\begin{task}
	Wyznaczyć średni pęd elektronu w stanie podstawowym jonu wodoropodobnego.
	\end{task}
	\begin{sol}
	Ponieważ pęd jest wielkością wektorową, musimy obliczyć średnie wszystkich składowych wektora pędu. Musimy ponadto zapisać operatory składowych pędu we współrzędnych sferycznych --- postępujemy tak samo, jak w ćwiczeniu~\ref{ts:op_mom_pd_wsp_sfer}. Można dodatkowo wykorzystać fakt, że funkcja falowa $\psi_{100}$ nie zależy od kątów sferycznych, zatem wystarczy rozważyć tylko część operatorów zawierającą różniczkowanie po $r$:
	\beq
	\hat{p}_q \psi_{100}(r) = -i \hbar \frac{\partial r}{\partial q} \frac{d \psi_{100}(r)}{d r} \mbox{, } q \in \{x; y; z\}.
	\eeq
	Obliczamy kolejne pochodne:
	\beq
	\left\{ \begin{array}{l}
	\frac{\partial r}{\partial x} = \frac{x}{r} = \sin{\theta} \sin{\phi} \\
	\frac{\partial r}{\partial y} = \frac{y}{r} = \sin{\theta} \cos{\phi} \\
	\frac{\partial r}{\partial z} = \frac{z}{r} = \cos{\theta}
	\end{array} \right..
	\label{eq:poch_sr_skl_pd}
	\eeq
	Średnią wartość składowych pędu wyznaczamy z zależności
	\beq
	\ev**{p_q} = \ev**{\hat{p}_q}{\psi_{100}} =
	\int_0^\infty \int_0^\pi \int_0^{2 \pi} \psi^*_{100}(r) \hat{p}_q \psi_{100}(r) r^2 \sin{\theta} \, d r d \theta d \phi.
	\label{eq:sr_skl_pd}
	\eeq
	Z wyrażeń~(\ref{eq:poch_sr_skl_pd}) wynika, że we wzorze~(\ref{eq:sr_skl_pd}) dla wszystkich składowych pędu pojawią się zerowe całki
	\beq
	\int_0^{2 \pi} \sin{\phi} \, d \phi = \int_0^{2 \pi} \cos{\phi} \, d \phi =
	\int_0^\pi \sin{\theta} \cos{\theta} \, d \theta = 0,
	\eeq
	zatem średnie wartości wszystkich składowych pędu są zerowe:
	\beq
	\ev**{p_x} = \ev**{p_y} = \ev**{p_z} = 0
	\eeq
	i to samo dotyczy średniej wartości pędu:
	\beq
	\ev**{p} = \sqrt{\ev**{p_x}^2 + \ev**{p_y}^2 + \ev**{p_z}^2} = 0.
	\eeq
	\end{sol}

\subsection{Jednostki atomowe}

Jak zauważyliśmy w poprzednich podrozdziałach, obliczając różne wartości oczekiwane dla atomu wodoru mamy w ogólności do czynienia z dość niewygodnymi, z uwagi na piętrowość stałych, całkami. Pojawiła się więc potrzeba wprowadzenia dogodniejszego układu jednostek, pozwalającego na prostszy i klarowniejszy zapis wyrażeń, nie tylko dla atomu wodoru, ale i pozostałych atomów. Wprowadźmy nową zmienną bezwymiarową $\vec{\mathscr{R}}$ taką, że
	\beq
	\mathbf{r} = \lambda \vec{\mathscr{R}},
	\eeq
wówczas
	\beq
	\Delta_\mathbf{r} = \frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2} + \frac{\partial^2}{\partial z^2} =
	\frac{\partial^2}{\partial (\lambda \mathscr{X})^2} + \frac{\partial^2}{\partial (\lambda \mathscr{Y})^2} + \frac{\partial^2}{\partial (\lambda \mathscr{Z})^2} =
	\frac{1}{\lambda^2} \Delta_{\vec{\mathscr{R}}}.
	\eeq
Oczywiste jest, że $\lambda$ ma wymiar długości. Zapiszmy równanie Schrödingera dla atomu wodoru z hamiltonianem~\eqref{eq:ham_jn_wod} w nowych zmiennych:
	\beq
	\left( -\frac{\hbar^2}{2 \mu \lambda^2} \Delta_{\vec{\mathscr{R}}} - \frac{e^2}{4 \pi \epsilon_0 \lambda \mathscr{R}} \right) \psi \left( \lambda \vec{\mathscr{R}} \right) =
	E \psi \left( \lambda \vec{\mathscr{R}} \right).
	\eeq
Dobierzmy stałą $\lambda$ tak, by współczynniki przy części kinetycznej i potencjalnej hamiltonianu były jednakowe i równe stałej \eau, mającej wymiar energii, to znaczy
	\beq
	\frac{\hbar^2}{\mu \lambda^2} = \frac{e^2}{4 \pi \epsilon_0 \lambda} = \eau,
	\eeq
skąd otrzymujemy
	\beq
	\lambda = \frac{4 \pi \epsilon_0 \hbar^2}{\mu e^2},
	\eeq
a zatem $\lambda$ jest po prostu zredukowanym promieniem Bohra:
	\beq
	\lambda = \lau.
	\eeq
Wprowadzając bezwymiarową zmienną proporcjonalną do energii,
	\beq
	\mathscr{E} = \frac{E}{\eau},
	\eeq
otrzymujemy równanie Schrödingera dla atomu wodoru w \emph{jednostkach atomowych}:
	\beq
	\boxed{\left( -\frac{1}{2} \Delta_{\vec{\mathscr{R}}} - \frac{1}{\mathscr{R}} \right) \phi \left( \vec{\mathscr{R}} \right) =
	\mathscr{E} \phi \left( \vec{\mathscr{R}} \right)},
	\eeq
gdzie $\phi(\vec{\mathscr{R}})$ jest bezwymiarową funkcją falową. Hamiltonian w jednostkach atomowych dla atomu wodoru ma więc postać
	\beq
	\boxed{\hat{\mathscr{H}} = -\frac{1}{2} \Delta_{\vec{\mathscr{R}}} - \frac{1}{\mathscr{R}}}.
	\label{eq:ham_jat_jn_wod}
	\eeq
Postać funkcji falowej w jednostkach atomowych znajdujemy z warunku normalizacji funkcji $\psi$:
	\beq
	1 = \braket{\psi} = \int_{\mathbf{r}} \psi^*(\mathbf{r}) \psi(\mathbf{r}) \, d^3\mathbf{r} =
	\lambda^3 \int_{\vec{\mathscr{R}}} \psi^* \left( \lambda \vec{\mathscr{R}} \right) \psi \left( \lambda \vec{\mathscr{R}} \right) \, d^3\vec{\mathscr{R}} =
	\int_{\vec{\mathscr{R}}} \left[ \lambda^\frac{3}{2} \psi \left( \lambda \vec{\mathscr{R}} \right) \right]^* \lambda^\frac{3}{2} \psi \left( \lambda \vec{\mathscr{R}} \right) \, d^3\vec{\mathscr{R}},
	\label{eq:ff_wypr_jn_wod}
	\eeq
czyli
	\beq
	\boxed{\phi \left( \vec{\mathscr{R}} \right) = \lau^\frac{3}{2} \psi \left( \lau \vec{\mathscr{R}} \right) }.
	\label{eq:ff_jat_jn_wod}
	\eeq
Oczywiście funkcja falowa w jednostkach atomowych jest także unormowana, co wynika bezpośrednio z przekształceń~\eqref{eq:ff_wypr_jn_wod}. Jednostki atomowe to więc \textbf{bezwymiarowe} zmienne proporcjonalne do zmiennych wymiarowych. Na przykład chcąc uzyskać wartość energii w danych jednostkach, E, mając jej wartość w jednostkach atomowych, $\mathscr{E}$, musimy tę wartość przemnożyć przez współczynnik proporcjonalności dla energii, którym w tym przypadku jest \eau:
	\beq
	E = \mathscr{E} \eau.
	\label{eq:hartree_jn_wod}
	\eeq
Współczynniki proporcjonalności dla pozostałych jednostek tworzymy wyrażając je przez współczynniki proporcjonalności dla energii i długości, czyli przez $\eau$ i $\lau$. $\eau$ jest często używaną jednostką energii i nosi nazwę hartree (h):
	\beq
	\eau = \frac{\mu e^4}{4 \epsilon_0^2 h^2} = 1 \text{ h}.
	\eeq
Współczynnik proporcjonalności dla czasu, \tmau, możemy znaleźć ze współczynnika dla energii. W jednostkach SI mamy $[\hbar] = \text{J} \cdot \text{s}$, zatem
	\beq
	\tmau = \frac{\hbar}{\eau} = \left( \frac{4 \pi \epsilon_0 \hbar}{e^2} \right)^2 \frac{\hbar}{\mu}.
	\eeq
W dalszej części jednostki atomowe będziemy oznaczać tymi samymi symbolami co jednostki wymiarowe, sygnalizując, że obliczenia będziemy prowadzić w jednostkach atomowych.
	\begin{task}
	Wyrazić energię $n$-tego stanu jonu wodoropodobnego i funkcję falową dla stanu $2p_0$ atomu wodoru w jednostkach atomowych.
	\end{task}

	\begin{sol}
	Wstawiając~\eqref{eq:sep_ff_jn_wod} do równania~\eqref{eq:hartree_jn_wod} uzyskujemy
	\beq
	\mathscr{E}_n = \frac{E_n}{\eau} = -\frac{Z^2}{2 n^2},
	\eeq
	w szczególności dla atomu wodoru $\mathscr{E}_1 = -\frac{1}{2}$. Funkcja dla stanu $2p_0$ atomu wodoru ma postać
	\beq
	\psi_{210}(\mathbf{r}) = \frac{1}{4 \sqrt{2 \pi} \lau^{5 / 2}} r \cos{\theta} e^{-\frac{r}{2 \lau}}.
	\eeq
	Korzystając z zależności~\eqref{eq:ff_jat_jn_wod} uzyskujemy
	\beq
	\phi_{210}(\vec{\mathscr{R}}) = \frac{1}{4 \sqrt{2 \pi}} \mathscr{R} \cos{\theta} e^{-\frac{\mathscr{R}}{2}}.
	\eeq

	\end{sol}
