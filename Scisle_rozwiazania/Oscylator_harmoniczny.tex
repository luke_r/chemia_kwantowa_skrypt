\section{Oscylator harmoniczny}

\subsection{Obraz klasyczny oscylatora harmonicznego}

\emph{Oscylator harmoniczny} to punkt wykonujący drgania wokół położenia równowagi pod wpływem siły proporcjonalnej do wychylenia i skierowanej zawsze do punktu równowagi. Siła działająca na ten punkt spełnia więc prawo Hooke'a:
	\beq
	\mathbf{F} = -k (\mathbf{r} - \mathbf{r}_e),
	\label{eq:pr_hk}
	\eeq
gdzie $\mathbf{r}_e$ oznacza położenie równowagi. Ograniczając nasze rozważania do przypadku jednowymiarowego i zakładając $x_e = 0$, równanie~(\ref{eq:pr_hk}) przybiera postać
	\beq
	F = - k x.
	\eeq
Korzystając ze związku energii potencjalnej z siłą:
	\beq
	F = -\frac{d V}{d x},
	\eeq
i zakładając, że energia potencjalna jest zerowa dla zerowego wychylenia z położenia równowagi, otrzymujemy
	\beq
	V = \frac{k x^2}{2}.
	\label{eq:pot_oh}
	\eeq
W ujęciu klasycznym trajektorię punktu znajdujemy przez rozwiązanie równania Newtona:
	\beq
	m \frac{d^2 x}{d t^2} = -k x \Rightarrow \frac{d^2 x}{d t^2} = - \omega^2 x,
	\label{eq:rnt_oh}
	\eeq
gdzie
	\beq
	\omega = \sqrt{\frac{k}{m}}.
	\eeq
Równanie~(\ref{eq:rnt_oh}) ma rozwiązanie ogólne postaci
	\beq
	x(t) = a e^{i \omega t} + b e^{-i \omega t}.
	\eeq
Zakładając
	\beq
	x(0) = 0 \Leftrightarrow a + b = 0,
	\eeq
otrzymujemy
	\beq
	x(t) = a (e^{i \omega t} - e^{-i \omega t}) = A \sin{\omega t},
	\label{eq:traj_oh}
	\eeq
gdzie $A = 2 i a$ jest amplitudą drgań oscylatora. Łącząc~(\ref{eq:pot_oh}) i~(\ref{eq:traj_oh}), otrzymujemy wyrażenie na energię potencjalną oscylatora w funkcji czasu:
	\beq
	V = \frac{1}{2} k A^2 \sin^2{\omega t}.
	\eeq
Energię kinetyczną oscylatora klasycznego obliczamy z zależności
	\beq
	T = \frac{m v^2}{2} = \frac{m}{2} \left( \frac{d x}{d t} \right)^2 = \frac{1}{2} k A^2 \cos^2{\omega t}.
	\eeq
Zatem energia całkowita oscylatora klasycznego wynosi
	\beq
	E = T + U = \frac{k A^2}{2}
	\eeq
i, jak widać, może przyjmować dowolne wartości.

\subsection{Obraz kwantowy oscylatora harmonicznego}
\label{ssc:ok_oh}

Znając z równania~(\ref{eq:pot_oh}) postać energii potencjalnej jednowymiarowego oscylatora harmonicznego, możemy zapisać równanie Schrödingera dla tego układu:
	\beq
	\left( -\frac{\hbar^2}{2 m} \frac{d^2}{d x^2} + \frac{k x^2}{2} \right) \psi(x) =
	\left( -\frac{\hbar^2}{2 m} \frac{d^2}{d x^2} + \frac{m \omega^2 x^2}{2} \right) \psi(x) =
	E \psi(x).
	\label{eq:rs_oh}
	\eeq
Równanie~(\ref{eq:rs_oh}) można zapisać w zmiennych bezwymiarowych
	\beq
	\left\{ \begin{array}{l}
	\xi = \sqrt{\frac{m \omega}{\hbar}} x \\
	\epsilon = \frac{E}{\hbar \omega}
	\end{array} \right.
	\label{eq:bezw}
	\eeq
w postaci
	\beq
	\left( \frac{d^2}{d \xi^2} - \xi^2 \right) \psi(\xi) = -2 \epsilon \psi(\xi).
	\label{eq:bezw_ww}
	\eeq
Zauważmy, że
	\begin{eqnarray}
	\nonumber \left( \frac{d}{d \xi} - \xi \right) \left( \frac{d}{d \xi} + \xi \right) \psi(\xi) & = &
	\left( \frac{d}{d \xi} - \xi \right) \left[ \frac{d \psi(\xi)}{d \xi} - \xi \psi(\xi) \right] =
	\frac{d^2 \psi(\xi)}{d \xi^2} + \underbrace{\psi(\xi) + \xi \frac{d \psi(\xi)}{d \xi}}_{\frac{d [\xi \psi(\xi)]}{d \xi}} - \xi \frac{d \psi(\xi)}{d \xi} - \xi^2 \psi(\xi) = \\
	 & = & \left( \frac{d^2}{d \xi^2} - \xi^2 \right) \psi(\xi) + \psi(\xi).
	 \label{eq:bezw_prz}
	\end{eqnarray}
łącząc~(\ref{eq:bezw}) i ~(\ref{eq:bezw_prz}) otrzymujemy
	\beq
	\left( \frac{d}{d \xi} - \xi \right) \left( \frac{d}{d \xi} + \xi \right) \psi(\xi) = (-2 \epsilon + 1) \psi(\xi).
	\label{eq:ilop1}
	\eeq
Przekształcenie analogiczne do~(\ref{eq:bezw_prz}) pozwala otrzymać zależność
	\beq
	\left( \frac{d}{d \xi} + \xi \right) \left( \frac{d}{d \xi} - \xi \right) \psi(\xi) = (-2 \epsilon - 1) \psi(\xi),
	\label{eq:ilop2}
	\eeq
tak więc zachodzi
	\beq
	\left[ \left( \frac{d}{d \xi} - \xi \right) \left( \frac{d}{d \xi} + \xi \right) \psi(\xi) -
	\left( \frac{d}{d \xi} + \xi \right) \left( \frac{d}{d \xi} - \xi \right) \psi(\xi) \right] \psi(\xi) =
	2 \psi(\xi).
	\label{eq:sum_ilop}
	\eeq
Jeśli dana funkcja $\phi_0(\xi)$ spełnia równanie własne~(\ref{eq:ilop1}) z wartością własną $-2 \epsilon_0 + 1$, to wartość własna $\epsilon_x$, z jaką spełnia równanie~(\ref{eq:ilop2}), na podstawie równania~(\ref{eq:sum_ilop}), spełnia zależność
	\beq
	-2 \epsilon_0 +1 - \epsilon_x = 2 \Rightarrow \epsilon_x = -2 \epsilon_0 - 1.
	\label{eq:zal_ww}
	\eeq
Utwórzmy funkcję
	\beq
	\phi_1(\xi) = \left( \xi - \frac{d}{d \xi} \right) \phi_0(\xi)
	\eeq
i sprawdźmy, z jaką wartością własną spełnia równanie~(\ref{eq:ilop1}):
	\beq
	\begin{split}
	\left( \frac{d}{d \xi} - \xi \right) \left( \frac{d}{d \xi} + \xi \right) \phi_1(\xi) & =
	-\left( \frac{d}{d \xi} - \xi \right) \left( \frac{d}{d \xi} + \xi \right) \left( \frac{d}{d \xi} - \xi \right) \phi_0(\xi) = \\
	 & = (-2 \epsilon_0 - 1) \left( \xi - \frac{d}{d \xi} \right) \phi_0(\xi) = \\
	 & = (-2 \epsilon_0 - 1) \phi_1(\xi) = \\
	 & = (-2 \epsilon_1 + 1) \phi_1(\xi),
	\end{split}
	\eeq
gdzie
	\beq
	\epsilon_1 = \epsilon_0 + 1.
	\eeq
Utwórzmy teraz funkcję
	\beq
	\phi_{-1}(\xi) = \left( \xi + \frac{d}{d \xi} \right) \phi_0(\xi).
	\eeq
Tym razem sprawdźmy, z jaką wartością własną funkcja ta spełnia równanie~\eqref{eq:ilop2}:
	\beq
	\begin{split}
	\left( \frac{d}{d \xi} + \xi \right) \left( \frac{d}{d \xi} - \xi \right) \phi_{-1}(\xi) & =
	\left( \frac{d}{d \xi} + \xi \right) \left( \frac{d}{d \xi} - \xi \right) \left( \frac{d}{d \xi} + \xi \right) \phi_0(\xi) = \\
	 & = (-2 \epsilon_0 + 1) \left( \xi + \frac{d}{d \xi} \right) \phi_0(\xi) = \\
	 & = (-2 \epsilon_0 + 1) \phi_{-1}(\xi) = \\
	 & = (-2 \epsilon_{-1} -1) \phi_{-1}(\xi),
	\end{split}
	\eeq
gdzie
	\beq
	\epsilon_{-1} = \epsilon_0 - 1.
	\eeq
Zauważmy, że operator $\xi - \frac{d}{d \xi}$ utworzył ze stanu $\phi_0$ o energii $\epsilon_0$ stan $\phi_1$ o energii $\epsilon_0 + 1$. Z kolei operator $\xi + \frac{d}{d \xi}$ działając na $\phi_0$ utworzył stan $\phi_{-1}$ o energii $\epsilon_0 - 1$. Postępowanie to możemy powtórzyć dla funkcji $\phi_1$ i $\phi_{-1}$, tworząc funkcje $\phi_2$ i $\phi_{-2}$. W ogólności powtarzając powyższą procedurę $v$ razy uzyskamy funkcje
	\beq
	\left\{ \begin{array}{l}
	\phi_v(\xi) = \left( \xi - \frac{d}{d \xi} \right)^v \phi_0(\xi) \\
	\phi_{-v}(\xi) = \left( \xi + \frac{d}{d \xi} \right)^v \phi_0(\xi)
	\end{array} \right. \mbox{, } v \in \mathbb{N},
	\label{eq:oh_phi}
	\eeq
o wartościach własnych
	\beq
	\left\{ \begin{array}{l}
	\epsilon_v = \epsilon_0 + v \\
	\epsilon_{-v} = \epsilon_0 - v
	\end{array} \right. \mbox{, } v \in \mathbb{N}.
	\label{eq:oh_eps}
	\eeq
$\epsilon_v$ i $\epsilon_{-v}$ odpowiadają stanom energetycznym oscylatora~[równanie~\eqref{eq:bezw}]. Energia oscylatora nie jest ograniczona z góry, ale nie może przyjmować dowolnie małych wielkości, co jest dopuszczone w równaniu~(\ref{eq:oh_eps}). Można zatem przyjąć, że
	\beq
	\forall_{v \in \mathbb{N}}: \epsilon_v > 0 \wedge \epsilon_{-v} = 0 \Rightarrow
	\forall_{v \in \mathbb{N}}: \phi_{-v}(\xi) = 0,
	\eeq
co pozwala wyznaczyć funkcję $\phi_0(\xi)$ z warunku
	\beq
	\phi_{-1}(\xi) = 0 \Leftrightarrow \left( \xi + \frac{d}{d \xi} \right) \phi_0(\xi) = 0.
	\label{eq:rphi-1}
	\eeq
Rozwiązaniem równania~(\ref{eq:rphi-1}) jest funkcja gaussowska
	\beq
	\phi_0(\xi) = e^{-\frac{\xi^2}{2}}.
	\eeq
	\begin{task}
	Sprawdzić, czy funkcja $f(x) = e^{-\frac{x^2}{2}}$ jest funkcją własną operatora $\ana = \left( \frac{d^2}{d x^2} - x^2 \right)$. Jeśli tak, to wyznaczyć jej wartość własną.
	\end{task}
	\begin{sol}
	Obliczamy
	\beq
	\ana f(x) = \left( \frac{d^2}{d x^2} - x^2 \right) e^{-\frac{x^2}{2}} =
	-\frac{d}{d x} \left( x e^{-\frac{x^2}{2}} \right) - x^2 e^{-\frac{x^2}{2}} =
	-e^{-\frac{x^2}{2}} + x^2 e^{-\frac{x^2}{2}} - x^2 e^{-\frac{x^2}{2}} =
	-f(x),
	\label{eq:ww_A}
	\eeq
	zatem funkcja $f(x)$ jest funkcją własną operatora $\ana$ z wartością własną $-1$.
	\end{sol}
łącząc~\eqref{eq:bezw_ww} oraz~\eqref{eq:ww_A} otrzymujemy
	\beq
	\epsilon_0 = \frac{1}{2}.
	\eeq
Rozwiązania~(\ref{eq:oh_phi}) oraz~(\ref{eq:oh_eps}) można więc zapisać w postaci
	\beq
	\left\{ \begin{array}{l}
	\phi_v(\xi) = \left( \xi - \frac{d}{d \xi} \right)^v e^{-\frac{\xi^2}{2}} \\
	\epsilon_v = v + \frac{1}{2}
	\end{array} \right. \mbox{, } v \in \mathbb{N} \cap \{0\}.
	\eeq
W tabeli~\ref{tab:phiv} przedstawiono postaci kilku pierwszych funkcji $\phi_v$.
	\begin{table}[htbp]
	\caption{Postać funkcji $\phi_v(\xi)$ dla kilku wartości $v$}
	\begin{center}
	\begin{tabular}{|l|l|}
	\hline
	\tp{|c}{$v$} & \tp{|c|}{$\phi_v(\xi) / e^{-\frac{\xi^2}{2}}$} \\
	\hline \hline
	0 & 1 \\
	1 & $2 \xi$ \\
	2 & $4 \xi^2 -2$ \\
	3 & $8 \xi^3 - 12 \xi$ \\
	4 & $16 \xi^4 - 48 \xi^2 + 12$ \\
	5 & $32 \xi^5 - 160 \xi^3 + 120 \xi$ \\
	6 & $64 \xi^6 - 480 \xi^4 + 720 \xi^2 - 120$ \\
	7 & $128 \xi^7 - 1344 \xi^5 + 3360 \xi^3 - 1680 \xi$ \\
	\hline
	\end{tabular}
	\end{center}
	\label{tab:phiv}
	\end{table}
Łatwo zauważyć, że funkcje $\phi_v(\xi)$ mają postać wielomianu zawierającego tylko parzyste bądź tylko nieparzyste potęgi $\xi$ przemnożonego przez funkcję $\phi_0$. W ogólności funkcje
	\beq
	H_v(\xi) = e^{\frac{\xi^2}{2}} \left( \xi - \frac{d}{d \xi} \right)^v e^{-\frac{\xi^2}{2}} =
	(-1)^v e^{\xi^2} \frac{d^v e^{-\xi^2}}{d \xi^v}
	\eeq
to tzw. \emph{wielomiany Hermite'a}. Można je zdefiniować jako współczynniki rozwinięcia w szereg względem zmiennej $t$ funkcji
	\beq
	S(\xi; t) = e^{2 \xi t - t^2}
	\label{eq:ftw_wherm}
	\eeq
(tak zwanej \emph{funkcji tworzącej}). Rozwinięcie funkcji eksponencjalnej w szereg potęgowy ma postać
	\beq
	e^t = \sum_{v = 0}^\infty \frac{t^v}{v!},
	\label{eq:exp_pot}
	\eeq
stąd wielomiany Hermite'a są zdefiniowane zależnością
	\beq
	e^{2 \xi t - t^2} = \sum_{v = 0}^\infty H_v(\xi) \frac{t^v}{v!}.
	\label{eq:wherm_zal}
	\eeq
Zróżniczkujmy funkcję tworzącą~\eqref{eq:ftw_wherm} kolejno po $\xi$ i $t$:
	\beq
	\begin{split}
	\frac{\partial S(\xi; t)}{\partial \xi} & = 2 t e^{2 \xi t - t^2} = \sum_{v = 0}^\infty H_v(\xi) \frac{2 t^{v + 1}}{v!} =
	\sum_{v = 0}^\infty H_v(\xi) \frac{2 (v + 1) t^{v + 1}}{v! (v + 1)} = \\
	 & = \sum_{v = 0}^\infty H_v(\xi) \frac{2 (v + 1) t^{v + 1}}{(v + 1)!} =
	\sum_{v = 1}^\infty H_{v - 1}(\xi) \frac{2 v t^v}{v!},
	\end{split}
	\eeq

	\beq
	\begin{split}
	\frac{\partial S(\xi; t)}{\partial t} & = (2 \xi - 2 t) e^{2 \xi t - t^2} = \sum_{v = 0}^\infty H_v(\xi) \frac{(2 \xi - 2 t) t^v}{v!} = \\
	 & = \sum_{v = 0}^\infty H_v(\xi) \frac{2 \xi t^v}{v!} - \sum_{v = 0}^\infty H_v(\xi) \frac{2 (v + 1) t^{v + 1}}{v! (v + 1)} = \\
	 & = \sum_{v = 0}^\infty H_v(\xi) \frac{2 \xi t^v}{v!} - \sum_{v = 1}^\infty H_{v - 1}(\xi) \frac{2 v t^v}{v!}.
	\end{split}
	\eeq
Następnie powtórzmy tę procedurę, ale dla funkcji tworzącej w postaci~\eqref{eq:wherm_zal}:
	\beq
	\frac{\partial S(\xi; t)}{\partial \xi} = \sum_{v = 0}^\infty H'_v(\xi) \frac{t^v}{v!},
	\eeq
	\beq
	\frac{\partial S(\xi; t)}{\partial t} = \sum_{v = 0}^\infty H_v(\xi) \frac{v t^{v - 1}}{v!} = \sum_{v = 0}^\infty H_v(\xi) \frac{t^{v - 1}}{(v - 1)!} =
	\sum_{v = -1}^\infty H_{v + 1}(\xi) \frac{t^v}{v!}.
	\eeq
Porównując wyrazy zawierające jednakowe potęgi $t$:
	\beq
	\sum_{v = 1}^\infty H_{v - 1}(\xi) \frac{2 v t^v}{v!} = \sum_{v = 0}^\infty H'_v(\xi) \frac{t^v}{v!}
	\eeq
i
	\beq
	\sum_{v = 0}^\infty H_v(\xi) \frac{2 \xi t^v}{v!} - \sum_{v = 1}^\infty H_{v - 1}(\xi) \frac{2 v t^v}{v!} = \sum_{v = -1}^\infty H_{v + 1}(\xi) \frac{t^v}{v!},
	\eeq
uzyskujemy dwie istotne własności rekurencyjne wielomianów Hermite'a:
	\beq
	\boxed{\frac{d H_v(\xi)}{d \xi} = 2 v H_{v - 1}(\xi)}
	\label{eq:wherm_r1}
	\eeq
oraz
	\beq
	\boxed{H_{v+1}(\xi) = 2 \xi H_v(\xi) - 2 v H_{v - 1}(\xi)}.
	\label{eq:wherm_r2}
	\eeq
Wielomiany Hermite'a są ortogonalne z funkcją ważącą $e^{-\xi^2}$:
	\beq
	\int_{-\infty}^\infty H_v(\xi) H_w(\xi) e^{-\xi^2} \, d \xi = \delta_{v w} 2^v v! \sqrt{\pi}.
	\label{eq:wherm_ort}
	\eeq
Parzystość wielomianów Hermite'a jest określona przez parzystość $v$:
	\beq
	H_v(-\xi) = (-1)^v H_v(\xi),
	\eeq
więc $H_v$ jest funkcją parzystą dla parzystego $v$, a nieparzystą --- dla nieparzystego $v$.
W tabeli~\ref{tab:wiel_herm} przedstawiono kilka przykładów tych wielomianów.
	\begin{table}[htbp]
	\caption{Postać wielomianów Hermite'a dla kilku wartości $v$}
	\begin{center}
	\begin{tabular}{|l|l|}
	\hline
	\tp{|c}{$v$} & \tp{|c|}{$H_v(\xi)$} \\
	\hline \hline
	0 & 1 \\
	1 & $2 \xi$ \\
	2 & $4 \xi^2 -2$ \\
	3 & $8 \xi^3 - 12 \xi$ \\
	4 & $16 \xi^4 - 48 \xi^2 + 12$ \\
	5 & $32 \xi^5 - 160 \xi^3 + 120 \xi$ \\
	6 & $64 \xi^6 - 480 \xi^4 + 720 \xi^2 - 120$ \\
	7 & $128 \xi^7 - 1344 \xi^5 + 3360 \xi^3 - 1680 \xi$ \\
	\hline
	\end{tabular}
	\end{center}
	\label{tab:wiel_herm}
	\end{table}

Zatem funkcja falowa dla oscylatora harmonicznego ma postać
	\beq
	\boxed{\psi_v(x) = N_v H_v \left( \sqrt{\frac{m \omega}{\hbar}} x \right) e^{-\frac{m \omega x^2}{2 \hbar}}},
	\label{eq:oh_ff}
	\eeq
zaś dozwolone energie dana są wzorem
	\beq
	\boxed{E_v = \hbar \omega \left( v + \frac{1}{2} \right) =
	h \nu \left( v + \frac{1}{2} \right)}.
	\label{eq:oh_en}
	\eeq
	\begin{task}
	Znaleźć stałą normalizacyjną dla funkcji falowej jednowymiarowego oscylatora harmonicznego.
	\end{task}
	\begin{sol}
	Warunek normalizacji dla funkcji~(\ref{eq:oh_ff}) ma postać
	\beq
	N_v^2 \int_{-\infty}^\infty H_v^2(\sqrt{\frac{m \omega}{\hbar}} x) e^{-\frac{m \omega x^2}{2 \hbar}} \, dx = 1.
	\eeq
	Stosując podstawienie jak w równaniach~(\ref{eq:bezw})\footnote{W kolejnych obliczeniach będziemy stosować to samo podstawienie.} i korzystając z zależności~(\ref{eq:wherm_ort}) otrzymujemy
	\beq
	N_v^2 \sqrt{\frac{\hbar}{m \omega}} \int_{-\infty}^\infty H_v^2(\xi) e^{-\xi^2} \, d \xi = 2^v v! \sqrt{\frac{h}{2 m \omega}} N_v^2 = 1
	\Rightarrow N_v = \frac{1}{\sqrt{2^v v!}} \left( \frac{2 m \omega}{h} \right)^\frac{1}{4}.
	\eeq
	Zatem unormowana funkcja falowa ma postać
	\beq
	\boxed{\psi_v(x) = \frac{1}{\sqrt{2^v v!}} \left( \frac{2 m \omega}{h} \right)^\frac{1}{4} H_v \left( \sqrt{\frac{m \omega}{\hbar}} x \right) e^{-\frac{m \omega x^2}{2 \hbar}}}.
	\label{eq:ff_prwk_oh}
	\eeq
	\end{sol}
	\begin{task}
	Obliczyć średnią wartość wychylenia z położenia równowagi jednowymiarowego oscylatora harmonicznego.
	\end{task}
	\begin{sol}
	Musimy obliczyć całkę
	\beq
	\ev**{x} = \int_{-\infty}^\infty x \psi_v^2(x) \, dx =
	\frac{N_v^2 \hbar}{m \omega} \int_{-\infty}^\infty \xi H_v^2(\xi) e^{-\xi^2} \, d \xi =
	\frac{N_v^2 \hbar}{m \omega} \int_{-\infty}^\infty H_v(\xi) \xi H_v(\xi) e^{-\xi^2} \, d \xi.
	\eeq
	Z zależności~(\ref{eq:wherm_r2}) otrzymujemy
	\beq
	\xi H_v(\xi) = \frac{1}{2} H_{v + 1}(\xi) + v H_{v - 1}(\xi),
	\eeq
	więc
	\beq
	\ev**{x} = \frac{N_v^2 \hbar}{m \omega} \left[
	\frac{1}{2} \underbrace{\int_{-\infty}^\infty H_v(\xi) H_{v + 1}(\xi) e^{-\xi^2} \, d \xi}_0 +
	v \underbrace{\int_{-\infty}^\infty H_v(\xi) H_{v - 1}(\xi) e^{-\xi^2} \, d \xi}_0 \right] = 0.
	\eeq
	\end{sol}
	\begin{task}
	Obliczyć średnią energię potencjalną i kinetyczną jednowymiarowego oscylatora harmonicznego.
	\end{task}
	\begin{sol}
	Na podstawie równania~(\ref{eq:pot_oh}) otrzymujemy
	\beq
	\ev**{V} = \frac{k \ev**{x^2}}{2}.
	\eeq
	Korzystając z zależności~(\ref{eq:wherm_r2}) i~(\ref{eq:wherm_ort}) obliczamy więc
	\begin{eqnarray}
	\nonumber \ev**{x^2} & = & \int_{-\infty}^\infty x^2 \psi_v^2(x) \, dx =
	N_v^2 \left( \frac{\hbar}{m \omega} \right)^\frac{3}{2}
	\int_{-\infty}^\infty \xi^2 H_v^2(\xi) e^{-\xi^2} \, d \xi =
	N_v^2 \left( \frac{\hbar}{m \omega} \right)^\frac{3}{2}
	\int_{-\infty}^\infty \xi H_v(\xi) \xi H_v(\xi) e^{-\xi^2} \, d \xi = \\
	\nonumber & = & \frac{N_v^2 \hbar}{m \omega} \left[
	\frac{1}{4} \int_{-\infty}^\infty \sqrt{\frac{\hbar}{m \omega}} H_{v + 1}^2(\xi) e^{-\xi^2} \, d \xi +
	v^2 \int_{-\infty}^\infty \sqrt{\frac{\hbar}{m \omega}} H_{v - 1}^2(\xi) e^{-\xi^2} \, d \xi \right] = \\
	 & = & \frac{N_v^2 \hbar}{m \omega} \left( \frac{1}{4 N_{v + 1}^2} + \frac{v^2}{N_{v - 1}^2} \right),
	\end{eqnarray}
	a następnie
	\beq
	\left( \frac{N_v}{N_{v + 1}} \right)^2 = \frac{2^{v + 1} (v + 1)!}{2^v v!} = 2 (v + 1)
	\eeq
	oraz
	\beq
	\left( \frac{N_v}{N_{v - 1}} \right)^2 = \frac{2^{v - 1} (v - 1)!}{2^v v!} = \frac{1}{2 v}.
	\eeq
	Zatem
	\beq
	\ev**{x^2} = \frac{\hbar}{m \omega} \left( v + \frac{1}{2} \right).
	\eeq
	Ponieważ $k = m \omega^2$, średnia energia potencjalna wynosi
	\beq
	\ev**{V} = \frac{\hbar \omega}{2} \left( v + \frac{1}{2} \right) = \frac{E_v}{2}.
	\eeq
	Ponieważ energia całkowita jest funkcją własną hamiltonianu, zachodzi $\ev**{E_v} = E_v$, a ponieważ na energia ta jest sumą energii potencjalnej i kinetycznej, więc
	\beq
	E_v = \ev**{V} + \ev**{T},
	\eeq
	zatem średnia wartość energii kinetycznej wynosi
	\beq
	\ev**{T} = \frac{\hbar \omega}{2} \left( v + \frac{1}{2} \right) = \frac{E_v}{2}.
	\eeq
	\end{sol}
Gęstość prawdopodobieństwa znalezienia cząstki w danym punkcie wynosi
	\beq
	\rho_v(x) = |\psi_v(x)|^2 = \psi_v^2(x),
	\eeq
bo funkcje falowe oscylatora harmonicznego są rzeczywiste. Na rysunku~\ref{fig:oh_ff_gp} przedstawiono osiem pierwszych funkcji falowych i odpowiadających im gęstości prawdopodobieństwa.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Osc_harm_ff}
	\includegraphics{Scisle_rozwiazania/Rysunki/Osc_harm_gp}
	\caption{Postać kilku pierwszych funkcji falowych i odpowiadających im gęstości prawdopodobieństwa dla oscylatora harmonicznego}
	\label{fig:oh_ff_gp}
	\end{center}
	\end{figure}
Jak widać z rysunku~\ref{fig:oh_ff_gp}, liczba węzłów funkcji falowej wzrasta z liczbą kwantową $v$ i jest jej równa. Dla stanu podstawowego cząstka z największym prawdopodobieństwem przebywa w położeniu równowagi. Dla wyższych stanów kwantowych sytuacja jest odmienna: maksima gęstości prawdopodobieństwa przesuwają się symetrycznie w kierunku brzegów krzywej energii potencjalnej. Przypomina to przypadek klasyczny, dla którego najbardziej prawdopodobnym położeniem jest to odpowiadające maksymalnemu wychyleniu, a więc punkt zwrotu, w którym energia potencjalna jest równa energii całkowitej (i w związku z tym energia kinetyczna jest zerowa). Ale analogia nie jest pełna! W przypadku oscylatora kwantowego pojęcie wychylenia maksymalnego nie ma sensu, bowiem gęstość prawdopodobieństwa jest niezerowa dla wszystkich możliwych wychyleń z położenia równowagi. W przypadku oscylatora klasycznego wychylenie nie może przekroczyć amplitudy drgań $A$, bowiem dla $|x| > A$ energia potencjalna byłaby większa od energii całkowitej, a ponieważ działa zasada zachowania energii, stąd energia kinetyczna byłaby ujemna. W przypadku oscylatora kwantowego nie znamy wartości energii kinetycznej ani potencjalnej, tylko ich wartości średnie. Energia całkowita jest oczywiście wartością własną hamiltonianu, ale operatory $\hat{H}$ i $\hat{V}$ nie komutują.
	\begin{task}
	Obliczyć komutator $\comm{\hat{H}}{\hat{V}}$ dla jednowymiarowego oscylatora harmonicznego.
	\end{task}
	\begin{sol}
	Rozważamy działanie komutatora na dowolną funkcję $f(x)$:
	\begin{align}
		\comm{\hat{H}}{\hat{T}} f(x) & = \comm{\hat{T} + \hat{V}}{\hat{T}} f(x) \nonumber \\
		 & = \comm{\hat{V}}{\hat{T}} f(x) \nonumber \\
		 & = \comm{\frac{k x^2}{2}}{-\frac{\hbar^2}{2 m} \frac{d^2}{d x^2}} f(x) \nonumber \\
		 & = -\frac{k \hbar^2}{4 m} \comm{x^2}{\frac{d^2}{d x^2}} f(x) \nonumber \\
		 & = -\frac{k \hbar^2}{4 m} \pqty\bigg{x^2 f''(x) - \pqty\Big{x^2 f(x)}''} \nonumber \\
		 & = -\frac{k \hbar^2}{4 m} \pqty\bigg{x^2 f''(x) - \pqty\Big{2 x f(x) + x^2 f'(x)}'} \nonumber \\
		 & = \frac{k \hbar^2}{2 m} \pqty\Big{f(x) + 2 x f'(x)},
	\end{align}
	zatem
	\beq
	\comm{\hat{H}}{\hat{T}} = \frac{k \hbar^2}{2 m} \pqty{1 + 2 x \dv{x}}.
	\eeq
	\end{sol}

Bardzo istotną rolę odgrywa \emph{parzystość} funkcji falowej. W ogólnym przypadku dla $v$-tego stanu kwantowego funkcja falowa jest iloczynem funkcji gaussowskiej przez wielomian $v$-tego stopnia zawierający tylko wyrazy parzyste lub tylko nieparzyste w zależności od parzystości $v$. Parzystość liczby kwantowej $v$ określa więc jednoznacznie parzystość funkcji falowej oscylatora harmonicznego:
	\beq
	\left\{ \begin{array}{l}
	\psi_v(x) = \psi_v(x) \mbox{, } v = 2 n \\
	\psi_v(-x) = -\psi_v(x) \mbox{, } v = 2 n + 1
	\end{array} \right. \mbox{, } n \in \mathbb{N} \cup \{0\}.
	\eeq

\subsection{Druga kwantyzacja}
\label{ssc:drkw_oh}

\emph{Druga kwantyzacja} jest alternatywnym podejściem do problemu oscylatora harmonicznego, zaproponowanym przez Paula Diraca. Jest to metoda o tyle użyteczna, że pozwala uniknąć konieczności rozwiązywania równania różniczkowego. Poza tym, metoda ta znajduje szerokie zastosowanie w mechanice kwantowej, znakomicie upraszczając szereg skomplikowanych algebraicznie problemów. Wprowadźmy operator
	\beq
	\ana = \sqrt{\frac{m \omega}{2 \hbar}} \left( x + \frac{i}{m \omega} \hat{p}_x \right).
	\label{eq:an_drkw_oh}
	\eeq
Operator ten oczywiście nie jest hermitowski, bowiem jego sprzężenie hermitowskie jest innym operatorem:
	\beq
	\cra = \sqrt{\frac{m \omega}{2 \hbar}} \left( x - \frac{i}{m \omega} \hat{p}_x \right).
	\label{eq:cr_drkw_oh}
	\eeq
Naszym celem jest zapisanie hamiltonianu dla oscylatora,
	\beq
	\hat{H} = \frac{\hat{p}_x^2}{2 m} + \frac{m \omega^2 x^2}{2},
	\label{eq:hamnzab_oh}
	\eeq
przy użyciu operatorów $\ana$ i \cra. W tym celu wyznaczmy najpierw iloczyn
	\beq
	\begin{split}
	\cra \ana & = \frac{m \omega}{2 \hbar} \left( x - \frac{i}{m \omega} \hat{p}_x \right) \left( x + \frac{i}{m \omega} \hat{p}_x \right) =
	\frac{m \omega}{2 \hbar} \left[ x^2 + \frac{i}{m \omega} \underbrace{(x \hat{p}_x - \hat{p}_x x)}_{[x; \hat{p}_x] = i \hbar} + \frac{\hat{p}_x^2}{m^2 \omega^2} \right] = \\
	 & = \frac{\hat{p}_x^2}{2 \hbar m \omega} + \frac{m \omega x^2}{2 \hbar} - \frac{1}{2} =
	\frac{1}{\hbar \omega} \left( \frac{\hat{p}_x^2}{2 m} + \frac{m \omega^2 x^2}{2} \right) - \frac{1}{2} = \\
	 & = \frac{\hat{H}}{\hbar \omega} - \frac{1}{2},
	\end{split}
	\label{eq:cran_ham_oh}
	\eeq
a więc
	\beq
	\boxed{\hat{H} = \hbar \omega \left( \cra \ana + \frac{1}{2} \right)}.
	\label{eq:ham_drkw_cran_oh}
	\eeq
Oczywiście hamiltonian możemy także wyrazić poprzez iloczyn
	\beq
	\begin{split}
	\ana \cra & = \frac{m \omega}{2 \hbar} \left( x + \frac{i}{m \omega} \hat{p}_x \right) \left( x - \frac{i}{m \omega} \hat{p}_x \right) =
	\frac{m \omega}{2 \hbar} \left[ x^2 + \frac{i}{m \omega} \underbrace{(\hat{p}_x x - x \hat{p}_x)}_{[\hat{p}_x; x] = -i \hbar} + \frac{\hat{p}_x^2}{m^2 \omega^2} \right] = \\
	 & = \frac{\hat{H}}{\hbar \omega} + \frac{1}{2}
	\end{split}
	\label{eq:ancr_ham_oh}
	\eeq
jako
	\beq
	\boxed{\hat{H} = \hbar \omega \left( \ana \cra - \frac{1}{2} \right)}.
	\label{eq:ham_drkw_ancr_oh}
	\eeq

	\begin{task}
	\label{ts:drkw_kom}
	Obliczyć komutatory $\comm{\ana}{\cra}$, $\comm{\hat{H}}{\ana}$ i $\comm{\hat{H}}{\cra}$.
	\end{task}
	\begin{sol}
	Wyznaczamy kolejne komutatory:
	\begin{align}
		\comm{\ana}{\cra} & = \frac{m \omega}{2 \hbar} \comm{\hat{x} + \frac{i}{m \omega} \hat{p}_x}{\hat{x} - \frac{i}{m \omega} \hat{p}_x} \\
		 & = \frac{m \omega}{2 \hbar} \pqty{-\frac{i}{m \omega} \underbrace{\comm{\hat{x}}{\hat{p}_x}}_{i \hbar} + \frac{i}{m \omega} \underbrace{\comm{\hat{p}_x}{ \hat{x}}}_{-i \hbar}} \\
		 & = \frac{m \omega}{2 \hbar} \cdot \frac{2 \hbar}{m \omega} \\
		 & = 1,
	\end{align}

	\beq
	\comm{\hat{H}}{\ana} = \hbar \omega \comm{\cra \ana}{\ana} = \hbar \omega \left( \cra \ana \ana - \ana \cra \ana \right) =
	-\hbar \omega \comm{\ana}{\cra} \ana = -\hbar \omega \ana
	\eeq
	oraz
	\beq
	\comm{\hat{H}}{\cra} = \hbar \omega \comm{\cra \ana}{\cra} = \hbar \omega \left( \cra \ana \cra - \cra \cra \ana \right) =
	\hbar \omega \cra \comm{\ana}{\cra} = \hbar \omega \cra.
	\eeq
	\end{sol}
Zbierzmy dla porządku komutatory obliczone w ćwiczeniu~\ref{ts:drkw_kom}:
	\beq
	\boxed{
	\begin{dcases}
		\comm{\ana}{\cra} = 1 \\
		\comm{\hat{H}}{\ana} = -\hbar \omega \ana \\
		\comm{\hat{H}}{\cra} = \hbar \omega \cra
	\end{dcases}
	}.
	\label{eq:kom_ancr_oh}
	\eeq
Niech $\ket{E}$ będzie stanem własnym $\hat{H}$ z wartością własną $E$, czyli
	\beq
	\hat{H} \ket{E} = E \ket{E}.
	\label{eq:HEEE}
	\eeq
Wynikiem działania operatora, w szczególności operatora \ana, na ten stan jest inny stan, $\ket{P}$:
	\beq
	\ana \ket{E} = \ket{P}.
	\label{eq:aEP}
	\eeq
Stan doń sprzężony znajdujemy z zależności
	\beq
	(\ket{P})^\dag = \bra{P} = \bra{E} \cra.
	\label{eq:Pdag}
	\eeq
Z definicji iloczynu skalarnego wynika, że
	\beq
	\braket{P} \ge 0.
	\label{eq:PP0}
	\eeq
Z zależności~\eqref{eq:cran_ham_oh} i~\eqref{eq:HEEE}--\eqref{eq:PP0} uzyskujemy
	\beq
	\braket{P} = \ev**{\cra \ana}{E} = \ev**{\frac{\hat{H}}{\hbar \omega} - \frac{1}{2}}{E} =
	\frac{E}{\hbar \omega} - \frac{1}{2} \ge 0,
	\eeq
skąd mamy
	\beq
	\boxed{E \ge \frac{1}{2} \hbar \omega}.
	\label{eq:zpe_oh}
	\eeq
Warunek~\eqref{eq:zpe_oh} oznacza, że energia oscylatora harmonicznego nie może być mniejsza aniżeli
	\beq
	E_0 = \frac{1}{2} \hbar \omega.
	\eeq
Dostaliśmy więc od razu energię drgania zerowego!

Przyjrzyjmy się teraz bliżej stanowi $\ket{P}$. Sprawdźmy, korzystając z relacji komutacyjnych~\eqref{eq:kom_ancr_oh}, czy jest to stan własny $\hat{H}$:
	\beq
	\hat{H} \ket{P} = \hat{H} \ana \ket{E} =
		\pqty{\comm{\hat{H}}{\ana} + \ana \hat{H}} \ket{E} =
		\pqty{-\hbar \omega \ana + \ana E} \ket{E} =
		\pqty{E - \hbar \omega} \ket{P}.
	\label{eq:hp_oh}
	\eeq
Tak więc stan $\ket{P}$ jest stanem własnym $\hat{H}$ z wartością własną $E - \hbar \omega$. W podobny sposób zbadajmy stan
	\beq
	\ket{Q} = \cra \ket{E}:
	\eeq

	\beq
	\hat{H} \ket{Q} =
		\hat{H} \cra \ket{E} =
		\pqty{\comm{\hat{H}}{\cra} + \cra \hat{H}} \ket{E} =
		\pqty{\hbar \omega \cra + \ana E} \ket{E} =
		\pqty{E + \hbar \omega} \ket{Q}.
	\label{eq:hq_oh}
	\eeq
A zatem stan $\ket{Q}$ jest stanem własnym $\hat{H}$ z wartością własną $E + \hbar \omega$. Możemy więc stwierdzić, że operatory $\ana$ i $\cra$ działając na stany własne $\hat{H}$ tworzą stany własne tegoż operatora o energii odpowiednio niższej i wyższej, o $\hbar \omega$, od energii stanu wyjściowego. Wiemy już, że energia oscylatora jest ograniczona od dołu --- nie może być niższa niż energia drgań zerowych. Oznaczmy stan o tej energii przez $\ket{0}$:
	\beq
	\hat{H} \ket{0} = E_0 \ket{0} = \frac{1}{2} \hbar \omega \ket{0}.
	\label{eq:h0_oh}
	\eeq
Załóżmy, że stan ten jest unormowany (możemy to zrobić bez starty ogólności naszych rozważań, bo jeśli $\ket{0}$ jest stanem własnym $\hat{H}$, to jest nim także stan $\alpha \ket{0} \text{, } \alpha \in \mathbb{C}$):
	\beq
	\braket{0} = 1.
	\eeq
Z równań~\eqref{eq:hp_oh} i~\eqref{eq:hq_oh} wiemy też, że poziomy oscylatora są równoodległe na skali energii. Utwórzmy teraz stan
	\beq
	\ket{E_1} = \cra \ket{0}.
	\eeq
Na podstawie~\eqref{eq:hq_oh} i~\eqref{eq:h0_oh} wiemy, że energia tego stanu wynosi
	\beq
	E_1 = \frac{3}{2} \hbar \omega.
	\eeq
Powyższą procedurę możemy powtarzać uzyskując stany o coraz większej energii. Po $n$-tym kroku uzyskamy stan
	\beq
	\ket{E_n} = \left( \cra \right)^n \ket{0}
	\eeq
o energii
	\beq
	E_n = \hbar \omega \left(n + \frac{1}{2} \right).
	\label{eq:endrkw_oh}
	\eeq
Chcemy jednak, by uzyskiwane przez nas stany były unormowane. Uzyskujemy to mnożąc uzyskane stany przez stałą normalizacyjną:
	\beq
	\ket{n} = N_n \ket{E_n} = N_n \left( \cra \right)^n \ket{0}
	\eeq
W ten sposób uzyskaliśmy dozwolone energie stanów oscylatora (czyli \emph{drabinkę} poziomów energetycznych). Oczywiście stany $\ket{n}$, jako stany własne operatora $\hat{H}$, są wzajemnie ortogonalne. Ponieważ założyliśmy, że są ponadto unormowane, możemy zapisać
	\beq
	\bra{m}\ket{n} = \delta_{mn}.
	\label{eq:mn_drkw_oh}
	\eeq
Energia oscylatora nie może być jednak ujemna --- a przecież energia stanu
	\beq
	\ket{E_{-1}} = \ana \ket{0}
	\eeq
powinna, zgodnie z~\eqref{eq:hp_oh} i~\eqref{eq:h0_oh}, powinna wynosić $-\frac{1}{2} \hbar \omega$, więc na pierwszy rzut oka mamy sprzeczność. Istnieje jednak wyjście z tej sytuacji. Podziałajmy operatorem $\ana$ na stan $\ket{n}$ i sprzęgnijmy całą operację; jak już wiemy, stanowi $\ana \ket{n}$ odpowiada energia $E_{n - 1}$, a więc z dokładnością do stałej otrzymujemy stan $\ket{n - 1}$:
	\beq
	\ana \ket{n} = \beta_n \ket{n - 1} \Rightarrow \bra{n - 1} \beta_n^* = \bra{n} \cra,
	\label{eq:ana_n_oh}
	\eeq
a ponieważ stany $\ket{n}$ są unormowane, otrzymujemy
	\beq
	\ev**{\cra \ana}{n} = \ev**{\beta_n^* \beta_n}{n - 1} = |\beta_n|^2 \braket{n - 1} = |\beta_n|^2.
	\label{eq:betan_oh}
	\eeq
Korzystając z wyników~\eqref{eq:cran_ham_oh} i~\eqref{eq:endrkw_oh} uzyskujemy
	\beq
	\ev**{\cra \ana}{n} = \ev**{\frac{\hat{H}}{\hbar \omega} - \frac{1}{2}}{n} = \frac{E_n}{\hbar \omega} - \frac{1}{2} =
	n + \frac{1}{2} - \frac{1}{2} = n.
	\eeq
Stąd operator $\cra \ana$ nazywa się \emph{operatorem liczby cząstek}:
	\beq
	\boxed{\cra \ana \ket{n} = n \ket{n}}.
	\label{eq:aan_oh}
	\eeq
Z zależności~\eqref{eq:betan_oh} i~\eqref{eq:aan_oh} otrzymujemy
	\beq
	\beta_n = \sqrt{n},
	\eeq
tak więc
	\beq
	\ana \ket{0} = 0.
	\label{eq:an0_drkw_oh}
	\eeq
Operator $\ana$ \emph{anihiluje} więc stan $\ket{0}$, więc nie uzyskany stan nie ma ujemnej energii, gdyż po prostu stan taki nie istnieje --- jest to właśnie wyjście z omawianej wcześniej pozornej, jak się okazało, sprzeczności.

Podobnie jak w przekształceniu~\eqref{eq:ana_n_oh} działamy teraz operatorem $\cra$ na stan $\ket{n}$ --- otrzymujemy oczywiście, z dokładnością do stałej, stan $\ket{n + 1}$:
	\beq
	\cra \ket{n} = \alpha_n \ket{n + 1} \Rightarrow \bra{n + 1} \alpha_n^* = \bra{n} \ana,
	\eeq
skąd, korzystając z ortonormalności stanów oscylatora, otrzymujemy
	\beq
	\ev**{\ana \cra}{n} = \ev**{\alpha_n^* \alpha_n}{n + 1} = |\alpha_n|^2 \braket{n + 1} = |\alpha_n|^2.
	\eeq
Korzystamy teraz z wyniku~\eqref{eq:ancr_ham_oh}:
	\beq
	\ev**{\ana \cra}{n} = \ev**{\frac{\hat{H}}{\hbar \omega} + \frac{1}{2}}{n} = \frac{E_n}{\hbar \omega} + \frac{1}{2} =
	n + \frac{1}{2} + \frac{1}{2} = n + 1,
	\eeq
tak więc
	\beq
	\alpha_n = \sqrt{n + 1}.
	\eeq
Operatory $\ana$ i $\cra$ działają więc na stan $\ket{n}$ w następujący sposób:
	\beq
	\boxed{
	\begin{cases}
	\ana \ket{n} = \sqrt{n} \ket{n - 1} \\
	\cra \ket{n} = \sqrt{n + 1} \ket{n + 1}
	\end{cases}
	}.
	\label{eq:ancr_n_oh}
	\eeq
Są to odpowiednio \emph{operator anihilacji} (\emph{anihilator}) i \emph{operator kreacji} (\emph{kreator}).

Znajdziemy teraz ogólne wyrażenie na stan $\ket{n}$. Z zależności~\eqref{eq:ancr_n_oh} dostajemy
	\beq
	\cra \ket{n - 1} = \sqrt{n} \ket{n} \Rightarrow \ket{n} = \frac{\cra}{\sqrt{n}} \ket{n - 1} = \frac{\cra}{\sqrt{n}} \frac{\cra}{\sqrt{n - 1}} \ket{n - 2} = \ldots =
	\frac{\cra}{\sqrt{n}} \frac{\cra}{\sqrt{n - 1}} \cdots \frac{\cra}{\sqrt{1}} \ket{0},
	\eeq
zatem
	\beq
	\boxed{\ket{n} = \frac{1}{\sqrt{n!}} \left( \cra \right)^n \ket{0}}.
	\label{eq:stn_drkw_oh}
	\eeq

Naszym kolejnym celem jest wyrażenie operatorów $\hat{x}$, $\hat{x}^2$ i $\hat{p}_x$ w formalizmie drugiej kwantyzacji, czyli przy użyciu operatorów kreacji i anihilacji. Korzystając z równań~\eqref{eq:an_drkw_oh} i~\eqref{eq:cr_drkw_oh} znajdujemy od razu
	\beq
	\boxed{\hat{x} = \sqrt{\frac{\hbar}{2 m \omega}} \left( \ana + \cra \right)},
	\label{eq:x_drkw_oh}
	\eeq

	\beq
	\hat{x}^2 = \frac{\hbar}{2 m \omega} \left( \ana + \cra \right) \left( \ana + \cra \right) =
	\frac{\hbar}{2 m \omega} \left[ \ana^2 + \left( \cra \right)^2 +
	\underbrace{\ana \cra}_{\frac{\hat{H}}{\hbar \omega} + \frac{1}{2}} +
	\underbrace{\cra \ana}_{\frac{\hat{H}}{\hbar \omega} - \frac{1}{2}} \right] =
	\frac{\hbar}{2 m \omega} 	\left[ \ana^2 + \left( \cra \right)^2 + \frac{2 \hat{H}}{\hbar \omega} \right]
	\label{eq:x2_drkw_oh}
	\eeq
oraz
	\beq
	\boxed{\hat{p}_x = i \sqrt{\frac{\hbar \omega m}{2}} \left( \ana - \cra \right)}.
	\label{eq:px_drkw_oh}
	\eeq

	\begin{task}
	\label{ts:mv_drkw_oh}
	Obliczyć średnią wartość wychylenia z położenia równowagi, średnią wartość pędu i średnią energię potencjalną jednowymiarowego oscylatora harmonicznego przy użyciu formalizmu drugiej kwantyzacji.
	\end{task}
	\begin{sol}
	Korzystając z zależności~\eqref{eq:mn_drkw_oh}, \eqref{eq:x_drkw_oh}, \eqref{eq:x2_drkw_oh} i \eqref{eq:px_drkw_oh} dostajemy
	\beq
	\ev**{x} = \sqrt{\frac{\hbar}{2 m \omega}} \ev**{\ana + \cra}{n} =
	\sqrt{\frac{\hbar}{2 m \omega}} \left( \sqrt{n} \bra{n}\ket{n - 1} + \sqrt{n + 1} \bra{n}\ket{n + 1} \right) = 0,
	\eeq

	\beq
	\begin{split}
	\ev**{x^2} & = \frac{\hbar}{2 m \omega} \ev**{\ana^2 + \left( \cra \right)^2 + \frac{2 \hat{H}}{\hbar \omega}}{n} = \\
	 & = \frac{\hbar}{2 m \omega} \left( \sqrt{n (n - 1)} \bra{n}\ket{n - 2} + \sqrt{(n + 1) (n + 2)} \bra{n}\ket{n + 2} + 2 n + 1 \right) = \\
	 & = \frac{\hbar}{m \omega} \left( n + \frac{1}{2} \right),
	\end{split}
	\eeq
	skąd
	\beq
	\ev**{V} = \frac{k \ev**{x^2}}{2} = \frac{m \omega^2 \ev**{x^2}}{2} = \hbar \omega \left( n + \frac{1}{2} \right) = \frac{E_n}{2}.
	\eeq
	Pozostaje jeszcze wyznaczyć
	\beq
	\ev**{p_x} = i \sqrt{\frac{\hbar \omega m}{2}} \ev{\ana - \cra}{n} =
i \sqrt{\frac{\hbar \omega m}{2}} \left( \sqrt{n} \bra{n}\ket{n - 1} - \sqrt{n + 1} \bra{n}\ket{n + 1} \right) = 0.
	\eeq
	\end{sol}
W ćwiczeniu~\eqref{ts:mv_drkw_oh} otrzymaliśmy oczywiście takie same wyniki jak w podrozdziale~\ref{ssc:ok_oh}, gdzie użyliśmy formalizmu \emph{pierwszej kwantyzacji}. Jak widać, druga kwantyzacja znakomicie upraszcza obliczanie wartości średnich.

Zwróćmy uwagę, że używając drugiej kwantyzacji, nigdzie nie operowaliśmy jawną postacią funkcji falowej oscylatora --- taka postać nie była nam w ogóle potrzebna, gdyż interesowały nas wartości oczekiwane operatorów i energie własne. Znalezienie jawnej postaci funkcji wymaga już rozwiązania równania różniczkowego, podobnie jak zrobiliśmy to w podrozdziale~\ref{ssc:ok_oh}.W tym celu do równania~\eqref{eq:an0_drkw_oh} wstawiamy jawną postać operatora anihilacji~\eqref{eq:an_drkw_oh}. Oznaczając $\ket{0} = \psi_0(x)$, w wyniku tej operacji uzyskujemy równanie
	\beq
	\psi'_0(x) = -\frac{m \omega}{\hbar} x \psi_0(x),
	\eeq
którego ogólne rozwiązanie ma postać
	\beq
	\psi_0(x) = N_0 e^{-\frac{m \omega x^2}{2 \hbar}}.
	\eeq
Pozostaje już tylko wyznaczenie stałej normalizacyjnej:
	\beq
	1 = \ev**{\psi_0} = |N_0|^2 \int_{-\infty}^\infty e^{-\frac{m \omega x^2}{\hbar}} \, dx = |N_0|^2 \sqrt{\frac{\pi \hbar}{m \omega}}
	\Rightarrow N_0 = \left( \frac{m \omega}{\pi \hbar} \right)^\frac{1}{4}.
	\eeq
Oznaczając $\ket{n} = \psi_n(x)$ i wstawiając jawną postać operatora kreacji~\eqref{eq:cr_drkw_oh}do równania~\eqref{eq:stn_drkw_oh} otrzymujemy wyrażenie na jawną postać funkcji falowych oscylatora harmonicznego:
	\beq
	\boxed{\psi_n(x) = \frac{1}{\pi^\frac{1}{4} \sqrt{2^n n!}} \left( \frac{m \omega}{\hbar} \right)^\frac{2 n + 1}{4}
	\left( x + \frac{\hbar}{m \omega} \frac{d}{d x} \right)^n e^{-\frac{m \omega x^2}{2 \hbar}}}.
	\label{eq:ff_drwk_oh}
	\eeq
Oczywiście wyrażenia~\eqref{eq:ff_drwk_oh} oraz~\eqref{eq:ff_prwk_oh} są tożsame.
