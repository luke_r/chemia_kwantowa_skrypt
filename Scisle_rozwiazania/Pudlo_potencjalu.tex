\section{Cząstka w pudle potencjału}

\subsection{Przypadek jednowymiarowy}

Cząstka może poruszać się w ograniczonym obszarze wzdłuż osi $x$. Potencjał układu opisany jest równaniem:
	\beq
	V(x) =
	\left\{ \begin{array}{l}
	0 \mbox{, } x \in A = \langle 0; L \rangle, \\
	\infty \mbox{, } x \in A',
	\end{array} \right.
	\eeq
co schematycznie przedstawiono na rysunku~\ref{fig:pot_czwp}.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_1W}
	\caption{Potencjał dla cząstki w pudle jednowymiarowym}
	\label{fig:pot_czwp}
	\end{center}
	\end{figure}
Oznacza to, że cząstka może znajdować się tylko w obszarze $\langle 0; L \rangle$, a ponieważ kwadrat modułu funkcji falowej jest proporcjonalny do gęstosci prawdopodobieństwa, stąd funkcja falowa musi znikać poza tym przedziałem. Równanie Schrödingera dla tego przedziału jest takie samo, jak równanie~(\ref{eq:rs_czsw}), a jego rozwiązania są dane równaniem~(\ref{eq:rs_czsw_rozw}) lub~(\ref{eq:rs_czwp_rozw}). Do naszych rozważań wybierzmy tę drugą postać. Ponieważ funkcja falowa musi być ciągła w całej dziedzinie, muszą być spełnione warunki brzegowe:
	\beq
	\left\{ \begin{array}{l}
	\psi(0) = 0 \\
	\psi(L) = 0
	\end{array} \right..
	\label{eq:wbrz}
	\eeq
Łącząc~(\ref{eq:rs_czwp_rozw}) i~(\ref{eq:wbrz}) otrzymujemy
	\beq
	\left\{ \begin{array}{l}
	a = 0 \\
	k L = n \pi \mbox{, } n \in \mathbb{N}
	\end{array} \right..
	\label{eq:war_brz_rozw}
	\eeq
Łącząc~(\ref{eq:wekt_fal}) i~(\ref{eq:war_brz_rozw}) otrzymujemy wyrażenie na energię cząstki w pudle jednowymiarowym:
	\beq
	E_n = \frac{n^2 h^2}{8 m L^2},
	\eeq
oraz jej funkcje falowe:
	\beq
	\psi_n(x) = b \sin{\frac{n \pi x}{L}}.
	\label{eq:ff_czwp}
	\eeq
Tak więc energia cząstki w pudle jest \emph{skwantowana}. W notacji braketowej Diraca funkcję falową cząstki w stanie kwantowym opisanym liczbą $n$ oznacza się jako $\ket{n}$.
	\begin{task}
	Znajdż znormalizowaną postać funkcji falowej cząstki w pudle.
	\end{task}
	\begin{sol}
	Ponieważ funkcja falowa jest niezerowa tylko wewnątrz pudła, dlatego całkowanie można ograniczyć tylko do tego obszaru. Warunek normalizacji funkcji~(\ref{eq:ff_czwp}) ma postać
	\beq
	b^2 \int_0^L \sin^2{\frac{n \pi x}{L}} = 1.
	\label{eq:wnorm_czwp}
	\eeq
	Obliczamy całkę
	\begin{eqnarray}
	\nonumber \int \sin^2{x} \, d x & = &
	\left\{ \begin{array}{l}
	f(x) = \sin{x} \mbox{, } f'(x) = \cos{x} \\
	g'(x) = \sin{x} \mbox{, } g(x) = -\cos{x}
	\end{array} \right\} =
	-\sin{x} \cos{x} + \int \cos^2{x} \, d x = \\
	 & = & -\sin{x} \cos{x} + \int (1 - \sin^2{x}) \, d x =
	-\sin{x} \cos{x} + x - \int \sin^2{x} \, d x,
	\end{eqnarray}
	skąd
	\beq
	\int \sin^2{x} \, d x = \frac{1}{2} (x - \sin{x} \cos{x}) + C.
	\eeq
	Zatem
	\beq
	\int_0^L \sin^2{\frac{n \pi x}{L}}  =
	\left\{ \begin{array}{l}
	t = \frac{n \pi x}{L} \mbox{, } d t = \frac{n \pi}{L} d x \\
	x = 0 \Rightarrow t =0 \mbox{, } x = L \Rightarrow t = n \pi
	\end{array} \right\} =
	\frac{L}{n \pi} \int_0^{n \pi} \sin^2{t} \, d t = \frac{L}{2}.
	\label{eq:calk_czwp}
	\eeq
	Z równań~(\ref{eq:wnorm_czwp}) i ~(\ref{eq:calk_czwp}) otrzymujemy
	\beq
	b = \sqrt \frac{2}{L},
	\eeq
	zatem unormowana funkcja falowa cząstki w pudle ma postać
	\beq
	\psi_n(x) =  \sqrt \frac{2}{L} \sin{\frac{n \pi x}{L}}.
	\label{eq:ffnorm_1W}
	\eeq
	\end{sol}
Wykresy funkcji falowych dla $n \in \{1; 2; 3; 4\}$ przedstawiono na rysunku~\ref{fig:ff_czwp}.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_1W_ff}
	\caption{Postać kilku pierwszych funkcji falowych dla cząstki w pudle}
	\label{fig:ff_czwp}
	\end{center}
	\end{figure}
Punkty, dla których zachodzi $\psi_n(x) = 0$ i w otoczeniu których funkcja falowa zmienia znak to \emph{węzły}. W ogólnym przypadku mogą to być punkty lub powierzchnie (tzw. \emph{powierzchnie węzłowe}). Jak widać, w przypadku cząstki w pudle liczba węzłów wzrasta wraz z liczbą kwantową $n$. W stanie $\ket{n}$ funkcja falowa posiada $n - 1$ węzłów.
	\begin{task}
	Obliczyć średnią wartość położenia cząstki w pudle w dowolnym stanie kwantowym.
	\end{task}
	\begin{sol}
	Ponieważ $\hat{x} = x$, przeto
	\beq
	\ev**{x} = \ev**{x}{\psi_n} =
	\frac{2}{L} \int_0^L x \sin^2{\frac{n \pi x}{L}} \, d x.
	\label{eq:woczx}
	\eeq
	Obliczamy całkę
	\begin{eqnarray}
	\nonumber \int x \sin^2{x} \, d x & = &
	\left\{ \begin{array}{l}
	f(x) = x \mbox{, } f'(x) = 1 \\
	g'(x) = \sin^2{x} \mbox{, } g(x) = \frac{1}{2} (x - \sin{x} \cos{x})
	\end{array} \right\} = \\
	\nonumber & = & \frac{1}{2} (x^2 - x \sin{x} \cos{x}) - \frac{1}{2} \int (x- \sin{x} \cos{x}) \, d x = \\
	 & = &  \frac{x^2}{4} - \frac{1}{2} x \sin{x} \cos{x} + \frac{1}{2} \int \sin{x} \cos{x} \, d x,
	 \label{eq:c1_woczx}
	\end{eqnarray}
	oraz całkę
	\begin{eqnarray}
	\nonumber \int \sin{x} \cos{x} \, d x & = & \frac{1}{2} \int \sin{2 x} \, d x =
	\frac{1}{2} \{t = 2 x \mbox{, } d t = 2 d x\} =
	\frac{1}{4} \int \sin{t} \, d t = -\frac{1}{4} \cos{t} + C = \\
	 & = & -\frac{1}{4} \cos{2 x} + C.
	\label{eq:c2_woczx}
	\end{eqnarray}
	Wstawiając~(\ref{eq:c2_woczx}) do~(\ref{eq:c1_woczx}) otrzymujemy
	\beq
	\int x \sin^2{x} \, d x = \frac{x^2}{4} - \frac{1}{2} x \sin{x} \ cos{x} - \frac{1}{8} \cos{x} + C,
	\eeq
	czyli
	\beq
	\int_0^L x \sin^2{\frac{n \pi x}{L}} \, d x =
	\left\{ \begin{array}{l}
	t = \frac{n \pi x}{L} \mbox{, } d t = \frac{n \pi}{L} d x \\
	x = 0 \Rightarrow t =0 \mbox{, } x = L \Rightarrow t = n \pi
	\end{array} \right\} =
	\left( \frac{L}{n \pi} \right)^2 \int_0^{n \pi} t \sin^2{t} \, d t = \frac{L^2}{4}.
	\label{eq:c3_woczx}
	\eeq
	Wstawiając~(\ref{eq:c3_woczx}) do~(\ref{eq:woczx}) otrzymujemy
	\beq
	\ev**{x} = \frac{L}{2}.
	\eeq
	\end{sol}
	\begin{task}
	Oblicz wartość średnią pędu cząstki w pudle potencjału.
	\end{task}
	\begin{sol}
	Ponieważ $\hat{p}_x = -i \hbar \frac{d}{d x}$, więc
	\beq
	\ev**{p_x} = \ev**{\hat{p}_x}{\psi_n},
	\label{eq:wocz_px}
	\eeq
	obliczamy
	\beq
	\hat{p}_x \psi_n(x) = -i \hbar \frac{d}{d x} \left( \sqrt{\frac{2}{L}} \sin{\frac{n \pi x}{L}} \right) =
	- \frac{i n h}{\sqrt{2} L^{3/2}} \cos{\frac{n \pi x}{L}}
	\label{eq:pxpsix_1W}
	\eeq
	i wstawiamy do~(\ref{eq:wocz_px}):
	\begin{eqnarray}
	\nonumber \mv{p_x} & = & -\frac{i n h}{L^2} \int_0^L \sin{\frac{n \pi x}{L}} \cos{\frac{n \pi x}{L}} \, d x =
	-\frac{i n h}{L^2}
	\left\{ \begin{array}{l}
	t = \frac{n \pi x}{L} \mbox{, } d t = \frac{n \pi}{L} d x \\
	x = 0 \Rightarrow t =0 \mbox{, } x = L \Rightarrow t = n \pi
	\end{array} \right\} = \\
	 & = & -\frac{i h}{\pi L} \int_0^{n \pi} \sin{t} \cos{t} \, d t = \frac{i \hbar}{4 \pi L} \left. \cos{t} \right|_0^{n \pi} = 0.
	\end{eqnarray}
	\end{sol}
Z równania~(\ref{eq:pxpsix_1W}) wynika, że funkcja $\ket{n}$ nie jest funkcją własną operatora pędu. Korzystając jednak z zależnosci
	\beq
	\sin{\phi} = -\frac{i}{2} \left( e^{i \phi} - e^{-i \phi} \right),
	\eeq
można tę funkcję zapisać w postaci kombinacji liniowej funkcji własnych tego operatora:
	\beq
	\ket{n} = -\frac{i}{\sqrt{2 L}} \left( e^{\frac{i n \pi x}{2 L}} - e^{-\frac{i n \pi x}{2 L}} \right) =
		\frac{1}{\sqrt{2}} \pqty\Big{\ket{n_+} - \ket{n_-}},
	\label{eq:rs_czwp_npm}
	\eeq
zachodzi bowiem
	\beq
	\left\{ \begin{array}{l}
	\hat{p}_x \ket{n_+} = \frac{n h}{2 L} \ket{n_+} \\
	\hat{p}_x \ket{n_-} = -\frac{n h}{2 L} \ket{n_-}
	\end{array} \right..
	\eeq
Zatem pomiar pędu cząstki w pudle da wynik $\frac{n h}{2 L}$ z prawdopodobieństwem wynoszącym $\left| \frac{1}{\sqrt{2}} \right|^2 = \frac{1}{2}$, za{\,s} wynik $-\frac{n h}{2 L}$ --- z prawdopodobieństwem $\left| -\frac{1}{\sqrt{2}} \right|^2 = \frac{1}{2}$, stąd srednia z pomiaró wyniesie $0$. Jest to zgodne z klasycznym obrazem cząstki poruszającej się od jednej scianki do drugiej i odbijającej się bez strat energii od tych scianek. Cząstka taka połowę czasu porusza się w prawo, połowę --- w lewo. W przeciwieństwie do cząstki swobodnej~(\ref{eq:rs_czsw_ind_k}), funkcja cząstki w pudle jest zawsze kombinacją~(\ref{eq:rs_czwp_npm}), cząstka w pudle odbija się bowiem od ścian studni i porusza się w obie strony.

\subsection{Przypadek dwuwymiarowy}

Cząstka zamnknięta jest w pudle dwuwymiarowym:
	\beq
	V(x;y) =
	\left\{ \begin{array}{l}
	0 \mbox{, } (x;y) \in A = \langle 0; L_1 \rangle \times \langle 0; L_2 \rangle \\
	\infty \mbox{, } (x; y) \in A'
	\end{array} \right..
	\eeq
Rozumując analogicznie jak w przypadku dwuwymiarowym dochodzimy do wniosku, że funkcja falowa cząstki nie znika tylko w obszarze $A$. Równanie Schrödingera w tym obszarze ma postać:
	\beq
	-\frac{\hbar^2}{2 m} \left( \frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2} \right)
	\psi(x; y) = E \psi(x; y).
	\label{eq:rs_cz2W}
	\eeq
	\begin{task}
	Wykazać, że jesli hamiltonian $\hat{H}$ daje się przedstawić w postaci sumy hamiltonianów $\hat{H}_1$ i $\hat{H}_2$ zależnych odpowiednio od zmiennych $x$ i $y$, to wówczas funkcję falową układu można zapisać jako
	\beq
	\psi(x; y) = X(x) Y(y),
	\eeq
	zas energię jako
	\beq
	E = E_1 + E_2,
	\eeq
	gdzie
	\beq
	\hat{H}_1 X(x) = E_1 X(x)
	\label{eq:hx_e1x}
	\eeq
	i
	\beq
	\hat{H}_2 Y(y) = E_2 Y(y).
	\label{eq:hy_e2y}
	\eeq
	\end{task}
	\begin{sol}
	Rozpisujemy równanie Schrödingera układu:
	\begin{eqnarray}
	\nonumber \hat{H} \psi(x; y) & = & (\hat{H}_1 + \hat{H}_2) X(x) Y(y) =
	Y(y) \hat{H}_1 X(x) + X(x) \hat{H}_2 Y(y) = \\
	 & = & E_1 X(x) Y(y) + E_2 X(x) Y(y) = (E_1 + E_2) X(x) Y(y) = E \psi(x; y).
	\end{eqnarray}
	\label{ts:separ}
	\end{sol}
Ponieważ hamiltonian występujący w równaniu~(\ref{eq:rs_cz2W}) jest sumą operatorów zależnych wyłącznie od współrzędnych $x$ lub $y$, stąd, korzystając z zależnosci~(\ref{eq:hx_e1x}) i ~(\ref{eq:hy_e2y}), równanie~(\ref{eq:rs_cz2W}) można przedstawić w postaci
	\beq
	 \left\{ \begin{array}{l}
	-\frac{\hbar^2}{2 m} \frac{d^2 X(x)}{d x^2} = E_1 X(x) \\
	-\frac{\hbar^2}{2 m} \frac{d^2 Y(y)}{d y^2} = E_2 Y(y)
	\end{array} \right.,
	\label{eq:rs_cz2W_separ}
	\eeq
przy czym każde z równań układu~(\ref{eq:rs_cz2W_separ}) ma postać taką, jak funkcja~(\ref{eq:ffnorm_1W}). Tak więc funkcja falowa cząstki w pudle dwuwymiarowym ma postać
	\beq
	\psi_{n_1 n_2}(x; y) = \frac{2}{\sqrt{L_1 L_2}} \sin{\frac{n_1 \pi x}{L_1}} \sin{\frac{n_2 \pi y}{L_2}},
	\eeq
a jej energia dana jest wzorem
	\beq
	E_{n_1 n_2} = \left( \frac{n_1^2}{L_1^2} + \frac{n_2^2}{L_2^2} \right) \frac{h^2}{8 m},
	\eeq
gdzie $n_1, n_2 \in \mathbb{N}$. W notacji braketowj Diraca funkcję falową stanu opisanego liczbami kwantowymi $n_1$ i $n_2$ oznacza się jako $\ket{n_1 n_2}$. Na rysunkach~\ref{fig:fig:ff_czwp_11}--\ref{fig:fig:ff_czwp_33} przedstawiono wykresy funkcji falowych dla kilku stanów $\ket{n_1 n_2}$ elektronu znajdującego się w studni o wymiarach $L_1 = 1$~\AA \, i $L_2 = 2$~\AA.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_2W_11}
	\caption{Postać funckcji falowej dla cząstki w dwuwymiarowym pudle potencjału dla $n_1 = 1$ i $n_2 = 1$}
	\label{fig:fig:ff_czwp_11}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_2W_12}
	\caption{Postać funckcji falowej dla cząstki w dwuwymiarowym pudle potencjału dla $n_1 = 1$ i $n_2 = 2$}
	\label{fig:fig:ff_czwp_12}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_2W_13}
	\caption{Postać funckcji falowej dla cząstki w dwuwymiarowym pudle potencjału dla $n_1 = 1$ i $n_2 = 3$}
	\label{fig:fig:ff_czwp_13}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_2W_22}
	\caption{Postać funckcji falowej dla cząstki w dwuwymiarowym pudle potencjału dla $n_1 = 2$ i $n_2 = 2$}
	\label{fig:fig:ff_czwp_22}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_2W_23}
	\caption{Postać funckcji falowej dla cząstki w dwuwymiarowym pudle potencjału dla $n_1 = 2$ i $n_2 = 3$}
	\label{fig:fig:ff_czwp_23}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics{Scisle_rozwiazania/Rysunki/Pudlo_2W_33}
	\caption{Postać funckcji falowej dla cząstki w dwuwymiarowym pudle potencjału dla $n_1 = 3$ i $n_2 = 3$}
	\label{fig:fig:ff_czwp_33}
	\end{center}
	\end{figure}
Widać wyrażnie, jak w miarę wzrostu liczb kwantowych $n_1$ i $n_2$ rosnie liczba płaszczyzn węzłowych. Gdy cząstka znajduje sięw pudle kwadratowym $(L_1 = L_2 = L)$, wówczas wyrażenie na energię przyjmuje postać
	\beq
	E_{n_1 n_2} = \left( n_1^2 + n_2^2 \right) \frac{h^2}{8 m L^2}.
	\label{eq:en_2W_kw}
	\eeq
Jak łatwo zauważyć, stanom $\ket{n_1 n_2}$ i $\ket{n_2 n_1}$ odpowiada ta sama energia dana wzorem~(\ref{eq:en_2W_kw}) i jesli $n_1 \not= n_2$, to stan o tej energii jest \emph{podwójnie zdegenerowany}. W ogólnosci degeneracja jest związana z symetrią układu i rosnie wraz ze wzrostem tejże symetrii. W naszym przypadku widać, że degeneracja opisana przed chwilą nie wystąpiłaby w przypadku pudła niekwadratowego $(L_1 \not= L_2)$.

\subsection{Cząstka na okręgu}

Ruch cząstki odbywa się po okręgu o promieniu~$R$, danym równaniem~$x^2 + y^2 = R^2$. Ruch ten jest wymuszony potencjałem
	\beq
		V(x, y) =
			\begin{cases}
				0, \, & x^2 + y^2 = R^2, \\
				\infty, \, & x^2 + y^2 \not= R^2.
			\end{cases}
		\label{eq:czok_pot}
	\eeq
Przypadek ten najwygodniej jest rozpatrzyć w biegunowym układzie współrzędnych,
	\beq
		\begin{cases}
			x = r \cos{\varphi}, \\
			y = r \sin{\varphi}.
		\end{cases}
		\label{eq:czok_bieg}
	\eeq
Musimy znaleźć postać laplasjanu
	\beq
		\Delta_\mathbf{r} = \frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2}.
	\eeq
w biegunowym układzie współrzędnych. Ponieważ kąt~$\varphi \in [0, 2 \pi)$ nie daje się wyrazić jednoznacznie przy użyciu którejś z odwrotnych funkcji trygonometrycznych, korzystając z~(\ref{eq:czok_bieg}) wyznaczamy najpierw pochodne
	\begin{align}
		\frac{\partial}{\partial r} & =
			\frac{\partial x}{\partial r} \frac{\partial}{\partial x} +
			\frac{\partial y}{\partial r} \frac{\partial}{\partial y} \\
		 & = \cos{\varphi} \frac{\partial}{\partial x} + \sin{\varphi} \frac{\partial}{\partial y} \label{eq:czok_d_dx}
	\end{align}
i
	\begin{align}
		\frac{\partial}{\partial \varphi} & =
			\frac{\partial x}{\partial \varphi} \frac{\partial}{\partial x} +
			\frac{\partial y}{\partial \varphi} \frac{\partial}{\partial y} \\
		 & = -r \sin{\varphi} \frac{\partial}{\partial x} + r \cos{\varphi} \frac{\partial}{\partial y}. \label{eq:czok_d_dy}
	\end{align}
Po prostych przekształceniach, z~równań~(\ref{eq:czok_d_dx}) i~(\ref{eq:czok_d_dy}) dostajemy
	\beq
		\begin{cases}
			\frac{\partial}{\partial x} =
				\cos{\varphi} \frac{\partial}{\partial r} - \frac{1}{r} \sin{\varphi} \frac{\partial}{\partial \varphi}, \\
			\frac{\partial}{\partial y} = \sin{\varphi} \frac{\partial}{\partial r} + \frac{1}{r} \cos{\varphi} \frac{\partial}{\partial \varphi},
		\end{cases}
		\label{eq:czok_d_dx_d_dy}
	\eeq
a następnie
	\beq
		\Delta_\mathbf{r} =
			\frac{1}{r} \frac{\partial}{\partial r} \left( r \frac{\partial}{\partial r} \right) +
			\frac{1}{r^2} \frac{\partial^2}{\partial \varphi^2}.
	\eeq
Potencjał~(\ref{eq:czok_pot}) nie pozwala cząstce opuścić okręgu, zatem laplasjan przyjmuje postać
	\beq
		\left. \Delta_\mathbf{r} \right|_{r = R} = \frac{1}{R^2} \frac{\partial^2}{\partial \varphi^2},
	\eeq
zaś funkcja falowa zależy tylko od kąta~$\varphi$. Równanie Schrödingera dla naszego problemu ma więc postać
	\beq
		-\frac{\hbar^2}{2 m R^2} \frac{d^2 \psi(\varphi)}{d \varphi^2} = E \psi(\varphi).
		\label{eq:czok_rS}
	\eeq
Wprowadzając wielkość
	\beq
		k^2 = \frac{2 I E}{\hbar^2},
		\label{eq:czok_k2_en}
	\eeq
gdzie
	\beq
		I = m R^2
	\eeq
jest momentem bezwładności cząstki, równanie~(\ref{eq:czok_rS}) przyjmuje postać
	\beq
		\frac{d^2 \psi(\varphi)}{d \varphi^2} = -k^2 \psi(\varphi),
	\eeq
skąd uzyskujemy
	\beq
		\psi(\varphi) = A e^{i k \varphi} + B e^{-i k \varphi}.
		\label{eq:czok_ff_og}
	\eeq
Ponieważ cząstka porusza się po okręgu, musi być spełniony warunek periodyczności, tzn.
	\beq
		\psi(\varphi + 2 \pi) = \psi(\varphi).
		\label{eq:czok_ff_1wart}
	\eeq
Inaczej mówiąc, funkcja~$\psi(\varphi)$ musi być jednowartościowa. Z zależności~(\ref{eq:czok_ff_og}) i~(\ref{eq:czok_ff_1wart}) wynika
	\beq
		k \in \mathbb{Z}.
	\eeq
Ponieważ cząstka porusza się po okręgu w płaszczyźnie~$xy$, jej moment pędu ma tylko składową~zetową. Operator tej składowej we współrzędnych biegunowych łatwo z zależności~(\ref{ts:mom_pd_kart}) i~(\ref{eq:czok_d_dx_d_dy}):
	\beq
		\hat{l}_z = -i \hbar \frac{\partial}{\partial \varphi}.
	\eeq
Ponieważ zachodzi
	\beq
		\hat{l}_z e^{\pm i k \varphi} = \pm k \hbar e^{\pm i k \varphi},
	\eeq
widzimy, że funkcja~(\ref{eq:czok_ff_og}) jest kombinacją funkcji opisujących ruch cząstki w przeciwnych kierunkach. Mamy więc do czynienia z sytuacją analogiczną jak w przypadku cząstki swobodnej~(\ref{eq:rs_czsw_ind_k}). Cząstka w ruchu po okręgu nie zmienia kierunku poruszania, zatem jej stan opisany jest jedną z dwóch funkcji
	\beq
		\Ket{k_\pm} \equiv \psi_\pm(\varphi) = A_\pm e^{\pm i k \varphi}.
	\eeq
Z warunku normalizacji
	\beq
		\int_0^{2 \pi} |\psi_\pm(\varphi)|^2  \, d \varphi = 1
	\eeq
uzyskujemy
	\beq
		\Ket{k_\pm} = \frac{1}{\sqrt{2 \pi}} e^{\pm i k \varphi}.
	\eeq
Stanom~$\Ket{k_+}$ i~$\Ket{k_-}$, zgodnie z~(\ref{eq:czok_k2_en}), odpowiada energia
	\beq
		E_k = \frac{k^2 \hbar^2}{2 I},
		\label{eq:czok_en_k}
	\eeq
zatem stany te mają tę samą energię. Stan o energii~$E_k$ jest dwukrotnie zdegenerowany.

W przeciwieństwie do cząstki swobodnej, energia cząstki na okręgu jest skwantowana i przyjmuje tylko ściśle określone wielkości, dane wzorem~(\ref{eq:czok_en_k}).
