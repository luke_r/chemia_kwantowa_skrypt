\section{Rotator sztywny}

\subsection{Podstawy algebry momentu pędu}
\label{rz:podst_alg}

Cząstka o masie $m$ poruszająca się z prędkością $v$ i opisana wektorem wodzącym $\mathbf{r}$ posiada moment pędu
	\beq
	\mathbf{l} = \mathbf{r} \times \mathbf{p} = \mathbf{r} \times (m \mathbf{v}).
	\eeq
Zajmijmy się teraz ogólnym momentem pędu $\mathbf{j}$ danej cząstki. Niech jego składowe spełniają warunki komutacyjne takie, jak w przypadku orbitalnego momentu pędu:
	\beq
		\begin{dcases}
			\comm{\amq{x}}{\amq{y}} = i \hbar \amq{z} \\
			\comm{\amq{y}}{\amq{z}} = i \hbar \amq{x} \\
			\comm{\amq{z}}{\amq{x}} = i \hbar \amq{y}
		\end{dcases}.
		\label{eq:mp_kom}
	\eeq
Warunek~(\ref{eq:mp_kom}) oznacza, że stan kwantowy układu może być opisany przez co najwyżej jedną ze składowych momentu pędu. Całkowity moment pędu wynosi oczywiście
	\beq
	\mathbf{j}^2 = j^2 = j_x^2 + j_y^2 + j_z^2.
	\eeq
	\begin{task}
	Obliczyć komutator $\comm{\am}{\amq{x}}$.
	\end{task}
	\begin{sol}
	Obliczamy
	\begin{align}
	\comm{\am}{\amq{x}} & =
		[\amq{x}^2 + \amq{y}^2 + \amq{y}^2; \amq{x}] \nonumber \\
		 & = \comm{\amq{y}^2}{\amq{x}} + \comm{\amq{z}^2}{\amq{x}} \nonumber \\
		 & = \amq{y} \comm{\amq{y}}{\amq{x}} + \comm{\amq{y}}{\amq{x}} \amq{y} + \amq{z} \comm{\amq{z}}{\amq{x}} + \comm{\amq{z}}{\amq{x}} \amq{z} \nonumber \\
		 & = -\hbar \amq{y} \amq{z} - i \hbar \amq{z} \amq{y} + i \hbar \amq{z} \amq{y} + i \hbar \amq{y} \amq{z} \nonumber \\
		 & = 0.
\end{align}
	\label{ts:kom_mopp_kw}
	\end{sol}
Co oczywiste, wynik uzyskany w ćwiczeniu~(\ref{ts:kom_mopp_kw}) jest prawdziwy dla wszystkich składowych momentu pędu w przestrzeni izotropowej:
	\beq
	\comm{\am}{\amq{q}} = 0 \mbox{, } q \in \{x; y; z\}.
	\eeq
Wobec tego można skonstruować stany $\ket{j m}$ takie, że ich funkcja falowa jest jednocześnie funkcją własną operatorów kwadratu całkowitego momentu pędu $\am$ i jednej z jego składowych, np. $\amq{z}$:
	\beq
	\left\{ \begin{array}{l}
	\am \ket{j m} = \lambda_j \hbar^2 \ket{j m} \\
	\amq{z} \ket{j m} = m \hbar \ket{j m}
	\end{array} \right..
	\label{eq:ur_j2_jz_jm}
	\eeq
Wartości własne operatora hermitowskiego są liczbami rzeczywistymi, stąd wartości własne kwadratu operatora hermitowskiego są nieujemne. Zatem operator $\amq{x}^2 + \amq{y}^2 = \am - \amq{x}^2$ ma nieujemne wartości własne spełniające równanie własne
	\beq
	(\amq{x}^2 + \amq{y}^2) \ket{j m} = (\lambda_j - m^2) \hbar^2 \ket{j m}.
	\eeq
Dla danego $j$ musi zatem istnieć dolne i górne ograniczenie na wartości $m$: $m \in \langle m_\text{min}; m_\text{max} \rangle$. Wprowadźmy teraz operatory postaci
	\beq
	\left\{ \begin{array}{l}
	\amq{+} = \amq{x} + i \amq{y} \\
	\amq{-} = \amq{x} - i \amq{y}
	\end{array} \right..
	\eeq
	\begin{task}
	Obliczyć komutatory $\comm{\amq{z}}{\amq{\pm}}$ i $\comm{\amq{+}}{\amq{-}}$.
	\end{task}
	\begin{sol}
	Obliczamy
	\beq
	\comm{\amq{z}}{\amq{\pm}} = \comm{\amq{z}}{\amq{x}} \pm i \comm{\amq{z}}{\amq{y}} =
	i \hbar \amq{y} \pm \hbar \amq{x} = \pm \hbar \amq{\pm}
	\eeq
	oraz
	\beq
	\comm{\amq{+}}{\amq{-}} = \comm{\amq{x} + i \amq{y}}{\amq{x} - i \amq{y}} =
	-i \comm{\amq{x}}{\amq{y}} + i \comm{\amq{y}}{\amq{x}} = -2 i \comm{\amq{x}}{\amq{y}} =
	2 \hbar \amq{z}.
	\eeq
	\end{sol}
Oczywiście operatory $\am$ i $\amq{\pm}$ są przemienne:
	\beq
	\comm{\am}{\amq{\pm}} = 0.
	\eeq
Rozważmy funkcję $\amq{\pm} \ket{j m}$. Zauważmy, że
	\beq
	\am \amq{\pm} \ket{j m} = \amq{\pm} \am \ket{j m} =
	\lambda_j \hbar^2 \amq{\pm} \ket{j m}
	\eeq
oraz
	\beq
	\amq{z} \amq{\pm} \ket{j m} =
	\pqty{\amq{\pm} \amq{z} + \comm{\amq{z}}{\amq{\pm}}} \ket{j m} =
	(m \pm 1) \hbar \amq{\pm} \ket{j m},
	\label{eq:jz_jplmin}
	\eeq
tak więc $\amq{\pm} \ket{j m}$ jest funkcją własną operatora $\am$ z wartością własną $\lambda_j \hbar^2$ i operatora $\amq{z}$ z wartością własną $(m \pm 1) \hbar$. W wyniku działania operatora $\amq{\pm}$ na funkcję $\ket{j m}$ otrzymujemy zatem, z dokładnością do czynnika normalizacyjnego $C_{\pm}$, funkcję $\ket{j m \pm 1}$:
	\beq
	\amq{\pm} \ket{j m} = C_{\pm} \ket{j m \pm 1}.
	\label{eq:Cplmin}
	\eeq
Stąd operatory $\amq{+}$ i $\amq{-}$ to \emph{operatory podnoszący} i \emph{opuszczający}.
	\begin{task}
	Wyrazić iloczyny $\amq{+} \amq{-}$ i $\amq{-} \amq{+}$ przez operatory $\am$ i $\amq{z}$.
	\end{task}
	\begin{sol}
	Rozpisujemy
	\begin{align}
		\amq{\pm} \amq{\mp} & = (\amq{x} \pm i \amq{y}) (\amq{x} \mp i \amq{y}) \nonumber \\
		 & = \amq{x}^2 + \amq{y}^2 \mp i (\amq{x} \amq{y} -\amq{y} \amq{x}) \nonumber \\
		 & = \am - \amq{z} \mp i \comm{\amq{x}}{\amq{y}} \nonumber \\
		 & = \am - \amq{z}^2  \pm \hbar \amq{z} \nonumber \\
		 & = \am - \amq{z} (\amq{z} \mp \hbar).
	\label{eq:jpl_jmin}
\end{align}
	\end{sol}
	
Ponieważ zakres wartości, jakie może przyjmować $m$, jest ograniczony, stąd muszą być spełnione zależności:
	\beq
	\amq{+} \ket{j m_\text{max}} = 0
	\label{eq:mmax_0}
	\eeq
i
	\beq
	\amq{-} \ket{j m_\text{min}} = 0.
	\label{eq:mmin_0}
	\eeq
Działając na równania~(\ref{eq:mmax_0}) i~(\ref{eq:mmin_0}) odpowiednio operatorami $\amq{-}$ i $\amq{+}$ oraz korzystając z zależności~(\ref{eq:jpl_jmin}) otrzymujemy
	\beq
	\amq{-} \amq{+} \ket{j m_\text{max}} =
	[\lambda_j - m_\text{max} (m_\text{max} + 1)] \hbar^2 \ket{j m_\text{max}}
	\label{eq:jminpl_mmax}
	\eeq
oraz
	\beq
	\amq{+} \amq{-} \ket{j m_\text{min}} =
	[\lambda_j - m_\text{min} (m_\text{min} - 1)] \hbar^2 \ket{j m_\text{min}}.
	\label{eq:jplmin_mmin}
	\eeq
Łącząc~(\ref{eq:mmax_0}), (\ref{eq:mmin_0}), (\ref{eq:jminpl_mmax}) oraz~(\ref{eq:jplmin_mmin}) otrzymujemy
	\begin{eqnarray}
	\nonumber \left\{ \begin{array}{l}
	\lambda_j - m_\text{max} (m_\text{max} + 1) = 0 \\
	\lambda_j - m_\text{min} (m_\text{min} - 1) = 0
	\end{array} \right. & \Rightarrow &
	m_\text{max} (m_\text{max} + 1) = m_\text{min} (m_\text{min} - 1) \Rightarrow \\
	& \Rightarrow & (m_\text{max} + m_\text{min}) (m_\text{max} - m_\text{min} + 1) = 0,
	\label{eq:mmax_mmin}
	\end{eqnarray}
a ponieważ $m_\text{max} \ge m_\text{min}$, równanie~(\ref{eq:mmax_mmin}) jest spełnione wówczas, gdy
	\beq
	m_\text{max} = -m_\text{min}.
	\label{eq:mmax_minmmin}
	\eeq
Jak wynika z równania~(\ref{eq:jz_jplmin}), kolejne wartości liczby $m$ różnią się o $1$, zatem w ogólności
	\beq
	m_\text{max} - m_\text{min} \in \mathbb{N} \cup \{0\},
	\eeq
co można zapisać jako
	\beq
	m_\text{max} - m_\text{min} = 2 j \mbox{, } j = \frac{n}{2} \mbox{, } n \in \mathbb{N} \cup \{0\}.
	\label{eq:mmax_mmin_2j}
	\eeq
Z równań~(\ref{eq:mmax_minmmin}) i~(\ref{eq:mmax_mmin_2j}) otrzymujemy
	\beq
	\left\{ \begin{array}{l}
	m_\text{max} = j \\
	m_\text{min} = -j
	\end{array} \right.,
	\label{eq:ur_mmax_mmin}
	\eeq
zatem dla danego $j$, $m$ przyjmuje $2 j + 1$ wartości:
	\beq
	m \in \{-j; -j + 1; \ldots; j - 1; j\}.
	\eeq
Z zależności~(\ref{eq:mmax_mmin}) i~(\ref{eq:ur_mmax_mmin}) wynika, że
	\beq
	\lambda_j = j (j + 1).
	\eeq
Teraz możemy już wyznaczyć stałą normalizacyjną z równania~(\ref{eq:Cplmin}). W tym celu, korzystając z oczywistej zależności
	\beq
	\amq{\pm}^\dag = \amq{\mp}
	\eeq
sprzęgamy równanie~(\ref{eq:Cplmin}):
	\beq
	\bra{j m} \amq{\mp} = \bra{j m \pm 1} C_{\pm}^*
	\eeq
i obie strony tak uzyskanej zależności mnożymy skalarnie przez odpowiednie strony równania~(\ref{eq:Cplmin}). Wykorzystując zależność~(\ref{eq:jpl_jmin}) oraz ortonormalność funkcji $\ket{j m}$ otrzymujemy
	\beq
	|C_{\pm}|^2 = \bra{j m} \amq{\mp} \amq{\pm} \ket{j m} =
	j (j + 1) - m (m \pm 1) \hbar^2,
	\eeq
więc
	\beq
	C_{\pm} = \sqrt{j (j + 1) - m (m \pm 1)} \hbar.
	\eeq
	
\subsection{Funkcje falowe momentu pędu}
\label{ff_mompd}

W przypadku $j = l \in \mathbb{N} \cup \{0\}$ wygodnie jest przejść z reprezentacji stanów $\ket{j m}$ w abstrakcyjnej $(2 j + 1)$-wymiarowej przestrzeni wektorowej do reprezentacji tych stanów względem konkretnego układu współrzędnych. Najdogodniejszym układem jest \emph{sferyczny układ współrzędnych}, związany z układem kartezjańskim zależnościami:
	\beq
	\left\{ \begin{array}{l}
	x = r \sin{\theta} \cos{\phi} \\
	y = r \sin{\theta} \sin{\phi} \\
	z = r \cos{\theta}
	\end{array} \right.
	\label{eq:uk_sfer_kart}
	\eeq
skąd łatwo otrzymać zależności odwrotne:
	\beq
	\left\{ \begin{array}{l}
	r = \sqrt{x^2 + y^2 + z^2} \\
	\theta = \arccos{\frac{z}{\sqrt{x^2 + y^2 + z^2}}} \\
	\phi = \arctan{\frac{y}{x}}
	\end{array} \right..
	\label{eq:uk__kart_sfer}
	\eeq
	\begin{task}
	Znaleźć postać operatorów $\hat{l}_x$, $\hat{l}_y$, $\hat{l}_z$ i $\hat{l}^2$ we współrzędnych sferycznych.
	\end{task}
	\begin{sol}
	Wyrażenia te znajdujemy korzystając z postaci tych operatorów we współrzędnych kartezjańskich, otrzymanych tak jak w ćwiczeniu~(\ref{ts:mom_pd_kart}):
	\beq
	\left\{ \begin{array}{l}
	\hat{l}_x = i \hbar \left( z \frac{\partial}{\partial y} - y \frac{\partial}{\partial z} \right) \\
	\hat{l}_y = i \hbar \left( x \frac{\partial}{\partial z} - z \frac{\partial}{\partial x} \right) \\
	\hat{l}_z = i \hbar \left( y \frac{\partial}{\partial x} - x \frac{\partial}{\partial y} \right)
	\end{array} \right.
	\eeq
	oraz z zależności~(\ref{eq:uk__kart_sfer}). W przypadku składowej $x$-owej otrzymujemy:
	\begin{eqnarray}
	\nonumber \hat{l}_x & = & i \hbar \left( z \frac{\partial}{\partial y} - y \frac{\partial}{\partial z} \right) = \\
	 & = & i \hbar \left[ r \cos{\theta} \left( \frac{\partial r}{\partial y} \frac{\partial}{\partial r} +
	\frac{\partial \theta}{\partial y} \frac{\partial}{\partial \theta} +
	\frac{\partial \phi}{\partial y} \frac{\partial}{\partial \phi} \right) -
	r \sin{\theta} \sin{\phi} \left( \frac{\partial r}{\partial z} \frac{\partial}{\partial r} +
	\frac{\partial \theta}{\partial z} \frac{\partial}{\partial \theta} +
	\frac{\partial \phi}{\partial z} \frac{\partial}{\partial \phi} \right)  \right].
	\label{eq:lx_sfer}
	\end{eqnarray}
	Aby przekształcić dalej wyrażenie~(\ref{eq:lx_sfer}), musimy obliczyć szereg pochodnych cząstkowych, na przykład
	\beq
	\frac{\partial r}{\partial y} = \frac{y}{\sqrt{x^2 + y^2 + z^2}} =
	\sin{\theta} \sin{\phi},
	\eeq
	\beq
	\frac{\partial \theta}{\partial y} = -\frac{1}{\sqrt{1 - \frac{z^2}{r^2}}} =
	\frac{\cos{\theta} \sin{\phi}}{r},
	\eeq
	etc. Ostatecznie otrzymujemy
	\beq
	\boxed{\begin{cases}
	\hat{l}_x = i \hbar \left( \sin{\phi} \frac{\partial}{\partial \theta} + \cot{\theta} \cos{\phi} \frac{\partial}{\partial \phi} \right) \\
	\hat{l}_y = i \hbar \left( -\cos{\phi} \frac{\partial}{\partial \theta} + \cot{\theta} \sin{\phi} \frac{\partial}{\partial \phi} \right) \\
	\hat{l}_z = -i \hbar \frac{\partial}{\partial \phi}
	\end{cases}}.
	\label{eq:wsp_sf_lxyz}
	\eeq
	oraz
	\beq
	\boxed{\hat{l}^2 = -\hbar^2 \left[ \frac{1}{\sin{\theta}} \frac{\partial}{\partial \theta} \left( \sin{\theta} \frac{\partial}{\partial \theta} \right) + \frac{1}{\sin^2{\theta}} \frac{\partial^2}{\partial \phi^2} \right]}.
	\label{eq:wsp_sf_l2}
	\eeq
	\label{ts:op_mom_pd_wsp_sfer}
	\end{sol}
	
Zagadnienie własne~(\ref{eq:ur_j2_jz_jm}) w tym przypadku ma postać
	\beq
	\left\{ \begin{array}{l}
	\hat{l}^2 \ket{l m} = l (l + 1) \hbar^2 \ket{l m} \\
	\hat{l}_z \ket{l m} = m \hbar \ket{l m}
	\end{array} \right..
	\label{eq:ur_l2_lz_lm}
	\eeq
Wprowadzając oznaczenie $\sh{l}{m} = \ket{l m}$ i korzystają z zależności~(\ref{eq:wsp_sf_lxyz}), (\ref{eq:wsp_sf_l2}) i~(\ref{eq:ur_l2_lz_lm}) otrzymujemy równania różniczkowe pozwalające wyznaczyć funkcje falowe $\sh{l}{m}$:
	\beq
	-\left[ \frac{1}{\sin{\theta}} \frac{\partial}{\partial \theta} \left( \sin{\theta} \frac{\partial}{\partial \theta} \right) + \frac{1}{\sin^2{\theta}} \frac{\partial^2}{\partial \phi^2} \right] \sh{l}{m} =
	l (l + 1) \sh{l}{m}
	\label{eq:l2_lm}
	\eeq
i
	\beq
	-i \frac{\partial \sh{l}{m}}{\partial \phi} = m \sh{l}{m}.
	\label{eq:lz_lm}
	\eeq
Przyjrzyjmy się równaniu~(\ref{eq:lz_lm}). Łatwo zauważyć, że równanie podobne:
	\beq
	-i \frac{d f(\phi)}{d \phi} = m f(\phi)
	\eeq
ma rozwiązania postaci
	\beq
	f(\phi) = A e^{i m \phi},
	\label{eq:rozw_pod}
	\eeq
gdzie $A$ jest dowolną stałą. W równaniu~(\ref{eq:lz_lm}) występuje jednak funkcja dwóch zmiennych, $\sh{l}{m}$. Zauważmy jednak, że w obu równaniach występuje to samo działanie --- różniczkowanie po zmiennej $\phi$. Rozwiązania równania~(\ref{eq:lz_lm}) będą mieć więc postać analogiczną do funkcji~(\ref{eq:rozw_pod}), ale w tym rozwiązaniu musi pojawić się zależność od zmiennej $\theta$. Tę zależność można wprowadzić tylko poprzez uzmiennienie stałej w rozwiązaniu~(\ref{eq:rozw_pod}). Uwzględniając ponadto zależność od liczb kwantowych $l$ i $m$, rozwiązania równania~(\ref{eq:lz_lm}) przyjmują postać
	\beq
	\sh{l}{m} = \tsh{l}{m} \Phi_m(\phi),
	\eeq
gdzie
	\beq
	\Phi_m(\phi) = C e^{i m \phi},
	\eeq
przy czym $C$ oznacza stałą normalizacyjną, którą można wyznaczyć z warunku
	\beq
	\int_0^{2 \pi} \Phi^*_m(\phi) \Phi_m(\phi) \, d \phi =
	C^2 \int_0^{2 \pi} e^{-i m \phi} e^{i m \phi} \, d \phi = 2 \pi C^2 = 1 \Rightarrow C = \frac{1}{\sqrt{2 \pi}}.
	\eeq
Funkcja $\Phi_m(\phi)$ jako funkcja porządna musi być funkcją jednoznaczną, a więc musi przyjmować tę samą wartość po obrocie o dowolną wielokrotność kąta pełnego:
	\begin{eqnarray}
	\nonumber \Phi_m(\phi + 2 k \pi) = \Phi_m(\phi) \mbox{, } k \in \mathbb{Z} & \Rightarrow &
	e^{i m (\phi + 2 k \pi)} = e^{i m \phi} \Rightarrow e^{2 i k m \pi} = 1 \Rightarrow \\
	 & \Rightarrow & \cos{2 k m \pi} + i \sin{2 k m \pi} = 1 \Leftrightarrow
	m \in \mathbb{Z}. 
	\end{eqnarray}
A zatem \emph{periodyczny warunek brzegowy} prowadzi do ograniczenia możliwych wartości liczby kwantowej $m$ do zbioru liczb całkowitych. Tak właśnie wynika z naszego założenia, a więc potwierdza to słuszność wyboru reprezentacji uczynionego na początku tego podrozdziału. Ostatecznie,
	\beq
	\Phi_m(\phi) = \frac{1}{\sqrt{2 \pi}} e^{i m \phi}.
	\eeq
Wstawiając funkcję
	\beq
	\sh{l}{m} = \frac{1}{\sqrt{2 \pi}} \tsh{l}{m} e^{i m \phi}
	\eeq
do równania~(\ref{eq:l2_lm}) i korzystając z zależności
	\beq
	\frac{\partial^2 \sh{l}{m}}{\partial \phi^2} =
	-\frac{m^2}{\sqrt{2 \pi}} \tsh{l}{m} e^{i m \phi}
	\eeq
otrzymujemy równanie różniczkowe pozwalające wyznaczyć funkcję $\tsh{l}{m}$:
	\beq
	\left[ \frac{1}{\sin{\theta}} \frac{d}{d \theta} \left( \sin{\theta} \frac{d}{d \theta} \right) - \frac{m^2}{\sin{\theta}} + l (l + 1) \right] \tsh{l}{m} = 0.
	\label{eq:theta_lm}
	\eeq
Rozwiązania równania~(\ref{eq:theta_lm}) mają postać
	\beq
	\tsh{l}{m} = N_l^m P_l^{|m|}(\cos{\theta}),
	\eeq
gdzie
	\beq
	N_l^m = \sqrt{\frac{2 l + 1}{2} \frac{(l - |m|)!}{(l + |m|)!}}
	\eeq
jest stałą normalizacyjną,
	\beq
	P_l^{|m|} = (1 - x^2)^{\frac{|m|}{2}} \frac{d^{|m|} P_l(x)}{d x^{|m|}}
	\eeq
jest \emph{stowarzyszonym wielomianem Legendre'a}, zas
	\beq
	P_l(x) = \frac{1}{2^l l!} \frac{d^l (x^2 - 1)^l}{d x^l}
	\eeq
jest \emph{zwykłym wielomianem Legendre'a}. Ostatecznie zatem funkcje falowe dla stanów $\ket{l m}$ mają postać
	\beq
	\boxed{\sh{l}{m} = \frac{N_l^m}{\sqrt{2 \pi}} P_l^{|m|}(\cos{\theta}) e^{i m \phi}}.
	\eeq
Są to tzw. \emph{harmoniki sferyczne}. Zauważmy, że zwykłe wielomiany Legendre'a, a więc i harmoniki sferyczne, są niezerowe tylko dla $|m| \le l$. Fizycznie oznacza to, że rzut wektora nie może być większy od samego wektora. Potwierdza to wyniki, które uzyskaliśmy w podrozdziale~\ref{rz:podst_alg} dla liczb kwantowych opisujących rzut momentu pędu i całkowity moment pędu. Możemy więc stwierdzić, że dokonany przez nas wybór reprezentacji momentu pędu okazał się prawidłowy. W tabeli~\ref{tab:harm_sfer} podano przykłady kilku pierwszych harmonik sferycznych.
	\begin{table}[htbp]
	\caption{Postać harmonik sferycznych dla kilku wartości $l$ i $m$}
	\begin{center}
	\begin{tabu} spread 0pt {*{2}{D{.}{.}{0}}X[$$]}
	\toprule
	l & m & \sh{l}{m} \\
	\midrule
	0 & 0 & \frac{1}{2 \sqrt{\pi}} \\
	1 & 0 & \frac{1}{2} \sqrt{\frac{3}{\pi}} \cos{\theta} \\
	1 & \pm 1 & \frac{1}{2} \sqrt{\frac{3}{2 \pi}} \sin{\theta} e^{\pm i \phi} \\
	2 & 0 & \frac{1}{4} \sqrt{\frac{5}{\pi}} (3 \cos^2{\theta} - 1) \\
	2 & \pm 1 & \frac{1}{2} \sqrt{\frac{15}{2 \pi}} \sin{\theta} \cos{\theta} e^{\pm i \phi} \\
	2 & \pm 2 & \frac{1}{4} \sqrt{\frac{15}{2 \pi}} \sin^2{\theta} e^{\pm 2 i \phi} \\
	\bottomrule
	\end{tabu}
	\end{center}
	\label{tab:harm_sfer}
	\end{table}
	
Harmoniki sferyczne dla $m \not= 0$ są funkcjami zespolonymi. Można jednak uzyskać z nich funkcje rzeczywiste biorąc odpowiednie kombinacje liniowe harmonik zespolonych. W ogólności postępuje się zgodnie ze schematem
	\beq
	\left\{ \begin{array}{l}
	Y_l^{m+} = \frac{1}{2} (Y_l^m + Y_l^{-m}) \\
	Y_l^{m-} = -\frac{i}{2} (Y_l^m - Y_l^{-m})
	\end{array} \right..
	\eeq
Tak uzyskane rzeczywiste funkcje można zwizualizować wykreślając na przykład odpowiadającą im gęstość prawdopodobieństwa:
	\beq
	\rho_l^{m \delta}(\theta; \phi) = \left( Y_l^{m \delta}(\theta; \phi) \right)^2
	\mbox{, } \delta \in \{+; -\}.
	\eeq
Wykresy kilku takich funkcji we współrzędnych kartezjańskich przedstawiono na rysunkach~\ref{fig:Y00}-\ref{fig:Y126-}.
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y00}
	\caption{Wykres funkcji $\rho_0^0$}
	\label{fig:Y00}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y11+}
	\caption{Wykres funkcji $\rho_1^{1+}$}
	\label{fig:Y11+}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y11-}
	\caption{Wykres funkcji $\rho_1^{1-}$}
	\label{fig:Y11-}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y20}
	\caption{Wykres funkcji $\rho_2^0$}
	\label{fig:Y20}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y21+}
	\caption{Wykres funkcji $\rho_2^{1+}$}
	\label{fig:Y21+}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y21-}
	\caption{Wykres funkcji $\rho_2^{1-}$}
	\label{fig:Y21-}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y22+}
	\caption{Wykres funkcji $\rho_2^{2+}$}
	\label{fig:Y22+}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y22-}
	\caption{Wykres funkcji $\rho_2^{2-}$}
	\label{fig:Y22-}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y126+}
	\caption{Wykres funkcji $\rho_{12}^{6+}$}
	\label{fig:Y126+}
	\end{center}
	\end{figure}
	
	\begin{figure}[htbp]
	\begin{center}
	\includegraphics[scale=0.9]{Scisle_rozwiazania/Rysunki/Harm_orb/Y126-}
	\caption{Wykres funkcji $\rho_{12}^{6-}$}
	\label{fig:Y126-}
	\end{center}
	\end{figure}
	
W przypadku połówkowych wartości liczby $j$ nie można skonstruować funkcji falowych $\ket{j m}$ tak jak w przypadku funkcji $\ket{l m}$. Można natomiast zbudować te funkcje wychodząc z funkcji falowych dla $j = s = \frac{1}{2}$, a więc z funkcji spinowych pojedynczego elektronu. Funkcje te mają następującą postać:
	\beq
	\ket{\alpha} = \Ket{\frac{1}{2} \frac{1}{2}} =
	\left[ \begin{array}{c}
	1 \\
	0
	\end{array} \right]
	\label{eq:sp_alpha}
	\eeq
i
	\beq
	\ket{\beta} = \Ket{\frac{1}{2} \, -\frac{1}{2}} =
	\left[ \begin{array}{c}
	0 \\
	1
	\end{array} \right].
	\label{eq:sp_beta}
	\eeq
Przy takim zapisie operatory składowych momentu pędu mają postać macierzową:	
	\beq
	\amq{q} = \hat{s}_q = \frac{\hbar}{2} \sigma_q \mbox{, } q \in \{x; y; z\},
	\eeq
gdzie $\sigma_q$ oznacza macierze Pauliego:
	\beq
	\left\{ \begin{array}{l}
	\sigma_x =
	\left[ \begin{array}{cc}
	0 & 1 \\
	1 & 0
	\end{array} \right] \\ \\
	\sigma_y =
	\left[ \begin{array}{cc}
	0 & -i \\
	i & 0
	\end{array} \right] \\ \\
	\sigma_z =
	\left[ \begin{array}{cc}
	1 & 0 \\
	0 & -1
	\end{array} \right]
	\end{array} \right..
	\eeq
Operatory podnoszenia i opuszczania momentu pędu mają więc postać
	\beq
	\hat{s}_+ = \hat{s}_x + i \hat{s}_y = \hbar
	\left[ \begin{array}{cc}
	0 & 1 \\
	0 & 0
	\end{array} \right]
	\eeq
i
	\beq
	\hat{s}_- = \hat{s}_x - i \hat{s}_y = \hbar
	\left[ \begin{array}{cc}
	0 & 0 \\
	1 & 0
	\end{array} \right].
	\eeq
Operator kwadratu momentu pędu ma postać
	\beq
	\hat{s}^2 = \hat{s}_x^2 + \hat{s}_y^2 + \hat{s}_z^2 = \frac{3}{4} \hbar^2
	\left[ \begin{array}{cc}
	1 & 0 \\
	0 & 1
	\end{array} \right].
	\label{eq:sp2}
	\eeq
Korzystając z zależności~(\ref{eq:sp_alpha})--(\ref{eq:sp2}) uzyskujemy
	\beq
	\left\{ \begin{array}{l}
	\hat{s}_x \ket{\alpha} = \frac{\hbar}{2} \ket{\beta} \\
	\hat{s}_y \ket{\alpha} = \frac{i \hbar}{2} \ket{\beta} \\
	\hat{s}_z \ket{\alpha} = \frac{\hbar}{2} \ket{\alpha} \\
	\hat{s}_+ \ket{\alpha} = \mathbf{0} \\
	\hat{s}_- \ket{\alpha} = \hbar \ket{\beta} \\
	\hat{s}^2 \ket{\alpha} = \frac{3}{4} \hbar^2 \ket{\alpha}
	\end{array} \right. \mbox{, }
	\left\{ \begin{array}{l}
	\hat{s}_x \ket{\beta} = \frac{\hbar}{2} \ket{\alpha} \\
	\hat{s}_y \ket{\beta} = -\frac{i \hbar}{2} \ket{\alpha} \\
	\hat{s}_z \ket{\beta} = \frac{\hbar}{2} \ket{\beta} \\
	\hat{s}_+ \ket{\beta} = \hbar \ket{\alpha} \\
	\hat{s}_- \ket{\beta} = \mathbf{0} \\
	\hat{s}^2 \ket{\beta} = \frac{3}{4} \hbar^2 \ket{\beta}
	\end{array} \right..
	\label{eq:op_sp}
	\eeq

\subsection{Sprzężenie dwóch wektorów momentu pędu w ujęciu mechaniki kwantowej}

W mechanice klasycznej całkowity moment pędu układu złożonego z podukładów o momentach pędu odpowiednio $\mathbf{j}_1$ i $\mathbf{j}_2$ dany jest sumą wektorową
	\beq
	\mathbf{j} = \mathbf{j}_1 + \mathbf{j}_2.
	\label{eq:sp_j}
	\eeq
Moment pędu uzyskany wg równania~(\ref{eq:sp_j}) zachowuje swój charakter także w ujęciu kwantowomechanicznym, w sensie spełnienia relacji komutacyjnych~(\ref{eq:mp_kom}).
	\begin{task}
	Obliczyć komutator składowych $\amq{x}$ i $\amq{y}$ operatora całkowitego momentu pędu~(\ref{eq:sp_j}).
	\end{task}
	\begin{sol}
	Korzystając z zależności~(\ref{eq:mp_kom}), z faktu, iż składowe wektorów dodają się skalarnie oraz z przemienności operatorów z różnych przestrzeni, uzyskujemy
	\beq
	\left[  \amq{x}; \amq{y} \right] = \left[ \amq{1x} + \amq{2x}; \amq{1y} + \amq{2y} \right] =
	\underbrace{\left[ \amq{1x}; \amq{1y} \right]}_{i \hbar \amq{1z}} +
	\underbrace{\left[ \amq{1x}; \amq{2y} \right]}_{0} + \underbrace{\left[ \amq{2x}; \amq{1y} \right]}_{0} +
	\underbrace{\left[ \amq{2x}; \amq{2y} \right]}_{i \hbar \amq{2z}} = i \hbar \amq{z}.
	\eeq
	\end{sol}
W zależności od wyboru zestawu przemiennych operatorów (a więc operatorów mających wspólny zestaw funkcji własnych), dwuciałowemu układowi można przypisać dwa typy funkcji falowych. Przy wyborze operatorów $A = \{\am_1; \amq{1z}; \am_2; \amq{2z}\}$ stany własne układu mają postać $\ket{j_1 m_1 j_2 m_2} \equiv \ket{j_1 m_1} \ket{j_2 m_2}$ i są funkcjami własnymi tych operatorów:
	\beq
	\left\{ \begin{array}{l}
	\am_i \ket{j_1 m_1 j_2 m_2} = j_i (j_i + 1) \hbar^2 \ket{j_1 m_1 j_2 m_2} \\
	\amq{iz} \ket{j_1 m_1 j_2 m_2} = m_i \hbar \ket{j_1 m_1 j_2 m_2} 
	\end{array} \right. \mbox{, } i \in \{1; 2\}.
	\eeq
Wektory własne $\ket{j_1 m_1 j_2 m_2}$ stanowią \emph{reprezentację niesprzężoną} i rozpinają $(2 j_1 + 1)(2 j_2 +1)$-wymiarową przestrzeń. Z kolei wybierając zestaw operatorów $B = \{\am_1; \am_2; \am; \amq{z}\}$, gdzie
	\beq
	\am = (\hat{\jmath}_1 + \hat{\jmath}_2)^2,
	\eeq
stany własne układu możemy zapisać w postaci $\ket{j m} \equiv \ket{j_1 j_2 j m}$, przy czym zagadnienia własne mają postać
	\beq
	\left\{ \begin{array}{l}
	\am_i \ket{j m} = j_i (j_i + 1) \hbar^2 \ket{j m} \\
	\am \ket{j m} = j (j + 1) \hbar^2 \ket{j m} \\
	\amq{z} \ket{j m} = m \hbar \ket{j m}
	\end{array} \right. \mbox{, } i \in \{1; 2\}.
	\eeq
Wektory własne $\ket{j m}$ stanowią \emph{reprezentację sprzężoną} i rozpinają $(2 j + 1)$-wymiarową przestrzeń dla danego $j$. Ponieważ $\bar{\bar{A}} = \bar{\bar{B}}$ (oba zbiory odpowiadają tej samej liczbie obserwabli), obie reprezentacje są równoważne i powiązane ze sobą przez przekształcenia unitarne:
	\beq
	\ket{jm} = \sum_{m_1 = -j_1}^{j_1} \sum_{m_2 = -j_2}^{j_2} \braket{ j_1 m_1 j_2 m_2 | j m } \ket{j_1 m_1 j_2 m_2}
	\eeq
oraz
	\beq
	\ket{j_1 m_1 j_2 m_2} = \sum_{j = |j_1 - j_2|}^{j_1 + j_2} \sum_{m = -j}^{j} \braket{ j_1 m_1 j_2 m_2 | j m } \ket{j m},
	\eeq
gdzie $\braket{ j_1 m_1 j_2 m_2 | j m } \in \mathbb{R}$ to tzw. \emph{współczynniki Clebscha-Gordana}, w miejsce których często używa się \emph{symboli 3-$j$ Wignera} z uwagi na ich większą symetrię:
	\beq
	\jthree{j_1}{j_2}{j_3}{m_1}{m_2}{m_3} = \frac{(-1)^{j_1 - j_2 - m_3}}{\sqrt{2 j_3 + 1}} \braket{ j_1 m_1 j_2 m_2 | j_3 -m_3 }.
	\eeq

\subsection{Separacja translacji dla układu dwóch cząstek}
\label{rz:sep_trans}

Rozpatrzmy ruch układu dwóch cząstek o masach $m_1$ i $m_2$, których położenia opisują wektory wodzące $\mathbf{r}_1 = (x_1; y_1; z_1)$ i $\mathbf{r}_2 = (x_2; y_2; z_2)$. Operator energii kinetycznej takiego układu ma postać
	\beq
	\hat{T} = -\frac{\hbar^2}{2 m_1} \Delta_{\mathbf{r}_1} -\frac{\hbar^2}{2 m_2} \Delta_{\mathbf{r}_2},
	\label{eq:op_en_kin_dwmas}
	\eeq
gdzie
	\beq
	\Delta_{\mathbf{r}_i} = \frac{\partial^2}{\partial x_i^2} + \frac{\partial^2}{\partial y_i^2} + \frac{\partial^2}{\partial z_i^2}.
	\label{eq:lapl_dwmas}
	\eeq
Zatem układ opisywany jest przez sześć współrzędnych położeniowych. Można jednak opisać układ wprowadzając trzy współrzędne środka masy układu $\mathbf{R} = (X; Y; Z)$:
	\beq
	\mathbf{R} = \frac{m_1 \mathbf{r}_1 + m_2 \mathbf{r}_2}{m_1 + m_2}
	\eeq
oraz trzy współrzędne względne $\mathbf{r} = (x; y; z)$:
	\beq
	\mathbf{r} = \mathbf{r}_1 - \mathbf{r}_2.
	\eeq
Aby zapisać hamiltionian w nowych współrzędnych, musimy obliczyć drugie pochodne występujące w laplasjanach~(\ref{eq:lapl_dwmas}). W tym celu obliczamy najpierw pierwsze pochodne według receptury:
	\begin{eqnarray}
	\nonumber \frac{\partial}{\partial x_1} & = & \frac{\partial X}{\partial x_1} \frac{\partial}{\partial X} +
	\frac{\partial Y}{\partial x_1} \frac{\partial}{\partial Y} +
	\frac{\partial Z}{\partial x_1} \frac{\partial}{\partial Z} +
	\frac{\partial x}{\partial x_1} \frac{\partial}{\partial x} +
	\frac{\partial y}{\partial x_1} \frac{\partial}{\partial y} +
	\frac{\partial z}{\partial x_1} \frac{\partial}{\partial z} =
	\frac{\partial X}{\partial x_1} \frac{\partial}{\partial X} +
	\frac{\partial x}{\partial x_1} \frac{\partial}{\partial x} = \\
	 & = & \frac{m_1}{m_1 + m_2} \frac{\partial}{\partial X} + \frac{\partial}{\partial x},
	\end{eqnarray}
	\begin{eqnarray}
	\nonumber \frac{\partial}{\partial x_2} & = & \frac{\partial X}{\partial x_2} \frac{\partial}{\partial X} +
	\frac{\partial Y}{\partial x_2} \frac{\partial}{\partial Y} +
	\frac{\partial Z}{\partial x_2} \frac{\partial}{\partial Z} +
	\frac{\partial x}{\partial x_2} \frac{\partial}{\partial x} +
	\frac{\partial y}{\partial x_2} \frac{\partial}{\partial y} +
	\frac{\partial z}{\partial x_2} \frac{\partial}{\partial z} =
	\frac{\partial X}{\partial x_2} \frac{\partial}{\partial X} +
	\frac{\partial x}{\partial x_2} \frac{\partial}{\partial x} = \\
	 & = & \frac{m_1}{m_1 + m_2} \frac{\partial}{\partial X} - \frac{\partial}{\partial x},
	\end{eqnarray}
podobnie dla pozostałych współrzędnych i następnie drugie pochodne:
	\begin{eqnarray}
	\nonumber \frac{\partial^2}{\partial x_1^2} & = &
	\left( \frac{m_1}{m_1 + m_2} \frac{\partial}{\partial X} + \frac{\partial}{\partial x} \right)
	\left( \frac{m_1}{m_1 + m_2} \frac{\partial}{\partial X} + \frac{\partial}{\partial x} \right) = \\
	 & = & \left( \frac{m_1}{m_1 + m_2} \right)^2 \frac{\partial^2}{\partial X^2} +
	\frac{2 m_1}{m_1 + m_2} \frac{\partial^2}{\partial X \partial x} +
	\frac{\partial^2}{\partial x^2},
	\label{eq:dr_poch_x1}
	\end{eqnarray}
	\begin{eqnarray}
	\nonumber \frac{\partial^2}{\partial x_2^2} & = &
	\left( \frac{m_1}{m_1 + m_2} \frac{\partial}{\partial X} - \frac{\partial}{\partial x} \right)
	\left( \frac{m_1}{m_1 + m_2} \frac{\partial}{\partial X} - \frac{\partial}{\partial x} \right) = \\
	 & = & \left( \frac{m_1}{m_1 + m_2} \right)^2 \frac{\partial^2}{\partial X^2} -
	\frac{2 m_1}{m_1 + m_2} \frac{\partial^2}{\partial X \partial x} +
	\frac{\partial^2}{\partial x^2},
	\label{eq:dr_poch_x2}
	\end{eqnarray}
i znów analogicznie dla pozostałych współrzędnych. Po podstawieniu drugich pochodnych~(\ref{eq:dr_poch_x1}) i~(\ref{eq:dr_poch_x2}) do operatora~(\ref{eq:op_en_kin_dwmas}) otrzymujemy
	\beq
	\boxed{\hat{T} = -\frac{\hbar^2}{2 M} \Delta_\mathbf{R} - \frac{\hbar^2}{2 \mu} \Delta_\mathbf{r}},
	\label{eq:op_en_kin_sm_wt}
	\eeq
gdzie
	\beq
	\Delta_\mathbf{R} = \frac{\partial^2}{\partial X^2} + \frac{\partial^2}{\partial Y^2} + \frac{\partial^2}{\partial Z^2},
	\eeq
	\beq
	\Delta_\mathbf{r} = \frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2} + \frac{\partial^2}{\partial z^2},
	\eeq
$M = m_1 + m_2$ jest całkowitą masą układu, zaś $\mu$ jest \emph{masą zredukowaną} układu:
	\beq
	\frac{1}{\mu} = \frac{1}{m_1} + \frac{1}{m_2}.
	\eeq
Jeśli układ dwóch cząstek porusza się swobodnie w przestrzeni (czyli cząstki ze sobą nie oddziałują, $V = 0$), wówczas operator~(\ref{eq:op_en_kin_sm_wt}) jest hamiltonianem układu. Zauważmy, że można go zapisać w postaci
	\beq
	\hat{H} = \hat{H}_1 + \hat{H}_2,
	\eeq
gdzie $\hat{H}_1$ zależy od zestawu zmiennych $(X; Y; Z)$, a $\hat{H}_2$ --- od $(x; y; z)$. Jak wykazaliśmy w ćwiczeniu~\ref{ts:separ}, oznacza to, iż całkowite równanie Schrödingera dla tego układu:
	\beq
	\hat{H} \psi(\mathbf{R}; \mathbf{r}) = E \psi(\mathbf{R}; \mathbf{r})
	\eeq
można rozdzielić na dwa równania:
	\beq
	\hat{H}_1 \psi_1(\mathbf{R}) = E_1 \psi_1(\mathbf{R})
	\label{eq:dwmas_r1}
	\eeq
i
	\beq
	\hat{H}_2 \psi_2(\mathbf{r}) = E_2 \psi_2(\mathbf{r}),
	\label{eq:dwmas_r2}
	\eeq
przy czym
	\beq
	\psi(\mathbf{R}; \mathbf{r}) = \psi_1(\mathbf{R}) \psi_2(\mathbf{r})
	\eeq
oraz
	\beq
	E = E_1 + E_2.
	\eeq
Zauważmy, że równanie~(\ref{eq:dwmas_r1}) jest \textit{de facto} równaniem Schrödingera dla cząstki swobodnej o masie $m = m_1 + m_2$ opisanej wektorem wodzącym $\mathbf{R}$. Równanie to opisuje zatem ruch translacyjny środka masy układu. Z kolei równanie~(\ref{eq:dwmas_r2}) opisuje ruch cząstki o masie $\mu$ opisanej wektorem wodzącym $\mathbf{r}$.

\subsection{Obraz klasyczny rotatora sztywnego}

\emph{Rotator sztywny} jest układem dwóch cząstek mogących swobodnie rotować w przestrzeni przy zachowaniu stałej odległości między cząstkami. \emph{Moment bezwładności} dla tego układu względem środka masy
	\beq
	I = m_1 r_1^2 + m_2 r_2^2,
	\label{eq:mom_bezwl}
	\eeq
przy czym
	\beq
	r = r_1 + r_2
	\label{eq:r1_r2}
	\eeq
jest odległością rotujących cząstek. $r_1$ i $r_2$ to odległości cząstki pierwszej i drugiej od środka masy, więc zachodzi
	\beq
	m_1 r_1 = m_2 r_2.
	\label{eq:m1r1_m2r2}
	\eeq
Z równań~(\ref{eq:r1_r2}) i~(\ref{eq:m1r1_m2r2}) otrzymujemy
	\beq
	\left\{ \begin{array}{l}
	r_1 = \frac{m_2}{m_1 + m_2} r \\
	r_2 = \frac{m_1}{m_1 + m_2} r
	\end{array} \right.,
	\label{eq:ur_r1_r2}
	\eeq
zaś podstawiając wynik~(\ref{eq:ur_r1_r2}) do wyrażenia~(\ref{eq:mom_bezwl}) uzyskujemy równoważną definicję momentu bezwładności względem środka masy układu:
	\beq
	\boxed{I = \mu r^2},
	\label{eq:mom_bezw_srmas}
	\eeq
gdzie $\mu$ jest masą zredukowaną obu cząstek.

Moment pędu rotatora sztywnego jest sumą momentów pędu obu cząstek:
	\beq
	\mathbf{J} = \mathbf{J}_1 + \mathbf{J}_2 =
	m_1 (\mathbf{r}_1 \times \mathbf{v}_1) + m_2 (\mathbf{r}_2 \times \mathbf{v}_2),
	\label{eq:J_J1J2}
	\eeq
a ponieważ $\measuredangle(\mathbf{r}_i; \mathbf{v}_i) = \frac{\pi}{2}$ oraz wektory $\mathbf{J}_1$ i $\mathbf{J}_2$ są równoległe, stąd wyrażenie~(\ref{eq:J_J1J2}) przyjmuje postać
	\beq
	J = m_1 r_1 v_1 + m_2 r_2 v_2,
	\eeq
a korzystając z zależności pomiędzy prędkością liniową i kątową:
	\beq
	v_i = r_i \omega
	\eeq
uzyskujemy
	\beq
	J = (m_1 r_1^2 + m_2 r_2^2) \omega = I \omega.
	\eeq
Energia rotacji rotatora sztywnego jest sumą energii kinetycznych obu rotujących cząstek:
	\beq
	E = T_1 + T_2 = \frac{m_1 v_1^2}{2} + \frac{m_2 v_2^2}{2} =
	\frac{m_1 r_1^2 + m_2 r_2^2}{2} \omega^2 = \frac{I \omega^2}{2} = \frac{J^2}{2 I}.
	\label{eq:en_rszt_klas}
	\eeq

\subsection{Obraz kwantowy rotatora sztywnego}

Jak już wiemy, rotator sztywny jest układem dwóch cząstek mogących wzajemnie obracać się przy zachowaniu stałej odległości między cząstkami. Hamiltonian takiego układu ma postać~\eqref{eq:op_en_kin_dwmas}, i daje się rozdzielić na człon opisujący ruch translacyjny środka masy i człon opisujący ruch wewnętrzny obu cząstek w układzie srodka mas. Ponieważ w ruchu translacyjnym nie ma dla nas nic ciekawego, przeto zajmiemy się tym drugim członem, a więc będziemy rozwiązywać równanie postaci~\eqref{eq:dwmas_r2}. W równaniu~\eqref{eq:op_en_kin_dwmas} nie ma jednak żadnego ograniczenia na odległość między cząstkami --- układ ma więc trzy stopnie swobody i funkcja będąca rozwiązaniem równania~\eqref{eq:op_en_kin_dwmas} zależy od trzech współrzędnych. W przypadku rotatora sztywnego na współrzędne nakładamy jednak \emph{więz}
	\beq
	x^2 + y^2 + z^2 = r^2 = \text{const}.
	\eeq
Ponieważ układ bez więzów miał trzy stopnie swobody, zaś obecnie na układ nałożony jest jeden więz, przeto układ ma już tylko dwa stopnie swobody i funkcja falowa tego układu zależeć będzie od dwóch zmiennych. Równanie~\eqref{eq:op_en_kin_dwmas} w tym przypadku ma więc postać
	\beq
	-\frac{\hbar^2}{2 \mu} \left. \Delta_{\mathbf{r}} \right|_{r = \text{const}} \psi(\vh{r}) = E \psi(\vh{r}),
	\label{eq:rs_rotszt}
	\eeq
gdzie $\vh{r}$ oznacza współrzędne kątowe wektora $\mathbf{r}$:
	\beq
	\vh{r} = (\theta; \phi),
	\eeq
bowiem najdogodniej jest narzucić więz stałej odległości między cząstkami we współrzędnych sferycznych.
	\begin{task}
	Wyrazić laplasjan $\Delta_\mathbf{r} = \frac{\partial ^2}{\partial x^2} + \frac{\partial ^2}{\partial y^2} + \frac{\partial ^2}{\partial z^2}$ we współrzędnych sferycznych.
	\end{task}
	\begin{sol}
	Korzystają ze związku~(\ref{eq:uk__kart_sfer}) musimy wyrazić drugie pochodne występujące w laplasjanie we współrzędnych sferycznych. W tym celu najpierw wyznaczamy pierwsze pochodne według receptury:
	\beq
	\frac{\partial}{\partial x} = 
	\frac{\partial r}{\partial x} \frac{\partial}{\partial r} +
	\frac{\partial \theta}{\partial x} \frac{\partial}{\partial \theta} +
	\frac{\partial \phi}{\partial x} \frac{\partial}{\partial \phi},
	\eeq
	zaś pochodne cząstkowe obliczyliśmy już w rozdziale~\ref{ff_mompd}. Otrzymujemy
	\beq
	\frac{\partial}{\partial x} =
	\sin{\theta} \cos{\phi} \frac{\partial}{\partial r} +
	\frac{\cos{\theta} \sin{\phi}}{r} \frac{\partial}{\partial \theta} -
	\frac{\sin{\phi}}{r \sin{\theta}} \frac{\partial}{\partial \phi},
	\eeq
	co pozwala nam obliczyć drugą pochodną:
	\beq
	\frac{\partial^2}{\partial x^2} =
	\left( \sin{\theta} \cos{\phi} \frac{\partial}{\partial r} +
	\frac{\cos{\theta} \sin{\phi}}{r} \frac{\partial}{\partial \theta} -
	\frac{\sin{\phi}}{r \sin{\theta}} \frac{\partial}{\partial \phi} \right)
	\left( \sin{\theta} \cos{\phi} \frac{\partial}{\partial r} +
	\frac{\cos{\theta} \sin{\phi}}{r} \frac{\partial}{\partial \theta} -
	\frac{\sin{\phi}}{r \sin{\theta}} \frac{\partial}{\partial \phi} \right).
	\eeq
	Podobnie postępujemy dla pozostałych pochodnych cząstkowych. Po prostych, acz uciążliwych, przekształceniach otrzymujemy
	\beq
	\boxed{\Delta_\mathbf{r} = \frac{1}{r^2} \left[
	\frac{\partial}{\partial r} \left( r^2 \frac{\partial}{\partial r} \right) +
	\frac{1}{\sin{\theta}} \frac{\partial}{\partial \theta} \left( \sin{\theta} \frac{\partial}{\partial \theta} \right) +
	\frac{1}{\sin^2{\theta}} \frac{\partial^2}{\partial \phi^2} \right]}.
	\label{eq:lapl_sfer}
	\eeq
	\end{sol}
Po narzuceniu więzu stałej odległości laplasjan ma więc postać
	\beq
	\boxed{\left. \Delta_\mathbf{r} \right|_{r = \text{const}} = \frac{1}{r^2} \left[
	\frac{1}{\sin{\theta}} \frac{\partial}{\partial \theta} \left( \sin{\theta} \frac{\partial}{\partial \theta} \right) +
	\frac{1}{\sin^2{\theta}} \frac{\partial^2}{\partial \phi^2} \right]}.
	\label{eq:lapl_sfer_rc}
	\eeq
Teraz możemy zapisać już hamiltonian rotatora sztywnego:
	\beq
	\hat{H} = -\frac{\hbar^2}{2 I} \left[
	\frac{1}{\sin{\theta}} \frac{\partial}{\partial \theta} \left( \sin{\theta} \frac{\partial}{\partial \theta} \right) +
	\frac{1}{\sin^2{\theta}} \frac{\partial^2}{\partial \phi^2} \right],
	\eeq
gdzie $I$ jest momentem bezwładności rotatora, określonym równaniem~(\ref{eq:mom_bezw_srmas}), oraz równanie Schrödingera:
	\beq
	-\frac{\hbar^2}{2 I} \left[
	\frac{1}{\sin{\theta}} \frac{\partial}{\partial \theta} \left( \sin{\theta} \frac{\partial}{\partial \theta} \right) +
	\frac{1}{\sin^2{\theta}} \frac{\partial^2}{\partial \phi^2} \right]
	\psi(\theta; \phi) = E \psi(\theta; \phi).
	\label{eq:rs_rot_szt}
	\eeq
Jest to równanie analogiczne do równania~(\ref{eq:l2_lm}), różni się jedynie stałą. Zatem zagadnieniu własnemu~(\ref{eq:rs_rot_szt}) odpowiadają takie same funkcje własne jak w przypadku równania~(\ref{eq:l2_lm}), a więc harmoniki sferyczne, w przypadku rotatora sztywnego oznaczane zwyczajowo jako
	\beq
	\ket{J M} = \sh{J}{M}
	\eeq
zaś wartość własna, czyli energia rotatora sztywnego, jest, podobnie jak moment pędu, kwantowana. Możemy ją obliczyć wstawiając do wyrażenia klasycznego~\eqref{eq:en_rszt_klas} wyrażenie kwantowe na kwadrat momentu pędu:
	\beq
	|\mathbb{J}|^2 = J (J + 1) \hbar^2.
	\eeq
Uzyskujemy
	\beq
	\boxed{E_J = \frac{\hbar^2}{2 I} J (J + 1) = B J (J + 1)},
	\label{eq:en_rot_szt}
	\eeq
gdzie
	\beq
	B = \frac{\hbar^2}{2 I} = \frac{\hbar^2}{2 \mu r^2}
	\eeq
jest \emph{stałą rotacyjną}. Oczywiście liczby kwantowe $J$ i $M$ spełniają zależności
	\beq
	J \in \mathbb{N} \cap \{0\}
	\eeq
oraz
	\beq
	M \in \{-J; -J + 1; \ldots; J - 1; J\},
	\eeq
więc dla danego $J$ liczba $M$ przyjmuje $2 J + 1$ wartości. Oznacza to, że dla danego $J$ mamy do czynienia z $(2 J + 1)$-krotną degeneracją, gdyż w wyrażeniu na energię~(\ref{eq:en_rot_szt}) występuję tylko zależność od $J$, zaś funkcja falowa zależy od liczb $J$ i $M$.
