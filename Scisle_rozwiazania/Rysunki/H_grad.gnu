set terminal postscript eps enhanced colour solid "Times New Roman 12"

epsilon0 = 8.854187817E-12 #F/m
e = 1.60217653E-19 #C
h = 6.6260693E-34 #J*s
hbar = h / (2 * pi)
me = 9.1093826E-31 #kg
mp = 1.67262171E-27 #kg
mu = me * mp / (me + mp)
a0 = 4 * pi * epsilon0 * hbar**2 / (mu * e**2)

N = a0**(-3 / 2)

R10(r) = 2 * N * exp(-r / a0)
R20(r) = N / (2 * sqrt(2)) * (2 - r / a0) * exp(-r / (2 * a0))
R21(r) = N / (2 * sqrt(6)) * r / a0 * exp(-r / (2 * a0))
R30(r) = 4 * N / (81 * sqrt(3)) * (27 / 2 - 9 * r / a0 + (r / a0)**2) * exp(-r / (3 * a0))
R31(r) = 4 * N / (81 * sqrt(6)) * (6 - r / a0) * exp(-r / (3 * a0))
R32(r) = 4 * N / (81 * sqrt(30)) * (r / a0)**2 * exp(-r / (3 * a0))

rho10(r) = r**2 * R10(r)**2
rho20(r) = r**2 * R20(r)**2
rho21(r) = r**2 * R21(r)**2
rho30(r) = r**2 * R30(r)**2
rho31(r) = r**2 * R31(r)**2
rho32(r) = r**2 * R32(r)**2

set output "grad.eps"
set grid
set xlabel "{/Italic r}/{/Italic a}_0"
set ylabel "{/Symbol r}({/Italic r})/eV"

ltp = 1
lwd = 4

set output "grad_10.eps"
rk = 5 * a0
set ylabel "{/Symbol-Oblique r}_{10}({/Italic r})/m^2"
set xtics(0,"1" a0, "2" 2 * a0,"3" 3 * a0,"4" 4 * a0,"5" 5 * a0)
plot [r = 0:rk] rho10(r) notitle w l lt ltp lw lwd

set output "grad_20.eps"
rk = 15 * a0
set ylabel "{/Symbol-Oblique r}_{20}({/Italic r})/m^2"
set xtics(0,"5" 5 * a0, "10" 10 * a0,"15" 15 * a0)
plot [r = 0:rk] rho20(r) notitle w l lt ltp lw lwd

set output "grad_21.eps"
rk = 15 * a0
set ylabel "{/Symbol-Oblique r}_{21}({/Italic r})/m^2"
set xtics(0,"5" 5 * a0, "10" 10 * a0,"15" 15 * a0)
plot [r = 0:rk] rho21(r) notitle w l lt ltp lw lwd

set output "grad_30.eps"
rk = 25 * a0
set ylabel "{/Symbol-Oblique r}_{30}({/Italic r})/m^2"
set xtics(0,"5" 5 * a0, "10" 10 * a0,"15" 15 * a0,"20" 20 * a0,"25" 25 * a0)
plot [r = 0:rk] rho30(r) notitle w l lt ltp lw lwd

set output "grad_31.eps"
rk = 20 * a0
set ylabel "{/Symbol-Oblique r}_{31}({/Italic r})/m^2"
set xtics(0,"5" 5 * a0, "10" 10 * a0,"15" 15 * a0,"20" 20 * a0)
plot [r = 0:rk] rho31(r) notitle w l lt ltp lw lwd

set output "grad_32.eps"
rk = 25 * a0
set ylabel "{/Symbol-Oblique r}_{32}({/Italic r})/m^2"
set xtics(0,"5" 5 * a0, "10" 10 * a0,"15" 15 * a0,"20" 20 * a0,"25" 25 * a0)
plot [r = 0:rk] rho32(r) notitle w l lt ltp lw lwd