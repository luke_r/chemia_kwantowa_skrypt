set terminal postscript eps enhanced colour solid "NewCenturySchlbk-Roman" 12

set grid
set pm3d map
set isosample 100
set xlabel "{/Italic x}/m"
set ylabel "{/Italic y}/m"
set zlabel "{/Symbol-Oblique y}_{{/Italic n}_1{/Italic n}_2}({/Italic x}; {/Italic y})/m^{-2}" 0,2
set key below

h = 6.6260693E-34 #J*s
m = 9.1093826E-31 #kg
ang = 1E-10 #m
L1 = 1 * ang
L2 = 3 * ang

psi(x,y) = 2 / sqrt(L1 * L2) * sin(n1 * pi * x / L1) * sin(n2 * pi * y / L2)

set output "Pudlo_2W_11.eps"
splot [0:L1] [0:L2] n1 = 1, n2 = 1, psi(x, y) notitle 

set output "Pudlo_2W_12.eps"
splot [0:L1] [0:L2] n1 = 1, n2 = 2, psi(x, y) notitle

set output "Pudlo_2W_22.eps"
splot [0:L1] [0:L2] n1 = 2, n2 = 2, psi(x, y) notitle

set output "Pudlo_2W_13.eps"
splot [0:L1] [0:L2] n1 = 1, n2 = 3, psi(x, y) notitle 

set output "Pudlo_2W_23.eps"
splot [0:L1] [0:L2] n1 = 2, n2 = 3, psi(x, y) notitle

set output "Pudlo_2W_33.eps"
splot [0:L1] [0:L2] n1 = 3, n2 = 3, psi(x, y) notitle
