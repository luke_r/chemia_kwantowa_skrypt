set terminal postscript eps enhanced colour solid "Times New Roman 12"

epsilon0 = 8.854187817E-12 #F/m
e = 1.60217653E-19 #C
h = 6.6260693E-34 #J*s
hbar = h / (2 * pi)
me = 9.1093826E-31 #kg
mp = 1.67262171E-27 #kg
mu = me * mp / (me + mp)
a0 = 4 * pi * epsilon0 * hbar**2 / (mu * e**2)

V(r) = - e**2 / (4 * pi * epsilon0 * r)
U(r) = (l * (l + 1) * hbar**2 / (2 * me * r**2) + V(r)) / e

set output "ekinrad_epot.eps"
set grid
set xlabel "{/Italic r}/{/Italic a}_0"
set ylabel "{/Italic U}({/Italic r})/eV"
set yrange[-200:200]
set xtics(0,"1" a0, "2" 2 * a0,"3" 3 * a0)

rk = 3 * a0

lwd = 4

plot [r = 0:rk] l = 0, U(r) t "{/Italic l}=0" w l lt 1 lw lwd, \
l = 1, U(r) t "{/Italic l}=1" w l lt 3 lw lwd, \
l = 2, U(r) t "{/Italic l}=2" w l lt 2 lw lwd, \
l = 3, U(r) t "{/Italic l}=3" w l lt 4 lw lwd