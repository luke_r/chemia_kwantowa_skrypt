# tunneling of electron with kinetic energy E through a barrier of length L and height V0

Lang = 10 # Å
V0ev = 1 # eV
m = 9.10938188e-31 # kg

L = Lang * 1e-10
V0 = V0ev * 1.60217646e-19 # J
h = 6.626068e-34 # J * s
hbar = h / (2 * pi)
beta = sqrt(2 * m * V0) * L / hbar

T1(alpha) = (1 + sinh(sqrt(1 - alpha) * beta)**2 / (4 * alpha * (1 - alpha)))**-1
T2(alpha) = 4 / (beta**2 + 4)
T3(alpha) = (1 + sin(sqrt(alpha - 1) * beta)**2 / (4 * alpha * (alpha - 1)))**-1
T(alpha) = 0 < alpha && alpha < 1 ? T1(alpha) : alpha == 1 ? T2(alpha) : T3(alpha)
R(alpha) = 1 - T(alpha)

# plots
set term postscript eps enhanced colour solid "NewCenturySchlbk-Roman" 18
set samples 10000
set style line 1 lt 1 lw 4 pt 2 ps 1.3
set style line 2 lt 3 lw 4 pt 2 ps 1.3
set style line 3 lt -1 lw 4 pt 2 ps 1.3

# trasmittance coefficient plot
set output "coefficients.eps"
set xlabel "{/Symbol-Oblique a}"
pb = sprintf("{/Symbol-Oblique b = }%-6.4g", beta)
set key right center title pb
plot [alpha = 0.5 : 3] \
	T(alpha) t "{/Italic T}({/Symbol a})" w l ls 1, \
	R(alpha) t "{/Italic R}({/Symbol a})" w l ls 2

# wavefunction for E < V0
i = {0, 1}
ctanh(x) = 1 / tanh(x)
B1(alpha) = (2 * alpha - 1 + 2 * i * sqrt(alpha * (1 - alpha)) * ctanh(sqrt(1 - alpha) * beta))**-1
A3(alpha) = (cosh(sqrt(1 - alpha) * beta) + i / 2 * (sqrt((1 - alpha) / alpha) - sqrt(alpha / (1 - alpha))) * sinh(sqrt(1 - alpha) * beta))**-1 * exp(-i * sqrt(alpha) * beta)
A2(alpha) = 0.5 * (1 + i * sqrt(alpha / (1 - alpha))) * A3(alpha) * exp(i * sqrt(alpha) * beta) * exp(-sqrt(1 - alpha) * beta)
B2(alpha) = 0.5 * (1 - i * sqrt(alpha / (1 - alpha))) * A3(alpha) * exp(i * sqrt(alpha) * beta) * exp(sqrt(1 - alpha) * beta)

psi1(alpha, x) = exp(i * sqrt(alpha) * beta * x) + B1(alpha) * exp(-i * sqrt(alpha) * beta * x)
psi2(alpha, x) = A2(alpha) * exp(sqrt(1 - alpha) * beta * x) + B2(alpha) * exp(-sqrt(1 - alpha) * beta * x)
psi3(alpha, x) = A3(alpha) * exp(i * sqrt(alpha) * beta * x)
psi(alpha, x) = x < 0 ? psi1(alpha, x) : x < 1 ? psi2(alpha, x) : psi3(alpha, x)
modpsi(alpha, x) = abs(psi(alpha, x))**2

alpha0 = 0.8

# function module
set output "wavefunction.eps"
set arrow from 0, graph 0 to 0, graph 1 nohead ls 3
set arrow from 1, graph 0 to 1, graph 1 nohead ls 3
set xzeroaxis lt -1
set xlabel "{/Italic x}/{/Italic L}"
set ylabel "|{/Symbol-Oblique y}({/Italic x})|^2"
set xtics add (1)
set y2tics ("|{/Italic A}_3|^2" abs(A3(alpha0)**2))
pab = sprintf("{/Symbol-Oblique a = }%-1.1g, ", alpha0) . pb
set key right top title pab
set xrange [-4 : 5]
plot modpsi(alpha0, x) w l ls 1 notitle
unset ylabel
unset y2tics

# real and imaginary parts
set output "wavefunction_re_im.eps"
set xrange [-2 : 3]
plot \
	real(psi(alpha0, x)) w l ls 1 t "Re{/Symbol-Oblique y}({/Italic x})", \
	imag(psi(alpha0, x)) w l ls 2 t "Im{/Symbol-Oblique y}({/Italic x})"
