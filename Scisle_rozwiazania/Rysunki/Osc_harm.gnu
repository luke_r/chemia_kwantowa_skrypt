set terminal postscript eps enhanced colour solid "Times New Roman 12"

set grid
set xlabel "{/Italic x}"
set ytics ("{/Italic v}=0" 0,"{/Italic v}=1" 2,"{/Italic v}=2" 4,"{/Italic v}=3" 6,"{/Italic v}=4" 8,"{/Italic v}=5" 10,"{/Italic v}=6" 12,"{/Italic v}=7" 14)
set yrange [-1:16]

h = 6.6260693E-34 #J*s
hbar = h / (2 * pi)
u = 1.66053886E-27 #kg
mH = u
mCl = 35 * u
m = mH * mCl / (mH + mCl)
k = 481 #N/m
omega = sqrt(k / m)
ang = 1E-10 #m
L = 0.5 * ang
alpha = sqrt(m * omega / hbar)

set xtics ("" -L/2,"0" 0,"" L/2)

H0(z) = 1
H1(z) = 2 * z
H2(z) = 4 * z**2 - 2
H3(z) = 8 * z**3 - 12 * z
H4(z) = 16 * z**4 - 48 * z**2 + 12
H5(z) = 32 * z**5 - 160 * z**3 + 120 * z
H6(z) = 64 * z**6 - 480 * z**4 + 720 * z**2 - 120
H7(z) = 128 * z**7 - 1344 * z**5 + 3360 * z**3 - 1680 * z
gauss(x) = exp(-(alpha * x)**2/2)
V(x) = k * x**2 / 2
DE(v) = 2 * v

N(v) = 1 / sqrt(2**v * v!) * (2 * m * omega / h)**(1 / 4)

psi0(x) = N(0) * H0(alpha * x) * gauss(x)
psi1(x) = N(1) * H1(alpha * x) * gauss(x)
psi2(x) = N(2) * H2(alpha * x) * gauss(x)
psi3(x) = N(3) * H3(alpha * x) * gauss(x)
psi4(x) = N(4) * H4(alpha * x) * gauss(x)
psi5(x) = N(5) * H5(alpha * x) * gauss(x)
psi6(x) = N(6) * H6(alpha * x) * gauss(x)
psi7(x) = N(7) * H7(alpha * x) * gauss(x)

set output "Osc_harm_ff.eps"

plot [x = -L:L] psi0(x) t "{/Symbol y}_{/Italic v}({/Italic x})" w l lt 3 lw 4, \
psi1(x) + DE(1) notitle w l lt 3 lw 4, \
psi2(x) + DE(2) notitle w l lt 3 lw 4, \
psi3(x) + DE(3) notitle w l lt 3 lw 4, \
psi4(x) + DE(4) notitle w l lt 3 lw 4, \
psi5(x) + DE(5) notitle w l lt 3 lw 4, \
psi6(x) + DE(6) notitle w l lt 3 lw 4, \
psi7(x) + DE(7) notitle w l lt 3 lw 4, \
DE(0) notitle w l lt 7 lw 4, \
DE(1) notitle w l lt 7 lw 4, \
DE(2) notitle w l lt 7 lw 4, \
DE(3) notitle w l lt 7 lw 4, \
DE(4) notitle w l lt 7 lw 4, \
DE(5) notitle w l lt 7 lw 4, \
DE(6) notitle w l lt 7 lw 4, \
DE(7) notitle w l lt 7 lw 4, \
V(x) axes x2y2 t "{/Italic V}({/Italic x})" w l lt 1 lw 4

set output "Osc_harm_gp.eps"

plot [x = -L:L] psi0(x)**2 t "{/Symbol r}_{/Italic v}({/Italic x})" w l lt 3 lw 4, \
psi1(x)**2 + DE(1) notitle w l lt 3 lw 4, \
psi2(x)**2 + DE(2) notitle w l lt 3 lw 4, \
psi3(x)**2 + DE(3) notitle w l lt 3 lw 4, \
psi4(x)**2 + DE(4) notitle w l lt 3 lw 4, \
psi5(x)**2 + DE(5) notitle w l lt 3 lw 4, \
psi6(x)**2 + DE(6) notitle w l lt 3 lw 4, \
psi7(x)**2 + DE(7) notitle w l lt 3 lw 4, \
DE(0) notitle w l lt 7 lw 4, \
DE(1) notitle w l lt 7 lw 4, \
DE(2) notitle w l lt 7 lw 4, \
DE(3) notitle w l lt 7 lw 4, \
DE(4) notitle w l lt 7 lw 4, \
DE(5) notitle w l lt 7 lw 4, \
DE(6) notitle w l lt 7 lw 4, \
DE(7) notitle w l lt 7 lw 4, \
V(x) axes x2y2 t "{/Italic V}({/Italic x})" w l lt 1 lw 4