set terminal postscript eps enhanced colour solid "Times New Roman 12"
set output "Pudlo_1W_ff.eps"

set grid
set xlabel "{/Italic x}/m"
set ylabel "{/Symbol y}_{/Italic n}({/Italic x})/m^{-1/2}

h = 6.6260693E-34 #J*s
m = 9.1093826E-31 #kg
L = 1E-10 #m

psi(x) = sqrt(2 / L) * sin(n * pi * x / L)

plot [x = 0:L] n = 1, psi(x) t "{/Italic n}=1" w l lt 1 lw 2, \
n = 2, psi(x) t "{/Italic n}=2" w l lt 2 lw 2, \
n = 3, psi(x) t "{/Italic n}=3" w l lt 3 lw 2, \
n = 4, psi(x) t "{/Italic n}=4" w l lt 9 lw 2
