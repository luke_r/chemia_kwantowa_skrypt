set terminal postscript eps enhanced colour solid "NewCenturySchlbk-Roman" 18
set output "Transmisja.eps"

a = 2
T(e) = 1 / (1 + sinh(a * sqrt(abs(1 - e)))**2 / (4 * e * abs(1 - e)))

unset label
set xlabel "{/Italic E}/{/Italic V}_0"
set ylabel "{/Italic T}"
set size 1,1
set multiplot
set origin 0,0
plot [e = 0:8] T(e) notitle w l lt 1 lw 4
unset xlabel
unset ylabel
set origin 0.3,0.2
set size 0.65,0.65
plot [e = 6:20] T(e) notitle w l lt 1 lw 4
unset multiplot