function C = Nlm(x,y);
C = sqrt((2 * x + 1)/2 * factorial(x - abs(y))/factorial(x + abs(y)))/sqrt(2 * pi);