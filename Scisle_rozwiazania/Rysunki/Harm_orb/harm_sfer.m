clear all;
ls = 200;
dtheta = pi / ls;
dphi = 2 * pi / ls;
theta = 0:dtheta:pi;
phi = 0:dphi:2*pi;

l = 0; m = 0;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylm = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(1);
mesh(x,y,z);
axis fill;
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

l = 1; m = 0;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylm = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(2);
mesh(x,y,z);
axis fill;
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

l = 1; m = -1;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylma = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

l = 1; m = 1;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylmb = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

Ylm = 1 / 2 * (Ylma + Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(3);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

Ylm = -i / 2 * (Ylma - Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(4);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

l = 2; m = 0;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylm = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(5);
mesh(x,y,z);
axis fill;
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

l = 2; m = -1;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylma = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

l = 2; m = 1;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylmb = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

Ylm = 1 / 2 * (Ylma + Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(6);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

Ylm = -i / 2 * (Ylma - Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(7);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

l = 2; m = -2;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylma = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

l = 2; m = 2;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylmb = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

Ylm = 1 / 2 * (Ylma + Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(8);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

Ylm = -i / 2 * (Ylma - Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(9);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

l = 12; m = -6;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylma = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

l = 12; m = 6;
P = legendre(l,cos(theta));
% P is a (l+1) x q matrix, q = size(theta)
Ylmb = Nlm(l,m) * P(abs(m) + 1,:)' * exp(i * m * phi);

Ylm = 1 / 2 * (Ylma + Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(10);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;

Ylm = -i / 2 * (Ylma - Ylmb);
rho = Ylm.^2;

x = rho .* (sin(theta)' * cos(phi));
y = rho .* (sin(theta)' * sin(phi));
z = rho .* (cos(theta)' * ones(size(phi)));

figure(11);
mesh(x,y,z);
axis equal;
box on;
xlabel('\it x');
ylabel('\it y');
zlabel('\it z');
set(gca,'XTick',0,'YTick',0,'ZTick',0);
hold on;