clear all;
epsilon0 = 8.854187817E-12 %F/m;
e = 1.60217653E-19 %C;
h = 6.6260693E-34 %J*s;
hbar = h / (2 * pi);
me = 9.1093826E-31 %kg;
mp = 1.67262171E-27 %kg;
mu = me * mp / (me + mp);
a0 = 4 * pi * epsilon0 * hbar^2 / (mu * e^2);
N1s = 1 / sqrt(pi * a0^3);
N2p = 1 / (4 * sqrt(2 * pi * a0^5));
N3d = 1 / (81 * sqrt(6 * pi * a0^7));
na = 100;
theta = 0:pi/na:pi;
phi = 0:pi/na:2*pi;
n = length(theta);
m = length(phi);
[theta,phi] = meshgrid(theta,phi);
% theta and phi are now m x n matrices

Ep = 0.01 * a0^(-3 / 2);
r0 = 10 * a0;
c = sprintf('%g * exp(-r / %g) - %g',N1s,a0,Ep);
r = fzero(inline(c,'r'),r0);

x = r * sin(theta) .* cos(phi);
y = r * sin(theta) .* sin(phi);
z = r * cos(theta);

rr = 4 * a0;
tc = -rr:2*a0:rr;
tcs = {'-2','-1','0','1','2'};

figure(1);
h = contour3(x,y,z,100);
box on;
axis equal;
axis([-rr rr -rr rr -rr rr]);
xlabel('\it x\rm /\it a\rm _0');
ylabel('\it y\rm /\it a\rm _0');
zlabel('\it z\rm /\it a\rm _0');
set(gca,'XTick',tc,'YTick',tc,'ZTick',tc);
set(gca,'XTickLabel',tcs,'YTickLabel',tcs,'ZTickLabel',tcs);
saveas(gca,'1s','eps');
hold on;

r = zeros(1,n);

for i = 1:n
    c = sprintf('abs(%g * r * exp(-r / (2 * %g)) * cos(%g)) - %g',N2p,a0,theta(1,i),Ep);
    r(i) = fzero(inline(c,'r'),r0);
end;

index = find(r < 0);
r(index) = 0;
index = find(r == NaN);
r(index) = 0;

x = sin(theta) .* cos(phi);
x = x * diag(r);
y = sin(theta) .* sin(phi);
y = y * diag(r);
z = cos(theta);
z = z * diag(r);

rr = 8 * a0;
tc = -rr:4*a0:rr;
tcs = {'-8','-4','0','4','8'};

figure(2);
contour3(x,y,z,100);
box on;
axis equal;
axis([-rr rr -rr rr -rr rr]);
xlabel('\it x\rm /\it a\rm _0');
ylabel('\it y\rm /\it a\rm _0');
zlabel('\it z\rm /\it a\rm _0');
set(gca,'XTick',tc,'YTick',tc,'ZTick',tc);
set(gca,'XTickLabel',tcs,'YTickLabel',tcs,'ZTickLabel',tcs);
hold on;

x0 = x; y0 = y; z0 = z;
x = z0;
y = y0;
z = -x0;

figure(3);
contour3(x,y,z,100);
box on;
axis equal;
axis([-rr rr -rr rr -rr rr]);
xlabel('\it x\rm /\it a\rm _0');
ylabel('\it y\rm /\it a\rm _0');
zlabel('\it z\rm /\it a\rm _0');
set(gca,'XTick',tc,'YTick',tc,'ZTick',tc);
set(gca,'XTickLabel',tcs,'YTickLabel',tcs,'ZTickLabel',tcs);
hold on;

x = x0;
y = z0;
z = -y0;

figure(4);
contour3(x,y,z,100);
box on;
axis equal;
axis([-rr rr -rr rr -rr rr]);
xlabel('\it x\rm /\it a\rm _0');
ylabel('\it y\rm /\it a\rm _0');
zlabel('\it z\rm /\it a\rm _0');
set(gca,'XTick',tc,'YTick',tc,'ZTick',tc);
set(gca,'XTickLabel',tcs,'YTickLabel',tcs,'ZTickLabel',tcs);
hold on;

r = zeros(1,n);

for i = 1:n
    c = sprintf('abs(%g * r * r * exp(-r / (3 * %g)) * (3 * cos(%g)^2 - 1)) - %g',N3d,a0,theta(1,i),Ep);
    r(i) = fzero(inline(c,'r'),r0);
end;

index = find(r < 0);
r(index) = 0;
index = find(r == NaN);
r(index) = 0;

x = sin(theta) .* cos(phi);
x = x * diag(r);
y = sin(theta) .* sin(phi);
y = y * diag(r);
z = cos(theta);
z = z * diag(r);

rr = 10 * a0;
tc = -rr:5*a0:rr;
tcs = {'-10','-5','0','5','10'};

figure(5);
contour3(x,y,z,100);
box on;
axis equal;
axis([-rr rr -rr rr -rr rr]);
xlabel('\it x\rm /\it a\rm _0');
ylabel('\it y\rm /\it a\rm _0');
zlabel('\it z\rm /\it a\rm _0');
set(gca,'XTick',tc,'YTick',tc,'ZTick',tc);
set(gca,'XTickLabel',tcs,'YTickLabel',tcs,'ZTickLabel',tcs);
hold on;

r = zeros(m,n);
 
for i = 1:m
    for j = 1:n
        c = sprintf('abs(%g * r^2 * exp(-r / (3 * %g)) * sin(%g)^2 * cos(2 * %g)) - %g',sqrt(3) * N3d,a0,theta(1,j),phi(i,1),Ep);
        r(i,j) = fzero(inline(c,'r'),r0);
    end;
end;
 
index = find(r < 0);
r(index) = 0;
index = find(r == NaN);
r(index) = 0;
 
x = sin(theta) .* cos(phi);
y = sin(theta) .* sin(phi);
z = cos(theta);
x = r .* x;
y = r .* y;
z = r .* z;
 
rr = 10 * a0;
tc = -rr:5*a0:rr;
tcs = {'-10','-5','0','5','10'};
 
figure(6);
contour3(x,y,z,100);
box on;
axis equal;
axis([-rr rr -rr rr -rr rr]);
xlabel('\it x\rm /\it a\rm _0');
ylabel('\it y\rm /\it a\rm _0');
zlabel('\it z\rm /\it a\rm _0');
set(gca,'XTick',tc,'YTick',tc,'ZTick',tc);
set(gca,'XTickLabel',tcs,'YTickLabel',tcs,'ZTickLabel',tcs);
hold on;