+ klonowanie:

	```bash
	git clone --recursive git@gitlab.com:luke_r/chemia_kwantowa_skrypt.git
	```

+ repo zawiera moduł z definicjami koniecznymi do kompilacji skryptu: plik `lrajchel_texmf/tex/latex/lrajchel_doc.sty` należy skopiować do głównego katalogu repo bądź do `~/texmf/tex/latex/`

+ jeżeli katalog modułu (`lrajchel_texmf`) jest pusty, należy zainicjować i sklonować moduł:

	```bash
	git submodule update --init --recursive
	```

+ PDF dostępny [tutaj](http://tiger.chem.uw.edu.pl/staff/lrajchel/Skrypt.pdf)
