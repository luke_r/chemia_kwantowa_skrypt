\section{Podstawowe pojęcia algebry liniowej}

\subsection{Przestrzeń wektorowa i jej baza}
\label{wm:pw}

Aby dany zbiór $V$ był \emph{przestrzenią wektorową} nad ciałem $\mathbb{K}$, jego elementy muszą spełniać następujące warunki:
	\begin{enumerate}
	\item
	przemiennosć dodawania:
	$\forall_{\mathbf{u} \wedge \mathbf{v} \in V}: \mathbf{u} + \mathbf{v} = \mathbf{v} + \mathbf{u}$,
	\item
	łącznosć dodawania:
	$\forall_{\mathbf{u}, \mathbf{v} \wedge \mathbf{w} \in V}: (\mathbf{u} + \mathbf{v}) + \mathbf{w} = \mathbf{u} + (\mathbf{v} + \mathbf{w})$,
	\item
	istnienie elementu neutralnego dodawania:
	$\forall_{\mathbf{u} \in V} \exists_{\mathbf{e} \in V}: \mathbf{u} + \mathbf{e} = \mathbf{e} + \mathbf{u} = \mathbf{u}$,
	\item
	istnienie elementu odwrotnego dodawania:
	$\forall_{\mathbf{u} \in V} \exists_{-\mathbf{u} \in V}: \mathbf{u} + (-\mathbf{u}) = (-\mathbf{u}) + \mathbf{u} = \mathbf{e}$,
	\item
	łącznosć mnożenia skalarnego:
	$\forall_{\alpha \wedge \beta \in \mathbb{K}, \, \mathbf{u} \in V}: \alpha (\beta \mathbf{u}) = (\alpha \beta) \mathbf{u}$,
	\item
	rozdzielnosć dodawania skalarnego:
	$\forall_{\alpha \wedge \beta \in \mathbb{K}, \, \mathbf{u} \in V}: (\alpha + \beta) \mathbf{u} = \alpha \mathbf{u} + \beta \mathbf{u}$,
	\item
	rozdzielnosć dodawania wektorowego:
	$\forall_{\alpha \in \mathbb{K}, \, \mathbf{u} \wedge \mathbf{v} \in V}: \alpha (\mathbf{u} + \mathbf{v}) = \alpha \mathbf{u} + \alpha \mathbf{v}$,
	\item
	istnienie elementu neutralnego mnożenia skalarnego:
	$\forall_{\mathbf{u} \in V} \exists_{\epsilon \in \mathbb{K}}: \epsilon \mathbf{u} = \mathbf{u}$.
	\end{enumerate}
Elementy zbioru $V$ nazywamy \emph{wektorami}, zas zbioru $\mathbb{K}$ --- \emph{skalarami}.

\emph{Kombinacją liniową} wektorów $\mathbf{v}_i$ jest wektor postaci
	\beq
	\mathbf{u} = \sum_{i=1}^n \alpha_i \mathbf{v}_i,
	\label{eq:kl}
	\eeq
gdzie $\alpha_i \in \mathbb{K}$.

Wektory $\mathbf{v}_1$, $\mathbf{v}_2$, $\ldots$, $\mathbf{v}_n$ są \emph{liniowo niezależne}, jesli
	\beq
	\alpha_1 \mathbf{v}_1 + \alpha_2 \mathbf{v}_2 + \ldots + \alpha_n \mathbf{v}_n = \mathbf{0} \Leftrightarrow \alpha_1 = \alpha_2 = \ldots = \alpha_n = 0.
	\eeq
	
Zbiór wektorów $\mathcal{B} = \{\mathbf{v}_1, \mathbf{v}_2, \ldots, \mathbf{v}_n\}$ jest \emph{bazą} przestrzeni wektorowej $V$, jesli każdy wektor z tej przestrzeni można przedstawić jako kombinację liniową wektorów z $\mathcal{B}$, jak w równaniu~(\ref{eq:kl}). Liczba wektorów bazy to \emph{wymiar} przestrzeni wektorowej (ang. \emph{dimension}), oznaczany symbolem $\dim V$. Współczynniki w kombinacji liniowej przedstawiającej wektor za pomocą wektorów bazowych to \emph{współrzędne} wektora w danej bazie. Dana przestrzeń $V$ ma wiele baz, ale każda baza składa się z takiej samej liczby wektorów, równej wymiarowi tej przestrzeni.

Oto przykłady przestrzeni wektorowych:
	\begin{enumerate}
	\item
	\label{wm:ppw_pocz}
	$n$-wymiarowa \emph{przestrzeń euklidesowa} $\mathbb{R}^n$ nad ciałem liczb rzeczywistych; wektory tej przestrzeni mają postać
	\beq
		\mathbf{v} =
			\begin{bmatrix}
				x_1 \\
				x_2 \\
				\vdots \\
				x_n
			\end{bmatrix},
	\eeq
gdzie $x_i \in \mathbb{R}$. Dla $n = 1$ otrzymujemy po prostu zbiór liczb rzeczywistych $\mathbb{R}^1$. Dla $n = 2$ przestrzeń ta jest zbiorem wektorów na płaszczyżnie dwuwymiarowej, zas dla $n = 3$ --- zbiorem wektorów w przestrzeni trójwymiarowej. Dla tego ostatniego przypadku jako bazę możemy wybrać np. wersory osi $x$, $y$ i $z$ kartezjańskiego układu współrzędnych:
	\beq
	\mathcal{B}_1 = \{\hat{\mathbf{x}}, \hat{\mathbf{y}}, \hat{\mathbf{z}} \} =
	\left\{
		\begin{bmatrix}
			1 \\
			0 \\
			0
		\end{bmatrix},
		\begin{bmatrix}
			0 \\
			1 \\
			0
		\end{bmatrix},
		\begin{bmatrix}
			0 \\
			0 \\
			1
		\end{bmatrix}
	\right\}.
	\label{eq:baza_B1}
	\eeq
Wektor 
	$\mathbf{u} =
		\begin{bmatrix}
			-2 \\
			-1 \\
			5
	\end{bmatrix}$
jest następującą kombinacją liniową wektorów bazy $\mathcal{B}_1$:
	\beq
		\mathbf{u} = -2
			\begin{bmatrix}
				1 \\
				0 \\
				0
			\end{bmatrix} + (-1)
			\begin{bmatrix}
				0 \\
				1 \\
				0
			\end{bmatrix} + 5
			\begin{bmatrix}
				0 \\
				0 \\
				1
			\end{bmatrix},
	\eeq
tak więc współrzędne wektora $\mathbf{u}$ w bazie $\mathcal{B}_1$ to $\begin{bmatrix} -2 \\  -1 \\ 5 \end{bmatrix}$. Możemy jednak wybrać inną bazę, np.
	\beq
		\mathcal{B}_2 = \left\{
			\begin{bmatrix}
				0 \\
				1 \\
				1
			\end{bmatrix},
			\begin{bmatrix}
				1 \\
				0 \\
				1
			\end{bmatrix},
			\begin{bmatrix}
				1 \\
				1 \\
				0
			\end{bmatrix}
	\right\},
	\eeq
wówczas
	\beq
		\mathbf{u} = 3
			\begin{bmatrix}
				0 \\
				1 \\
				1
			\end{bmatrix} + 2
			\begin{bmatrix}
				1 \\
				0 \\
				1
			\end{bmatrix} + (-4)
			\begin{bmatrix}
				1 \\
				1 \\
				0
			\end{bmatrix},
	\eeq
a więc współrzędne wektora $\mathbf{u}$ w bazie $\mathcal{B}_2$ to $\begin{bmatrix} 3 \\ 2 \\ -4 \end{bmatrix}$. Zauważmy, że np. zbiór
	\beq
		\mathcal{B}_3 = \left\{
			\begin{bmatrix}
				1 \\
				0 \\
				0
			\end{bmatrix},
			\begin{bmatrix}
				0 \\
				1 \\
				0
			\end{bmatrix},
			\begin{bmatrix}
				2 \\
				3 \\
				0
			\end{bmatrix}
	\right\}
	\eeq
nie jest już bazą przestrzeni $\mathbb{R}^3$, bowiem jeden z wektorów jest kombinacją liniową pozostałych dwóch wektorów, np.
	\beq
		\begin{bmatrix}
			2 \\
			3 \\
			0
		\end{bmatrix} = 2
		\begin{bmatrix}
			1 \\
			0 \\
			0
		\end{bmatrix} + 3
		\begin{bmatrix}
			0 \\
			1 \\
			0
		\end{bmatrix},
	\eeq
czyli wektory należące do zbioru $\mathcal{B}_3$ są liniowo zależne (w przypadku przestrzeni trójwymiarowej oznacza to, że trzy wektory leżą na jednej płaszczyżnie). W analogiczny sposób można zdefiniować przestrzeń $\mathbb{C}^n$ nad ciałem liczb zespolonych.
	\item
	zbiór macierzy o wymiarach $m \times n$, $M^{m \times n}$, zbudowanych z liczb zespolonych. W tym przypadku dodawanie wektorowe jest po prostu dodawaniem macierzy. W przypadku, gdy przestrzeń ta jest definiowana nad $\mathbb{C}$, wówczas jej bazą może być np. zbiór $m n$ macierzy zerowych, w których dokładnie jedno zero zastąpiono jedynką\footnote{Ale można też stworzyć bazę złożoną z macierzy zespolonych, w których jedynki macierzy bazy zastąpiono liczbą urojoną $i$. Wówczas tworzenie macierzy rzeczywistych wymaga użycia zespolonych współczynników kombinacji liniowej macierzy bazy. Oczywiście w przypadku tej przestrzeni baza może zawierać zarówno macierze rzeczywiste, jak i zespolone, pod warunkiem, że w wyjściowych macierzach zerowych nie zastąpiono dwukrotnie tego samego zera.} i żadna z macierzy nie pojawia się dwukrotnie. Ponieważ moc takiego zbioru, będącego bazą, wynosi $m n$, jest to przestrzeń $m n$-wymiarowa. W tym wypadku tworzenie macierzy zespolonych wymaga użycia zespolonych współczynników kombinacji liniowej macierzy bazy. W wypadku przestrzeni $M^{m \times n}$ nad ciałem $\mathbb{R}$ baza jest większa --- do poprzedniego zbioru należy dodać jeszcze np. zbiór $m n$ macierzy zerowych, w których dokładnie jedno zero zastąpiono liczbą $i$ i żadna z macierzy nie pojawia się dwukrotnie. Rozszerzenie bazy w stosunku do poprzedniej przestrzeni jest konieczne, aby można było tworzyć macierze zespolone --- współczynniki kombinacji liniowej są rzeczywiste, a więc macierze zespolone muszą być częścią bazy. Wymiar tej przestrzeni wynosi zatem $2 m n$.
	\item
	zbiór $P^n$ wielomianów stopnia co najwyżej $n$-tego. Bazą tej przestrzeni może być np.
	\beq
	\mathcal{B} = \{1, x, x^2, \ldots, x^n\},
	\eeq
więc jest to przestrzeń $(n + 1)$-wymiarowa. Jednakże nie musimy ograniczać stopnia wielomianów i rozpatrzyć zbiór wielomianów dowolnego stopnia. Co oczywiste, baza takiej przestrzeni zawiera nieskończenie wiele elementów, ale jest \emph{przeliczalna}, bo zawiera tyle elementów, co zbiór liczb naturalnych, $\mathbb{N}$.
	\item
	\label{wm:ppw_kon}
	zbiór~$\mathscr{H}$ wszystkich funkcji $f: \mathbb{R} \to \mathbb{C}$ takich, że
	\beq
	\int_{-\infty}^\infty f^*(x) f(x) \, dx < \infty.
	\eeq
	Jest to przestrzeń wektorowej o nieskończonym wymiarze i \emph{nieprzeliczalnej} bazie. Przykładem funkcji z przestrzeni~$\mathscr{H}$ jest~$e^{-\alpha x^2}, \, \alpha > 0$.
	\end{enumerate}
	
\subsection{Iloczyn skalarny wektorów}
\label{ssc:il_skal_wekt}

\emph{Iloczyn skalarny} wektorów $\mathbf{u}$ i $\mathbf{v}$ z przestrzeni wektorowej $V$ przyporządkowuje im liczbę $\braket{ \mathbf{u} | \mathbf{v} }$ należącą do ciała $\mathbb{K}$. Iloczyn skalarny spełnia następujące zależnosci:
	\begin{enumerate}
	\item
	$\braket{ \mathbf{u} | \mathbf{v }_1 + \mathbf{v}_2} =
	\braket{ \mathbf{u} | \mathbf{v}_1 } + \braket{ \mathbf{u} | \mathbf{v}_2 }$,
	\item
	$\forall_{c \in \mathbb{K}}: \braket{ \mathbf{u} | c \mathbf{v} } =
	c \braket{ \mathbf{u} | \mathbf{v} }$,
	\item
	$\braket{ \mathbf{u} | \mathbf{v} } = \braket{ \mathbf{v} | \mathbf{u} }^*$,
	\item
	$\braket{ \mathbf{u} | \mathbf{u} } \ge 0 \wedge
	\braket{ \mathbf{u} | \mathbf{u} } = 0 \Leftrightarrow
	\mathbf{u} = \mathbf{0}$.
	\end{enumerate}
\emph{Norma} wektora (czyli jego długość) dana jest wzorem
	\beq
	||\mathbf{u}|| = \sqrt{\braket{ \mathbf{u} | \mathbf{u}} }.
	\eeq
Dla dowolnych wektorów $\mathbf{u}$ i $\mathbf{v}$ zachodzi \emph{nierównosć Cauchy'ego-Buniakowskiego}:
	\beq
	||\mathbf{u}|| \, ||\mathbf{v}|| \ge |\braket{ \mathbf{u} | \mathbf{v} }|.
	\eeq
Wektory $\mathbf{u}$ i $\mathbf{v}$ są ortogonalne, jesli
	\beq
	\braket{ \mathbf{u} | \mathbf{v} } = 0.
	\eeq
Definicja iloczynów skalarnych w przestrzeniach wektorowych opisanych w przykładach~\ref{wm:ppw_pocz}--\ref{wm:ppw_kon} w rozdziale~\ref{wm:pw}:
	\begin{enumerate}
	\item
	iloczyn skalarny wektorów
	$\mathbf{u} =
		\begin{bmatrix}
			x_1 \\
			x_2 \\
			\vdots \\
			x_n
		\end{bmatrix}$
i
	$\mathbf{v} =
		\begin{bmatrix}
			y_1 \\
			y_2 \\
			\vdots \\
			y_n
		\end{bmatrix}$
$\in \mathbb{C}^n$ dany jest wzorem
	\beq
	\braket{ \mathbf{u} | \mathbf{v} } = \mathbf{u}^\dag \mathbf{v} =
	\sum_{i = 1}^n x_i^* y_i.
	\label{eq:euk_isk}
	\eeq
	\item
	iloczynem skalarnym macierzy kwadratowych $\mathbb{A}$ i $\mathbb{B}$ z przestrzeni wektorowej $M^{n \times n}$ nad $\mathbb{C}$ jest
	\beq
	\braket{ \mathbb{A} | \mathbb{B} } = \mbox{Tr} (\mathbb{A}^\dag \mathbb{B}).
	\eeq
	\item
	iloczyn skalarny wielomianów $P_n(x) = p_0 + p_1 x + \ldots p_n x^n$ i $Q_n(x) = q_0 + q_1 x + \ldots q_n x^n$ można np. zdefiniować jako
	\beq
	\braket{ P_n | Q_n } = \sum_{i = 1}^n p_i^* q_i.
	\eeq
	\item
	iloczyn skalarny funkcji $f$ i $g$ z przestrzeni~$\mathscr{H}$ definiujemy jako
	\beq
	\braket{ f | g } = \int_{-\infty}^{\infty} f^*(x) g(x) \, dx.
	\eeq
	\end{enumerate}
	\begin{task}
	Znależć wektor ortogonalny do wektorów
	$\mathbf{u} =
		\begin{bmatrix}
			1 \\
			1 \\
			1
		\end{bmatrix}$
	i
	$\mathbf{v} =
		\begin{bmatrix}
			5 \\
			3 \\
			1
		\end{bmatrix}$.
	\end{task}
	\begin{sol}
	Oznaczmy współrzędne szukanego wektora jako
	$\begin{bmatrix}
		x \\
		y \\
		z
	\end{bmatrix}$. Równanie~(\ref{eq:euk_isk}) prowadzi do układu równań
	\beq
	\left\{ \begin{array}{l}
	x + y + z = 0 \\
	5 x + 3 y + z = 0
	\end{array} \right.,
	\eeq
	skąd otrzymujemy
	\beq
	4 x = -2 y.
	\eeq
	Wybierając na przykład $y = 2$ dostajemy wektor ortogonalny (jeden z nieskończenie wielu) do wektorów $\mathbf{u}$ i $\mathbf{v}$:
	$\begin{bmatrix}
		-1 \\
		2 \\
		-1
	\end{bmatrix}$.
	\end{sol}
Dla przestrzeni $\mathbb{R}^2$ i $\mathbb{R}^3$ ortogonalnosć wektorów oznacza, iż są one prostopadłe.

Dany wektor $\mathbf{v}$ jest \emph{unormowany}, jesli
	\beq
	\braket{ \mathbf{v} | \mathbf{v} } = 1,
	\eeq
czyli jego norma jest jednoscią. Każdy niezerowy wektor $\mathbf{u}$ można unormować dzieląc go przez jego normę:
	\beq
		\hat{\mathbf{u}} = \frac{\mathbf{u}}{\sqrt{\braket{ \mathbf{u} | \mathbf{u}}} } = \frac{\mathbf{u}}{||\mathbf{u}||},
	\eeq
przy czym wektor jednostkowy~$\hat{\mathbf{u}}$ ma zwrot identyczny jak wektor~$\mathbf{u}$.

Baza $\mathcal{B} = \{\mathbf{v}_1, \mathbf{v}_2, \ldots, \mathbf{v}_n\}$ jest \emph{ortonormalna}, jeśli
	\beq
		\forall_{i \wedge j \in \langle 1; n \rangle \cap \mathbb{N}}: \braket{ \mathbf{v}_i | \mathbf{v}_j } = \delta_{ij}.
	\eeq

\subsection{Przekształcenie liniowe}

\emph{Przekształcenie liniowe} jest odwzorowaniem jednej przestrzeni wektorowej w drugą: $A: V \to W$ spełniającym warunki\footnote{$A$ oznacza samo przekształcenie, zas $\hat{A}$ -- jego operator. Jesli przekształceniu $A$ odpowiada macierz $\mathbb{A}$, wówczas $\hat{A} \equiv \mathbb{A}$.}:
	\begin{enumerate}
	\item
	addytywnosci:
	$\forall_{\mathbf{u} \wedge \mathbf{v} \in V}: \hat{A}(\mathbf{u} + \mathbf{v}) = \hat{A}(\mathbf{u}) + \hat{A}(\mathbf{v})$,
	\item
	jednorodnosci:
	$\forall_{\alpha \in \mathbb{K}} \forall_{\mathbf{u} \in V}: \hat{A}(\alpha \mathbf{u}) = \alpha \hat{A}(\mathbf{u})$.
	\end{enumerate}
W $n$-wymiarowej przestrzeni euklidesowej każdemu przekształceniu liniowemu odpowiada macierz. Dla przykładu, jesli dla przestrzeni $\mathbb{R}^3$ wybierzemy bazę $\mathcal{B}_1$ [równanie~(\ref{eq:baza_B1})], to przekształceniu polegającemu na odbiciu w płaszczyżnie $xy$ (czyli zmianiającemu współrzędną $z$ danego wektora na $-z$) odpowiada macierz
	\beq
		\mathbb{A} =
			\begin{bmatrix}
				1 & 0 & 0 \\
				0 & 1 & 0 \\
				0 & 0 & -1
			\end{bmatrix}.
	\eeq
Istotnie, wektor
	$\mathbf{u} =
		\begin{bmatrix}
			x \\
			y \\
			z
		\end{bmatrix}$
po odbiciu w płaszczyżnie $xy$ będzie miał współrzędne
	\beq
		\mathbf{u}' = \mathbb{A} \mathbf{u} =
			\begin{bmatrix}
				1 & 0 & 0 \\
				0 & 1 & 0 \\
				0 & 0 & -1
			\end{bmatrix}
			\begin{bmatrix}
				x \\
				y \\
				z
			\end{bmatrix} =
			\begin{bmatrix}
				x \\
				y \\
				-z
			\end{bmatrix}.
	\eeq
Macierz przekształcenia liniowego jest uzależniona od wyboru bazy. Wyjątkiem jest \emph{przekształcenie identycznosciowe}\footnote{Przekształcenie identycznosciowe $E$ nie zmienia wektora, na który działa: $E (\mathbf{u}) = \mathbf{u}$.}, któremu w każdej bazie w przestrzeni $\mathbb{R}^n$ odpowiada macierz jednostkowa $n \times n$:
	\beq
		\mathbb{E} =
			\begin{bmatrix}
				1 & 0 & \cdots & 0 \\
				0 & 1 & \cdots & 0 \\
				\vdots & \vdots  & \ddots & \vdots \\
				0 & 0 & \cdots & 1
			\end{bmatrix}.
	\eeq

W ogólnosci danemu przekształceniu liniowemu $A: V \to W$ można przypisać macierz wtedy, gdy przestrzenie $V$ i $W$ mają skończony wymiar i gdy okreslono bazy tych przestrzeni. Niech $\mathcal{B} = \{\mathbf{v}_1; \mathbf{v}_2; \ldots; \mathbf{v}_n\}$ i $\mathcal{C} = \{\mathbf{w}_1; \mathbf{w}_2; \ldots; \mathbf{w}_m\}$ będą odpowiednio bazami przestrzeni wektorowych $V$ i $W$, zas $A$ --- przekształceniem liniowym $V \to W$. Wówczas elementy macierzy przekształcenia $A$ mają postać
	\beq
	a_{ij} = \braket{ \mathbf{w}_i | \hat{A} | \mathbf{v}_j } = \braket{ \hat{A} | \mathbf{v}_j | \mathbf{w}_i }^*.
	\eeq
Zatem macierze operatorów odbicia, obrotu w trójwymiarowym układzie współrzędnych kartezjańskich w bazie ortonormalnej~(\ref{eq:baza_B1}) znajdujemy z zależności
	\beq
	a_{ij} = \braket{\hat{\mathbf{e}}_i | \hat{A} | \hat{\mathbf{e}}_j},
	\eeq
gdzie $\{ \hat{\mathbf{e}}_1; \hat{\mathbf{e}}_2; \hat{\mathbf{e}}_3 \} = \{ \hat{\mathbf{x}}; \hat{\mathbf{y}}; \hat{\mathbf{z}} \}$. Jest to bowiem przekształcenie $\mathbb{R}^3 \to \mathbb{R}^3$.

Operator \emph{hermitowsko sprzężony} do operatora $\hat{A}$, $\hat{A}^\dag$, jest zdefiniowany równością
	\beq
	\braket{ \mathbf{w} | \hat{A} | \mathbf{v} } = \braket{ \hat{A}^\dag \mathbf{w} | \mathbf{v} }.
	\eeq
Macierz operatora $\hat{A}^\dag$ uzyskujemy przez hermitowskie sprzężenie macierzy operatora $\hat{A}$:
	\beq
	\mathbb{A}^\dag = \left( \mathbb{A}^{\scr{T}} \right)^* = (\mathbb{A}^*)^{\scr{T}}.
	\eeq
W ogólności operatory przekształceń liniowych są nieprzemienne:	
	\beq
	\hat{A} \hat{B} \neq \hat{B} \hat{A}.
	\eeq

\subsection{Diagonalizacja macierzy}

Niech $A$ będzie przekształceniem liniowym w przestrzeni $\mathbb{R}^n$, któremu odpowiada macierz kwadratowa
	\beq
	\mathbb{A} =
	\left[ \begin{array}{cccc}
	a_{11} & a_{12} & \cdots & a_{1n} \\
	a_{21} & a_{22} & \cdots & a_{2n} \\
	\vdots & \vdots & \ddots & \vdots \\
	a_{n1} & a_{n2} & \cdots & a_{nn} \\
	\end{array} \right].
	\eeq
Jesli istnieje niezerowy wektor $\mathbf{v} \in \mathbb{R}^n$ taki, że
	\beq
	\mathbb{A} \mathbf{v} = \lambda \mathbf{v}
	\label{eq:ww}
	\eeq
dla pewnego skalara $\lambda$, to $\lambda$ jest \emph{wartoscią własną} macierzy $\mathbb{A}$, a $\mathbf{v}$ --- \emph{wektorem własnym} odpowiadającym wartosci własnej $\lambda$. Jesli wektor $\mathbf{v}$ ma współrzędne
	$\left[ \begin{array}{c}
	x_1 \\
	x_2 \\
	\vdots \\
	x_n
	\end{array} \right]$,
równanie~(\ref{eq:ww}) można zapisać w postaci układu równań jednorodnych:
	\beq
	\left[ \begin{array}{cccc}
	a_{11} - \lambda & a_{12} & \cdots & a_{1n} \\
	a_{21} & a_{22} - \lambda & \cdots & a_{2n} \\
	\vdots & \vdots & \ddots & \vdots \\
	a_{n1} & a_{n2} & \cdots & a_{nn} - \lambda \\
	\end{array} \right]
	\left[ \begin{array}{c}
	x_1 \\
	x_2 \\
	\vdots \\
	x_n
	\end{array} \right] =
	\left[ \begin{array}{c}
	0 \\
	0 \\
	\vdots \\
	0
	\end{array} \right],
	\eeq
lub zwiężle
	\beq
	(\mathbb{A} - \lambda \mathbb{E}) \mathbf{v} = \mathbf{0}.
	\label{eq:rj}
	\eeq
Równanie~(\ref{eq:rj}) ma rozwiązania nietrywialne wtedy i tylko wtedy, gdy
	\beq
	\det (\mathbb{A} - \lambda \mathbb{E}) = 0.
	\label{eq:rch}
	\eeq
Równanie~(\ref{eq:rch}) to tzw. \emph{równanie charakterystyczne} macierzy $\mathbb{A}$, jego lewa strona jest zas \emph{wielomianem charakterystycznym} tej macierzy. Jak łatwo zauważyć, jesli $\mathbf{v}$ jest wektorem własnym macierzy $\mathbb{A}$, jest nim także wektor $\alpha \mathbf{v}$, $\alpha \in \mathbb{C}$:
	\beq
	\mathbb{A} \mathbf{v} = \lambda \mathbf{v} \Rightarrow
	\mathbb{A} (\alpha \mathbf{v}) = \alpha \mathbb{A} \mathbf{v} =
	\alpha \lambda \mathbf{v} = \lambda (\alpha \mathbf{v}),
	\eeq
stąd często zamiast pojęcia wektora własnego używa się okreslenia \emph{kierunek własny}. Jesli krotnosć danej wartosci własnej jako pierwiastka wielomianu charakterystycznego wynosi $k$ i $k \geq 2$, wówczas wartosć ta jest $k$-krotnie \emph{zdegenerowana} (\emph{zwyrodniała}) i odpowiada jej $k$ wektorów własnych. Zbiór wszystkich wartosci własnych danej macierzy to jej \emph{widmo} (ang. \emph{spectrum}):
	\beq
	\mbox{sp} (\mathbb{A}) = \{\lambda: \det (\mathbb{A} - \lambda \mathbb{E}) = 0\}.
	\eeq
	\begin{task}
	Znależć wartosci własne macierzy
	$\mathbb{A} =
	\left[ \begin{array}{ccc}
	1 & 0 & 0 \\
	3 & 1 & 2 \\
	0 & 5 & 4
	\end{array} \right]$.
	\end{task}
	\begin{sol}
	Równanie~(\ref{eq:rch}) dla macierzy $\mathbb{A}$ ma postać
	\beq
	\left| \begin{array}{ccc}
	1 - \lambda & 0 & 0 \\
	3 & 1 - \lambda & 2 \\
	0 & 5 & 4 - \lambda
	\end{array} \right| = 0 \Rightarrow (\lambda + 1)(\lambda - 1)(\lambda - 6) = 0 \Rightarrow \mbox{sp} (\mathbb{A}) = \{-1; 1; 6\}.
	\eeq
Dla każdej wartosci własnej $\lambda_i$ rozwiązujemy układ równań postaci
	\beq
	\left[ \begin{array}{ccc}
	1 & 0 & 0 \\
	3 & 1 & 2 \\
	0 & 5 & 4
	\end{array} \right]
	\left[ \begin{array}{c}
	\alpha_i \\
	\beta_i \\
	\gamma_i
	\end{array} \right] =
	\lambda_i
	\left[ \begin{array}{c}
	\alpha_i \\
	\beta_i \\
	\gamma_i
	\end{array} \right],
	\eeq
gdzie
	$\mathbf{v}_i =
	\left[ \begin{array}{c}
	\alpha_i \\
	\beta_i \\
	\gamma_i
	\end{array} \right]$
jest wektorem własnym macierzy $\mathbb{A}$ odpowiadającym wartosci własnej $\lambda_i$. Po podstawieniu kolejnych wartosci własnych otrzymujemy
	\beq
	\left\{ \begin{array}{l}
	\alpha_1 = 0 \\
	\beta_1 = - \gamma_1
	\end{array} \right. \mbox{, }
	\left\{ \begin{array}{l}
	3 \alpha_2 = -2 \gamma_2 \\
	5 \beta_2 = -3 \gamma_2
	\end{array} \right. \mbox{, }
	\left\{ \begin{array}{l}
	\alpha_3 = 0 \\
	5 \beta_3 = 2 \gamma_3
	\end{array} \right..
	\eeq
Zauważmy, że równania te pozwalają wyznaczyć wektory własne z dokładnoscią do stałej, czyli po prostu kierunki własne. Przyjmując na przykład $\beta_1 = 1$, $\gamma_2 = -15$ i $\gamma_3 = 5$ otrzymujemy
	\beq
	\mathbf{v}_1 = \left[ \begin{array}{c}
	0 \\
	1 \\
	-1
	\end{array} \right] \mbox{, }
	\mathbf{v}_2 = \left[ \begin{array}{c}
	10 \\
	9 \\
	-15
	\end{array} \right] \mbox{, }
	\mathbf{v}_3 = \left[ \begin{array}{c}
	0 \\
	2 \\
	5
	\end{array} \right] \mbox{. }
	\eeq
	\end{sol}
	
Niech macierz $\mathbb{A}$ będzie macierzą kwadratową $n \times n$, niech $\mbox{sp} (\mathbb{A}) = \{\lambda_1; \lambda_2; \ldots; \lambda_n\}$, niech $\forall_{i \wedge j \in \langle 1; n \rangle \cap \mathbb{N}}: \lambda_i \not= \lambda_j$ i niech $i$-ty wektor własny ma postać
	$\mathbf{v}_i =
	\left[ \begin{array}{c}
	x_{1i} \\
	x_{2i} \\
	\vdots \\
	x_{ni}
	\end{array} \right]$.
Utwórzmy macierz $\mathbb{P}$, której kolumny będą utworzone z wektorów własnych macierzy $\mathbb{A}$:
	\beq
	\mathbb{P} =
	\left[ \begin{array}{cccc}
	\mathbf{v}_1 & \mathbf{v}_2 & \cdots & \mathbf{v}_n
	\end{array} \right] =
	\left[ \begin{array}{cccc}
	x_{11} & x_{12} & \cdots & x_{1n} \\
	x_{21} & x_{22} & \cdots & x_{2n} \\
	\vdots & \vdots & \ddots & \vdots \\
	x_{n1} & x_{n2} & \cdots & x_{nn} \\
	\end{array} \right],
	\eeq
oraz diagonalną macierz $\mathbb{D}$ mającą na przekątnej wartosci własne macierzy $\mathbb{A}$:
	\beq
	\mathbb{D} =
	\left[ \begin{array}{cccc}
	\lambda_1 & 0 & \cdots & 0 \\
	0 & \lambda_2 & \cdots & 0 \\
	\vdots & \vdots & \ddots & \vdots \\
	0 & 0 & \cdots & \lambda_n \\
	\end{array} \right].
	\eeq
Zauważmy, że
	\beq
	\mathbb{A} \mathbb{P} = \mathbb{A}
	\left[ \begin{array}{cccc}
	\mathbf{v}_1 & \mathbf{v}_2 & \cdots & \mathbf{v}_n
	\end{array} \right] =
	\left[ \begin{array}{cccc}
	\mathbb{A} \mathbf{v}_1 & \mathbb{A} \mathbf{v}_2 & \cdots & \mathbb{A} \mathbf{v}_n
	\end{array} \right] =
	\left[ \begin{array}{cccc}
	\lambda_1 \mathbf{v}_1 & \lambda_2 \mathbf{v}_2 & \cdots & \lambda_n \mathbf{v}_n
	\end{array} \right] =
	\mathbb{P} \mathbb{D},
	\eeq
skąd otrzymujemy \textbf{bardzo ważną zależnosć}:
	\beq
	\mathbb{D} = \mathbb{P}^{-1} \mathbb{A} \mathbb{P}.
	\label{eq:mpod}
	\eeq
Jest to tzw. \emph{przekształcenie przez podobieństwo}. Macierze $\mathbb{A}$ i $\mathbb{D}$, powiązane zależnością~(\ref{eq:mpod}), są \emph{macierzami podobnymi}.
