\section{Macierze}

Macierzą~$m \times n$ nazywamy tablicę liczb
	\beq
		\mathbb{A} =
			\begin{bmatrix}
				a_{11} & a_{12} & \ldots & a_{1n} \\
				a_{21} & a_{22} & \ldots & a_{2n} \\
				\vdots & \vdots & \ddots & \vdots \\
				a_{m1} & a_{m2} & \ldots & a_{mn}
			\end{bmatrix}.
	\eeq

\subsection{Operacje macierzowe}

Sumę $\mathbb{A} + \mathbb{B}$ i różnicę $\mathbb{A} - \mathbb{B}$ macierzy tego samego wymiaru uzyskujemy sumując i odejmując wszystkie elementy tych macierzy. Iloczynem macierzy~$\mathbb{A}$ o wymiarze~$l \times m$ i~macierzy~$\mathbb{B}$ o~wymiarze~$m \times n$~ jest macierz
	\beq
		\mathbb{C} = \mathbb{A} \mathbb{B}
	\eeq
o wymiarze~$l \times n$, której elementy spełniają zależność
	\beq
		c_{ij} = \sum_{k = 1}^m a_{ik} b_{kj}.
	\eeq
Szczególnym (i w chemii kwantowej najczęściej spotykanym) typem macierzy jest macierz kwadratowa, a więc macierz o wymiarze~$n \times n$. Elementy~$\{a_{ii}\}_{i = 1}^n$ takiej macierzy nazywamy \emph{diagonalą} tej macierzy. Macierzą jednostkową o wymiarze~$n$ jest macierz~$n \times n$ złożna z jedynek na diagonali i zer poza nią,
	\beq
		\mathbb{I}_n =
			\begin{bmatrix}
				1 & 0 & 0 & 0 \\
				0 & 1 & \ldots & 0 \\
				\vdots & \vdots & \ddots & \vdots \\
				0 & 0 & \ldots & 1
			\end{bmatrix}.
	\eeq
Odwrotnością macierzy kwadratowej~$\mathbb{A}$ jest macierz~$\mathbb{A}^{-1}$ taka, że
	\beq
		\mathbb{A} \mathbb{A}^{-1} = \mathbb{A}^{-1} \mathbb{A} = \mathbb{I}_n.
	\eeq

\subsection{Wyznacznik macierzy}

\emph{Wyznacznik} macierzy kwadratowej~$\mathbb{A}$ oznaczamy ogólnym symbolem
	\beq
		\det{\mathbb{A}} = |\mathbb{A}| =
			\begin{vmatrix}
				a_{11} & a_{12} & \ldots & a_{1n} \\
				a_{21} & a_{22} & \ldots & a_{2n} \\
				\vdots & \vdots & \ddots & \vdots \\
				a_{n1} & a_{n2} & \ldots & a_{nn}
			\end{vmatrix}.
		\label{eq:wm_detA}
	\eeq

\subsubsection{Rozwinięcie Laplace'a}

Wyznacznik~\eqref{eq:wm_detA} możemy obliczyć z rozwinięcia~Laplace'a wzdłuż wybranego rzędu lub kolumny macierzy,
	\begin{align}
		|\mathbb{A}| & =
			\sum_{j = 1}^n a_{ij} C_{ij} \\
		 & = \sum_{i = 1}^n a_{ij} C_{ij},
	\end{align}
gdzie \emph{kofaktor}
	\beq
		C_{ij} = (-)^{i + j} M_{ij},
		\label{eq:wm_kofaktor}
	\eeq
zaś~$M_{ij}$ jest wyznacznikiem macierzy rzędu~$n - 1$ uzyskanej z macierzy~$\mathbb{A}$ przez wycięcie z niej~$i$-tego rzędu i~$k$-tej kolumny. $M_{ij}$~nazywamy \emph{minorem} elementu~$a_{ij}$. W~wyrażeniu~\eqref{eq:wm_minorij} na czerwono oznaczono elementy, które należy usunąć z macierzy~$\mathbb{A}$, aby uzyskać minor~$M_{ij}$.
	\beq
		|\mathbb{A}| =
			\begin{vmatrix}
				a_{11} & a_{12} & \ldots & \textcolor{red}{a_{1j}} & \ldots & a_{1n} \\
				a_{21} & a_{22} & \ldots & \textcolor{red}{a_{2j}} & \ldots & a_{2n} \\
				\vdots & \vdots & \ddots & \textcolor{red}{\vdots} & \vdots & \vdots \\
				\textcolor{red}{a_{i1}} & \textcolor{red}{a_{i2}} & \textcolor{red}{\ldots} & \textcolor{red}{a_{ij}} & \textcolor{red}{\ldots} & \textcolor{red}{a_{in}} \\
				\vdots & \vdots & \vdots & \textcolor{red}{\vdots} & \ddots & \vdots \\
				a_{n1} & a_{n2} & \ldots & \textcolor{red}{a_{nj}} & \ldots & a_{nn}
			\end{vmatrix}.
		\label{eq:wm_minorij}
	\eeq

\subsubsection{Antysymetryzator}

Antysymetryzatorem ciągu $n$-elementowego~$(1, 2, \ldots, n)$ bądź funkcji $n$~zmiennych jest operator postaci
	\beq
		\asop = \frac{1}{n!} \sum_{i = 1}^{n!} {(-)^{p_i} \hat{P}_i},
		\label{eq:wm_ants}
	\eeq
gdzie~$\hat{P}_i$ jest operatorem $i$-tej permutacji ciągu $n$-elementowego, zaś~$(-)^{p_i}$ jest \emph{parzystością} $i$-tej permutacji, przy czym~$p_i$ jest liczbą przestawień par prowadzących do uzyskania $i$-tej permutacji. Przestawienie pary elementów $k$~oraz~$l$ ciągu uzyskujemy działając operatorem~$\hat{\mathscr{P}}_{kl}$. Przyjmujemy ponadto, że pierwsza permutacja jest zawsze identycznościowa, więc
	\begin{align}
		\hat{P}_1 & = 1, \\
		p_1 & = 0.
	\end{align}
Liczba pojedynczych przestawień~$(p_i = 1)$ ciągu $n$-elementowego wynosi oczywiście
	\beq
		{n \choose 2} = \frac{n (n - 1)}{2}.
	\eeq
W tab.~\ref{tab:wm_perm12} przedstawiono permutacje ciągu 2-elementowego, w tab.~\ref{tab:wm_perm123} --- permutacje ciągu 3-elementowego.
	\begin{table}[htbp]
	\caption{Permutacje ciągu 2-elementowego}
	\begin{center}
	\begin{tabular}{*{3}{>{$}c<{$}}}
	\toprule
	\hat{P}_i & \hat{P}_i(1, 2) & p_i \\
	\midrule
	1 & (1, 2) & 0 \\
	\hat{\mathscr{P}}_{12} & (2, 1) & 1 \\
	\bottomrule
	\end{tabular}
	\end{center}
	\label{tab:wm_perm12}
	\end{table}
	\begin{table}[htbp]
	\caption{Permutacje ciągu 3-elementowego}
	\begin{center}
	\begin{tabular}{*{3}{>{$}c<{$}}}
	\toprule
	\hat{P}_i & \hat{P}_i(1, 2, 3) & p_i \\
	\midrule
	1 & (1, 2, 3) & 0 \\
	\hat{\mathscr{P}_{12}} & (2, 1, 3) & 1 \\
	\hat{\mathscr{P}_{13}} & (3, 2, 1) & 1 \\
	\hat{\mathscr{P}_{23}} & (1, 3, 2) & 1 \\
	\hat{\mathscr{P}_{12}} \hat{\mathscr{P}_{23}} & (3, 1, 2) & 2 \\
	\hat{\mathscr{P}_{12}} \hat{\mathscr{P}_{13}} & (2, 3, 1) & 2 \\
	\bottomrule
	\end{tabular}
	\end{center}
	\label{tab:wm_perm123}
	\end{table}
Tak więc jawna postać antysymetryzatora ciągu dwuelementowego to
	\beq
		\asop = \frac{1}{2} \Big( 1 - \hat{\mathscr{P}}_{12} \Big),
	\eeq
zaś dla ciągu 3-elementowego antysymetryzatorem jest
	\beq
		\asop =
			\frac{1}{6} \Big(
				1 -
				\hat{\mathscr{P}}_{12} -
				\hat{\mathscr{P}}_{13} -
				\hat{\mathscr{P}}_{23} +
				\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{23} +
				\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{13}
			\Big).
		\label{eq:wm_antys123}
	\eeq
Co oczywiste, podwójne działanie operatorem permutacji par nie zmienia ciągu,
	\beq
		\hat{\mathscr{P}}_{kl}^2 = 1.
		\label{eq:wm_perm2r}
	\eeq
Zauważmy, że operatory permutacji par w ogólności nie są przemienne, np.
	\begin{align}
		\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{23} (1, 2, 3) & = \hat{\mathscr{P}}_{12} (1, 3, 2) = (3, 1, 2), \\
		\hat{\mathscr{P}}_{23} \hat{\mathscr{P}}_{12} (1, 2, 3) & = \hat{\mathscr{P}}_{23} (2, 1, 3) = (2, 3, 1) \not= (3, 1, 2).
	\end{align}

	\begin{task}
		Dla antysymetryzatora ciągu 3-elementowego wyznaczyć operatory~$\hat{\mathscr{P}}_{12} \asop$ oraz~$\asop \hat{\mathscr{P}}_{12}$.
	\end{task}
	\begin{sol}
		Korzystając z~\eqref{eq:wm_antys123},~\eqref{eq:wm_perm2r} oraz tab.~\ref{tab:wm_perm123} uzyskujemy
			\begin{align}
				\hat{\mathscr{P}}_{12} \asop & = \frac{1}{6} \Big(
					\hat{\mathscr{P}}_{12} -
					1 -
					\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{13} -
					\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{23} +
					\hat{\mathscr{P}}_{23} +
					\hat{\mathscr{P}}_{13}
				\Big) \\
				 & = -\asop.
			\end{align}
		Następnie
			\beq
				\asop \hat{\mathscr{P}}_{12} = \frac{1}{6} \Big(
					\hat{\mathscr{P}}_{12} -
					1 -
					\hat{\mathscr{P}}_{13} \hat{\mathscr{P}}_{12} -
					\hat{\mathscr{P}}_{23} \hat{\mathscr{P}}_{12} +
					\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{23} \hat{\mathscr{P}}_{12} +
					\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{13} \hat{\mathscr{P}}_{12}
				\Big).
			\eeq
		Łatwo sprawdzamy, że
			\begin{align}
				\hat{\mathscr{P}}_{13} \hat{\mathscr{P}}_{12} & = \hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{23}, \\
				\hat{\mathscr{P}}_{23} \hat{\mathscr{P}}_{12} & = \hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{13}, \\
				\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{23} \hat{\mathscr{P}}_{12} & = \hat{\mathscr{P}}_{13}, \\
				\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{13} \hat{\mathscr{P}}_{12} & = \hat{\mathscr{P}}_{23}.
			\end{align}
		Tak więc
			\begin{align}
				\asop \hat{\mathscr{P}}_{12} & = \frac{1}{6} \Big(
					\hat{\mathscr{P}}_{12} -
					1 -
					\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{23} -
					\hat{\mathscr{P}}_{12} \hat{\mathscr{P}}_{13} +
					\hat{\mathscr{P}}_{13} +
					\hat{\mathscr{P}}_{23}
				\Big) \\
				 & = -\asop.
			\end{align}
		\qed
	\end{sol}
Działanie antysymetryzatora i operatora permutacji par jest przemienne,
	\beq
		\hat{\mathscr{P}}_{kl} \asop = \asop \hat{\mathscr{P}}_{kl} = -\asop.
	\eeq

Przyjmujemy konwencję, że jeśli antysymetryzator działa na iloczyn wielkości dwuindeksowych mających powtórzony indeks (np. na elementów macierzy~$a_{kl}$), wówczas działa tylko na jeden z indeksów, np.
	\begin{align}
		\asop \Big( a_{11} a_{22} \Big) & = \frac{1}{2} \sum_{i = 1}^{2} a_{1\hat{P}_1(1)} a_{2\hat{P}_2(2)} \\
		 & \equiv \frac{1}{2} \sum_{i = 1}^{2} a_{\hat{P}_1(1)1} a_{\hat{P}_2(2)2} \\
		 & = \frac{1}{2} \Big( a_{11} a_{22} - a_{12} a_{21} \Big)
	\end{align}

	\begin{task}
	Wykazać, że operator antysymetryzacji~$\asop$ jest idempotentny, tzn.
	\beq
	\asop^2 = \asop.
	\label{eq:hf_ansid}
	\eeq
	\end{task}
	\begin{sol}
	Wyrażenie~(\ref{eq:wm_ants}) definiujące antysymetryzator jest sumą wszystkich możliwych permutacji $n$~elementów. Wynik działania tego operatora na $n$-elementowy zbiór jest niezmienniczy ze względu na początkowe ustawienie elementów --- zawsze uzyskamy te same permutacje. Stąd w wyrażeniu
		\begin{align}
			\asop^2 & =
				\frac{1}{n!} \sum_{i = 1}^{n!} (-1)^{p_i} \hat{P}_i
					\left( \frac{1}{n!} \sum_{j = 1}^{n!} (-1)^{q_j} \hat{Q}_j \right) \\
			& = \frac{1}{(n!)^2} \sum_{i = 1}^{n!} \sum_{j = 1}^{n!} (-1)^{p_i + q_j} \hat{P}_i \hat{Q}_j
		\end{align}
	dla danej permutacji $Q_j$ w wyniku działania operatora permutacji $\hat{P}_i$ uzyskamy zawsze taki sam wynik, a zatem po wysumowaniu po permutacjach $Q_j$ uzyskamy $n!$ takich samych wyników, więc możemy zapisać
		\beq
			\asop^2 = n! \frac{1}{(n!)^2} \sum_{i = 1}^{n!} (-1)^{p_i} \hat{P}_i =
			 \frac{1}{n!} \sum_{i = 1}^{n!} (-1)^{p_i} \hat{P}_i \equiv
			 \asop.
			 \label{eq:wm_antys_idemp}
		\eeq
	Widzimy też, że z warunku idempotentności antysymetryzatora,~\eqref{eq:hf_ansid}, wynika czynnik~$\frac{1}{n!}$ w~\eqref{eq:wm_ants}. \qed
	\end{sol}

\subsubsection{Wzór Leibniza}

Równoważnym wobec rozwinięcia Laplace'a sposobem obliczania wyznacznika macierzy jest użycie wzoru Leibniza,
	\begin{align}
		|\mathbb{A}| & = n! \asop \Big( a_{11} a_{22} \ldots a_{nn} \Big) \\
		 & = \sum_{i = 1}^{n!} {(-)^{p_i} a_{1\hat{P}_i(1)} a_{2\hat{P}_i(2)} \ldots a_{n\hat{P}_i(n)}} \\
		 & \equiv \sum_{i = 1}^{n!} {(-)^{p_i} a_{\hat{P}_i(1)1} a_{\hat{P}_i(2)2} \ldots a_{\hat{P}_i(n)n}}.
	\end{align}