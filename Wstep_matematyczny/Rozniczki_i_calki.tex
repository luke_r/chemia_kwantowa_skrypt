\section{Podstawy rachunku różniczkowego i całkowego}

\subsection{Pochodna funkcji}

\emph{Pochodną} funkcji $f$ w punkcie $x$ nazywamy granicę właściwą postaci
	\beq
		f'(x) = \lim_{h \to 0} \frac{f(x + h) - f(x)}{h}.
		\label{eq:wmat_poch_def}
	\eeq
Warunkiem koniecznym istnienia takiej granicy jest ciągłosć funkcji $f$ w punkcie~$x$. Jesli granica taka nie istnieje, funkcja $f$ nie jest różniczkowalna w punkcie $x$. W definicji~\eqref{eq:wmat_poch_def} zastosowaliśmy symbol pochodnej Lagrange'a. Jednakże często stosować będziemy notację Leibniza, którą wprowadza się następująco:
	\begin{align}
		f'(x_0) & = \lim_{\Delta x \to 0} \frac{f(x_0 + \Delta x) - f(x_0)}{\Delta x} \\
		 & = \lim_{\Delta x \to 0} \frac{\Delta f(x_0)}{\Delta x} \\
		 & \equiv \frac{d f(x_0)}{d x} \label{eq:leibniz_poch} =
			\left. \frac{d f}{d x} \right|_{x_0},
	\end{align}
gdzie~$d x$ jest infinitezymalnym~(nieskończenie małym) przyrostem argumentu funkcji, któremu odpowiada infinitezymalny przyrost funkcji~$d f(x)$, czyli tzw.~\emph{różniczka} funkcji. Zatem przyrost funkcji w danym punkcie, zgodnie z~\eqref{eq:leibniz_poch}, wynosi
	\beq
		d f(x) = f'(x) d x.
		\label{eq:wmat_rozn}
	\eeq

Niech $D_f$ będzie zbiorem tych argumentów funkcji $f$, dla których jej pochodna w danym punkcie istnieje.

Podstawowe twierdzenia o pochodnej wynikające z~\eqref{eq:wmat_poch_def}:
	\begin{enumerate}
	\item
	liniowosć:
		\beq
			\forall_{a_1 \wedge a_2 \in \mathbb{C}}: \Big( a_1 f(x) + a_2 g(x) \Big)' = a_1 f'(x) + a_2 g'(x).
		\eeq
	\item \label{it:wmat_poch_ilocz}
	pochodna iloczynu funkcji:
		\beq
			\Big( f(x) g(x) \Big)' = f'(x) g(x) + f(x) g'(x).
		\eeq
	\item \label{it:wmat_poch_ilor}
	pochodna ilorazu funkcji:
		\beq
			\left( \frac{f(x)}{g(x)} \right)' = \frac{f'(x) g(x) - f(x) g'(x)}{g^2(x)}.
		\eeq
	\item \label{it:wmat_poch_zloz}
	pochodna funkcji złożonej\footnote{Gdy mamy dane funkcje $g: X \to Y$ i $f: V \to Z$, gdzie $Y \subset V$, wówczas funkcja $h: X \to Z$ okreslona wzorem $h(x) = (f \circ g)(x) = f \Big( g(x) \Big)$ jest złożeniem (superpozycją) funkcji $f$ z $g$.}:
		\beq
			(f \circ g)'(x)
				= \Big( f \big( g(x) \big) \Big)'
				= f' \Big( g(x) \Big) g'(x).
		\eeq
	\item \label{it:wmat_poch_odwr}
	pochodna funkcji odwrotnej\footnote{Gdy mamy daną wzajemnie jednoznaczną (tzn. taką, która każdy element przeciwdziedziny przyporządkowuje dokładnie jednemu argumentowi --- jest to tzw. \emph{bijekcja}) funkcję $f: X \to Y$, funkcją odwrotną do niej jest funkcja $g \equiv f^{(-1)}: Y \to X$ taka, że $g(y) = x \Leftrightarrow f(x) = y$.}:
		\beq
			\Big( f^{(-1)}(x) \Big)' = \frac{1}{f'\Big( f^{(-1)}(x) \Big)}.
		\eeq
	\item
	pochodna logarytmu funkcji:
		\beq
			\Big( \ln \big| f(x) \big| \Big)' = \frac{f'(x)}{f(x)}.
		\eeq
	\end{enumerate}

Sprawdźmy, że własność~\ref{it:wmat_poch_ilocz} istotnie wynika z definicji~\eqref{eq:wmat_poch_def}:
	\begin{align}
		\Big( f(x) g(x) \Big)' & =
			\lim_{h \to 0} \frac{f(x + h) g(x + h) - f(x) g(x)}{h} \\
		 & \equiv \lim_{h \to 0} \frac{f(x + h) g(x + h) - f(x) g(x) + f(x) g(x + h) - f(x) g(x + h)}{h} \\
		 & = \lim_{h \to 0} \frac{\Big( f(x + h) - f(x) \Big) g(x + h) + f(x) \Big( g(x + h) - g(x) \Big)}{h} \\
		 & = \lim_{h \to 0} \frac{f(x + h) - f(x)}{h} \lim_{h \to 0} g(x + h) + f(x) \lim_{h \to 0} \frac{g(x + h) - g(x)}{h} \\
		 & \equiv f'(x) g(x) + f(x) g'(x).
	\end{align}

Udowodnijmy teraz własność~\ref{it:wmat_poch_ilor}. Zauważmy najpierw, że zgodnie z własnością~\ref{it:wmat_poch_ilocz} zachodzi
	\begin{align}
		\left( \frac{f(x)}{g(x)} \right)' & = \left( f(x) \frac{1}{g(x)} \right)' \\
		 & = \frac{f'(x)}{g(x)} + f(x) \left( \frac{1}{g(x)} \right)'. \label{eq:apfy}
	\end{align}
Zgodnie z~\eqref{eq:wmat_poch_def} znajdujemy pochodną
	\begin{align}
		\left( \frac{1}{g(x)} \right)' & = \lim_{h \to 0} \frac{\frac{1}{g(x + h)} - \frac{1}{g(x)}}{h} \\
		 & = \lim_{h \to 0} \frac{g(x) - g(x + h)}{h g(x) g(x + h)} \\
		 & = -\frac{1}{g(x)} \lim_{h \to 0} \frac{g(x + h) - g(x)}{h} \lim_{h \to 0} \frac{1}{g(x + h)} \\
		 & = -\frac{g'(x)}{g^2(x)}. \label{eq:ylmj}
	\end{align}
Wstawiamy teraz~\eqref{eq:ylmj} do~\eqref{eq:apfy} i uzyskujemy
	\begin{align}
		\left( \frac{f(x)}{g(x)} \right)' & = \frac{f'(x)}{g(x)} - \frac{f(x) g'(x)}{g^2(x)} \\
		 & = \frac{f'(x) g(x) - f(x) g'(x)}{g^2(x)}.
	\end{align}

Z kolei dla właśności~\ref{it:wmat_poch_zloz} zgodnie z~\eqref{eq:wmat_poch_def} uzyskujemy
	\beq
		\Big( f \big( g(x) \big) \Big)' =
			\lim_{h \to 0} \frac{f \Big( g(x + h) \Big) - f \Big( g(x) \Big)}{h}.
		\label{eq:wmat_poch_zloz_lim}
	\eeq
Oznaczmy
	\begin{align}
		y & = g(x), \\
		k & = g(x + h) - g(x),
	\end{align}
więc
	\begin{align}
		\lim_{h \to 0} k & = 0, \label{eq:wmat_lim_h_k} \\
		\lim_{h \to 0} \frac{k}{h} & = g'(x). \label{eq:wmat_lim_h_ktoh}
	\end{align}
Korzystając z~\eqref{eq:wmat_poch_zloz_lim},~\eqref{eq:wmat_lim_h_k} i~\eqref{eq:wmat_lim_h_ktoh}, uzyskujemy
	\begin{align}
		\Big( f \big( g(x) \big) \Big)' & =
			\lim_{h \to 0} \frac{f(y + k) - f(y)}{h} \\
		 & \equiv \lim_{h \to 0} \frac{f(y + k) - f(y)}{k} \frac{k}{h} \\
		 & = \lim_{k \to 0} \frac{f(y + k) - f(y)}{k} \lim_{h \to 0} \frac{k}{h} \label{eq:hmia} \\
		 & = f' \Big( g(x) \Big) g'(x),
	\end{align}
przy czym zmiana zmiennej z~$h$ na~$k$ w lewej granicy w~\eqref{eq:hmia} jest możliwa właśnie ze względu na~\eqref{eq:wmat_lim_h_k}.

Własność~\ref{it:wmat_poch_zloz} możemy też udowodnić korzystając z notacji Leibniza~\eqref{eq:wmat_rozn}. Przyrost funkcji w danym punkcie, zgodnie z~\eqref{eq:wmat_rozn}, jest iloczynem pochodnej funkcji w tym pukcie przez przyrost argumentu funkcji. Argumentem funkcji złożonej~$f \Big( g(x) \Big)$ jest funkcja~$g(x)$, a jej przyrost rozważamy w punkcie~$g(x)$, zatem
	\beq
		\dd f \Big( g(x) \Big) = f' \Big( g(x) \Big) \dd g(x),
	\eeq
z kolei
	\beq
		\dd g(x) = g'(x) \dd x,
	\eeq
skąd
	\beq
		\dd f \Big( g(x) \Big) = f' \Big( g(x) \Big) g'(x) \dd x
		\label{eq:wmat_rozn_zloz}
	\eeq
i tym samym
	\beq
		\dv{f\Big( g(x) \Big)}{x} = f' \Big( g(x) \Big) g'(x),
	\eeq
co możemy zapisać w zwięzłej postaci,
	\begin{tcolorbox}[title = Reguła łańcuchowa]
		\beq
			\frac{d f}{d x} = \frac{d f}{d g} \frac{d g}{d x}.
			\label{eq:wmat_reglanc}
		\eeq
	\end{tcolorbox}

Własność~\ref{it:wmat_poch_odwr} wynika z własności~\ref{it:wmat_poch_zloz}:
	\begin{align}
		\left( f \circ f^{(-1)} \right)'(x)
			& = \Big( f \big( f^{(-1)}(x) \big) \Big)' \\
			& = f' \Big( f^{(-1)}(x) \Big) \Big( f^{(-1)}(x) \Big)', \label{eq:avpc}
	\end{align}
a ponieważ zachodzi
	\beq
		\left( f \circ f^{(-1)} \right)(x) = x,
	\eeq
stąd oczywiście
	\beq
		\left( f \circ f^{(-1)} \right)'(x) = 1,
	\eeq
co w połączeniu z~\eqref{eq:avpc} kończy dowód.

Musimy zaznaczyć, że podane sposoby dowodzenia własności~\ref{it:wmat_poch_zloz} i~\ref{it:wmat_poch_odwr} nie są rygorystycznie poprawne, pełne dowody podane są np. w~\cite{isbn:978-0471000051}.

W tabeli~\ref{tab:pochodne} zestawiono pochodne najważniejszych funkcji.

	\begin{table}[htbp]
	\caption{Najważniejsze pochodne}
	\begin{center}
	\tabulinesep = 1mm
	\begin{tabu} spread 0pt {*2{X[$$l]}}
	\toprule
	f(x) & f'(x) \\
	\midrule
	a x + b & a \\
	x^n & n x^{n-1} \\
	a^x & a^x \ln a \\
	e^x & e^x \\
	\ln{x} & \frac{1}{x} \\
	\sin{x} & \cos{x} \\
	\cos{x} & -\sin{x} \\
	\tan{x} & \frac{1}{\cos^2 x} \\
	\cot{x} & -\frac{1}{\sin^2 x} \\
	\bottomrule
	\end{tabu}
	\end{center}
	\label{tab:pochodne}
	\end{table}

Oczywiście dla danej funkcji~$f$ możemy obliczać kolejne pochodne, o ile istnieją:~$f'(x), f''(x), f'''(x), \ldots$ W ogólności $n$-tą pochodną funkcji~$f$, czyli pochodną~$n$-tego rzędu, oznaczamy symbolem
	\beq
		f^{(n)}(x) \equiv \frac{d^n f}{d x^n},
	\eeq
przy czym
	\beq
		f^{(0)}(x) = f(x).
	\eeq

\subsection{Szereg Taylora}

Zajmiemy się obecnie rozwinięciem funkcji~$f(x)$ wokół danego punktu~$x_0 \in D_f$. Załóżmy, że funkcję~$f$ można rozwinąć w nieskończony szereg potęgowy
	\begin{align}
		f(x) & = \sum_{n = 0}^\infty {a_n (x - x_0)^n} \label{wmat_rozw_szpot} \\
		 & = \sum_{n = 0}^\infty {a_n f_n(x)}. \label{wmat_rozw_fn}
	\end{align}
Musimy znaleźć wyrażenie na współczynniki~$a_n$ tego rozwinięcia. Obliczmy kolejne pochodne funkcji
	\beq
		f_n(x) = (x - x_0)^n:
		\label{eq:wmat_fn_Taylor}
	\eeq
	\begin{align}
		f'_n(x) & = n (x - x_0)^{n - 1}, \\
		f''_n(x) & = n (n - 1) (x - x_0)^{n - 2}, \\
		f'''_n(x) & = n (n - 1) (n - 2) (x - x_0)^{n - 3}, \\
		 & \ldots
	\end{align}
widzimy więc, że po $k$-krotnym zróżniczkowaniu~\eqref{eq:wmat_fn_Taylor} dla~$k \le n$ otrzymujemy
	\beq
		f^{(k)}_n(x) = n (n - 1) (n - 2) \cdots (n - k + 1) (x - x_0)^{n - k},
	\eeq
zaś dla~$k > n$ pochodna zeruje się. Zauważmy ponadto, że
	\beq
		\begin{split}
			I_n & = n (n - 1) (n - 2) \cdots (n - k + 1) \\
			 & \equiv \frac{n (n - 1) (n - 2) \ldots (n - k + 1) (n - k) (n - k - 1) \cdots 1}{(n - k) (n - k - 1) \cdots 1} \\
			 & = \frac{n!}{(n - k)!}.
		\end{split}
	\eeq
Ostatecznie, pochodna~$k$-tego rzędu funkcji~\eqref{eq:wmat_fn_Taylor} dana jest zależnością
	\beq
		f^{(k)}_n(x) =
			\begin{cases}
				\displaystyle \frac{n!}{(n - k)!} (x - x_0)^{n - k} & , \, k \le n, \\
				\displaystyle 0 & , \, k > n.
			\end{cases}
		\label{eq:wmat_poch_kn}
	\eeq
Pochodna~\eqref{eq:wmat_poch_kn} w punkcie~$x_0$ nie znika tylko w przypadku, gdy w wyrażeniu na pochodną nie występuje człon~$x - x_0$, a więc dla $k = n$:
	\beq
		f^{(n)}_n(x_0) = n!,
	\eeq
zatem
	\beq
		f^{(k)}_n(x_0) = \delta_{k n} n!
		\label{eq:wmat_fkn}
	\eeq
Z~\eqref{wmat_rozw_fn} i~\eqref{eq:wmat_fkn} uzyskujemy
	\beq
		f^{(k)}(x_0) = k! a_k.
	\eeq
Tak więc rozwinięcie~\eqref{wmat_rozw_szpot} ma postać:
	\begin{tcolorbox}[title = Rozwinięcie Taylora]
		\begin{align}
			f(x) & = \sum_{n = 0}^\infty {\frac{1}{n!} f^{(n)}(x_0) (x - x_0)^n} \\
			 & = \sum_{n = 0}^\infty \frac{1}{n!} \left. \frac{d^n f(x)}{d x^n} \right|_{x_0} (x - x_0)^n \\
			 & = f(x_0) + f'(x_0) (x - x_0) + \frac{1}{2} f''(x_0) (x - x_0)^2 + \ldots
			\label{eq:sz_tay}
		\end{align}
	\end{tcolorbox}
Jest to tzw.~\emph{rozwinięcie Taylora}.

Przykłady rozwinięcia Taylora wokół~$x_0 = 0$:
	\begin{align}
		e^x & = \sum_{k = 0}^\infty {\frac{x^k}{k!}} \label{eq:rT_ex} \\
		 & = 1 + x + \frac{x^2}{2} + \frac{x^3}{6} + \ldots, \nonumber \\
		\sin{x} & = \sum_{k = 0}^\infty {\frac{(-1)^k}{(2 k + 1)!} x^{2 k + 1}} \\
		 & = x - \frac{x^3}{6} + \frac{x^5}{120} - \frac{x^7}{5040} + \ldots, \nonumber \\
		\cos{x} & = \sum_{k = 0}^\infty {\frac{(-1)^k}{(2 k)!} x^{2 k}} \\
		 & = 1 - \frac{x^2}{2} + \frac{x^4}{24} - \frac{x^8}{40320} + \ldots \nonumber
	\end{align}

\subsection{Całka funkcji}

$F$ jest funkcją pierwotną funkcji $f$, jeśli
	\beq
		\forall_{x \in D_f}: F'(x) = f(x).
	\eeq
\emph{Całką nieoznaczoną} funkcji $f$ nazywamy zbiór jej wszystkich fukncji pierwotnych:
	\beq
	\forall_{c \in \mathbb{C}}: \int f(x) \, dx = F(x) + c \Leftrightarrow F'(x) = f(x).
	\eeq

Podstawowe własnosci całek wykorzystywane przy całkowaniu:
	\begin{enumerate}
	\item
	liniowosć:
		\beq
			\forall_{a_1 \wedge a_2 \in \mathbb{C}}: \int \Big( a_1 f(x) + a_2 g(x) \Big) \, dx =
				a_1 \int f(x) \, dx + a_2 \int g(x) \, dx.
		\eeq
	\item
	całkowanie przez częsci:
		\beq
			\int f(x) g'(x) \, dx = f(x) g(x) - \int f'(x) g(x) \, dx.
		\eeq
	\item
	całkowanie przez podstawienie:
		\beq
			x = g(u) \Rightarrow \int f(x) \, dx = \int f[g(u)]g'(u) \, du,
		\eeq
	w szczególności
		\beq
			\int \frac{f'(x)}{f(x)} \, dx = \ln |f(x)| + c, \, c \in \mathbb{C}.
		\eeq
	\end{enumerate}

\emph{Całką oznaczoną} w sensie Riemanna nazywamy granicę ciągu
	\beq
	\int_a^b f(x) \, dx = \lim_{n \to \infty} \sum_{i=1}^n f(\xi_i) \Delta x_i,
	\eeq
gdzie $n$ jest liczbą przedziałów $\langle x_{i-1}; x_i \rangle$, na które podzielono przedział $\langle a; b \rangle$, ciąg podziałów zdefiniowano następująco: $a = x_0 < x_1 < \ldots < x_n = b$, zas $\xi_i \in \langle x_{i-1}; x_i \rangle$, oraz $\Delta x_i = x_i - x_{i - 1}$. Jesli $F$ jest funkcją pierwotną funkcji $f$, wówczas
	\beq
	\int_a^b f(x) \, dx = F(b) - F(a).
	\eeq
Całki niewłasciwe na przedziale nieskończonym oblicza się w następujący sposób:
	\begin{align}
		\int_{-\infty}^\infty f(x) \, dx & =
			\int_{-\infty}^a f(x) \, dx + \int_a^\infty f(x) \, dx \\
			& = \lim_{t \to -\infty} \int_t^a f(x) \, dx + \lim_{t \to \infty} \int_a^t f(x) \, dx,
	\end{align}
przy czym wartosć całki nie zależy od wartosci $a$.

W tabeli~\ref{tab:calki} zestawiono całki najważniejszych funkcji.
	\begin{table}[htbp]
	\caption{Najważniejsze całki,~$c \in \mathbb{C}$}
	\begin{center}
	\tabulinesep = 1mm
	\begin{tabu} spread 0pt {*2{X[$$l]}}
	\toprule
	f(x) & \int f(x) \, dx - c \\
	\midrule
	x^n & \frac{x^{n+1}}{n+1} \\
	a^x & \frac{a^x}{\ln a} \\
	e^x & e^x \\
	\frac{1}{x} & \ln{|x|} \\
	\ln{x} & x (\ln{x} -1) \\
	\sin{x} & -\cos{x} \\
	\cos{x} & \sin{x} \\
	\tan{x} & -\ln{\cos{x}} \\
	\cot{x} & \ln{\sin{x}} \\
	\bottomrule
	\end{tabu}
	\end{center}
	\label{tab:calki}
	\end{table}

\subsection{Całkowanie po parametrze i całka gaussowska}

Całki typu $\displaystyle \int_0^\infty x^n e^{-\alpha x} \, dx$ wygodnie jest liczyć metodą całkowania po parametrach. Dla $n = 2$ mamy
	\beq
	\frac{\partial^2}{\partial \alpha^2} \int_0^\infty e^{-\alpha x} \, dx =
	-\frac{\partial}{\partial \alpha} \int_0^\infty x e^{-\alpha x} \, dx =
	\int_0^\infty x^2 e^{-\alpha x} \, dx,
	\eeq
	a ponieważ
	\beq
	\int_0^\infty e^{-\alpha x} \, dx = \left| -\frac{1}{\alpha} e^{-\alpha x} \right|_0^\infty =
	\frac{1}{\alpha},
	\eeq
	stąd
	\beq
	\int_0^\infty x^2 e^{-\alpha x} \, dx = \frac{2}{\alpha^3}.
	\label{eq:rzparam_x2}
	\eeq
	W ogólnosci całki tego typu obliczamy z zależnosci
	\beq
	\int_0^\infty x^n e^{-\alpha x} \, dx = \left( -\frac{d}{d \alpha} \right)^n \int_0^\infty e^{-\alpha x} \, dx = \left( -\frac{d}{d \alpha} \right)^n \frac{1}{\alpha} = \frac{n!}{\alpha^{n + 1}}.
	\label{eq:rzparam}
	\eeq

Wartosć całki $\displaystyle \int_{-\infty}^\infty e^{-x^2}$ obliczamy we wprowadzając współrzędne biegunowe:
	\beq
	\left\{ \begin{array}{l}
	x = r \cos{\varphi} \\
	y = r \sin{\varphi}
	\end{array} \right..
	\eeq
	Jakobian tego przekształcenia wynosi
	\beq
	\left| \begin{array}{cc}
	\frac{\partial x}{\partial r} & \frac{\partial x}{\partial \varphi} \\
	\frac{\partial y}{\partial r} & \frac{\partial y}{\partial \varphi}
	\end{array} \right| =
	r.
	\eeq
	Zatem
	\beq
	\begin{split}
	\left( \int_{-\infty}^\infty e^{-x^2} \, d x \right)^2 & =
	\int_{-\infty}^\infty e^{-x^2} \, d x \int_{-\infty}^\infty e^{-x^2} \, d x =
	\int_{-\infty}^\infty e^{-x^2} \, d x \int_{-\infty}^\infty e^{-y^2} \, d y = \\
	 & = \int_{-\infty}^\infty \int_{-\infty}^\infty e^{-x^2 + y^2} \, d x \, d y =
	\int_0^\infty \int_0^{2 \pi} r e^{-r^2} \, d r \, d \varphi = \\
	 & = 2 \pi \int_0^\infty r e^{-r^2} \, d r =
	\left\{ \begin{array}{l}
	t = e^{-r^2} \mbox{, } d t = -2 r e^{-r^2} d r \\
	r = 0 \Rightarrow t = 1 \mbox{, } r = \infty \Rightarrow t = 0
	\end{array} \right\} = \\
	 & = \pi \int_0^1 \, d t = \pi,
	\end{split}
	\eeq
a ponieważ
	\beq
	\forall_{x \in \mathbb{R}}: e^{-x^2} > 0,
	\eeq
stąd
	\beq
	\int_{-\infty}^\infty e^{-x^2} \, d x = \sqrt{\pi},
	\eeq
zas z uwagi na parzystosć funkcji podcałkowej otrzymujemy od razu
	\beq
	\int_0^\infty e^{-x^2} \, d x = \frac{\sqrt{\pi}}{2}.
	\eeq
Teraz już łatwo możemy obliczyć całki
	\beq
	\int_0^\infty e^{-\alpha x^2} \, d x =
	\left\{ \begin{array}{l}
	t = \sqrt{\alpha} x \mbox{, } d t = \sqrt{\alpha} d x \\
	x = 0 \Rightarrow t = 0 \mbox{, } x = \infty \Rightarrow t = \infty
	\end{array} \right\} =
	\frac{1}{\sqrt{\alpha}} \int_0^\infty e^{-t^2} \, d t =
	\frac{1}{2} \sqrt{\frac{\pi}{\alpha}}
	\label{eq:cgs_x0}
	\eeq
oraz
	\beq
	\int_0^\infty x e^{-\alpha x^2} \, d x =
	\left\{ \begin{array}{l}
	t = e^{-\alpha x^2} \mbox{, } d t = -2 \alpha x e^{-\alpha x^2} d x \\
	x = 0 \Rightarrow t = 1 \mbox{, } x = \infty \Rightarrow t = 0
	\end{array} \right\} =
	\frac{1}{2 \alpha} \int_0^1 \, d t = \frac{1}{2 \alpha}.
	\label{eq:cgs_x1}
	\eeq
Całki zawierające wyższe potęgi $x$ obliczamy metodą różniczkowania po parametrach całki~(\ref{eq:cgs_x0}) dla potęg parzystych lub całki~(\ref{eq:cgs_x1}) dla potęg nieparzystych. Zauważmy, że
	\beq
	\frac{d}{d \alpha} \int_0^\infty e^{-\alpha x^2} \, d x =
	- \int_0^\infty x^2 e^{-\alpha x^2} \, d x =
	-\frac{1}{4} \sqrt{\frac{\pi}{\alpha^3}}
	\eeq
oraz
	\beq
	\frac{d}{d \alpha} \int_0^\infty x e^{-\alpha x^2} \, d x =
	- \int_0^\infty x^3 e^{-\alpha x^2} \, d x =
	-\frac{1}{2 \alpha^2}.
	\eeq
W ogólnosci całki tego typu obliczamy z zależnosci
	\beq
	\int_0^\infty x^{2 n} e^{-\alpha x^2} \, d x = \frac{\sqrt{\pi}}{2} \left( -\frac{d}{d \alpha} \right)^n \frac{1}{\sqrt{\alpha}} = \frac{(2 n)! \sqrt{\pi}}{2^{2 n + 1} n! \alpha^{n + \frac{1}{2}}}
	\eeq
i
	\beq
	\int_0^\infty x^{2 n + 1} e^{-\alpha x^2} \, d x = \frac{1}{2} \left( -\frac{d}{d \alpha} \right)^n \frac{1}{\alpha} = \frac{n!}{2 \alpha^{n + 1}},
	\eeq
gdzie $n \in \mathbb{N} \cup \{0\}$.

\subsection{Funkcje wielu zmiennych}

Obecnie rozpatrzymy funkcje zależne od wielu zmiennych. Najpierw wprowadzimy wektor zmiennych,
	\beq
		\mathbb{R}^n \ni \mathbf{x} =
			\begin{bmatrix}
				x_1 \\
				\vdots \\
				x_n
			\end{bmatrix},
		\label{eq:wekt_zm}
	\eeq
zaś funkcję przyporządkowującą wektorowi pewien skalar oznaczymy jako
	\beq
		f(\mathbf{x}) = \alpha \in \mathbb{R}^1.
	\eeq
Dla dwóch wektorów~$\mathbf{x}$ i~$\mathbf{y}$ postaci~\eqref{eq:wekt_zm} zdefiniujemy ponadto iloczyn skalarny,
	\beq
		\begin{split}
			\mathbf{x} \cdot \mathbf{y} & \equiv \Braket{ \mathbf{x} | \mathbf{y} } \\
			 & = \mathbf{x}^\intercal \mathbf{y} \\
			 & = \sum_{i = i}^n x_i y_i.
		\end{split}
	\eeq
Zauważmy, że z uwagi na stosowaną przez nas konwencję kolumnowego zapisu wektorów, przy iloczynach skalarnych musimy transponować pierwszy wektor.

Pojęcie iloczynu skalarnego uogólnimy w rozdziale~\ref{ssc:il_skal_wekt}.

W dalszej części wektor zmiennych~\eqref{eq:wekt_zm} będziemy oznaczać jako~$\mathbf{r}$, a skalarną funkcję tego wektora --- jako~$f(\mathbf{r})$.

\subsubsection{Pochodne cząstkowe}

Pochodną cząstkową funkcji wielu zmiennych~$f(\mathbf{r})$ wyznaczamy podobnie jak pochodną funkcji jednej zmiennej traktując pozostałe zmienne jako parametry. Oznaczamy ją symbolami
	\beq
		\frac{\partial f}{\partial x_i} \equiv \partial_{x_i} f \equiv f'_{x_i}.
	\eeq
Możemy oczywiście obliczać pochodne wyższych rzędów i po różnych współrzędnych. W ogólności, poza pewnymi patologicznymi przypadkami (z którymi nie będziemy mieć do czynienia w mechanice kwantowej), kolejność różniczkowania nie ma znaczenia:
	\beq
		\frac{\partial^2 f}{\partial x_i \partial x_j} \equiv f''_{x_i x_j} =
		\frac{\partial^2 f}{\partial x_j \partial x_i} \equiv f''_{x_j x_i}.
		\label{eq:pochcz_kol}
	\eeq

\subsubsection{Przyrost i gradient funkcji}

Jak pamiętamy, różniczka funkcji jednej zmiennej dana jest zależnością~\eqref{eq:wmat_rozn}. Dla funkcji~$n$~zmiennych analogiczną wielkość uzyskujemy różniczkując kolejno po wszystkich zmiennych niezależnych i mnożąc pochodne przez różniczki tych zmiennych, a więc
	\begin{align}
		\dd{f(\vb{x})} & =
			\pdv{f(\vb{x})}{x_1} \dd{x_1} +
			\pdv{f(\vb{x})}{x_2} \dd{x_2} +
			\ldots +
			\pdv{f(\vb{x})}{x_n} \dd{x_n} + \\
		 & = \grad f(\vb{x}) \vdot \dd{\vb{x}}, \label{eq:rczzup_ilsk}
	\end{align}
przy czym wektor
	\beq
		\grad f(\vb{x}) =
			\mqty[
				\pdv{x_1} \\
				\vdots \\
				\pdv{x_n}]
			(\vb{r}).
		\label{eq:cqrn}
	\eeq
nazywamy gradientem funkcji~$f(\vb{x})$, zaś $\nabla$ jest operatorem różniczkowym, który z funkcji skalarnej tworzy funkcję wektorową. Wreszcie
	\beq
		\dd{\vb{r}} =
			\mqty[
				\dd{x_1} \\
				\vdots \\
				\dd{x_n}]
	\eeq
jest różniczką wektora~$\vb{x}$. Wielkość~\eqref{eq:rczzup_ilsk} nazywamy~\emph{różniczką zupełną funkcji}.

Gradient funkcji jest wektorem wskazującym kierunek największej zmienności funkcji.

Rozważmy teraz różniczkę złożonej funkcji wielu zmiennych, $f \Big( g_1(x_1), \ldots, g_n(x_n) \Big)$. Łącząc~\eqref{eq:wmat_rozn_zloz} i~\eqref{eq:wmat_rozn_zloz}, uzyskujemy
	\begin{align}
		\dd{f \Big( g_1(x_1), \ldots, g_n(x_n) \Big)} & =
			\sum_{i = 1}^n {\pdv{f}{g_i} \dd{g_i(x_i)}} \\
		 & = \sum_{i = 1}^n {\pdv{f}{g_i} g'_i(x_i) \dd{x_i}}. \label{eq:wmat_rozn_fzl}
	\end{align}
W szczególności dla funkcji iloczynowej
	\beq
		f \Big( g_1(x_1), \ldots, g_n(x_n) \Big) = \prod_{i = 1}^n {g_i(x_i)}
	\eeq
\eqref{eq:wmat_rozn_fzl} przyjmuje postać
	\beq
		\dd{\left( \prod_{i = 1}^n {g_i(x_i)} \right)} = \sum_{i = 1}^n {\left( \prod_{k \not= i}^n {g_k(x_k)} \right) \dd{g_i(x_i)}}.
		\label{eq:eq:wmat_rozn_fzl_ilocz}
	\eeq

\begin{task}
	Przekształcenie sferycznego układu współrzędnych w układ kartezjański ma postać
		\beq
			\begin{cases}
				x = r \sin{\theta} \cos{\varphi} \\
				y = r \sin{\theta} \sin{\varphi} \\
				z = r \cos{\theta}
			\end{cases}.
			\label{eq:qnbm}
		\eeq
	Wyznacz różniczki zmiennych~$x$,~$y$ i~$z$ w funkcji różniczek~$r$,~$\theta$ i~$\varphi$.
\end{task}
\begin{sol}
	Korzystając z~\eqref{eq:eq:wmat_rozn_fzl_ilocz} uzyskujemy np.
		\beq
			\begin{split}
				\dd{x} & = \dd{\Big( r \sin{\theta} \cos{\varphi} \Big)} \\
				 & = \sin{\theta} \cos{\varphi} \dd{r} + r \cos{\varphi} \dd(\sin{\theta}) + r \sin{\theta} \dd(\cos{\varphi}),
			\end{split}
		\eeq
	a korzystając z~\eqref{eq:wmat_rozn} mamy np.
		\beq
			\begin{split}
				\dd(\cos{\varphi}) & = (\cos{\varphi})' \dd{\varphi} \\
				 & = -\sin{\varphi} \dd{\varphi}.
			\end{split}
		\eeq
	Ostateczny wynik możemy zapisać w postaci macierzowej:
		\beq
			\mqty[
				\dd{x} \\
				\dd{y} \\
				\dd{z}
			] =
			\mqty[
				\sin{\theta} \cos{\varphi} & r \cos{\theta} \cos{\varphi} & -r \sin{\theta} \sin{\varphi} \\
				\sin{\theta} \sin{\varphi} & r \cos{\theta} \sin{\varphi} & r \sin{\theta} \cos{\varphi} \\
				\cos{\theta} & -r \sin{\theta} & 0
			]
			\mqty[
				\dd{r} \\
				\dd{\theta} \\
				\dd{\varphi}
			].
		\eeq
\end{sol}

\begin{task}
	Znajdź postać gradientu we współrzędnych sferycznych.
\end{task}

\begin{sol}
	Z~\eqref{eq:qnbm} znajdujemy przekształcenie kartezjańskiego układu współrzędnych w układ sferyczny,
	\begin{equation}
		\begin{cases}
			r & = \sqrt{x^2 + y^2 + z^2} \\
			\theta & = \arccos{\frac{z}{\sqrt{x^2 + y^2 + z^2}}} \\
			\varphi & = \arctantwo(y, x)
		\end{cases}.
		\label{eq:rxyb}
	\end{equation}
	Działanie gradientu~\eqref{eq:cqrn} na funkcję~$f(\vb{r})$ możemy wyrazić w postaci
	\begin{equation}
		\grad f(\vb{r}) =
			\pdv{f(\vb{r})}{x} \vu{x} +
			\pdv{f(\vb{r})}{y} \vu{y} +
			\pdv{f(\vb{r})}{z} \vu{z}.
			\label{eq:nskm}
	\end{equation}
	Załóżmy, że funkcja~$f(\vb{r})$ wyrażona jest we współrzędnych sferycznych:
	\begin{equation}
		f(\vb{r}) \equiv f(r, \theta, \varphi),
	\end{equation}
	a ponieważ współrzędne sferyczne można wyrazić przez współrzędne kartezjańskie~\eqref{eq:rxyb},
	\begin{equation}
		f(\vb{r}) \equiv
			f \Big(
				r(x, y, z),
				\theta(x, y, z),
				\varphi(x, y, z)
			\Big),
	\end{equation}
	więc np.
	\begin{equation}
		\pdv{f(\vb{r})}{x} =
			\pdv{r}{x} \pdv{f(\vb{r})}{r} +
			\pdv{\theta}{x} \pdv{f(\vb{r})}{\theta} +
			\pdv{\varphi}{x} \pdv{f(\vb{r})}{\varphi}.
		\label{eq:baei}
	\end{equation}
\end{sol}

\subsection{Krzywe parametryczne}

Krzywą parametryczną w~$\mathbb{R}^n$ jest zbiór punktów
	\beq
		\mathbf{r}(t) =
			\begin{bmatrix}
				x_1(t) \\
				x_2(t) \\
				\vdots \\
				x_n(t)
			\end{bmatrix}, \, t \in [a, b].
	\eeq
Ponieważ poszczególne współrzędne krzywej są funkcjami zależnymi od parametru~$t$, stąd ich przyrost przy infinitezymalnym przyroście parametru wynosi
	\beq
		d x_i(t) = \dot{x}_i(t) d t,
	\eeq
przy czym pochodną po parametrze oznaczyliśmy kropką (notacja często stosowana w mechanice klasycznej), zatem różniczka wektora,
	\beq
		d \mathbf{r}(t) =
			\begin{bmatrix}
				\dot{x}_1(t) \\
				\dot{x}_2(t) \\
				\vdots \\
				\dot{x}_n(t)
			\end{bmatrix} d t.
		\label{eq:kparam_rwekt}
	\eeq
Krzywe parametryczne są stosowane m.in. do opisu trajektorii punktów w przestrzeni 3-wymiarowej:
	\beq
		\mathbf{r}(t) =
			\begin{bmatrix}
				x(t) \\
				y(t) \\
				z(t)
			\end{bmatrix}
		\label{eq:kparam_traj}
	\eeq
Parametrem jest tutaj oczywiście czas, zaś prędkość i przyspieszenie ciała opisanego trajektorią~\eqref{eq:kparam_traj} dane są zależnościami
	\beq
		\mathbf{v}(t) =
			\mathbf{\dot{r}}(t) =
			\begin{bmatrix}
				\dot{x}(t) \\
				\dot{y}(t) \\
				\dot{z}(t)
			\end{bmatrix},
	\eeq
	\beq
		\mathbf{a}(t) =
			\mathbf{\dot{v}}(t) =
			\mathbf{\ddot{r}}(t) =
			\begin{bmatrix}
				\ddot{x}(t) \\
				\ddot{y}(t) \\
				\ddot{z}(t)
			\end{bmatrix}.
	\eeq

\subsubsection{Całka krzywoliniowa}

Rozważymy obecnie wektorową funkcję wielu zmiennych~$\mathbf{f}(\mathbf{r}) \in \mathbb{R}^n$.

\emph{Całką krzywoliniową skierowaną} funkcji wektorowej $\mathbf{f}$ po krzywej~$C_{ab}$ sparametryzowanej zależnością~$\mathbf{r}(t)$ o końcach~$\mathbf{r}(a)$ i~$\mathbf{r}(b)$ nazywamy granicę ciągu
	\beq
		\int_{C_{ab}} \mathbf{f}(\mathbf{r}) \cdot d \mathbf{r} =
			\lim_{n \to \infty}{\sum_{i = 1}^n{\mathbf{f}(\boldsymbol{\xi}_i) \cdot \Delta \mathbf{r}_i}},
		\label{eq:ckrz_lim}
	\eeq
gdzie~$n$ jest liczbą podziałów~$[t_{i - 1}, t_i]$ parametru~$t$ krzywej takich, że~$a = t_0 < t_1 < \ldots < t_n = b$, $\boldsymbol{\xi}_i = \mathbf{r}(\theta_i)$,~$\theta_i \in [t_{i - 1}, t]$, oraz $\Delta \mathbf{r}_i = \mathbf{r}_i - \mathbf{r}_{i - 1}$.
Całkę~(\ref{eq:ckrz_lim}) obliczamy z całki oznaczonej
	\beq
		\int_{C_{ab}} \mathbf{f}(\mathbf{r}) \cdot d \mathbf{r} =
			\int_{a}^{b} \mathbf{f} \big( \mathbf{r}(t) \big) \cdot \mathbf{r}'(t) \, dt.
		\label{eq:ckrz_Cab}
	\eeq

W ogólności całka~(\ref{eq:ckrz_Cab}) zależy nie tylko od końcowych punktów krzywej~$\mathbf{r}$, ale także od parametryzacji~$\mathbf{r}(t)$, czyli drogi łączącej końcowe punkty krzywej~$C_{ab}$.

Zauważmy, że różniczka zupełna~(\ref{eq:rczzup_ilsk}) jest iloczynem skalarnym wektorów, ma więc postać podobną do wyrażenia podcałkowego w~(\ref{eq:ckrz_Cab}). Przyrost funkcji~$f$ między argumentami~$\mathbf{r}_a$ i~$\mathbf{r}_b$ możemy wyznaczyć znając jej różniczkę zupełną:
	\begin{align}
		\Delta f & = f(\mathbf{r}_b) - f(\mathbf{r}_a) \\
		 & = \int_{C_{ab}} d f(\mathbf{r}).
		\label{eq:ckrz_rczzup}
	\end{align}
Istotną cechą rózniczki zupełnej~(\ref{eq:rczzup_ilsk}) jest to, że całka~(\ref{eq:ckrz_rczzup}) nie zależy od drogi całkowania, a tylko od końcowych punktów krzywej. W szczególności, całka różniczki zupełnej po krzywej zamkniętej~(stąd okrąg w symbolu całki) jest zerowa,
	\beq
		\oint_C d f(\mathbf{r}) = 0.
	\eeq

W szczególnym przypadku funkcji dwóch zmiennych, wyrażenie
	\beq
		P(x, y) \, dx + Q(x, y) \, dy
	\eeq
jest rózniczką zupełną, jeśli
	\beq
		\left( \frac{\partial P}{\partial y} \right)_x = \left( \frac{\partial Q}{\partial x} \right)_y,
	\eeq
co wynika z zależności~(\ref{eq:rczzup_ilsk})~i~(\ref{eq:pochcz_kol}). Zapis~$\left( \frac{\partial P}{\partial y} \right)_x$ oznacza, że zmienna~$x$ jest traktowana jako stały parametr przy różniczkowaniu po zmiennej~$y$. Jest to konwencja stosowana zwyczajowo w termodynamice.
