#!/usr/bin/env python3

import math
import numpy as np
import matplotlib.pyplot as plt

plot_file = "lennard-jones_potential.pdf"

def lj_pot_red(r):
    return (1 / r)**12 - 2 * (1 / r)**6

def lj_force_red(r):
    return 12 * ((1 / r)**13 - (1 / r)**7)

r_min = 0.5
r_max = 2
step_count = 100
r = np.linspace(r_min, r_max, step_count)
V_min = -3
V_max = 3

fig, ax = plt.subplots()

ax.axhline(color = "black")
ax.axvline(color = "black")

ax.set_xlim(r_min, r_max)
ax.set_xlabel(r"$R$")
ax.set_xticks([1])
ax.set_xticklabels([r"$R_\mathrm{eq}$"])
ax.vlines(1, V_min, 0, color = "black", linestyle = "dashed")

ax.set_ylim(V_min, V_max)
ax.set_yticks([-1, 0])
ax.set_yticklabels([r"$-\epsilon$", r"$0$"])
ax.hlines(-1, 0, 1, color = "black", linestyle = "dashed")

ax.plot(r, lj_pot_red(r), color = "red", label = "potential")
ax.plot(r, lj_force_red(r), color = "blue", label = "force")
ax.legend(loc = "upper right", frameon = False)

fig.tight_layout()

plt.savefig(plot_file)
